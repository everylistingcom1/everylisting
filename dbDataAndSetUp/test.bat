@echo off
set /P N=Enter parameter for merge: 

:switch-case-example
  :: Call and mask out invalid call targets
  goto :switch-case-N-%N% 2>nul || (
    :: Default case
    echo Nothing to do...
  )
  goto :switch-case-end
  
  :switch-case-N-merge
    xcopy /y "importWithData.sql" "import.changed.sql"
    goto :switch-case-end     

  :switch-case-N-notmerge
    xcopy /y "import.sql" "import.changed.sql"
    goto :switch-case-end

:switch-case-end
   echo Completed...