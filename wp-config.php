<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 
 *
 
 * @package WordPress
 */

// Cache settings
// Disable cache for now, as this does not work on App Engine for PHP 7.2
define('WP_CACHE', false);

// Disable pseudo cron behavior
define('DISABLE_WP_CRON', true);

// Determine HTTP or HTTPS, then set WP_SITEURL and WP_HOME
if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443)) {
    $protocol_to_use = 'https://';
} else {
    $protocol_to_use = 'http://';
}
if (isset($_SERVER['HTTP_HOST'])) {
    define('HTTP_HOST', $_SERVER['HTTP_HOST']);
} else {
    define('HTTP_HOST', 'localhost');
}
define('WP_SITEURL', $protocol_to_use . HTTP_HOST);
define('WP_HOME', $protocol_to_use . HTTP_HOST);

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */

define('BASEFILESYSTEMPATH', '/var/www/html');
switch (getenv('APP_ENV')) {
    case "prod":
        define('WP_DEBUG', false);
        define('DB_NAME', 'everylisting_prod');
        define('DB_HOST', '10.65.112.5');
        define('DB_USER', 'wordpress');
        define('DB_PASSWORD', 'everylisting365247prod');
        define('MAPS_IP_SERVLET_WRITE', 'dev-maps-set.everylisting.com');
        define('MAPS_IP_SERVLET', 'dev-maps-set.everylisting.com');
        define('IMAGES_POST_SERVLET', 'TBD');
        define('IMAGES_JOBS_DIR', 'TBD');
        break;
    case "qa":
        define('WP_DEBUG', false);
        break;
    case "dev":
        define('WP_DEBUG', true);
        define('DB_NAME', 'everylisting_dev');
        define('DB_HOST', '10.65.112.3');
        define('DB_USER', 'wordpress');
        define('DB_PASSWORD', 'everylisting365247');
        define('MAPS_IP_SERVLET_WRITE', 'dev-maps-set.everylisting.com');
        define('MAPS_IP_SERVLET', 'dev-maps-set.everylisting.com');
        define('IMAGES_POST_SERVLET', 'TBD');
        define('IMAGES_JOBS_DIR', 'TBD');
        break;
    default:
        define('DB_NAME', 'everylisting_dev');
        define('DB_HOST', 'localhost');
        define('DB_USER', 'everything');
        define('DB_PASSWORD', '1234');
        define('BASEFILESYSTEMPATH', 'D:/workspace/everylisting');
        define('WP_DEBUG', false);
        define('MAPS_IP_SERVLET_WRITE', '127.0.0.1');
        define('MAPS_IP_SERVLET', '127.0.0.1');
        define('IMAGES_POST_SERVLET', 'localhost:8080');
        define('IMAGES_JOBS_DIR', 'D:/workspace/imagegeneratorservlet/jobs');
}
define('MAPS_MAPMARKERINDEXES_PATH_FROM_WORDPRESS', BASEFILESYSTEMPATH . '/maps/mapmarkerindexes');

if (WP_DEBUG) {
    // Enable Debug logging to the /wp-content/debug.log file
    define('WP_DEBUG_LOG', true);
    // Enable display of errors and warnings
    define('WP_DEBUG_DISPLAY', true);
    @ini_set('display_errors', 1);
    // Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
    define('SCRIPT_DEBUG', true);
} else {
    define('WP_DEBUG_DISPLAY', false);
    @ini_set('display_errors', 0);
}

define('EMPTY_TRASH_DAYS', 0);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vbvyH6aCvqpamAFSNT1K5OUXZGazyO4O7o9GIi++D4+yk4AIHBnEtdQF8r47nkrRpG6llvfEWKZOD/Kh');
define('SECURE_AUTH_KEY',  'Hj/MJ91eeRk4xj0crD7p/dyCdpSKwNqoYFgJq+9ELtRBRu36SvzXGVN6ofoMWVgv+pSvS7mVM/FRplS5');
define('LOGGED_IN_KEY',    'sDh3Vb9PkZ5JrI3XPU0wGRhnEBxz7p+qzzEQzeJya+n1uvzGNqEjMiIYgaop+5vhjCzeI4b71JSRjdTK');
define('NONCE_KEY',        'leENsgexx9U2Bnl384smT8dZOJnfdxtF3sHenPXKyVEKdi9b/8jdZpymv4e8HFyXEFaccJhxSFvREzb+');
define('AUTH_SALT',        'BnTMWvbye/rKjX40ohPRRXd8GROAKI/Eit84EbxeCohR8mahHVXMZye9/7p81iWCJb+X07e+2qepZBPI');
define('SECURE_AUTH_SALT', '8kICw2ztxvjq9+kO3nHZ+v5CJ2Eeqr/utlG8797XWBd8aNMc9eiGBEeOhSDmee9x138azYptgajIGG1n');
define('LOGGED_IN_SALT',   'gGJitOJ2mMwAiSnahwIK2c0bnlXRvAlD/iorDg34gWL6Z8G3tKsqPfgotID8RPGLa0mP8CLL6PYPKYqK');
define('NONCE_SALT',       'gCbepFDciwL3e6IUH5m5VYicF+SPnl/FM5Ap6c2HrXYVTXu0Ac4qKao6K5l1D+5xQB039zruezBpzGEG');


/**#@-*/
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/* That's all, stop editing! Happy blogging. */
/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/wordpress/');
}

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
