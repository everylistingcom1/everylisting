package core;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import util.StringUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;

import org.eclipse.persistence.jaxb.MarshallerProperties;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RenderingJobProgress {
	private String originaljson;
	private ERenderingState state;
	private Long startedTime;
	private Long finishedTime;
	private String oneLinerCustomerFacingStatus;
	private Integer imagesCount;
	private String engineCalculating_ip;
	private String engineCalculating_host;
	private String signature;
	private Integer current;
	private Integer total;
	private String exceptionString;
	private Long totalProcessingTime;
	private Long gatheringTime;
	private Long renderingTime;
	private Integer framesCount;
	private Long lastActivity;
	private String previewFileName;
	private String finalFileName;
	private Long previewFileSize;
	private Long finalFileSize;
	@XmlTransient
	private File jobsDirectory = null;
	
	protected RenderingJobProgress() {}
	
	public RenderingJobProgress(String signature, File jobsDirectory) throws JobsDirectoryAbsentException {
		state = ERenderingState.inqueue;
		setOneLinerCustomerFacingStatus("Awaiting processing");
		setSignature(signature);
		if ((!jobsDirectory.exists()) || (!jobsDirectory.isDirectory())) {
			throw new JobsDirectoryAbsentException("Can't find jobs directory at "+ jobsDirectory.getAbsolutePath());
		}
		this.jobsDirectory = jobsDirectory;
	}
	
	public void setJobsDirectory(File jobsDirectory) throws JobsDirectoryAbsentException {
		if ((!jobsDirectory.exists()) || (!jobsDirectory.isDirectory())) {
			throw new JobsDirectoryAbsentException("Can't find jobs directory at "+ jobsDirectory.getAbsolutePath());
		}
		this.jobsDirectory = jobsDirectory;
	}

	public class AlreadyStartedException extends Exception {

		private static final long serialVersionUID = 3678382190788298418L;

		public AlreadyStartedException() {
			super();
		}

		public AlreadyStartedException(String arg0) {
			super(arg0);
		}
	}
	
	public class JobsDirectoryAbsentException extends Exception {

		private static final long serialVersionUID = 6648965143798569482L;

		public JobsDirectoryAbsentException(String arg0) {
			super(arg0);
		}
	}
	
	public class BreakRequestException extends Exception {

		private static final long serialVersionUID = 6828480930843582488L;

		public BreakRequestException() {
			super();
		}
	}

	public ERenderingState getState() {
		return state;
	}

	public void setState(ERenderingState state) {
		this.state = state;
	}

	public Long getStartedTime() {
		return startedTime;
	}

	private void setStartedTime(Long startedTime) {
		this.startedTime = startedTime;
	}

	public Long getFinishedTime() {
		return finishedTime;
	}

	public void setFinishedTime(Long finishedTime) {
		this.finishedTime = finishedTime;
	}

	public String getOneLinerCustomerFacingStatus() {
		return oneLinerCustomerFacingStatus;
	}

	public void setOneLinerCustomerFacingStatus(String oneLinerCustomerFacingStatus) {
		this.oneLinerCustomerFacingStatus = oneLinerCustomerFacingStatus;
	}

	public Integer getImagesCount() {
		return imagesCount;
	}

	public void setImagesCount(Integer imagesCount) {
		this.imagesCount = imagesCount;
	}

	private String getEngineCalculating_ip() {
		return engineCalculating_ip;
	}

	private void setEngineCalculating_ip(String engineCalculating_ip) {
		this.engineCalculating_ip = engineCalculating_ip;
	}

	private String getEngineCalculating_host() {
		return engineCalculating_host;
	}

	private void setEngineCalculating_host(String engineCalculating_host) {
		this.engineCalculating_host = engineCalculating_host;
	}

	public String getSignature() {
		return signature;
	}

	private void setSignature(String signature) {
		this.signature = signature;
	}

	public void start() throws AlreadyStartedException, UnknownHostException, JAXBException, JobsDirectoryAbsentException {
		if (startedTime == null) {
			setStartedTime(System.currentTimeMillis());
			setEngineCalculating_ip(InetAddress.getLocalHost().toString());
			setEngineCalculating_host(InetAddress.getLocalHost().getHostName());
			store();
		} else if (!isMine(false)) {
			throw new AlreadyStartedException();
		}
	}

	public boolean isStarted() {
		return (startedTime != null);
	}

	public boolean isMine(boolean readFromDisk) throws JAXBException, JobsDirectoryAbsentException {
		try {
			RenderingJobProgress use = this;
			if (readFromDisk) use = read();
			return (use.isStarted()
					&& ((use.getEngineCalculating_ip() != null)
							&& (use.getEngineCalculating_ip().equals(InetAddress.getLocalHost().toString())))
					|| ((use.getEngineCalculating_host() != null)
							&& (use.getEngineCalculating_host().equals(InetAddress.getLocalHost().getHostName()))));
		} catch (UnknownHostException e) {
			return false;
		}
	}
	
	public RenderingJobProgress read() throws JAXBException, JobsDirectoryAbsentException {
		if (jobsDirectory == null) {
			throw new JobsDirectoryAbsentException("jobs directory not set.");
		}
		File fromFile = new File(jobsDirectory, signature + ".info");
		JAXBContext jaxbContext = JAXBContext.newInstance(RenderingJobProgress.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		RenderingJobProgress dReturn = (RenderingJobProgress)jaxbUnmarshaller.unmarshal(fromFile);
		dReturn.jobsDirectory = jobsDirectory;
		return dReturn;
	}

	public void store() throws JAXBException, JobsDirectoryAbsentException {
		setLastActivity(System.currentTimeMillis());
		if (jobsDirectory == null) {
			throw new JobsDirectoryAbsentException("jobs directory not set.");
		}
		File intoFile = new File(jobsDirectory, signature + ".info");
		if (intoFile.exists()) {
			intoFile.delete();
		}
		JAXBContext jaxbContext = JAXBContext.newInstance(RenderingJobProgress.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(this, intoFile);
	}
	
	public String getProgressJson() throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(RenderingJobProgress.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        // To format JSON
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); 
        //Set JSON type
        jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        jaxbMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        //Print JSON String to Console
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(this, sw);
        return sw.toString();
	}
	
	public static RenderingJobProgress read(File fromFile, File jobsDirectory) throws JAXBException, JobsDirectoryAbsentException {
		JAXBContext jaxbContext = JAXBContext.newInstance(RenderingJobProgress.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		RenderingJobProgress dReturn = (RenderingJobProgress)jaxbUnmarshaller.unmarshal(fromFile);
		dReturn.setJobsDirectory(jobsDirectory);
		return dReturn;
	}
	
	public static String calculateSignature(String fromString) throws NoSuchAlgorithmException {
		return StringUtils.calculateMD5ofString(fromString);
	}

	protected Integer getCurrent() {
		return current;
	}

	protected void setCurrent(Integer current) {
		this.current = current;
	}

	protected Integer getTotal() {
		return total;
	}

	protected void setTotal(Integer total) {
		this.total = total;
	}
	
	public void setCurrentTotal(Integer current, Integer total) {
		this.current = current;
		this.total = total;
	}
	
	public void increaseCurrentTotal() {
		this.current = new Integer(((this.current != null)?this.current.intValue():0) + 1);
	}
	
	public String getExceptionString() {
		return exceptionString;
	}

	public void setExceptionString(String exceptionString) {
		this.exceptionString = exceptionString;
	}

	public File getJobsDirectory() {
		return jobsDirectory;
	}

	public String getOriginaljson() {
		return originaljson;
	}

	public void setOriginaljson(String originaljson) {
		this.originaljson = originaljson;
	}

	public Long getTotalProcessingTime() {
		return totalProcessingTime;
	}

	public void setTotalProcessingTime(Long totalProcessingTime) {
		this.totalProcessingTime = totalProcessingTime;
	}

	public Long getGatheringTime() {
		return gatheringTime;
	}

	public void setGatheringTime(Long gatheringTime) {
		this.gatheringTime = gatheringTime;
	}

	public Long getRenderingTime() {
		return renderingTime;
	}

	public void setRenderingTime(Long renderingTime) {
		this.renderingTime = renderingTime;
	}

	public Integer getFramesCount() {
		return framesCount;
	}

	public void setFramesCount(Integer framesCount) {
		this.framesCount = framesCount;
	}
	
	public static RenderingJobProgress getRenderingJobProgressBySignatureIfExists(File jobsDirectory, String signature) throws NoSuchAlgorithmException, JAXBException, JobsDirectoryAbsentException {
		File estimatedLocation = new File(jobsDirectory, signature + ".info");
		if ((estimatedLocation.exists()) && (estimatedLocation.isFile())) {
			return read(estimatedLocation, jobsDirectory);
		}
		return null;
	}

	public static RenderingJobProgress getRenderingJobProgressIfExists(File jobsDirectory, String json) throws NoSuchAlgorithmException, JAXBException, JobsDirectoryAbsentException {
		String signature = calculateSignature(json);
		File estimatedLocation = new File(jobsDirectory, signature + ".info");
		if ((estimatedLocation.exists()) && (estimatedLocation.isFile())) {
			return read(estimatedLocation, jobsDirectory);
		}
		return null;
	}
	
	public File getResultingFile(boolean mustExist) throws IOException {
		String fileFormat = StringUtils.getNodeValue(originaljson, "animation.fileformat");
		if (fileFormat != null) {
			File estimatedLocation = new File(jobsDirectory, signature + "." + fileFormat);
			if (!mustExist) {
				return estimatedLocation;
			} else if ((estimatedLocation.exists()) && (estimatedLocation.isFile())) {
				return estimatedLocation;
			}
		}
		return null;
	}
	
	public void breakOnException() throws BreakRequestException {
		if (exceptionString != null) {
			throw new BreakRequestException();
		}
	}

	public Long getLastActivity() {
		return lastActivity;
	}

	public void setLastActivity(Long lastActivity) {
		this.lastActivity = lastActivity;
	}

	public String getPreviewFileName() {
		return previewFileName;
	}

	public void setPreviewFileName(String previewFileName) {
		this.previewFileName = previewFileName;
	}

	public String getFinalFileName() {
		return finalFileName;
	}

	public void setFinalFileName(String finalFileName) {
		this.finalFileName = finalFileName;
	}

	public Long getPreviewFileSize() {
		return previewFileSize;
	}

	public void setPreviewFileSize(Long previewFileSize) {
		this.previewFileSize = previewFileSize;
	}

	public Long getFinalFileSize() {
		return finalFileSize;
	}

	public void setFinalFileSize(Long finalFileSize) {
		this.finalFileSize = finalFileSize;
	}
}
