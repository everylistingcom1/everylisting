package core;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;

import core.AnimationBuilder.Sequence;

public class VerticalBanner extends FrameSuperposer {
	
	private String content;
	private ELocation location;
	private int padding;
	private String color;
	private String style;
	private transient int calculatedfontsize = -1;

	public VerticalBanner(String content, ELocation location, int padding, String color, String style) {
		this.content = content;
		this.location = location;
		this.padding = padding;
		this.color = color;
		this.style = style;
	}

	@Override
	public Rectangle render(BufferedImage image, Dimension dimension, Rectangle notSuperposedRect, AnimationBuilder caller, Sequence sequence) throws Exception {
		Graphics2D graphics2D = (Graphics2D) image.getGraphics().create();
		Rectangle destRect = new Rectangle();
		destRect.y = notSuperposedRect.y;
		destRect.height = notSuperposedRect.y + notSuperposedRect.height;
		switch (location) {
			case eRight:
				if (caller.getFrameSuperposerAtLocation(ELocation.eTopRight) == null) {
					destRect.y = 0;
					destRect.height = notSuperposedRect.y + notSuperposedRect.height;
				} else if (caller.getFrameSuperposerAtLocation(ELocation.eTopRight).getUsedRectangle() != null) {
					destRect.y = caller.getFrameSuperposerAtLocation(ELocation.eTopRight).getUsedRectangle().y + caller.getFrameSuperposerAtLocation(ELocation.eTopRight).getUsedRectangle().height;
					destRect.height = (notSuperposedRect.y + notSuperposedRect.height - destRect.y);
				}
				if (caller.getFrameSuperposerAtLocation(ELocation.eBottomRight) == null) {
					destRect.height = dimension.height - destRect.y;
				} else if (caller.getFrameSuperposerAtLocation(ELocation.eBottomRight).getUsedRectangle() != null) {
					destRect.height = caller.getFrameSuperposerAtLocation(ELocation.eBottomRight).getUsedRectangle().y - destRect.y;
				}
				destRect.x = dimension.width - padding - 100;
				destRect.width = padding;
				break;
			case eLeft:
				if (caller.getFrameSuperposerAtLocation(ELocation.eTopLeft) == null) {
					destRect.y = 0;
					destRect.height = notSuperposedRect.y + notSuperposedRect.height;
				} else if (caller.getFrameSuperposerAtLocation(ELocation.eTopLeft).getUsedRectangle() != null) {
					destRect.y = caller.getFrameSuperposerAtLocation(ELocation.eTopLeft).getUsedRectangle().y + caller.getFrameSuperposerAtLocation(ELocation.eTopLeft).getUsedRectangle().height;
					destRect.height = (notSuperposedRect.y + notSuperposedRect.height - destRect.y);
				}
				if (caller.getFrameSuperposerAtLocation(ELocation.eBottomLeft) == null) {
					destRect.height = dimension.height - destRect.y;
				} else if (caller.getFrameSuperposerAtLocation(ELocation.eBottomLeft).getUsedRectangle() != null) {
					destRect.height = caller.getFrameSuperposerAtLocation(ELocation.eBottomLeft).getUsedRectangle().y - destRect.y;
				}
				destRect.x = 0;
				destRect.width = padding + 100;
				break;
			default:
				throw new Exception("Illegal location.");
		}
		int ypos = destRect.y;
		boolean drawit = false;
		if ((calculatedfontsize > 0) || (calculatedfontsize == -1)) {
			int fontSize = (calculatedfontsize == -1)?72:calculatedfontsize;
			int maxwidth = 0;
			graphics2D.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
			do {
				if (!drawit) {
					if (calculatedfontsize == -1) fontSize--;
					graphics2D.setFont(graphics2D.getFont().deriveFont((float)fontSize));
					if (style != null) {
						String[] tokens = style.toLowerCase().split(",");
						for (String token: tokens) {
							switch (token.trim()) {
								case "bold":
									graphics2D.setFont(graphics2D.getFont().deriveFont(Font.BOLD));
									break;
								case "italic":
									graphics2D.setFont(graphics2D.getFont().deriveFont(Font.ITALIC));
									break;
								case "uppercase":
									content = content.toUpperCase();
									break;
							}
						}
					}
					maxwidth = 0;
				} else {
				    graphics2D.setColor(new Color(1.0f, 1.0f, 1.0f, 0.5f));
					switch (location) {
						case eRight:
							destRect.x = dimension.width - padding - maxwidth;
							destRect.width = padding;
						    graphics2D.fillRoundRect(destRect.x - (padding/2), destRect.y + (padding/2), (dimension.width - (destRect.x - (padding/2))) * 2, destRect.height + padding, 15, 15);
							break;
						case eLeft:
							destRect.x = 0;
							destRect.width = padding + maxwidth;
							System.out.println(-(destRect.x + destRect.width - (padding/2)) + "," + destRect.y + (padding/2) + "," + (destRect.x + destRect.width) * 2 + "," +  destRect.height + padding);
						    graphics2D.fillRoundRect(-(destRect.x + destRect.width - (padding/2)), destRect.y + (padding/2), (destRect.x + destRect.width) * 2, destRect.height + padding, 15, 15);
							break;
						default:
							throw new Exception("Illegal location.");
					}
				}
		        FontMetrics fm = graphics2D.getFontMetrics();
				ypos = destRect.y + padding + graphics2D.getFont().getSize();
		        Color vcolor;
			    try {
			        Field field = Class.forName("java.awt.Color").getField(color);
			        vcolor = (Color)field.get(null);
			    } catch (Exception e) {
			    	vcolor = Color.BLACK;
			    }
				graphics2D.setColor(vcolor);
		        for (int i = 0; i < content.length(); i++) {
		            if (drawit) {
		            	graphics2D.drawString(Character.toString(content.charAt(i)), destRect.x + (destRect.width/2) + ((maxwidth - fm.charWidth(content.charAt(i)))/2), ypos);
		            } else {
		            	maxwidth = Math.max(maxwidth, fm.charWidth(content.charAt(i)));
		            }
		            ypos += ((content.charAt(i) == ' ')?((int)((float)fontSize*0.75)):(fontSize + 1));
		            // if (sequence.getSequenceIndex() == 0) System.out.println("fontSize: " + fontSize + ", " + graphics2D.getFont().getSize() + ", " + content.charAt(i) + ", " + ypos + " / " + (notSuperposedRect.y + notSuperposedRect.height));
		        }
		        if (drawit) {
		        	break;
		        } else if (((ypos + (padding/2)) < (notSuperposedRect.y + notSuperposedRect.height))) {
		        	drawit = true;
		        	destRect.height = ypos - (padding/2) - graphics2D.getFont().getSize() - destRect.y;
		        } else if (fontSize < 9) {
		        	break;
		        }
			} while (((ypos + (padding/2)) >= (notSuperposedRect.y + notSuperposedRect.height)) || (drawit));
			if (drawit) {
				if (calculatedfontsize == -1) calculatedfontsize = fontSize;
			} else {
				calculatedfontsize = 0;
			}
		}
        graphics2D.dispose();
		return null;
	}

	@Override
	public ELocation getLocation() {
		return location;
	}

}
