package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;

import javax.imageio.ImageIO;
import javax.servlet.http.*;
import javax.swing.ImageIcon;
import javax.xml.bind.JAXBException;

import com.defano.jsegue.AnimatedSegue;
import com.defano.jsegue.renderers.*;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;

import core.AgentSuperposer;
import core.AnimationBuilder;
import core.AnimationBuilder.Sequence;
import core.ERenderingState;
import core.EveryListingLogoSuperposer;
import core.FrameSuperposer;
import core.FrameSuperposer.ELocation;
import core.RenderingJobProgress;
import core.RenderingJobProgress.JobsDirectoryAbsentException;
import core.TextSuperposer;
import core.VerticalBanner;
import util.StringUtils;

public class ImageGeneratorServlet extends HttpServlet {

	private static final long serialVersionUID = -7544106449458391953L;
		
	private class Context {
		private List<FrameSuperposer> superposers = new ArrayList<>();
		private AnimationBuilder animation = null;
		private JsonNode animationnode = null;
		private List<AnimationBuilder.Sequence> sequence = new ArrayList<>();
		private String debugString = "";
	}
	
	private class VideoCalculationDaemonThread extends Thread {
		
		private RenderingJobProgress jobProgress;
		private Context context;
		
		public VideoCalculationDaemonThread(RenderingJobProgress jobProgress, Context context) {
			this.jobProgress = jobProgress;
			this.context = context;
			setDaemon(true);
		}
		
		public void run() {
			try {
				jobProgress.start();
		    	if (jobProgress.isMine(true)) {
		    		try {
		    			File srcFile = context.animation.create(context.sequence, jobProgress);
		    			File dstFile = new File(jobProgress.getJobsDirectory(), jobProgress.getSignature() + "." + context.animation.getFileformat());
		    			Files.copy(Paths.get(srcFile.getAbsolutePath()), Paths.get(dstFile.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
						jobProgress.setOneLinerCustomerFacingStatus("Completed");
						jobProgress.setState(ERenderingState.successfullycompleted);
						jobProgress.setCurrentTotal(null, null);
						jobProgress.setFinalFileName(dstFile.getName());
						jobProgress.setFinalFileSize(dstFile.length());
						jobProgress.store();
		    		} catch (Exception e) {
		    			jobProgress.setOneLinerCustomerFacingStatus("In error");
		    			jobProgress.setState(ERenderingState.failed);
		    			jobProgress.setExceptionString(StringUtils.getExceptionString(e));
		    			jobProgress.store();
		    		}
		    	}
			} catch (Exception e) {
				jobProgress.setExceptionString(StringUtils.getExceptionString(e));
				jobProgress.setState(ERenderingState.failed);
				try {
					jobProgress.store();
				} catch (JAXBException | JobsDirectoryAbsentException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doPost(request, response);
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	Context context = new Context();
    	String error = null;
    	String command = request.getParameter("request");
    	String signature = request.getParameter("signature");
    	String json = request.getParameter("json");
    	String jobsdir = request.getParameter("jobsdir");
    	boolean isDebug = (request.getParameter("debug") != null);
    	VideoCalculationDaemonThread threadOnExit = null;
    	RenderingJobProgress jobProgress = null;
    	OutputStream os = response.getOutputStream();
    	try {
	    	if (jobsdir != null) {
	    		if (isDebug) context.debugString += "jobsdir is " + new File(jobsdir).getAbsolutePath() + "\n";
	    		if (!new File(jobsdir).exists()) {
	    			if (!new File(jobsdir).mkdirs()) {
			    		error = "'jobsdir' query parameter could not create directories at " + new File(jobsdir).getAbsolutePath();
	    			}
	    		} else if (new File(jobsdir).isFile()) {
		    		error = "'jobsdir' query parameter points to a file at " + new File(jobsdir).getAbsolutePath();
	    		}
	    		// Locate the already existent job or content
		    	if ((signature != null) || (json != null)) {
		    		if (json != null) {
		    			json = normalizeJson(URLDecoder.decode(json.replace("\"", "%22"), "UTF-8"));
		    		}
		    		if (signature != null) {
			    		if (isDebug) context.debugString += "signature is " + signature + "\n";
		    			jobProgress = RenderingJobProgress.getRenderingJobProgressBySignatureIfExists(new File(jobsdir), signature);
			    		if (isDebug) context.debugString += "Located from signature: " + (jobProgress != null) + "\n";
		    		}
		    		if ((jobProgress == null) && (json != null)) {
		    			jobProgress = RenderingJobProgress.getRenderingJobProgressIfExists(new File(jobsdir), json);
			    		if (isDebug) context.debugString += "Located from json: " + (jobProgress != null) + " (calculated signature: "+ RenderingJobProgress.calculateSignature(json) + ")\n";
			    		if (json != null) {
				    		if (isDebug) context.debugString += "json is: " + json + "\n";
			    		}
		    		}
		    	}
	    	} else if (!"isalive".equals(command)) {
	    		error = "'jobsdir' query parameter is mandatory, none found.";
	    	}
	    	if ((command != null) && (error == null)) {
	    		if (isDebug) context.debugString += "request is: " + command + "\n";
		    	switch (command) {
				    case "isalive" : {
				    	String message = "OK";
				    	if (jobsdir != null) {
				    		message += "\n";
				    		if ((new File(jobsdir).exists()) && (new File(jobsdir).isDirectory())) {
				    			message += "Directory is in place at " + new File(jobsdir).getAbsolutePath();
				    		} else {
				    			message = "ERROR: Directory is not in place at " + new File(jobsdir).getAbsolutePath();
				    		}
				    	}
			    		response.setContentType("text/plain");
						response.setCharacterEncoding("UTF-8");
						os.write(message.getBytes());
						os.flush();
						return;
			    	}
			    	case "status": {
			    			if (jobProgress != null) {
				    			response.setContentType("application/json");
				    			response.setCharacterEncoding("UTF-8");
				    			os.write(jobProgress.getProgressJson().getBytes());
				    			os.flush();
				    			if (isDebug) {
				    	    		os.write(("\n\nDEBUG:\n" + context.debugString).getBytes());
				    				os.flush();
				    	    	}
				    			return;
			    			} else {
			    				error = "'status' query parameter requires the successful location of a progress, none found.";
			    			}
			    		}
			    		break;
		    		case "previewonly":
			    	case "create": {
				    		if ((jobProgress == null) && (json == null)) {
				    			if (isDebug) context.debugString += "'json' query parameter not specified.\n";
				    			error = "'json' query parameter not specified.";
				    		} else {
				    			if (isDebug) context.debugString += "into creation loop.\n";
				    			if ((jobProgress == null) && (json != null)) {
				    				if (isDebug) context.debugString += "recalculating from json.\n";
					    			createContextFromJson(json, context);
					    			jobProgress = new RenderingJobProgress(RenderingJobProgress.calculateSignature(json), new File(jobsdir));
					    			jobProgress.setOriginaljson(json);
					    		}
				    			if (jobProgress != null) {
				    				if (isDebug) context.debugString += "got a context.\n";
							    	jobProgress.setOriginaljson(json);
							    	if (command.equals("previewonly")) {
							    		ensurePreviewCreated(jobProgress, false, context);
							    		if (jobProgress.getPreviewFileName() != null) {
							    			File previewFile = new File(new File(jobsdir), jobProgress.getPreviewFileName());
							    			if ((previewFile.exists()) && (previewFile.isFile())) {
									    		response.setContentType("image/jpg");
									    		String ImageIOFileFormat = "JPEG";
										        ImageIO.write(ImageIO.read(previewFile), ImageIOFileFormat, os);
										        os.flush();
												if (isDebug) {
										    		os.write(("\n\nDEBUG:\n" + context.debugString).getBytes());
													os.flush();
										    	}
										        return;
							    			}
							    		}
							    	} else if (!ERenderingState.successfullycompleted.equals(jobProgress.getState())) {
							    		String fileformat = StringUtils.getNodeValue(jobProgress.getOriginaljson(), "animation.fileformat");
							    		if ("gif".equals(fileformat)) {
							    			boolean threadAlreadyExists = ((!ERenderingState.inqueue.equals(jobProgress.getState())) && (!ERenderingState.failed.equals(jobProgress.getState())));
								    		if (threadAlreadyExists) {
							    				if (isDebug) context.debugString += "the job was already processing.\n";
								    		}
							    			if ((threadAlreadyExists) && (jobProgress.getLastActivity() < System.currentTimeMillis() - 120000)) {
							    				if (isDebug) context.debugString += "previous processing was stalled for more than 2 minutes (will assume it is not active anymore).\n";
								    			threadAlreadyExists = false;
								    		}
							    			if (!threadAlreadyExists) {
							    				if (isDebug) context.debugString += "processing...\n";
									    		createContextFromJson(jobProgress.getOriginaljson(), context);
								    			jobProgress.setOriginaljson(json = calculateContextJson(context));
									    		ensurePreviewCreated(jobProgress, true, context);
								    			threadOnExit = new VideoCalculationDaemonThread(jobProgress, context);
								    		} else {
							    				if (isDebug) context.debugString += "skipped processing.\n";
								    		}
								    	} else {
								    		response.setContentType("image/" + fileformat);
									    	String ImageIOFileFormat = fileformat.toUpperCase();
									    	if (ImageIOFileFormat.equals("JPG")) {
									    		ImageIOFileFormat = "JPEG";
									    	}
									    	createContextFromJson(jobProgress.getOriginaljson(), context);
									    	jobProgress.setOriginaljson(json = calculateContextJson(context));
								    		File dFile = context.animation.create(context.sequence, jobProgress);
									        ImageIO.write(ImageIO.read(dFile), ImageIOFileFormat, os);
									        jobProgress.setFinalFileName(jobProgress.getPreviewFileName());
						    				jobProgress.setFinalFileSize(jobProgress.getPreviewFileSize());
						    				jobProgress.setState(ERenderingState.successfullycompleted);
						    				jobProgress.store();
								    	}
							    	} else {
							    		if (isDebug) context.debugString += "was detected as completed.\n";
							    	}
				    			} else {
				    				if (isDebug) context.debugString += "unable to determine or locate a context.\n";
				    				error = "unable to determine or locate a context.";
				    			}
				    		}
				    	}
			    		break;
			    	default:
			    		error = "'request' query parameter '" + command + "' not handled.";
		    	}
	    	} else if (error == null) {
	    		error = "'command' query parameter not specified.";
	    	}
	    	
	    	if (error != null) {
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				os.write(("ERROR: "+ error).getBytes());
				os.flush();
	    	} else {
	    		if (threadOnExit != null) threadOnExit.start();
	    	}
	    	
	    	if (response.getContentType() == null) {
	    		response.setContentType("application/json");
    			response.setCharacterEncoding("UTF-8");
    			os.write(jobProgress.getProgressJson().getBytes());
    			os.flush();
	    	}
    	} catch (Exception e) {
    		response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			os.write(StringUtils.getExceptionString(e).getBytes());
			os.flush();
    	}
		if (isDebug) {
    		os.write(("\n\nDEBUG:\n" + context.debugString).getBytes());
			os.flush();
    	}
    }
    
    private String normalizeJson(String json) throws JsonMappingException, JsonProcessingException {
    	ObjectMapper om = new ObjectMapper();
        om.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
        @SuppressWarnings("unchecked")
		Map<String, Object> map = om.readValue(json, HashMap.class);
        return om.writeValueAsString(map);
    }
        
    @SuppressWarnings("unchecked")
	private void construct(JsonNode node, Context context) throws IOException, ClassNotFoundException, URISyntaxException {
		switch (node.get("className").asText()) {
			case "EveryListingLogoSuperposer":
				context.superposers.add(new EveryListingLogoSuperposer(new File(this.getClass().getClassLoader().getResource("logoname_black_md_transparency_cropped.gif").toURI()), ELocation.valueOf(node.get("location").asText()), node.get("size").asInt(), node.get("padding").asInt()));
				break;
			case "TextSuperposer":
				context.superposers.add(new TextSuperposer(node.get("text").asText(), ELocation.valueOf(node.get("location").asText()), node.get("size").asInt(), node.get("padding").asInt(), node.get("color").asText()));
				break;
			case "AgentSuperposer":
				context.superposers.add(new AgentSuperposer(node.get("profileimageurl").asText(), node.get("name").asText(), ELocation.valueOf(node.get("location").asText()), node.get("size").asInt(), node.get("padding").asInt()));
				break;
			case "VerticalBanner":
				context.superposers.add(new VerticalBanner(node.get("content").asText(), ELocation.valueOf(node.get("location").asText()), node.get("padding").asInt(), node.get("color").asText(), node.get("style").asText()));
				break;
			case "Sequence":
				if (context.animation == null) {
					context.animation = new AnimationBuilder(context.superposers, new Dimension(context.animationnode.get("dimension").get("width").asInt(), context.animationnode.get("dimension").get("height").asInt()), context.animationnode.get("fileformat").asText());
				}
				boolean alreadyGathered = false;
				for (Sequence onesequence: context.sequence) {
					if (onesequence.getUrl().equals(node.get("url").asText())) {
						alreadyGathered = true;
					}
				}
				if (!alreadyGathered) {
					context.sequence.add(context.animation.addSequence(node.get("url").asText(), (Class<? extends AnimatedSegue>) Class.forName("com.defano.jsegue.renderers." + node.get("animationEndClass").asText()), node.get("fixedTime").asInt(), node.get("msTransitionTime").asInt()));
				}
				break;
			case "AnimationBuilder":
				context.animationnode = node;
				break;
		}
    }
    
    private void analyzeJsonNode(JsonNode node, Context context) throws IOException, ClassNotFoundException, URISyntaxException {
    	Iterator<Map.Entry<String,JsonNode>> fieldsIterator = node.fields();
        while (fieldsIterator.hasNext()) {
            Map.Entry<String,JsonNode> field = fieldsIterator.next();
            if (field.getValue().isArray()) {
            	for (int i = 0; i < field.getValue().size(); i++) {
            		analyzeJsonNode(field.getValue().get(i), context);
            	}
            } else if (field.getValue().isObject()) {
            	if (field.getValue().has("className")) {
            		construct(field.getValue(), context);
            	}
            } else {
            	if ("className".equals(field.getKey())) {
            		construct(node, context);
            	}
            }
        }
    }
    
    private void createContextFromJson(String json, Context context) throws IOException, ClassNotFoundException, URISyntaxException {
    	context.animation = null;
    	context.superposers = new ArrayList<>();
        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper(factory);
        JsonNode rootNode = mapper.readTree(json);  
        analyzeJsonNode(rootNode, context);
    }
    
    private String calculateContextJson(Context context) {
    	String json = "{" + "\"animation\": " + new Gson().toJson(context.animation) + ", " + "\"superposers\": " + new Gson().toJson(context.superposers) + ", " + "\"sequence\": " + new Gson().toJson(context.sequence) + "}";
    	return json;
    }
    
    private void ensurePreviewCreated(RenderingJobProgress jobProgress, boolean videosOnly, Context context) throws Exception {
    	if (jobProgress != null) {
    		if ((context.animation != null) && ((!videosOnly) || ((videosOnly) && (context.animation.isVideo()))) && (jobProgress.getPreviewFileName() == null)) {
    			String keepjson = calculateContextJson(context);
    			context.animation.setFileformat("jpg");
    			File srcFile = context.animation.create(context.sequence, null);
    			File dstFile = new File(jobProgress.getJobsDirectory(), jobProgress.getSignature() + "." + context.animation.getFileformat());
    			Files.copy(Paths.get(srcFile.getAbsolutePath()), Paths.get(dstFile.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
    			jobProgress.setPreviewFileName(jobProgress.getSignature() + "." + context.animation.getFileformat());
    			jobProgress.setPreviewFileSize(dstFile.length());
    			if (keepjson == "jpg") {
    				jobProgress.setFinalFileName(jobProgress.getPreviewFileName());
    				jobProgress.setFinalFileSize(jobProgress.getPreviewFileSize());
    				jobProgress.setState(ERenderingState.successfullycompleted);
    				jobProgress.store();
    			} else {
    				createContextFromJson(keepjson, context);
    			}
    		}
    	}
    }
    
    /*
    
    @SuppressWarnings("unused")
	public void main(String[] args) throws Exception {
    	try {
    		System.setProperty("java.awt.headless", "true");
			createContextFromJson("{\"animation\": {\"className\":\"AnimationBuilder\",\"dimension\":{\"width\":600,\"height\":600},\"fileformat\":\"gif\"}, \"superposers\": [{\"location\":\"eBottomRight\",\"size\":15,\"padding\":15,\"className\":\"EveryListingLogoSuperposer\"},{\"text\":\"Calle Juliana Luperon No. 39, Jarabacoa\",\"location\":\"eBottomLeft\",\"size\":40,\"padding\":15,\"color\":\"black\",\"className\":\"TextSuperposer\"},{\"name\":\"Jane Doe\",\"location\":\"eTopLeft\",\"size\":50,\"padding\":15,\"className\":\"AgentSuperposer\"},{\"text\":\"Price: $732,500 USD\",\"location\":\"eCenterOnLastSequence\",\"size\":20,\"padding\":15,\"color\":\"red\",\"className\":\"TextSuperposer\"},{\"content\":\"OPEN HOUSE SATURDAY OCT 1st 10 TO 2\",\"location\":\"eRight\",\"padding\":15,\"color\":\"black\",\"style\":\"bold\",\"className\":\"VerticalBanner\"}], \"sequence\": [{\"className\":\"Sequence\",\"animationEndClass\":\"ShrinkToTopEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/4de4ca253e7cd4c6401d667621f250eb\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"PixelDissolveEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/3f9c9e02b86c53dcdfb0e2e93c217b25\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"CheckerboardEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/f26ec287fbe59eceef3e03a6f97187bb\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"ScrollLeftEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/e53641ad0d306a27e48cf02d911422bf\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"ScrollRightEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/a2b06080e5ec4627e9e392e62b726556\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"ScrollUpEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/7a0a271207c413989aabba6c2ad66420\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"ScrollDownEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/f2b46c2dc7e7b5b6e00141a8416fbc97\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"WipeLeftEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/dcae90a4d05aa050c0a9846c64b261a2\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"WipeRightEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/040ab9f1e9bdfd30d1f45a67668232e1\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"WipeUpEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/34485cbbaa776d5f69de56f7ce0b2241\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"WipeDownEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/52607b897da78a63990f666cb15b90ba\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"ZoomOutEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/ba952f561fbea50bebe0c833804da5de\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"ZoomInEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/c00dbd93b944532afeee5f2ee09da0c3\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"IrisOpenEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/fb221179aa96f813006f50953cda5fbf\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"IrisCloseEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/9108ee54f7c70420a870228012ccb22e\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"BarnDoorOpenEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/68e3f46d15af8756ad8d7ceb426b4198\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"BarnDoorCloseEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/f601d72f7c08d0e3ba462da297f2c86c\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250}]}");
	    	String json = calculateContextJson();
	    	RenderingJobProgress jobProgress = null;
	    	File resultingFile = null;
	    	if (((jobProgress = RenderingJobProgress.getRenderingJobProgressIfExists(new File("jobs"), json)) != null) && (ERenderingState.successfullycompleted.equals(jobProgress.getState())) && ((resultingFile = jobProgress.getResultingFile(true)) != null)) {
	    		ensurePreviewCreated(jobProgress, true);
	    		System.out.println(jobProgress.getProgressJson());
	    	} else if ((jobProgress == null) || ((!jobProgress.isStarted()) || (jobProgress.isMine(false)))) {
	    		if (jobProgress == null) {
	    			jobProgress = new RenderingJobProgress(RenderingJobProgress.calculateSignature(json), new File("jobs"));
	    		}
		    	jobProgress.setOriginaljson(json);
		    	ensurePreviewCreated(jobProgress, true);
		    	jobProgress.start();
		    	if (jobProgress.isMine(true)) {
		    		try {
		    			File srcFile = animation.create(sequence, jobProgress);
		    			File dstFile = new File(jobProgress.getJobsDirectory(), jobProgress.getSignature() + "." + animation.getFileformat());
		    			Files.copy(Paths.get(srcFile.getAbsolutePath()), Paths.get(dstFile.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
	    				jobProgress.setOneLinerCustomerFacingStatus("Completed");
	    				jobProgress.setState(ERenderingState.successfullycompleted);
	    				jobProgress.setCurrentTotal(null, null);
	    				jobProgress.setFinalFileName(dstFile.getName());
	    				jobProgress.setFinalFileSize(dstFile.length());
	    				jobProgress.store();
		    		} catch (Exception e) {
		    			jobProgress.setOneLinerCustomerFacingStatus("In error");
		    			jobProgress.setState(ERenderingState.failed);
		    			jobProgress.setExceptionString(StringUtils.getExceptionString(e));
		    			jobProgress.store();
		    		}
		    	}
	    	} else {
	    		
	    	}
	    	if (jobProgress != null) {
	    		if ((animation != null) && (animation.isVideo()) && (jobProgress.getPreviewFileName() == null)) {
	    			animation.setFileformat("jpg");
	    			File dFile = animation.create(sequence, null);
	    			Files.copy(Paths.get(dFile.getAbsolutePath()), Paths.get(new File(jobProgress.getJobsDirectory(), jobProgress.getSignature() + "." + animation.getFileformat()).getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
	    			jobProgress.setPreviewFileName(jobProgress.getSignature() + "." + animation.getFileformat());
    				jobProgress.store();
	    		}
	    	}
		} catch (ClassNotFoundException | IOException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
    }
        
    public static void main2(String[] args) throws Exception {
    	System.setProperty("java.awt.headless", "true");
    	
    	// createContextFromJson("{\"animation\": {\"className\":\"AnimationBuilder\",\"dimension\":{\"width\":600,\"height\":600},\"fileformat\":\"gif\"}, \"superposers\": [{\"location\":\"eBottomRight\",\"size\":15,\"padding\":15,\"className\":\"EveryListingLogoSuperposer\"},{\"text\":\"Calle Gregorio Luperon No. 39, Jarabacoa\",\"location\":\"eBottomLeft\",\"size\":40,\"padding\":15,\"color\":\"black\",\"className\":\"TextSuperposer\"},{\"name\":\"Jane Doe\",\"location\":\"eTopLeft\",\"size\":50,\"padding\":15,\"className\":\"AgentSuperposer\"},{\"text\":\"Price: $732,500 USD\",\"location\":\"eCenterOnLastSequence\",\"size\":20,\"padding\":15,\"color\":\"red\",\"className\":\"TextSuperposer\"},{\"content\":\"OPEN HOUSE SATURDAY OCT 1st 10 TO 2\",\"location\":\"eRight\",\"padding\":15,\"color\":\"black\",\"style\":\"bold\",\"className\":\"VerticalBanner\"}], \"sequence\": [{\"className\":\"Sequence\",\"animationEndClass\":\"StretchFromCenterEffect\",\"url\":\"http://everylisting.localhost/wp-content/uploads/2020/09/4f101eae66fc6071cf9cff7617ba4e60-1-850x570.jpeg\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"ShrinkToTopEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/4de4ca253e7cd4c6401d667621f250eb\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"PixelDissolveEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/3f9c9e02b86c53dcdfb0e2e93c217b25\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"CheckerboardEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/f26ec287fbe59eceef3e03a6f97187bb\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"ScrollLeftEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/e53641ad0d306a27e48cf02d911422bf\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"ScrollRightEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/a2b06080e5ec4627e9e392e62b726556\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"ScrollUpEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/7a0a271207c413989aabba6c2ad66420\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"ScrollDownEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/f2b46c2dc7e7b5b6e00141a8416fbc97\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"WipeLeftEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/dcae90a4d05aa050c0a9846c64b261a2\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"WipeRightEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/040ab9f1e9bdfd30d1f45a67668232e1\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"WipeUpEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/34485cbbaa776d5f69de56f7ce0b2241\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"WipeDownEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/52607b897da78a63990f666cb15b90ba\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"ZoomOutEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/ba952f561fbea50bebe0c833804da5de\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"ZoomInEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/c00dbd93b944532afeee5f2ee09da0c3\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"IrisOpenEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/fb221179aa96f813006f50953cda5fbf\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"IrisCloseEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/9108ee54f7c70420a870228012ccb22e\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"BarnDoorOpenEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/68e3f46d15af8756ad8d7ceb426b4198\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250},{\"className\":\"Sequence\",\"animationEndClass\":\"BarnDoorCloseEffect\",\"url\":\"https://res.listglobally.com/listings/5588968/60334592/f601d72f7c08d0e3ba462da297f2c86c\",\"fps\":30,\"fixedTime\":2000,\"msTransitionTime\":250}]}");
    	
    	int transitionTime = 250;
    	int fixedTime = 2000;
    	List<FrameSuperposer> superposers = new ArrayList<>();
		superposers.add(new EveryListingLogoSuperposer(new File("D:\\workspace\\imagegeneratorservlet\\res\\logoname_black_md_transparency_cropped.gif"), ELocation.eBottomRight, 15, 15));
		superposers.add(new TextSuperposer("Calle Gregorio Luperon No. 39, Jarabacoa", ELocation.eBottomLeft, 40, 15, "black"));
		superposers.add(new AgentSuperposer(new File("D:\\workspace\\imagegeneratorservlet\\res\\Jane-Doe.jpg"), "Jane Doe", ELocation.eTopLeft, 50, 15));
		superposers.add(new TextSuperposer("Price: $732,500 USD", ELocation.eCenterOnLastSequence, 20, 15, "red"));
		superposers.add(new VerticalBanner("OPEN HOUSE SATURDAY OCT 1st 10 TO 2", ELocation.eRight, 15, "black", "bold"));
    	AnimationBuilder animation = new AnimationBuilder(superposers, new Dimension(600, 600), "gif");
    	List<AnimationBuilder.Sequence> sequence = new ArrayList<>();
    	sequence.add(animation.addSequence("http://everylisting.localhost/wp-content/uploads/2020/09/4f101eae66fc6071cf9cff7617ba4e60-1-850x570.jpeg", StretchFromCenterEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/4de4ca253e7cd4c6401d667621f250eb", ShrinkToTopEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/3f9c9e02b86c53dcdfb0e2e93c217b25", PixelDissolveEffect.class, fixedTime, transitionTime));
    	/*sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/f26ec287fbe59eceef3e03a6f97187bb", CheckerboardEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/e53641ad0d306a27e48cf02d911422bf", ScrollLeftEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/a2b06080e5ec4627e9e392e62b726556", ScrollRightEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/7a0a271207c413989aabba6c2ad66420", ScrollUpEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/f2b46c2dc7e7b5b6e00141a8416fbc97", ScrollDownEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/dcae90a4d05aa050c0a9846c64b261a2", WipeLeftEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/040ab9f1e9bdfd30d1f45a67668232e1", WipeRightEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/34485cbbaa776d5f69de56f7ce0b2241", WipeUpEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/52607b897da78a63990f666cb15b90ba", WipeDownEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/ba952f561fbea50bebe0c833804da5de", ZoomOutEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/c00dbd93b944532afeee5f2ee09da0c3", ZoomInEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/fb221179aa96f813006f50953cda5fbf", IrisOpenEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/9108ee54f7c70420a870228012ccb22e", IrisCloseEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/68e3f46d15af8756ad8d7ceb426b4198", BarnDoorOpenEffect.class, fixedTime, transitionTime));
    	sequence.add(animation.addSequence("https://res.listglobally.com/listings/5588968/60334592/f601d72f7c08d0e3ba462da297f2c86c", BarnDoorCloseEffect.class, fixedTime, transitionTime));
    	animation.setFileformat("gif");
    	// String json = "{" + "\"animation\": " + new Gson().toJson(animation) + ", " + "\"superposers\": " + new Gson().toJson(superposers) + ", " + "\"sequence\": " + new Gson().toJson(sequence) + "}";
		// System.out.println(json);
    	animation.create(sequence, null);
    }
    */
}
