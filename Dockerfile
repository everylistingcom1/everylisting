FROM php:7.3-apache
COPY --chown=www-data . /var/www/html

RUN chmod +x /var/www/html/dbDataAndSetUp/setup.sh
RUN chmod +x /var/www/html/dbDataAndSetUp/db_refresh.sh

EXPOSE 80 443
# use custom apache configuration file
COPY kubernetes/apache/apache2.conf /etc/apache2/apache2.conf
COPY kubernetes/apache/dev.everylisting.com.conf /etc/apache2/sites-available/dev.everylisting.com.conf
RUN ln -s /etc/apache2/sites-available/dev.everylisting.com.conf /etc/apache2/sites-enabled/dev.everylisting.com.conf

#copy ssl certificates
COPY kubernetes/apache/star-everylisting-com /etc/apache2/certificates/star-everylisting-com

# download and install packages
RUN apt-get update && apt-get -y install mariadb-client

RUN docker-php-ext-install mysqli
RUN a2enmod rewrite
RUN a2enmod ssl

#Drop the current database and recreate from scratch
RUN cd /var/www/html/dbDataAndSetUp && ./db_refresh.sh

CMD ["apache2-foreground"]
