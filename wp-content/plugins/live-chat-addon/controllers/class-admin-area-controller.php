<?php

class wbca_Admin_Area_Controller {
	private $plugin_name;
	private $plugin_version;
	
	function __construct(){
		add_action( 'admin_menu', array($this,'wbca_admin_menu') );
	}

	public function wbca_admin_menu(){
		$this->wbca_plugin_info();
		
		if(qcld_live_is_active_white_label() && get_option('wpwl_word_wpbot')!=''){
			$menutxt = get_option('wpwl_word_wpbot');
		}else{
			if(qclivechat_is_woowbot_active()){
				$menutxt = 'Bot';
			}else{
				$menutxt = 'Bot';
			}
		}
		
        if ( current_user_can( 'publish_posts' ) ){
          $page = add_menu_page( $menutxt.' - Live Chat', $menutxt.' - Live Chat', 'publish_posts', 'wbca-chat-page', array( $this, 'wbca_chat_page' ), 'dashicons-info', '7' );
		  //add_submenu_page( 'wbca-chat-page', 'wbca - Add QA', 'Add QA', 'publish_posts', 'wbca-add-qa-page', array( $this, 'wbca_add_qa_page' ) );
		  //add_submenu_page( 'wbca-chat-page', 'wbca - Update QA', 'Edit QA', 'publish_posts', 'wbca-edit-qa-page', array( $this, 'wbca_edit_qa_page' ) );
		  
		  add_submenu_page( 'wbca-chat-page', 'Chat History', 'Chat History', 'publish_posts', 'wbca-chat-history', array( $this, 'wbca_chat_history' ) );
		  
		  add_submenu_page( 'wbca-chat-page', 'Live Chat Options', 'Live Chat Options', 'publish_posts', 'wbca-chat-options', array( $this, 'wbca_chat_options' ) );
		  
		  add_submenu_page( 'wbca-chat-page', 'Help & License', 'Help & License', 'manage_options','qc-wplive-chat-help-license', 'qcld_livechat_license_callback' );
		  
        }
		
    }
	
	public function wbca_chat_options(){
		
		echo '';
	}
	
	public function wbca_chat_page(){
		$html = '';
		
		$getAvater = get_avatar(get_current_user_id());
		if(trim($getAvater)!=''){
			$doc = new DOMDocument();
			@$doc->loadHTML($getAvater);
			$xpath = new DOMXPath($doc);
			$src = $xpath->evaluate("string(//img/@src)");
		}else{
			
			$src = esc_url( get_avatar_url( get_current_user_id() ) );
		}
		
		$user_id = get_current_user_id();
		$user = get_userdata( $user_id );
		$name = $user->display_name;
		
		$alloperator = '';
		
		$user_query = new WP_User_Query( array( 'role' => 'operator' ) );
		
		// User Loop
		
		if ( ! empty( $user_query->results ) ) {
			foreach ( $user_query->results as $operator ) {
				
				if($user_id != $operator->ID){
					$alloperator .= '<li data-operatorid="'.$operator->ID.'" data-operatorname="' . $operator->display_name . '">&nbsp;</li>';
				}
			}
		}
		
		$user_query = new WP_User_Query( array( 'role' => 'administrator' ) );
		
		// User Loop
		
		if ( ! empty( $user_query->results ) ) {
			foreach ( $user_query->results as $operator ) {
				
				if($user_id != $operator->ID){
					$alloperator .= '<li data-operatorid="'.$operator->ID.'" data-operatorname="' . $operator->display_name . '">&nbsp;</li>';
				}
			}
		}
		
		
		
		$html .= '<div class="wbca-admin-wrap">';
			$html .= '<div class="wbca-admin-head">';
				$html .= '<div class="wbca-admin-head-left">';
					$html .= '<img class="wbca-operator-image" src="'.$src.'">';
					$html .= '<span class="wbca-operator-name">'.$name.'</span>';
				$html .= '</div>';
				$html .= '<div class="wbca-admin-head-right">';
					
				$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="wbca-admin-body wbca-clear">';
				$html .= '<div id="wbca-tabs-wrap">';
					$html .= '<ul id="wbca-chat-tabs">';
						$html .= '<li data-event="dashboard-client-tabs" class="wbca-current wbca_dashboard">Dashboard</li>';
						$html .= '<li data-event="" class="qct_no_client_msg">No active client. All active clients will be listed here.</li>';
					$html .= '</ul>';
				$html .= '</div>';
				$html .= '<div id="wbca-content-wrap">';
					$html .= '<div class="wbca-chat-box wbca-visible">';
						$html .='<div id="wpca_active_client">0 active Client currently!</div>';
						$html .= '<div class="wbca-dashbord">';
							$html .= '<div class="wbca-chat-items">';
							$html .= '<div style="padding-right:5px;">';
							$html .= '</div>';
							$html .= '<div class="wbca-search-items">';
							$html .= '<div style="padding-left:5px;">';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';
		$html .= '</div>';
		$html .= '<ul class="wbca_hide" id="wbca_select_operator_id">';
		$html .= $alloperator;
		$html .= '</ul>';
    	echo $html;
    }
	
	public function wbca_add_qa_page(){
		$html = '';
		
		$getAvater = get_avatar(get_current_user_id());
		$doc = new DOMDocument();
		$doc->loadHTML($getAvater);
		$xpath = new DOMXPath($doc);
		$src = $xpath->evaluate("string(//img/@src)");
		
		$user = get_userdata( get_current_user_id() );
		$name = $user->display_name;
		
		$html .= '<div class="wbca-admin-wrap">';
			$html .= '<div class="wbca-admin-head">';
				$html .= '<div class="wbca-admin-head-left">';
					$html .= '<img class="wbca-operator-image" src="'.$src.'">';
					$html .= '<span class="wbca-operator-name">'.$name.'</span>';
				$html .= '</div>';
				$html .= '<div class="wbca-admin-head-right">';
					$html .= '<span class="wbca-info">'.$this->plugin_name.'</span>';
					$html .= '<span class="wbca-info">v-'.$this->plugin_version.'</span>';
				$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="wbca-admin-body wbca-clear">';
				$html .= '<div id="wbca-add-content-wrap">';
					$html .= '<form id="wbca_add_search_form" action="" method="post">
								<div class="wbca_search_msg"></div>
							  <div>
							  	<!--[if IE ]>
								   <span>Type question</span><br/>
								<![endif]-->
								<input type="text" id="wbca_search_question" name="wbca_search_question" value="" placeholder="'.__('Type question', WBCA).'">
							  </div>
							  <div>
							  	<!--[if IE ]>
								   <span>Type answer</span><br/>
								<![endif]-->
								<textarea id="wbca_search_answer" name="wbca_search_answer" value="" placeholder="'.__('Type answer', WBCA).'"></textarea>
							  </div>
							  <div>
								<button class="wbca_button" type="submit" data-event="wbca_add_search" id="wbca_add_search" name="wbca_add_search">'.__('Submit',WBCA).'</button>
							  </div>
							</form>';
				$html .= '</div>';
			$html .= '</div>';
		$html .= '</div>';
    	echo $html;
       	   
    }
	
	public function wbca_chat_history(){
		global $wpdb;
		$wpdb->show_errors = true;	
		$prefix = $wpdb->prefix;
		$user = wp_get_current_user();
		
		if(isset($_GET['clientid']) && $_GET['clientid']!=''){
		$client = get_user_by( "ID", $_GET['clientid'] );
		
		?>	
		<div class="sld_menu_title" style="text-align:left;">
			<table class="form-table">
				<tbody>
				<tr><th style="padding: 5px;" scope="row">Client Name</th><td style="padding: 5px;"><?php echo $client->display_name ?></td></tr>
				<tr><th style="padding: 5px;" scope="row">Client Email</th><td style="padding: 5px;"><?php echo $client->user_email ?></td></tr>
				</tbody>
			</table>
		</div>
		<?php 
		
		$result = $wpdb->get_results('SELECT * FROM `'.$prefix.'wbca_message` WHERE 1 and (`user_sender` = '.$client->ID.' or `user_receiver` = '.$client->ID.') order by `chat_time` asc');
		
			if(!empty($result)):
		?>
			<div class="qchero_sliders_list_wrapper">
			<div class="qchero_slider_table_area" style="max-width: 650px;">
			<div class="sld_payment_table">
		<?php
			foreach($result as $row){
			$user_sender = get_user_by( "ID", $row->user_sender );
		?>
				<div class="sld_payment_row" style="padding:8px;display:block;<?php echo ($row->user_sender==$client->ID?'text-align:right':'') ?>">
					<div class="wbca_chat_msg">
						<?php echo stripslashes($row->message); ?>
					</div>
					<div class="wbca_chat_msg_bottom">
					From <b><?php echo $user_sender->display_name; ?></b> on <?php echo $row->chat_time ?>
					</div>
				</div>
			
		<?php
			}
		?>
			</div>
			</div>
			</div>
		<?php
			endif;
		}else{
		
		
		
		if(current_user_can('administrator')){
			$sql = 'SELECT m.`id`, m.`user_sender`, m.`user_receiver`, m.`message`, m.`chat_read`, m.`wbca_transferred`, m.`chat_time` FROM `'.$prefix.'wbca_message` as m, '.$prefix.'usermeta as um WHERE 1 and um.user_id = m.user_sender and um.`meta_value` like "%livechatuser%" group by m.`user_sender` order by m.`chat_time` desc';
			
			
		
		}else{
			$sql = 'SELECT m.`id`, m.`user_sender`, m.`user_receiver`, m.`message`, m.`chat_read`, m.`wbca_transferred`, m.`chat_time` FROM `'.$prefix.'wbca_message` as m, '.$prefix.'usermeta as um WHERE 1 and um.user_id = m.user_sender and um.`meta_value` like "%livechatuser%" and `user_receiver` = '.$user->ID.' group by m.`user_sender` order by m.`chat_time` desc';
			
			
			
		}
		
		$total = count($wpdb->get_results( $sql ));
		
		$items_per_page = 30;
		$page             = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;
		$offset         = ( $page * $items_per_page ) - $items_per_page;
		$sql .=" LIMIT ${offset}, ${items_per_page}";
		
		$result = $wpdb->get_results($sql);
		?>
		<div class="qchero_sliders_list_wrapper">
			<div class="sld_menu_title">
				<h2 style="font-size: 26px;text-align:center"><?php echo __('All Clients', 'qc-opd') ?></h2>
			</div>
			
			<?php 
			
				$totalPage         = ceil($total / $items_per_page);
				
				$customPagHTML = '';
				if($totalPage > 0){
					$customPagHTML     =  '<div><span class="wpbot_pagination">Page '.esc_html($page).' of '.esc_html($totalPage).'</span>'.paginate_links( array(
					'base' => add_query_arg( 'cpage', '%#%' ),
					'format' => '',
					'prev_text'    => __('« prev'),
					'next_text'    => __('next »'),
					'total' => esc_html($totalPage),
					'current' => esc_html($page),
					
					)).'</div>';
					?>
					
					<div class="sld_menu_title" style="text-align: left;">
						<?php echo $customPagHTML; ?>
					</div>
					
					<?php
				}
			
			?>
			
			<?php if(!empty($result)): ?>
			
			<div class="qchero_slider_table_area">
				<div class="sld_payment_table">
					<div class="sld_payment_row header">
						
						<div class="sld_payment_cell">
							<?php _e( 'Date', 'qc-opd' ) ?>
						</div>
						<div class="sld_payment_cell">
							<?php _e( 'Client Name', 'qc-opd' ) ?>
						</div>
						<div class="sld_payment_cell">
							<?php _e( 'Operator Name', 'qc-opd' ); ?>
						</div>
						<div class="sld_payment_cell">
							<?php _e( 'Action', 'qc-opd' ); ?>
						</div>
						
					</div>

			<?php
			foreach($result as $row){
				$user_sender = get_user_by( "ID", $row->user_sender );
				$user_receiver = get_user_by( "ID", $row->user_receiver );
				$url = admin_url( 'admin.php?page=wbca-chat-history&clientid='.$row->user_sender );
			?>
				<div class="sld_payment_row">
					
					<div class="sld_payment_cell">
						<div class="sld_responsive_head"><?php echo __('Date', 'qc-opd') ?></div>
						<?php echo date('m/d/Y', strtotime($row->chat_time)); ?>
					</div>
					<div class="sld_payment_cell">
						<div class="sld_responsive_head"><?php echo __('Client Name', 'qc-opd') ?></div>
						<?php echo $user_sender->display_name ?>
					</div>
					<div class="sld_payment_cell">
						<div class="sld_responsive_head"><?php echo __('Operator Name', 'qc-opd') ?></div>
						<?php
							echo $user_receiver->display_name
							
						?>
					</div>
					<div class="sld_payment_cell">
						<div class="sld_responsive_head"><?php echo __('Action', 'qc-opd') ?></div>
						<a href="<?php echo $url; ?>" class="button-primary">View Chat</a>
					</div>
					
				</div>
			<?php
			}
			?>

			</div>

		</div>
		<?php endif; ?>
		</div>
		<?php
		}
	}
	
	public function wbca_edit_qa_page(){
		global $wpdb;
		$wpdb->show_errors = true;			
		$html = '';
		$row = '';
		$getAvater = get_avatar(get_current_user_id());
		$doc = new DOMDocument();
		$doc->loadHTML($getAvater);
		$xpath = new DOMXPath($doc);
		$src = $xpath->evaluate("string(//img/@src)");
		
		$user = get_userdata( get_current_user_id() );
		$name = $user->display_name;
		
		$prefix = $wpdb->prefix;
		$result = $wpdb->get_results('SELECT * FROM '.$prefix.'wbca_search_document ORDER BY DOCUMENT_ID ASC LIMIT 10');
		
		if(!empty($result)){
			$row .= '<ul id="wbca_edit_chat_row">';
			foreach ($result as $docs){
				$row .= '<li id="docid_'.$docs->DOCUMENT_ID.'">';
					$row .= '<div class="wbca-clear" data-titleid="'.$docs->DOCUMENT_ID.'">';
					$row .= '<b class="wbca_edit_title" data-event="send-to-edit-form">'.$docs->DOCUMENT_TITLE.'</b>';
					$row .= '<button class="wbca-delete-chat-row" data-event="wbca-delete-chat-row"><span>&times;</span></button>';
					$row .= '<button class="expand-wbca-edit-desc" data-event="expand-wbca-edit-desc"><span>&or;</span></button>';
					$row .= '</div>';
					$row .= '<div class="wbca_edit_desc" id="descid_'.$docs->DOCUMENT_ID.'" data-desc-state="0">';
					$row .= $docs->DESCRIPTION;
					$row .= '</div>';
				$row .= '</li>';
			}
			$row .= '</ul>';
		}
		
		$html .= '<div class="wbca-admin-wrap">';
			$html .= '<div class="wbca-admin-head">';
				$html .= '<div class="wbca-admin-head-left">';
					$html .= '<img class="wbca-operator-image" src="'.$src.'">';
					$html .= '<span class="wbca-operator-name">'.$name.'</span>';
				$html .= '</div>';
				$html .= '<div class="wbca-admin-head-right">';
					$html .= '<span class="wbca-info">'.$this->plugin_name.'</span>';
					$html .= '<span class="wbca-info">v-'.$this->plugin_version.'</span>';
				$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="wbca-admin-body wbca-clear">';
				$html .= '<div class="wbca-chat-items">';
					$html .= '<form id="wbca_edit_form" action="" method="post">
								<div class="wbca_search_msg"></div>
							  <div>
							  	<!--[if IE ]>
								   <span>Type question</span><br/>
								<![endif]-->
								<input type="text" id="wbca_edit_search_question" required name="wbca_edit_search_question" value="" placeholder="'.__('Type question',WBCA).'">
							  </div>
							  <div>
							  	<!--[if IE ]>
								   <span>Type answer</span><br/>
								<![endif]-->
								<textarea id="wbca_edit_search_answer" value="" required name="wbca_edit_search_answer" placeholder="'.__('Type answer',WBCA).'"></textarea>
							  </div>
							  <div>
									<button class="wbca_button" type="submit" data-event="wbca_edit_form_button" id="wbca_edit_form_button" data-wbca-searchid="" name="wbca_edit_form_button">'.__('Edit',WBCA).'</button>
							  </div>
							</form>';
				$html .= '</div>';
				$html .= '<div class="wbca-search-items">';
					$html .= '<form id="wbca_edit_search_form" method="post" action="">';
					  $html .= '<input type="text" placeholder="Search..." name="wbca_edit_search_query" value=""/>';
					  $html .= '<button class="wbca_button" type="submit" data-event="wbca_edit_search_nav" id="wbca_edit_search_button" name="wbca_edit_search_nav">Search</button>';
					$html .= '</form>';
					$html .= '<div id="wbca_edit_row_container">';
					$html .= $row;
					$html .= '</div>';
					$html .= '<div id="wbca_edit_row_footer">';
					if(!empty($result) && count($result) >= 10){
						$html .= '<button class="wbca_button" data-event="wbca_edit_button_nav" id="wbca_edit_nav_back" data-pageid="0" data-direction="back" title="Back">&lsaquo;</button>';
						$html .= '<button class="wbca_button" data-event="wbca_edit_button_nav" id="wbca_edit_nav_next" data-pageid="2" data-direction="next" title="Next">&rsaquo;</button>';
					}
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';
		$html .= '</div>';
    	echo $html;
       	   
    }
	public function wbca_plugin_info(){	 
		$plugin_data = get_plugin_data( WBCA_PATH.'wpbot-chat-addon.php', false, false );
		$this->plugin_name = $plugin_data['Name'];
		$this->plugin_version = $plugin_data['Version'];
	}
}


?>