jQuery(document).ready(function($){
	"use strict";
	$('#wpbot_checked_all').on('click', function(){
		if($(this).is(':checked')){
			$('.wpbot_sessions_checkbox').prop('checked', true);
		}else{
			$('.wpbot_sessions_checkbox').prop('checked', false);
		}
	})
	
	$('#wpbot_submit_delcs_form').on('click', function(e){
		e.preventDefault();
		$( "#wpcs_form_sessions" ).submit();
	})
	
	
	// Get the modal
	var modal = document.getElementById("wpcsmyModal");


	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("wpcsclose")[0];

	$('.wpcsmyBtn').on('click', function(e){
		e.preventDefault();
		$('#wpcs_to_email_address').val($(this).attr('data-email'));
		$('#wpcs_show_email').html($(this).attr('data-email'));
		$('#wpcsmyModal').show();
	})

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	  modal.style.display = "none";
	  $('#wpcs_show_email').html('');
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	  if (event.target == modal) {
		modal.style.display = "none";
		$('#wpcs_show_email').html('');
	  }
	}
	
	
	jQuery( "#wpcs_email_form" ).submit(function( event ) {	  
	
		event.preventDefault();
		jQuery('#wpcs_email_loading').hide();
		jQuery( "#wpcs_email_status" ).html('');
		var subject = jQuery('#wpcs_email_subject').val();

		var message = jQuery('#wpcs_email_message').val();
		var to = jQuery('#wpcs_to_email_address').val();

		var error = false;

		if(subject==''){
		  alert('please provide the subject');
		  error = true;
		}
		if(message==''){
		  alert('please provide the message content');
		  error = true;
		}

		if(error==false){
			jQuery('#wpcs_email_loading').show();
			
			
			var data = {
			  'to': to,
			  'subject': subject,
			  'message': message,
			}
			$.post(
				ajax_object.ajax_url,
				{
					action : 'wpcs_send_email',
					data: data,
				},
				function(data){
					jQuery('#wpcs_email_loading').hide();
					var json = $.parseJSON(data);
					jQuery( "#wpcs_email_status" ).html(json.message);
					jQuery('#wpcs_email_subject').val('');
					jQuery('#wpcs_email_message').val('');
					jQuery('#wpcs_to_email_address').val('');
					
				}
			);
			
			
		}
	  
	});
	
})