<?php
define('chatsessions_LICENSING_PLUGIN_SLUG', 'chat-session-addon/wpbot-chat-history-addon.php');
define('chatsessions_LICENSING_PLUGIN_NAME', 'chat-session-addon');
define('chatsessions_LICENSING__DIR', plugin_dir_path(__DIR__));

define('chatsessions_LICENSING_REMOTE_PATH', 'https://www.ultrawebmedia.com/li/plugins/chat-session-addon/update.php');
define('chatsessions_LICENSING_PRODUCT_DEV_URL', 'https://quantumcloud.com/products/');

//start new-update-for-codecanyon
define('chatsessions_ENVATO_PLUGIN_ID', -1);
//end new-update-for-codecanyon

function get_chatsessions_licensing_plugin_data(){
	include_once(ABSPATH.'wp-admin/includes/plugin.php');
	return get_plugin_data(chatsessions_LICENSING__DIR.'/wpbot-chat-history-addon.php', false);
}

//License Options
function get_chatsessions_licensing_key(){
	return get_option('qcld_chatsessions_enter_license_key');
}

function get_chatsessions_envato_key(){
	return get_option('qcld_chatsessions_enter_envato_key');
}

function get_chatsessions_licensing_buy_from(){
	return get_option('qcld_chatsessions_buy_from_where');
}


//Update Transients
function get_chatsessions_update_transient(){
	return get_transient('qcld_update_chatsessions');
}

function set_chatsessions_update_transient($plugin_object){
	return set_transient( 'qcld_update_chatsessions', serialize($plugin_object), 1 * DAY_IN_SECONDS  );
}

function delete_chatsessions_update_transient(){
	return delete_transient( 'qcld_update_chatsessions' );
}


//Renewal Transients
function get_chatsessions_renew_transient(){
	return get_transient('qcld_renew_chatsessions_subscription');
}

function set_chatsessions_renew_transient($plugin_object){
	return set_transient( 'qcld_renew_chatsessions_subscription', serialize($plugin_object), 1 * DAY_IN_SECONDS  );
}

function delete_chatsessions_renew_transient(){
	return delete_transient( 'qcld_renew_chatsessions_subscription' );
}


//Invalid License Options
function get_chatsessions_invalid_license(){
	return get_option('chatsessions_invalid_license');
}

function set_chatsessions_invalid_license(){
	return update_option('chatsessions_invalid_license', 1);
}

function delete_chatsessions_invalid_license(){
	return delete_option('chatsessions_invalid_license');
}
function chatsessions_get_licensing_url(){
	return admin_url('admin.php?page=chatbot-sessions-help-license');
}

//Valid License
function get_chatsessions_valid_license(){
	return get_option('chatsessions_valid_license');
}
function set_chatsessions_valid_license(){
	return update_option('chatsessions_valid_license', 1);
}
function delete_chatsessions_valid_license(){
	return delete_option('chatsessions_valid_license');
}

//staging or live 
function get_chatsessions_site_type(){
	return get_option('qcld_chatsessions_site_type');
}



//start new-update-for-codecanyon
function get_chatsessions_license_purchase_code(){
	return get_option('qcld_chatsessions_enter_license_or_purchase_key');
}

function get_chatsessions_enter_license_notice_dismiss_transient(){
	return get_transient('get_chatsessions_enter_license_notice_dismiss_transient');
}

function set_chatsessions_enter_license_notice_dismiss_transient(){
	return set_transient('get_chatsessions_enter_license_notice_dismiss_transient', 1, DAY_IN_SECONDS);
}

function get_chatsessions_invalid_license_notice_dismiss_transient(){
	return get_transient('get_chatsessions_invalid_license_notice_dismiss_transient');
}

function set_chatsessions_invalid_license_notice_dismiss_transient(){
	return set_transient('get_chatsessions_invalid_license_notice_dismiss_transient', 1, DAY_IN_SECONDS);
}
//end new-update-for-codecanyon