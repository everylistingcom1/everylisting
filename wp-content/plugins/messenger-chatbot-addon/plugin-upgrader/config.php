<?php
define('messengerchatbot_LICENSING_PLUGIN_SLUG', 'messenger-chatbot-addon/wpbot-fb-messenger-addon.php');
define('messengerchatbot_LICENSING_PLUGIN_NAME', 'messenger-chatbot-addon');
define('messengerchatbot_LICENSING__DIR', plugin_dir_path(__DIR__));

define('messengerchatbot_LICENSING_REMOTE_PATH', 'https://www.ultrawebmedia.com/li/plugins/messenger-chatbot-addon/update.php');
define('messengerchatbot_LICENSING_PRODUCT_DEV_URL', 'https://quantumcloud.com/products/');

//start new-update-for-codecanyon
define('messengerchatbot_ENVATO_PLUGIN_ID', 23941596);
//end new-update-for-codecanyon

function get_messengerchatbot_licensing_plugin_data(){
	include_once(ABSPATH.'wp-admin/includes/plugin.php');
	return get_plugin_data(messengerchatbot_LICENSING__DIR.'/wpbot-fb-messenger-addon.php', false);
}

//License Options
function get_messengerchatbot_licensing_key(){
	return get_option('qcld_messengerchatbot_enter_license_key');
}

function get_messengerchatbot_envato_key(){
	return get_option('qcld_messengerchatbot_enter_envato_key');
}

function get_messengerchatbot_licensing_buy_from(){
	return get_option('qcld_messengerchatbot_buy_from_where');
}


//Update Transients
function get_messengerchatbot_update_transient(){
	return get_transient('qcld_update_messengerchatbot');
}

function set_messengerchatbot_update_transient($plugin_object){
	return set_transient( 'qcld_update_messengerchatbot', serialize($plugin_object), 1 * DAY_IN_SECONDS  );
}

function delete_messengerchatbot_update_transient(){
	return delete_transient( 'qcld_update_messengerchatbot' );
}


//Renewal Transients
function get_messengerchatbot_renew_transient(){
	return get_transient('qcld_renew_messengerchatbot_subscription');
}

function set_messengerchatbot_renew_transient($plugin_object){
	return set_transient( 'qcld_renew_messengerchatbot_subscription', serialize($plugin_object), 1 * DAY_IN_SECONDS  );
}

function delete_messengerchatbot_renew_transient(){
	return delete_transient( 'qcld_renew_messengerchatbot_subscription' );
}


//Invalid License Options
function get_messengerchatbot_invalid_license(){
	return get_option('messengerchatbot_invalid_license');
}

function set_messengerchatbot_invalid_license(){
	return update_option('messengerchatbot_invalid_license', 1);
}

function delete_messengerchatbot_invalid_license(){
	return delete_option('messengerchatbot_invalid_license');
}
function messengerchatbot_get_licensing_url(){
	return admin_url('admin.php?page=messenger-chatbot-help-license');
}

//Valid License
function get_messengerchatbot_valid_license(){
	return get_option('messengerchatbot_valid_license');
}
function set_messengerchatbot_valid_license(){
	return update_option('messengerchatbot_valid_license', 1);
}
function delete_messengerchatbot_valid_license(){
	return delete_option('messengerchatbot_valid_license');
}

//staging or live 
function get_messengerchatbot_site_type(){
	return get_option('qcld_messengerchatbot_site_type');
}



//start new-update-for-codecanyon
function get_messengerchatbot_license_purchase_code(){
	return get_option('qcld_messengerchatbot_enter_license_or_purchase_key');
}

function get_messengerchatbot_enter_license_notice_dismiss_transient(){
	return get_transient('get_messengerchatbot_enter_license_notice_dismiss_transient');
}

function set_messengerchatbot_enter_license_notice_dismiss_transient(){
	return set_transient('get_messengerchatbot_enter_license_notice_dismiss_transient', 1, DAY_IN_SECONDS);
}

function get_messengerchatbot_invalid_license_notice_dismiss_transient(){
	return get_transient('get_messengerchatbot_invalid_license_notice_dismiss_transient');
}

function set_messengerchatbot_invalid_license_notice_dismiss_transient(){
	return set_transient('get_messengerchatbot_invalid_license_notice_dismiss_transient', 1, DAY_IN_SECONDS);
}
//end new-update-for-codecanyon