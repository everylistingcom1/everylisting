<?php
/*
* Messenger settings area
*/
class wbfb_Admin_Area_Controller {
	
	function __construct(){
		add_action( 'admin_menu', array($this,'wbfb_admin_menu') );
		add_action( 'admin_init', array($this, 'wpfb_register_plugin_settings') );
	}

	public function wbfb_admin_menu(){

        if ( current_user_can( 'publish_posts' ) ){
			
			
			
			add_menu_page( 'Messenger Chatbot', 'Messenger Chatbot', 'publish_posts', 'wbfb-botsetting-page', array( $this, 'wbfb_setting_page' ), 'dashicons-facebook', '9' );
			
			add_submenu_page( 'wbfb-botsetting-page', 'Help & License', 'Help & License', 'manage_options','messenger-chatbot-help-license', array($this, 'qcld_license_callback') );
        }
		
    }
	
	public function wpfb_register_plugin_settings(){
		register_setting( 'qc-wpfb-plugin-settings-group', 'wpfb_enable_fbbot' );
		register_setting( 'qc-wpfb-plugin-settings-group', 'wpfb_verify_token' );
		register_setting( 'qc-wpfb-plugin-settings-group', 'wpfb_page_access_token' );
		register_setting( 'qc-wpfb-plugin-settings-group', 'wpfb_default_instruction' );
		register_setting( 'qc-wpfb-plugin-settings-group', 'wpfb_default_no_match' );
		register_setting( 'qc-wpfb-plugin-settings-group', 'wpfb_command_live_agent' );
		register_setting( 'qc-wpfb-plugin-settings-group', 'wpfb_contact_admin_text' );
		register_setting( 'qc-wpfb-plugin-settings-group', 'wpfb_remove_image' );
		register_setting( 'qc-wpfb-plugin-settings-group', 'wpfb_remove_video' );
		
	}
	
	public function wbfb_setting_page(){
		wp_enqueue_style('qc_messenger_chatbot_admin_styles', WBFB_URL . '/assets/css/style.css');
		?>
	<div class="wrap swpm-admin-menu-wrap">
		<h1><?php echo esc_html__('Facebook Bot Settings Page', 'wpfb'); ?></h1>
	
		<h2 class="nav-tab-wrapper sld_nav_container">
			<a class="nav-tab sld_click_handle nav-tab-active" href="#general_settings"><?php echo esc_html__('General Settings', 'wpfb'); ?></a>
		</h2>
		
		<h2 class="qcfb_msg_heading" ><?php echo esc_html__('You can follow the step by step instructions for setting up FaceBook App and other settings in our KnowledgeBase.', 'wpfb'); ?> <a href="<?php echo esc_url('https://www.quantumcloud.com/resources/knowledgebase/how-to-set-up-messenger-chatbot-for-wpbot/'); ?>" class="button button-primary" target="_blank"><?php echo esc_html__('View KnowledgeBase', 'wpfb'); ?></a></h2>
		
		<form method="post" action="options.php">
			<?php settings_fields( 'qc-wpfb-plugin-settings-group' ); ?>
			<?php do_settings_sections( 'qc-wpfb-plugin-settings-group' ); ?>
			<div id="general_settings">
				<table class="form-table">
					<tr valign="top">
						<th scope="row"><?php echo esc_html__('Enable Facebook Bot', 'wpfb'); ?></th>
						<td>
							<input type="checkbox" name="wpfb_enable_fbbot" value="on" <?php echo (esc_attr( get_option('wpfb_enable_fbbot') )=='on'?'checked="checked"':''); ?> />
							<i><?php echo esc_html__('Turn ON to enable facebook bot on top of WPBot.', 'wpfb'); ?></i>
						</td>
					</tr>
					
					<tr valign="top">
						<th scope="row"><?php echo esc_html__('Facebook Verify Token', 'wpfb'); ?></th>
						<td>
							<input type="text" name="wpfb_verify_token" size="100" value="<?php echo esc_attr( get_option('wpfb_verify_token') ); ?>"  />
							<i><?php echo esc_html__('Please add a verify token and also you have to put the same token in facebook messenger app settings. The token could be anything random unique character. Ex: sdf343sdfaewrf2343234ff.', 'wpfb'); ?></i>
						</td>
					</tr>
					
					<tr valign="top">
						<th scope="row"><?php echo esc_html__('Page Access Token', 'wpfb'); ?></th>
						<td>
							<input type="text" name="wpfb_page_access_token" size="100" value="<?php echo esc_attr( get_option('wpfb_page_access_token') ); ?>"  />
							<i><?php echo esc_html__('Please add a Page Access Token which you can find in Messenger Settings page in Access Tokens section.', 'wpfb'); ?></i>
						</td>
					</tr>
					
					<?php if(qcpdmca_is_wpbot_active()): ?>
					<tr valign="top">
						<th scope="row"><?php echo esc_html__('Callback URL', 'wpfb'); ?> <?php echo esc_html(mca_wpbot_text()); ?></th>
						<td>
							
							<input type="text" name="wpfb_callback_url" size="100" value="<?php echo esc_url(get_site_url().'/?action=fbinteraction'); ?>" readonly />
							<i><?php echo esc_html__('Please copy the url and add it to the Callback URL field in Webhooks section in Facebook App.', 'wpfb'); ?></i>
						</td>
					</tr>
					<?php endif; ?>
					
					<?php if(qcpdmca_is_woowbot_active()): ?>
					<tr valign="top">
						<th scope="row"><?php echo esc_html__('Callback URL', 'wpfb'); ?> <?php echo esc_html(mca_woowbot_text()); ?></th>
						<td>
							
							<input type="text" name="wpfb_callback_url_wow" size="100" value="<?php echo esc_url(get_site_url().'/?action=fbinteractionwow'); ?>" readonly />
							<i><?php echo esc_html__('Please copy the url and add it to the Callback URL field in Webhooks section in Facebook App.', 'wpfb'); ?></i>
						</td>
					</tr>
					<?php endif; ?>
					
					<tr valign="top">
						<th scope="row"><?php echo esc_html__('Default Instruction Message', 'wpfb'); ?></th>
						<td>
							
							<input type="text" name="wpfb_default_instruction" size="100" value="<?php echo (get_option('wpfb_default_instruction')!=''?get_option('wpfb_default_instruction'):esc_html__('For main menu type Start and hit enter. Or type anything related to our services.', 'wpfb')); ?>"  />
							
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php echo esc_html__('Default No Match Reply', 'wpfb'); ?></th>
						<td>
							
							<input type="text" name="wpfb_default_no_match" size="100" value="<?php echo (get_option('wpfb_default_no_match')!=''?get_option('wpfb_default_no_match'):esc_html__('Sorry, nothing matched your query. Please select from Start menu below.', 'wpfb')); ?>"  />
							
						</td>
					</tr>
					
					<tr valign="top">
						<th scope="row" class="qcfb_text_color_red"><?php echo esc_html__('Handover Protocol', 'wpfb'); ?></th>
						<td>
							
							
							<p class="qcfb_text_color_red">
							
							<?php
							 
							 
							 $allowed_tags = array(
								'b' => array()
								
							 );
							 echo wp_kses( 'Handover Protocol allows you to respond to customer messages as the page admin - taking over from the ChatBot. Once set up you can take over the conversation from the ChatBot by replying from your Inbox. In order to use Handover Protocol, you have to go to your <b>page settings > Messenger Platform > General Settings</b> and selete the "<b>Response Method</b>" as "<b>Responses are partially automated, with some support by people</b>". After that click on the "<b>Configure</b>" button of App Settings then choose your "<b>Facebook App</b>" as "<b>Responses are partially automated, with some support by people</b>" and choose "<b>Page Inbox</b>" as "<b>Secondary Receiver for Handover Protocol</b>". That\'s it.', $allowed_tags ); ?>
							
							
							</p>
							<br>
							<span><img class="qcfb_example_image_1" src="<?php echo esc_url(WBFB_URL.'assets/images/messenger_handover1.jpg'); ?>" />
							<img class="qcfb_example_image_2" src="<?php echo esc_url(WBFB_URL.'assets/images/messenger_handover2.jpg'); ?>" />
							</span>
						</td>
					</tr>
					
					<tr valign="top">
						<th scope="row"><?php echo esc_html__('Command for customer service with live agents', 'wpfb'); ?></th>
						<td>
							
							<input type="text" name="wpfb_command_live_agent" size="100" value="<?php echo (get_option('wpfb_command_live_agent')!=''?get_option('wpfb_command_live_agent'):' Livechat'); ?>"  />
							
							<p class="qcfb_text_color_red">
							<?php
							 
							 
							 $allowed_tags = array(
								'b' => array()
								
							 );
							 echo wp_kses( 'When user type the command "<b>Livechat</b>" the bot will pass the thread control to page inbox so admin can reply to the users message. When page admin done with the conversation then page admin need to "Mark as Done" the conversation in order to take the thread control back to the bot.', $allowed_tags ); ?>
							
							</p>
						</td>
					</tr>
					
					<tr valign="top">
						<th scope="row"><?php echo esc_html__('You are contacting admin of this page.', 'wpfb'); ?></th>
						<td>
							
							<input type="text" name="wpfb_contact_admin_text" size="100" value="<?php echo (get_option('wpfb_contact_admin_text')!=''?get_option('wpfb_contact_admin_text'):esc_html__('You are contacting admin of this page. Please type your question below.', 'wpfb')); ?>"  />
							
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php echo esc_html__('Remove Image URL from Reply', 'wpfb'); ?></th>
						<td>
							<input type="checkbox" name="wpfb_remove_image" value="on" <?php echo (esc_attr( get_option('wpfb_remove_image') )=='on'?'checked="checked"':''); ?> />
							<i><?php echo esc_html__('Turn ON to remove image url from reply.', 'wpfb'); ?></i>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php echo esc_html__('Remove youtube URL from Reply', 'wpfb'); ?></th>
						<td>
							<input type="checkbox" name="wpfb_remove_video" value="on" <?php echo (esc_attr( get_option('wpfb_remove_video') )=='on'?'checked="checked"':''); ?> />
							<i><?php echo esc_html__('Turn ON to remove youtube url from reply.', 'wpfb'); ?></i>
						</td>
					</tr>

				</table>
			</div>
			
			
			

			
			<?php submit_button(); ?>

		</form>
		
	</div>

	<?php
	}
	
	public function qcld_license_callback(){
		wp_enqueue_style('qcfb-wp-chatbot-license-style', WBFB_URL . '/assets/css/style.css');
		wp_enqueue_script('qcfb-wp-chatbot-license-js', WBFB_URL . '/assets/js/license.js', array('jquery'));
           
		?>
		<div id="licensing">
				<h1><?php echo esc_html__('Please Insert your license Key', 'wpfb'); ?></h1>
				<?php if( get_messengerchatbot_valid_license() ){ ?>
					<div class="qcld-success-notice">
						<p><?php echo esc_html__('Thank you, Your License is active', 'wpfb'); ?></p>
					</div>
				<?php } ?>
				
				<?php
				
					$track_domain_request = wp_remote_get(messengerchatbot_LICENSING_PRODUCT_DEV_URL."wp-json/qc-domain-tracker/v1/getdomain/?license_key=".get_messengerchatbot_licensing_key());
					if( !is_wp_error( $track_domain_request ) || wp_remote_retrieve_response_code( $track_domain_request ) === 200 ){
						$track_domain_result = json_decode($track_domain_request['body']);
						
						$max_domain_num = $track_domain_result[0]->max_domain + 1;
						$total_domains = @json_decode($track_domain_result[0]->domain, true);
						if(!empty($total_domains)){
						$total_domains_num = count($total_domains);

						if( $max_domain_num <= $total_domains_num){
					?>
							<div class="qcld-error-notice">
								<p>
								<?php
								 
								 
								 $allowed_tags = array(
									'a' => array(
										'href' => array(),
										'title' => array()
									)
								 );
								 echo wp_kses( 'You have activated this key for maximum number of sites allowed by your license. Please <a href="https://www.quantumcloud.com/products/">purchase additional license.</a>', $allowed_tags ); ?>

								</p>
							</div>
					<?php
						}
						}
						
					}
				?>
				
				<form onsubmit="return false" id="qc-license-form" method="post" action="options.php">
					<?php
						delete_messengerchatbot_update_transient();
						delete_messengerchatbot_renew_transient();
						
						delete_option('_site_transient_update_plugins');
						settings_fields( 'qcld_messengerchatbot_license' );
						do_settings_sections( 'qcld_messengerchatbot_license' );

					?>
					<table class="form-table">
						

						<tr id="quantumcloud_portfolio_license_row" class="qcfb_display_none">
							<th>
								<label for="qcld_messengerchatbot_enter_license_key"><?php echo esc_html__('Enter License Key:', 'wpfb'); ?></label>
							</th>
							<td>
								<input type="<?php echo (get_messengerchatbot_licensing_key()!=''?'password':'text'); ?>" id="qcld_messengerchatbot_enter_license_key" name="qcld_messengerchatbot_enter_license_key" class="regular-text" value="<?php echo get_messengerchatbot_licensing_key(); ?>">
								<p>
								<?php

								 $allowed_tags = array(
									'a' => array(
										'href' => array(),
										'title' => array()
									)
								 );
								 echo wp_kses( 'You can copy the license key from <a target="_blank" href="https://www.quantumcloud.com/products/account/">your account</a>', $allowed_tags ); ?>
								
								
								</p>
							</td>
						</tr>

						<tr id="show_envato_plugin_downloader" class="qcfb_display_none">
							<th>
								<label for="qcld_messengerchatbot_enter_envato_key"><?php echo esc_html__('Enter Purchase Code:', 'wpfb'); ?></label>
							</th>
							<td colspan="4">
								<input type="<?php echo (get_messengerchatbot_envato_key()!=''?'password':'text'); ?>" id="qcld_messengerchatbot_enter_envato_key" name="qcld_messengerchatbot_enter_envato_key" class="regular-text" value="<?php echo get_messengerchatbot_envato_key(); ?>">
								<p>
								<?php

								 $allowed_tags = array(
									'a' => array(
										'href' => array(),
										'title' => array()
									)
								 );
								 echo wp_kses( 'You can install the <a target="_blank" href="https://envato.com/market-plugin/">Envato Plugin</a> to stay up to date.', $allowed_tags ); ?>
								
								</p>
							</td>
						</tr>
						
						<tr>
							<th>
								<label for="qcld_messengerchatbot_enter_license_or_purchase_key"><?php echo esc_html__('Enter License Key or Purchase Code:', 'wpfb'); ?></label>
							</th>
							<td>
								<input type="<?php echo (get_messengerchatbot_license_purchase_code()!=''?'password':'text'); ?>" id="qcld_messengerchatbot_enter_license_or_purchase_key" name="qcld_messengerchatbot_enter_license_or_purchase_key" class="regular-text" value="<?php echo get_messengerchatbot_license_purchase_code(); ?>" required>
							</td>
						</tr>

					</table>
					<!-- //start new-update-for-codecanyon -->
					<input type="hidden" name="qcld_messengerchatbot_buy_from_where" value="<?php echo get_messengerchatbot_licensing_buy_from(); ?>" >
					<!-- //end new-update-for-codecanyon -->
					<?php submit_button(); ?>
				</form>
				
			</div>
		<?php 
		
	}
	
}
new wbfb_Admin_Area_Controller();