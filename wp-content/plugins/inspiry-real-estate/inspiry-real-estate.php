<?php
/**
 * Plugin Name:       Inspiry Real Estate
 * Plugin URI:        http://themeforest.net/item/real-places-responsive-wordpress-real-estate-theme/12579089
 * Description:       Inspiry real estate plugin provides plugin territory functionality for Real Places theme.
 * Version:           1.4.2
 * Author:            Inspiry Themes
 * Author URI:        https://themeforest.net/user/inspirythemes/portfolio
 * Text Domain:       inspiry-real-estate
 * Domain Path:       /languages
 */

//Enqueue Styles
function enqueue_stylesheet_links_to_head() {
	
	$plugin_url = plugin_dir_url( __FILE__ );
 
    wp_enqueue_style( 'style-profile',  esc_url( IRE_PLUGIN_URL ) . "public/css/profile.css");
	wp_enqueue_style( 'style-bitsclan',  esc_url( IRE_PLUGIN_URL ) . "public/css/bitsclan.css");
	wp_enqueue_style( 'style-main',  esc_url( IRE_PLUGIN_URL ) . "public/css/main.css");
	
	wp_enqueue_style( 'style-font-awesome',  esc_url( IRE_PLUGIN_URL ) . "public/css/font-awesome.min.css");
	
	
	wp_register_style( 'jquery-ui', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' );
    wp_enqueue_style( 'jquery-ui' );
	
}
 
add_action( 'wp_enqueue_scripts', 'enqueue_stylesheet_links_to_head' );


function bn_disable_google_map_js_on_profile_plugin() {
	// edit profile
	
            if (is_user_logged_in()) {
				
				 wp_dequeue_script('properties-google-map');
			     wp_deregister_script('properties-google-map');
				  wp_dequeue_script('property-google-map');
			     wp_deregister_script('property-google-map');
				 
				 
		
                   

                    // Google Map API
                    wp_dequeue_script('google-map-api');
					wp_deregister_script('google-map-api');
                
            } else {
                console('is_page_template is false', false);
            }

}
add_action('wp_print_scripts', 'bn_disable_google_map_js_on_profile_plugin',  100);







// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Plugin basename
if ( ! defined( 'INSPIRY_REAL_ESTATE_PLUGIN_BASENAME' ) ) {
	define( 'INSPIRY_REAL_ESTATE_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
}

// Plugin directory path
if ( ! defined( 'IRE_PLUGIN_DIR' ) ) {
	define( 'IRE_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

// Plugin directory path
if ( ! defined( 'IRE_PLUGIN_URL' ) ) {
	define( 'IRE_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}


/**
 * Redux framework
 */
require_once( IRE_PLUGIN_DIR . 'includes/redux-framework/inspiry-redux.php' );


/**
 * Functions
 */
require_once( IRE_PLUGIN_DIR . 'includes/functions/basic.php' );
require_once( IRE_PLUGIN_DIR . 'includes/functions/real-estate.php' );
require_once( IRE_PLUGIN_DIR . 'includes/functions/contact.php' );
require_once( IRE_PLUGIN_DIR . 'includes/functions/members.php' );

require_once( IRE_PLUGIN_DIR . 'includes/functions/profile.php' );

/**
 * Meta Boxes
 */
include_once( IRE_PLUGIN_DIR . 'includes/meta-boxes/load-meta-boxes.php' );
include_once( IRE_PLUGIN_DIR . 'includes/meta-boxes/contact.php' );
include_once( IRE_PLUGIN_DIR . 'includes/meta-boxes/properties-pages.php' );
include_once( IRE_PLUGIN_DIR . 'includes/meta-boxes/others.php' );


/**
 * Dynamic Sidebars
 */
include_once( IRE_PLUGIN_DIR . 'includes/sidebars/inspiry-sidebars.php' );


/**
 * Widgets
 */
include_once( IRE_PLUGIN_DIR . 'includes/widgets/advance-search-widget.php' );
include_once( IRE_PLUGIN_DIR . 'includes/widgets/featured-properties-widget.php' );
include_once( IRE_PLUGIN_DIR . 'includes/widgets/social-icons-widget.php' );


/**
 * Forms
 */
include_once( IRE_PLUGIN_DIR . 'includes/forms/members.php' );
include_once( IRE_PLUGIN_DIR . 'includes/forms/contact.php' );
include_once( IRE_PLUGIN_DIR . 'includes/forms/property.php' );
include_once( IRE_PLUGIN_DIR . 'includes/forms/search.php' );
include_once( IRE_PLUGIN_DIR . 'includes/forms/profile.php' );
include_once( IRE_PLUGIN_DIR . 'includes/forms/agent.php' );
include_once( IRE_PLUGIN_DIR . 'includes/forms/currency.php' );


/**
 * Social Navigations
 */
include_once( IRE_PLUGIN_DIR . 'includes/social-navs.php' );

/**
 * The core plugin class
 */
require IRE_PLUGIN_DIR . 'includes/class-inspiry-real-estate.php';
$inspiry_real_estate = Inspiry_Real_Estate::get_instance();
$inspiry_real_estate->run();