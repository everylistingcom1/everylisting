//
// $('#element').donetyping(callback[, timeout=1000])
// Fires callback when a user has finished typing. This is determined by the time elapsed
// since the last keystroke and timeout parameter or the blur event--whichever comes first.
//   @callback: function to be called when even triggers
//   @timeout:  (default=1000) timeout, in ms, to to wait before triggering event if not
//              caused by blur.
// Requires jQuery 1.7+
//
;(function($){
    $.fn.extend({
        donetyping: function(callback,timeout){
            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                doneTyping = function(el){
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function(i,el){
                var $el = $(el);
                // Chrome Fix (Use keyup over keypress to detect backspace)
                // thank you @palerdot
                $el.is(':input') && $el.on('keyup keypress paste',function(e){
                    // This catches the backspace button in chrome, but also prevents
                    // the event from triggering too preemptively. Without this line,
                    // using tab/shift+tab will make the focused element fire the callback.
                    if (e.type=='keyup' && e.keyCode!=8) return;
                    
                    // Check if timeout has been set. If it has, "reset" the clock and
                    // start over again.
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function(){
                        // if we made it here, our timeout has elapsed. Fire the
                        // callback
                        doneTyping(el);
                    }, timeout);
                }).on('blur',function(){
                    // If we can, fire the event since we're leaving the field
                    doneTyping(el);
                });
            });
        }
    });
})(jQuery);




jQuery(document).ready(function($){
	
$("#btnAgentView").fadeTo(100, 0.4);
$("#agent_Save").fadeTo(100, 0.4);



$("#callcenter_tab,#Country,#State,#City,#Sector").on( "click", function(e) {

	var jurisdisction_type = "Country";

	var jurisdisction_value = "";


	getJurisdisction(jurisdisction_type, jurisdisction_value);

	 $('.jurisdisction').change(function(){ 
		
		jurisdisction_type = $(this).attr('child');

		jurisdisction_value = $(this).val();
		//$( "#myselect option:selected" ).text();


		getJurisdisction(jurisdisction_type, jurisdisction_value);

	}); // change call 


	function getJurisdisction(jurisdisction_type, jurisdisction_value){


	//alert(jurisdisction_type);

	var param = {

	'action': 'get_jurisdisction',

	'jurisdisction_type': jurisdisction_type,

	'jurisdisction_value': jurisdisction_value

	};

	$.ajax({

	//contentType: "application/json; charset=utf-8",

	//dataType: "text",

	url : '/wp-admin/admin-ajax.php',

	data : param,

	type : 'POST',

	beforeSend : function ( xhr )  {
		$('#loading').addClass('loading');
		//$('#'+jurisdisction_type).html("<option value='0'> loading ... </option>");
		$('#'+jurisdisction_type+" option:first").val('loading ... ');
		$('#select2-'+jurisdisction_type+"-container").html('loading ... ');
		
		//alert($('#'+jurisdisction_type+" option:first").val());
		
		
		},

	success: function(response) {        

	if ($.trim(response).length != 0) {
	   
	$('#'+jurisdisction_type).html( response );

	$('#loading').removeClass('loading');
	
	$('#select2-'+jurisdisction_type+"-container").html(' Select '+jurisdisction_type+' ');

	$('#btnAgentView').removeAttr('disabled');
	$("#btnAgentView").fadeTo(500, 1);
		if(jurisdisction_type=='State'){
		$('#City').removeAttr('disabled');
		$('#Sector').removeAttr('disabled');
	}
		if(jurisdisction_type=='City'){ 
		$('#Sector').removeAttr('disabled'); 
	}
	

	} else {
	//$('#'+jurisdisction_type).html("<option value='0'> No data found </option>");
	//$('#'+jurisdisction_type).val('');
	$('#'+jurisdisction_type+" option:first").val('No result found');
	$('#select2-'+jurisdisction_type+"-container").html('No result found');
	$('#select2-'+jurisdisction_type+"-container").html(' No '+jurisdisction_type+' found ');
	$('.loading').html( 'No result found.' );

	if(jurisdisction_type=='State'){
		$('#City').attr('disabled', true); 
		$('#Sector').attr('disabled', true); 
	}
	if(jurisdisction_type=='City'){ 
		$('#Sector').attr('disabled', true); 
	}
	}
	
	},

	error: function (xhr, status, error) {


	console.log("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText);

	}

	}); //ajax


	} // call jurisdisction data


}); //multi on click 


var delay = (function(){
  var timer = 0;
  return function(callback, ms){
  clearTimeout (timer);
  timer = setTimeout(callback, ms);
 };
})();


//jQuery('#user-name').keyup(function() {
$('#user-name').donetyping(function(){

var username = $(this).val();
var param = {
'action': 'ire_ajax_isUser',
'user-name': username,
'user_profile_nonce': $('#user_profile_nonce').val()
};

//alert(username);
jQuery.ajax({
url : '/wp-admin/admin-ajax.php',
data : param,
dataType: "json",
type : 'POST',
//async: false,
beforeSend : function ( xhr )  { $('#loading').addClass('loading');},
success: function(response) {        
if ( response ) {
	//alert(response);
$("#user_name_msg").html(response.message);
$('#loading').removeClass('loading');
} else {
$('#loading').html( 'No responce.' );
}
}, // success / responce block
error: function (xhr, status, error) {

console.log("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText);
} // error ajx last block
}); // buton  ajax
//}, 1000 ); // delay function
}); // keyup


$('#combobox').on('change', function() {
  //alert( this.value );

  var param = {
'action': 'ire_ajax_getAgent',
'lagentid': this.value
//'user_profile_nonce': $('#user_profile_nonce').val()
};
getPostAgent(param);

});

$('#btnAgentView').on( "click", function() {
//var username = $(this).val();

var jurisdisction_country = $('#Country').val();
var  jurisdisction_state = $('#State').val();
var jurisdisction_city = $('#City').val();
var jurisdisction_sector = $('#Sector').val();



	
var param = {
'action': 'ire_ajax_getAgent',
'jurisdisction_country': jurisdisction_country,
'jurisdisction_state': jurisdisction_state,
'jurisdisction_city': jurisdisction_city,
'jurisdisction_sector': jurisdisction_sector
//'user-name': username,
//'user_profile_nonce': $('#user_profile_nonce').val()

};
getPostAgent(param);
}); // close


function getPostAgent(param){

//alert(username);
jQuery.ajax({
url : '/wp-admin/admin-ajax.php',
data : param,
dataType: "json",
type : 'POST',
//async: false,
beforeSend : function ( xhr )  { $('#loading').addClass('loading');},
success: function(response) {        
if ( response ) {
//alert(response.id);
$("#agent_gent_Name").val(response.name);
$("#agent_gent_Email").val(response.email);
$("#agent_gent_Phone").val(response.phone);
$("#agent_gent_Url").val(response.url);
$("#agent_Fallowup").val(response.fallowup);
$("#agent_Whatsapp").val(response.whatsapptxt);
$("#agent_walnk").attr('href', response.whatsapplnk);
$("#agent_Notes").val(response.notes);
$("#aId").val(response.id);
$('#loading').removeClass('loading');
$('#btnAgentView').removeAttr('disabled');
$('#agent_Save').removeAttr('disabled');
$("#agent_Save").fadeTo(500, 1);
} else {
$('#loading').html( 'No responce.' );
}
}, // success / responce block
error: function (xhr, status, error) {
$('#loading').html( 'Technical error occured' );

console.log("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText);
} // error ajx last block
}); // buton  ajax
} // close function




$('#agent_Save').on( "click", function() {


jQuery.ajax({
url : '/wp-admin/admin-ajax.php',
data : $("#save_agent").serialize(),
dataType: "json",
type : 'POST',
beforeSend : function ( xhr )  { $('#loading').addClass('loading');},
success: function(response) {        
if ( response ) {
//alert(response);
$('#loading').removeClass('loading');

} else {
$('#loading').html( 'No responce.' );
}
}, // success / responce block
error: function (xhr, status, error) {
$('#loading').html( 'Technical error occured' );
console.log("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText);
} // error ajx last block
}); // buton  ajax
}); // ooclick
}); 


 $( function() {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
 
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            classes: {
              "ui-tooltip": "ui-state-highlight"
            }
          });
 
        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },
 
          autocompletechange: "_removeIfInvalid"
        });
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Show All Items" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .on( "mousedown", function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .on( "click", function() {
            input.trigger( "focus" );
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", value + " didn't match any item" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.autocomplete( "instance" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
 
    $( "#combobox" ).combobox();

  } );