<?php
/**
 * Include redux framework
 */
if ( ! class_exists( 'ReduxFramework' ) && file_exists( plugin_dir_path( __FILE__ ) . '/ReduxCore/framework.php' ) ) {
    require_once( plugin_dir_path( __FILE__ ) . '/ReduxCore/framework.php' );
}


/**
 *  Remove redux submenu
 */
function ire_remove_redux_submenu() {
    remove_submenu_page('tools.php','redux-about');
}
add_action( 'admin_menu', 'ire_remove_redux_submenu', 12 );

