<style>
#avatar_upload #submit, #avatar_upload button {
	width: unset;
	padding: 5px 12px;
	border: none;
	background-color: #434343;
}
#avatar_upload #submit {
    float: right;
    background-color: #f44336;
}
</style>
<form id="inspiry-edit-user" class="submit-form" enctype="multipart/form-data" method="post">
	<?php $site = site_url('/', 'https');
	echo (((in_array('demo-user', (array)$current_user->roles)) && (!startsWith($site, "https://everylisting.localhost/")) && (!startsWith($site, "https://localhost/")))?'<fieldset disabled="disabled">':'<fieldset>'); ?>
    	<div class="genCont">
    		<div class="d-flex">
    			<div class="proDetCont">
    				<div class="proDetForm">
    					<div class="row">
    						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    							<div class="card">
    								<div class="card-body">
    									<div id="avatar_upload" class="fieldCont">
                                    		<?php echo do_shortcode("[avatar_upload]"); ?>
    									</div>
    								</div>
    							</div>
    						</div>
    						<div class="row proDetCont">
    							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    								<div class="fieldCont row">
    									<label class="lblbold">Biographical Information</label>
    									<textarea></textarea>
    								</div>
    							</div>
    						</div>
    					</div>
    					<div>
    						<div class="proDetForm">
    							<div class="row">
    								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    									<div class="fieldCont">
    										<label for="first-name"><?php esc_html_e( 'First Name', 'inspiry-real-estate' ); ?></label>
    										<input class="valid required" name="first-name" type="text"
    											id="first-name"
    											value="<?php
            if (isset($current_user_meta['first_name'])) {
                echo esc_attr($current_user_meta['first_name'][0]);
            }
            ?>"
    											title="<?php esc_html_e( '* Provide First Name!', 'inspiry-real-estate' ); ?>"
    											autofocus
    										/>
    									</div>
    								</div>
    								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    									<div class="fieldCont">
    										<label for="last-name"><?php esc_html_e( 'Last Name', 'inspiry-real-estate' ); ?></label>
    										<input class="required" name="last-name" type="text"
    											id="last-name"
    											value="<?php
            if (isset($current_user_meta['last_name'])) {
                echo esc_attr($current_user_meta['last_name'][0]);
            }
            ?>"
    											title="<?php esc_html_e( '* Provide Last Name!', 'inspiry-real-estate' ); ?>"
    										/>
    									</div>
    								</div>
    								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    									<div class="fieldCont">
    										<label for="display-name"><?php esc_html_e( 'User Name', 'inspiry-real-estate' ); ?>
                  								<?php echo (((in_array('demo-user', (array)$current_user->roles)) && (!startsWith($site, "https://everylisting.localhost/")) && (!startsWith($site, "https://localhost/")))?'':'*'); ?></label> <input class="required" name="user-name"
    											type="text" id="user-name"
    											value="<?php echo esc_attr( $current_user->user_login ); ?>"
    											title="<?php esc_html_e( '* Provide User Name!', 'inspiry-real-estate' ); ?>"
    											<?php echo (in_array('demo-user', (array)$current_user->roles)?' readonly ':' required'); ?>
    										/>
    										<div>
    											<span id="loading"> </span><span id="user_name_msg"></span>
    										</div>
    									</div>
    								</div>
    								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    									<div class="fieldCont">
    										<label for="display-name"><?php esc_html_e( 'Display Name', 'inspiry-real-estate' ); ?>
                  								<?php echo (((in_array('demo-user', (array)$current_user->roles)) && (!startsWith($site, "https://everylisting.localhost/")) && (!startsWith($site, "https://localhost/")))?'':'*'); ?></label> <input class="required"
    											name="display-name" type="text" id="display-name"
    											value="<?php echo esc_attr( $current_user->display_name ); ?>"
    											title="<?php esc_html_e( '* Provide Display Name!', 'inspiry-real-estate' ); ?>"
    											<?php echo (((in_array('demo-user', (array)$current_user->roles)) && (!startsWith($site, "https://everylisting.localhost/")) && (!startsWith($site, "https://localhost/")))?' readonly ':' required'); ?>
    										/>
    									</div>
    								</div>
    								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    									<div class="fieldCont">
    										<label for="email"><?php esc_html_e( 'Email', 'inspiry-real-estate' ); ?>
                  								<?php echo (((in_array('demo-user', (array)$current_user->roles)) && (!startsWith($site, "https://everylisting.localhost/")) && (!startsWith($site, "https://localhost/")))?'':'*'); ?></label> <input class="email required"
    											name="email" type="email" id="email"
    											value="<?php echo esc_attr( $current_user->user_email ); ?>"
    											title="<?php esc_html_e( '* Provide Valid Email Address!', 'inspiry-real-estate' ); ?>"
    											<?php echo (((in_array('demo-user', (array)$current_user->roles)) && (!startsWith($site, "https://everylisting.localhost/")) && (!startsWith($site, "https://localhost/")))?' readonly ':' required'); ?>
    										/>
    									</div>
    								</div>
    								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    									<div class="fieldCont">
    										<label for="mobile-number"><?php esc_html_e( 'Mobile Number', 'inspiry-real-estate' ); ?></label>
    										<input class="digits" name="mobile-number" type="text"
    											id="mobile-number"
    											value="<?php
            if (isset($current_user_meta['mobile_number'])) {
                echo esc_attr($current_user_meta['mobile_number'][0]);
            }
            ?>"
    											title="<?php esc_html_e( '* Only Digits are allowed!', 'inspiry-real-estate' ); ?>" 
    										/>
    									</div>
    								</div>
    								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    									<div class="fieldCont">
    										<label for="office-number"><?php esc_html_e( 'Office Number', 'inspiry-real-estate' ); ?></label>
    										<input class="digits" name="office-number" type="text"
    											id="office-number"
    											value="<?php
            if (isset($current_user_meta['office_number'])) {
                echo esc_attr($current_user_meta['office_number'][0]);
            }
            ?>"
    											title="<?php esc_html_e( '* Only Digits are allowed!', 'inspiry-real-estate' ); ?>" 
    										/>
    									</div>
    								</div>
    			<?php if ((in_array('referee', (array)$current_user->roles)) || (in_array('referrer', (array)$current_user->roles))) { ?>
    								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    									<div class="fieldCont">
    										<label for="facebook-url"><?php esc_html_e( 'Facebook URL', 'inspiry-real-estate' ); ?></label>
    										<div class="smField">
    
    											<input class="url" name="facebook-url" type="text"
    												id="facebook-url"
    												value="<?php
                if (isset($current_user_meta['facebook_url'])) {
                    echo esc_url($current_user_meta['facebook_url'][0]);
                }
                ?>"
    												title="<?php esc_html_e( '* Provide Valid URL!', 'inspiry-real-estate' ); ?>" />
    											<a class="smBtn fbbtn"><img
    												src="../wp-content/plugins/inspiry-real-estate/public/images/fb.svg"
    												width="10" alt="Facebook" /></a>
    										</div>
    									</div>
    								</div>
    								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    									<div class="fieldCont">
    										<label for="twitter-url"><?php esc_html_e( 'Twitter URL', 'inspiry-real-estate' ); ?></label>
    										<div class="smField">
    											<input class="url" name="twitter-url" type="text"
    												id="twitter-url"
    												value="<?php
                if (isset($current_user_meta['twitter_url'])) {
                    echo esc_url($current_user_meta['twitter_url'][0]);
                }
                ?>"
    												title="<?php esc_html_e( '* Provide Valid URL!', 'inspiry-real-estate' ); ?>" />
    											<a class="smBtn twbtn"><img
    												src="../wp-content/plugins/inspiry-real-estate/public/images/twitter.svg"
    												width="22" alt="Facebook" /></a>
    										</div>
    									</div>
    								</div>
    								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    									<div class="fieldCont">
    										<label for="linkedin-url"><?php esc_html_e( 'LinkedIn URL', 'inspiry-real-estate' ); ?></label>
    										<div class="smField">
    											<input class="url" name="linkedin-url" type="text"
    												id="linkedin-url"
    												value="<?php
                if (isset($current_user_meta['linkedin_url'])) {
                    echo esc_url($current_user_meta['linkedin_url'][0]);
                }
                ?>"
    												title="<?php esc_html_e( '* Provide Valid URL!', 'inspiry-real-estate' ); ?>" />
    											<a class="smBtn lnbtn"><img
    												src="../wp-content/plugins/inspiry-real-estate/public/images/linkedin.svg"
    												width="20" alt="Facebook" /></a>
    										</div>
    									</div>
    								</div>
    								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    									<div class="fieldCont">
    										<label for="instagram-url"><?php esc_html_e( 'Instagram URL', 'inspiry-real-estate' ); ?></label>
    										<div class="smField">
    											<input class="url" name="Instagram-url" type="text"
    												id="Instagram-url"
    												value="<?php
                if (isset($current_user_meta['Instagram_url'])) {
                    echo esc_url($current_user_meta['Instagram_url'][0]);
                }
                ?>"
    												title="<?php esc_html_e( '* Provide Valid URL!', 'inspiry-real-estate' ); ?>" />
    											<a class="smBtn instagrambtn"><img
    												src="../wp-content/plugins/inspiry-real-estate/public/images/instagram.svg"
    												width="22" alt="Facebook" /></a>
    										</div>
    									</div>
    								</div>
    						<?php } ?>
    							</div>
    							<div class="d-flex">
    								<div class="form-option">
                                    <?php
                                    // action hook for plugin and extra fields
                                    // do_action( 'edit_user_profile', $current_user );
    
                                    // WordPress Nonce for Security Check
                                    wp_nonce_field('update_user', 'user_profile_nonce');
                                    ?>
                					<input type="hidden" name="action"
    										value="inspiry_update_profile" />
    								</div>
    								<div class="ml-auto">
    									<!--<button class="gradBtn">SUBMIT</button>-->
    									<div class="form-option">
    										<input type="submit" id="update-user" name="update-user" class="btn btn-primary btn-lg" value="<?php esc_html_e( 'SUBMIT', 'inspiry-real-estate' ); ?>">
    										<img
    											src="../wp-content/plugins/inspiry-real-estate/public/images/ajax-loader-2.gif"
    											id="ajax-loader"
    											alt="<?php esc_attr_e( 'Loading...', 'inspiry-real-estate' ); ?>">
    									</div>
    									<p id="form-message"></p>
    									<ul id="form-errors"></ul>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
	</fieldset>
</form>

<?php
 
 // user role checker function_exists
 function check_user_role($roles, $user_id = null) {
	if ($user_id) $user = get_userdata($user_id);
	else $user = wp_get_current_user();
	if (empty($user)) return false;
	foreach ($user->roles as $role) {
		if (in_array($role, $roles)) {
			return true;
		}
	}
	return false;
}


// Registration agent code

	if (isset ($_POST['update-user'])) {
		
	if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL )  && !email_exists ($_POST['email'])) { ?>
	
		<div class="alert alert-danger" style="margin: 20px;">
				<p>The email entered is not Valid !!!</p>
		</div>
	
	<?php	
	} elseif ( email_exists ($_POST['email']) && username_exists($_POST['user-name']) ) {
			$WP_array = array (
			'user_login'    =>  $_POST['user-name'],
			'user_email'    =>  $_POST['email'],
			'first_name'    =>  $_POST['first-name'],
			'last_name'     =>  $_POST['last-name'],
			'nickname'      =>  $_POST['display-name'],
			'description'   =>  $_POST['bio_text'],
			'mobile_number'   =>  $_POST['mobile-number'],
			'office_number'   =>  $_POST['office-number'],
			'facebook_url'   =>  $_POST['facebook-url'],
			'twitter_url'	  =>  $_POST['twitter-url'],
			'instagram_url'   =>  $_POST['Instagram-url'],
			'linkedin_url'   =>  $_POST['linkedin-url'],
		) ;
		
		$current_user = wp_get_current_user();
		
	if ( check_user_role(array('subscriber')) && check_user_role(array( 'agent_-_inactive')) ) {
		
		$user_data = wp_update_user( array( 'ID' => $current_user->ID, 'first_name' => $_POST['first-name'], 'last_name' => $_POST['last-name'], 'mobile_number' => $_POST['mobile-number'], 'office_number' => $_POST['office-number'], 'facebook_url'   =>  $_POST['facebook-url'], 'twitter_url'   =>  $_POST['twitter-url'], 'instagram_url'   =>  $_POST['Instagram-url'], 'linkedin_url'   =>  $_POST['linkedin-url'] ) );
		
	}
		?>
		<script>
			window.location.href = "<?php echo site_url('', 'http'); ?>/edit-profile";
		</script>
		
		<?php
		//wp_update_user( array ('first_name'    =>  $_POST['first-name']) ) ;
/*		$id = wp_insert_user( $WP_array ) ;
		wp_update_user( array ('ID' => $id, 'role' => 'agent_-_inactive'  ) ) ;
		$fuser = get_userdata($id);
		$fuser->add_role('subscriber');		*/
		?>
		
		<div class="alert alert-success" style="margin: 20px;">
								<p>You have been registered to EveryListing.com.</p>
		</div>
		
	
	<?php
	} else{ ?>
		<div class="alert alert-danger" style="margin: 20px;">
								<p>The email or username already exists !!!</p>
	</div>
	<?php 	}
	}?>

<?php if ((in_array('demo-user', (array)$current_user->roles)) && (!startsWith($site, "https://everylisting.localhost/")) && (!startsWith($site, "https://localhost/"))) { ?>
<script>
	document.getElementById("submit").disabled = true;
	document.getElementById("update-user").disabled = true;
</script>
<?php } ?>
