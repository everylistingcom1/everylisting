<?php
global $inspiry_options, $field_counter;
// number of locations chosen from theme options
$location_select_count = ire_get_locations_number();
$location_select_names = ire_get_location_select_names();
$location_titles = ire_get_location_titles( $location_select_count );

// override select boxes titles based on theme options data
for ( $i = 1; $i <= $location_select_count; $i++  ) {
    $temp_location_title = $inspiry_options[ 'inspiry_search_location_title_' . $i  ];
    if( !empty( $temp_location_title ) ) {
        $location_titles[ $i - 1 ] = $temp_location_title;
    }
}?>


<div class="option-bar property-location">
	<select id="child-location" name="child-location" data-title="<?php echo esc_attr( $location_titles[2] ); ?>" class="search-select" >
											<option value="0">City or Sector (Any)</option>
											<?php 
											$jurisdictions_country__args = array(
												'post_type' => 'jurisdiction',
												'posts_per_page' => - 1,
												'meta_query' => array(
													array(
														'key' => 'IDXGenerator_jurisdiction_type',
														'value' => 'CITY',
														'compare' => '=',
														'type' => 'text'
													)
												)
											);
											$countries_query = new WP_Query(apply_filters('inspiry_home_properties', $jurisdictions_country__args));
											usort($countries_query->posts, 'compare_title');
											while ($countries_query->have_posts()) {
												$countries_query->the_post();
												$value = get_permalink();
												$site = site_url('', 'http');
												$value = str_replace($site, "", $value);
												$site = site_url('', 'https');
												$value = str_replace($site, "", $value);
												$value = str_replace("/jurisdictions/", "", $value);
												if (endsWith($value, "/")) {
													$value = substr($value, 0, strlen($value)-1);
												}
												?>
												<option value="<?php echo $value ?>"><?php echo get_the_title() ?></option>
												<?php
											}
											
											?>
	</select>
</div>
						
				

<?php
// Generate required location select boxes
 /*for( $i=0; $i < $location_select_count; $i++ ) {
    ?>
    <div class="option-bar property-location">
        <select name="<?php echo esc_attr( $location_select_names[$i] ); ?>" id="<?php echo esc_attr( $location_select_names[$i] );  ?>" data-title="<?php echo esc_attr( $location_titles[$i] ); ?>" class="search-select">
            <option value="any"><?php echo  esc_html( $location_titles[$i] ) . ' ' . esc_html__( '(Any)', 'inspiry-real-estate' ); ?></option>
        </select>
    </div>
    <?php

    if( ( $field_counter + $i ) === 2 && ( ( $field_counter + $location_select_count ) !== 3  ) ) {
	    ire_get_template_part( 'includes/forms/search-fields/hidden-fields-separator' );
    }
}*/



/* important action hook - related JS works based on it */
do_action( 'inspiry_after_location_fields' );

?>