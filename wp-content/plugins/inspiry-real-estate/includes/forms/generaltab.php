<?php 
	if ($is_secret_url) {
		$postmeta = get_post_meta($_COOKIE['secret-url-agent-id']);
		?>
			<div class="alert alert-success" role="alert">
				<p><strong>Welcome to EveryListing.com!</strong> <span id="propertiescountnotice">We have so far detected <span id="propertiescount">99</span> of your listings on the web.</span></p><p>Please review your information to ensure your prospect buyers see up-to-date information and can contact you.</p>
			</div>
		<?php
		require_once 'general_surl.php';
	} else {
	    require_once 'general_surl.php';
	}
?>