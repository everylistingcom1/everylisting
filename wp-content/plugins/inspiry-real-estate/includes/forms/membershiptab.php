<div class="col-xs-12 col-sm-12 col-md-12">
	<div class="fieldCont">
		<?php
		// check if customer is not member show this
		$isAlreadyMember = false;
		if (!$isAlreadyMember) {
			?>
			<H3 style="padding-bottom: 10px;">Membership benefits:</H3>
			<ul>
				<li>Includes 20 <i><b>"Promotable Properties"</b></i> per
					month.
				</li>
				<li>Add as many <i><b>"Promotable Properties"</b></i> as you
					wish for an extra $2.00 USD each monthly.
				</li>
				<li>Have automated publicities of your properties posted to your
					social-media.</li>
				<li>Oversee your social-network property search efforts.</li>
			</ul>
			<?php if (!$is_secret_url) { ?>
				<H3 style="padding-bottom: 10px;">Membership payment options:</H3>
				<div class='membership_benefits'>
				
								<div style="width: 126px; height: 37px;">
									<?php
									echo do_shortcode("[wp_paypal button='subscribe' name='Broker-Agent Monthly Membership' a3='99.95' t3='M' src='1' p3='12' button_image='/mediacode/images/Subscribe-paypal.jpg']");
									?>
								</div>
							<h3 style="padding:15px 0px;">Pay $99.95 USD monthly for a 1 year
						commitment membership.</h3>
						
					
							
								<div style="width: 142px; height: 38px;">
									<?php
									echo do_shortcode("[wp_paypal button='buynow' name='Broker-Agent Annual Membership' amount='599.95' button_image='/mediacode/images/Buy-now-paypal.jpg']");
									?>
								</div>
							
							<h3 style="padding:15px 0px;">Pre-pay $599.95 USD for a 1 year
								membership.</h3>
						
				</div>
			<?php } else { ?>
				<div class="col-xs-12 col-sm-12 col-md-6 float-left">
					<div class="alert alert-danger" role="alert">
						<p>You must register to access these features.</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 float-left">
			<?php } ?>
			<a href="/mediacode/downloads/EveryListing Products List.pdf"
				target="_blank"><img src="/mediacode/images/downloadbrochure.jpg" 
				style="width: 200px; height: auto;" class="float-right" /></a>
			<?php if ($is_secret_url) { ?>
				</div>
			<?php } ?>									
		<?php
		}
		?>
	</div>
</div>