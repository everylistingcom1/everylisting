<?php
/**
 * Generates search form

 *

 * @param $inspiry_options
 */

function ire_properties_search_modal($parentdivsuffix) {
    ?>
    <div class="modal fade"
    	id="searchListPreviewModal<?php echo $parentdivsuffix; ?>"
    	tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    	aria-hidden="true" style="z-index: 100000 !important"
    	style="min-width: 80%; min-height: 70%">
    	<div class="modal-dialog" role="document"
    		style="min-width: 80%; height: 100%;">
    		<div class="modal-content" style="min-width: 80%; height: 100%;">
    			<div class="modal-content" style="min-width: 80%; height: 100%;">
    				<div class="modal-header">
    					<h5 class="modal-title" id="exampleModalLabel">Properties List
    						Preview</h5>
    					<button type="button" class="close" data-dismiss="modal"
    						aria-label="Close">
    						<span aria-hidden="true">&times;</span>
    					</button>
    				</div>
    				<div class="modal-body"
    					style="min-width: 80%; height: 100%; display: flex; flex-direction: column; flex-grow: 1;">
    					<strong class="text-center" style="height: auto;"><span id="descriptor<?php echo $parentdivsuffix; ?>"></span></strong>
    					<span id="searcharraydebugstring"></span>
    					<div class="pre-scrollable" style="min-height: 95%; display: flex; flex-direction: column; flex-grow: 1;">
    						<div id="homeProperties" class="property-listing-two">
    							<div class="newContainer"
    								id="aftersearchshow_modal<?php echo $parentdivsuffix; ?>"></div>
    						</div>
    					</div>
    				</div>
    				<div class="modal-footer">
    					<button type="button" class="btn btn-secondary"
    						data-dismiss="modal">Close</button>
    				</div>
    				<span id="json<?php echo $parentdivsuffix; ?>"
    					style="display: none;"></span>
    			</div>
    		</div>
    	</div>
    </div>
    <script>
    $('#searchListPreviewModal<?php echo $parentdivsuffix; ?>').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var parentdivsuffix = button.data('parentdivsuffix');
	  	var modal = $(this)
	  	var json = calculateSearchStateJson(parentdivsuffix);
	  	modal.find('#json<?php echo $parentdivsuffix; ?>').html(json);
	  	modal.find('#descriptor<?php echo $parentdivsuffix; ?>').html(getSearchStateDescriptiveString(parentdivsuffix));
	  	var template = "<div class = 'col-md-12' style='text-align: center;'> \
          <a name='properties_list'> \
              <div id='ajax-load-more' class='ajax-load-more-wrap blue alm-0 alm-loading' data-alm-id='0' data-canonical-url='/news/' data-slug='home' data-post-id='0' data-localized='ajax_load_more_vars'> \
                  <div class='alm-btn-wrap' style='visibility: visible;'> \
                      <button class='alm-load-more-btn more loading' rel='next'>Searching Properties</button> \
                  </div> \
                  <div class='alm-no-results' style='display: none;'>No Property Found</div> \
              </div> \
          </a> </div>";
      	$("#aftersearchshow_modal<?php echo $parentdivsuffix; ?>").html(template);
        jQuery.ajax({
            method:'POST',
            url: '/wp-admin/admin-ajax.php',
            data: {action: 'folder_contents', json: encodeURIComponent($('#json<?php echo $parentdivsuffix; ?>').html()), aftersearchshowid: 'aftersearchshow_modal<?php echo $parentdivsuffix; ?>'},
            success: function(res) {
              	$("#aftersearchshow_modal<?php echo $parentdivsuffix; ?>").html(res);
            }
        });
	});
	</script>
<?php
}

function ire_properties_search_parameters_form($inspiry_options, $parentdivsuffix, $onchange) {
    // Search Fields
    $search_fields = $inspiry_options[ 'inspiry_search_fields' ][ 'enabled' ];
    $search_page   = $inspiry_options[ 'inspiry_search_page' ];     // search page id
    $search_target = (isset($inspiry_options['post_target']))?$inspiry_options['post_target']:'';
    
    $lgcells = 4;
    $mdcells = 6;
    $smcells = 12;
    $xscells = 12;
    $addStyle = " style='padding: 10px; width: 100%;'";
    
    $useOnChangeString = "";
    if (($onchange != null) && ($onchange != "")) {
        $useOnChangeString = $onchange;
        if (!endsWith(trim($useOnChangeString), ";")) {
            $useOnChangeString = trim($useOnChangeString) . ";";
        }
        if (!startsWith($useOnChangeString, " ")) {
            $useOnChangeString = " " . $useOnChangeString;
        }
    }
                
    // If WPML is installed then this function will return translated page id for current language against given page id
    $search_page = ire_wpml_translated_page_id( $search_page );
    
    ?>
	<form class="advance-search-form" action="<?php echo get_permalink( $search_page ); ?>" method="get">
	<style>
	.property-location {
        padding: 7px;
        width: 100%;
    }
    .option-bar {
        width: 100%;
    }
	</style>
	<script>
		var searchFormResetStates = (searchFormResetStates == undefined)?[]:searchFormResetStates;		
		function searchFormReset(parentdivsuffix) {
	        if ((searchFormResetStates != undefined) && (searchFormResetStates[parentdivsuffix] != undefined)) populateSearchWithJson(searchFormResetStates[parentdivsuffix], parentdivsuffix);
	    }
		var featuresArray = (featuresArray == undefined)?[]:featuresArray;		
		function populateSearchWithJson(json, parentdivsuffix) {
			var obj = JSON.parse(json);
			$('#adamdivofsearchbox' + parentdivsuffix).find($('input#keyword_txt' + parentdivsuffix)).val((obj.keyword != undefined)?obj.keyword:'');
			$('#adamdivofsearchbox' + parentdivsuffix).find($('input#property_id_txt' + parentdivsuffix)).val((obj.property_id != undefined)?obj.property_id:'');
			$('#adamdivofsearchbox' + parentdivsuffix).find($('input#min_area' + parentdivsuffix)).val((obj.min_area != undefined)?obj.min_area:'');
			$('#adamdivofsearchbox' + parentdivsuffix).find($('input#max_area' + parentdivsuffix)).val((obj.max_area != undefined)?obj.max_area:'');
			$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_property_type"  + parentdivsuffix + " option:selected")).removeAttr("selected"); if (obj.property_type != undefined) { $('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_property_type" + parentdivsuffix + " option:contains(" + obj.property_type +")")).prop("selected", true); }
			$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_status" + parentdivsuffix + " option:selected")).prop("selected", false); if (obj.property_status != undefined) { $('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_status" + parentdivsuffix + " option:contains(" + obj.property_status +")")).prop("selected", true); }
			$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_bathrooms" + parentdivsuffix + " option:selected")).prop("selected", false); if (obj.baths != undefined) { $('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_bathrooms" + parentdivsuffix + " option:contains(" + obj.baths +")")).prop("selected", true); }
			$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_bedrooms" + parentdivsuffix + " option:selected")).prop("selected", false); if (obj.beds != undefined) { $('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_bedrooms" + parentdivsuffix + " option:contains(" + obj.beds +")")).prop("selected", true); }
			$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_min_price" + parentdivsuffix + " option:selected")).prop("selected", false); if (obj.min_price != undefined) { $('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_min_price" + parentdivsuffix + " option:contains(" + obj.min_price +")")).prop("selected", true); }
			$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_max_price" + parentdivsuffix + " option:selected")).prop("selected", false); if (obj.max_price != undefined) { $('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_max_price" + parentdivsuffix + " option:contains(" + obj.max_price +")")).prop("selected", true); }

			$('#adamdivofsearchbox' + parentdivsuffix).find($("select#Country" + parentdivsuffix)).removeAttr('data-delayed-select');
			$('#adamdivofsearchbox' + parentdivsuffix).find($("select#Country" + parentdivsuffix + " option:selected")).prop("selected", false); 
			if (obj.country != undefined) { 
				if ($('#adamdivofsearchbox' + parentdivsuffix).find($("select#Country" + parentdivsuffix + " option:contains(" + obj.country +")")).length == 0) {
					$('#adamdivofsearchbox' + parentdivsuffix).find($("select#Country" + parentdivsuffix)).attr('data-delayed-select', obj.country);
				} else {
					$('#adamdivofsearchbox' + parentdivsuffix).find($("select#Country" + parentdivsuffix + " option:contains(" + obj.country +")")).prop("selected", true); 
				}
			} else {
				$('#adamdivofsearchbox' + parentdivsuffix).find($("select#Country" + parentdivsuffix + " option[value='0']")).prop("selected", true).change(); 
				$('#adamdivofsearchbox' + parentdivsuffix).find($("select#Country" + parentdivsuffix)).removeAttr('data-delayed-select');
			}
			$('#adamdivofsearchbox' + parentdivsuffix).find($("select#State" + parentdivsuffix)).removeAttr('data-delayed-select');
			$('#adamdivofsearchbox' + parentdivsuffix).find($("select#State" + parentdivsuffix + " option:selected")).prop("selected", false); 
			if (obj.state != undefined) { 
				if ($('#adamdivofsearchbox' + parentdivsuffix).find($("select#State" + parentdivsuffix + " option:contains(" + obj.state +")")).length == 0) {
					$('#adamdivofsearchbox' + parentdivsuffix).find($("select#State" + parentdivsuffix)).attr('data-delayed-select', obj.state);
				} else {
					$('#adamdivofsearchbox' + parentdivsuffix).find($("select#State" + parentdivsuffix + " option:contains(" + obj.state +")")).prop("selected", true); 
				}
			} else {
				$('#adamdivofsearchbox' + parentdivsuffix).find($("select#State" + parentdivsuffix + " option[value='0']")).prop("selected", true).change(); 
				$('#adamdivofsearchbox' + parentdivsuffix).find($("select#State" + parentdivsuffix)).removeAttr('data-delayed-select');
			}
			$('#adamdivofsearchbox' + parentdivsuffix).find($("select#City" + parentdivsuffix)).removeAttr('data-delayed-select');
			$('#adamdivofsearchbox' + parentdivsuffix).find($("select#City" + parentdivsuffix + " option:selected")).prop("selected", false); 
			if (obj.city != undefined) { 
				if ($('#adamdivofsearchbox' + parentdivsuffix).find($("select#City" + parentdivsuffix + " option:contains(" + obj.city +")")).length == 0) {
					$('#adamdivofsearchbox' + parentdivsuffix).find($("select#City" + parentdivsuffix)).attr('data-delayed-select', obj.city);
				} else {
					$('#adamdivofsearchbox' + parentdivsuffix).find($("select#City" + parentdivsuffix + " option:contains(" + obj.city +")")).prop("selected", true); 
				}
			} else {
				$('#adamdivofsearchbox' + parentdivsuffix).find($("select#City" + parentdivsuffix + " option[value='0']")).prop("selected", true).change(); 
				$('#adamdivofsearchbox' + parentdivsuffix).find($("select#City" + parentdivsuffix)).removeAttr('data-delayed-select');
			}

			$('#adamdivofsearchbox' + parentdivsuffix).find($('[id^="feature' + parentdivsuffix + '-"]')).filter(':checked').prop("checked", false);
			if (obj.features != undefined) {
				for (var i = 0; i < obj.features.length; i++) {
					var id = $('#adamdivofsearchbox' + parentdivsuffix).find($("label[for^='feature" + parentdivsuffix + "-']:contains('" + obj.features[i] + "')")).prop("htmlFor");
					$('#adamdivofsearchbox' + parentdivsuffix).find($("[id='" + id + "']")).prop("checked", true);
				}
			}
			updateFeatureSearchCount(parentdivsuffix);
		}

		function calculateSearchStateJson(parentdivsuffix, excludeDefaultValues) {
			var features = [];
			$('#adamdivofsearchbox' + parentdivsuffix).find($('[id^="feature' + parentdivsuffix + '-"]')).filter(':checked').each(function(index, element) {
				features.push($('#adamdivofsearchbox' + parentdivsuffix).find($("label[for='" + $(this).attr('id') + "']")).text().trim());
			});
			var obj = {'keyword': (!(excludeDefaultValues && ($('#adamdivofsearchbox' + parentdivsuffix).find($('input#keyword_txt' + parentdivsuffix)).val() == "")))?$('#adamdivofsearchbox' + parentdivsuffix).find($('input#keyword_txt' + parentdivsuffix)).val():undefined, 
					'property_id': $('#adamdivofsearchbox' + parentdivsuffix).find($('input#property_id_txt' + parentdivsuffix)).val(), 
					'property_type': (!$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_property_type" + parentdivsuffix)).find("option:selected").text().includes("(Any)"))?$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_property_type" + parentdivsuffix)).find("option:selected").text().trim():undefined, 
					'property_status': (!$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_status" + parentdivsuffix)).find("option:selected").text().includes("(Any)"))?(!(excludeDefaultValues && ($('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_status" + parentdivsuffix)).find("option:selected").text().trim() == featuresArray['<?php echo $parentdivsuffix;?>']['inspiry_search_fields']['default']['status'])))?$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_status" + parentdivsuffix)).find("option:selected").text().trim():undefined:undefined,  
					'baths': (!$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_bathrooms" + parentdivsuffix)).find("option:selected").text().includes("(Any)"))?$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_bathrooms" + parentdivsuffix)).find("option:selected").text().trim():undefined,  
					'beds': (!$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_bedrooms" + parentdivsuffix)).find("option:selected").text().includes("(Any)"))?$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_bedrooms" + parentdivsuffix)).find("option:selected").text().trim():undefined,  
					'min_area': (!(excludeDefaultValues && ($('#adamdivofsearchbox' + parentdivsuffix).find($('input#min_area' + parentdivsuffix)).val() == "")))?$('#adamdivofsearchbox' + parentdivsuffix).find($('input#min_area' + parentdivsuffix)).val():undefined, 
					'max_area': (!(excludeDefaultValues && ($('#adamdivofsearchbox' + parentdivsuffix).find($('input#max_area' + parentdivsuffix)).val() == "")))?$('#adamdivofsearchbox' + parentdivsuffix).find($('input#max_area' + parentdivsuffix)).val():undefined, 
					'min_price': (!$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_min_price" + parentdivsuffix)).find("option:selected").text().includes("(Any)"))?$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_min_price" + parentdivsuffix)).find("option:selected").text().trim():undefined, 
					'max_price': (!$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_max_price" + parentdivsuffix)).find("option:selected").text().includes("(Any)"))?$('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_max_price" + parentdivsuffix)).find("option:selected").text().trim():undefined,  
					'country': (!$('#adamdivofsearchbox' + parentdivsuffix).find($("select#Country" + parentdivsuffix)).find("option:selected").text().includes("(Any)"))?$('#adamdivofsearchbox' + parentdivsuffix).find($("select#Country" + parentdivsuffix)).find("option:selected").text().trim():undefined,  
					'state': (!$('#adamdivofsearchbox' + parentdivsuffix).find($("select#State" + parentdivsuffix)).find("option:selected").text().includes("(Any)"))?$('#adamdivofsearchbox' + parentdivsuffix).find($("select#State" + parentdivsuffix)).find("option:selected").text().trim():undefined,  
					'city': (!$('#adamdivofsearchbox' + parentdivsuffix).find($("select#City" + parentdivsuffix)).find("option:selected").text().includes("(Any)"))?$('#adamdivofsearchbox' + parentdivsuffix).find($("select#City" + parentdivsuffix)).find("option:selected").text().trim():undefined, 
					'features': (!(excludeDefaultValues && (features.length == 0)))?features:undefined
			};
			var dReturn = JSON.stringify(obj);
			return dReturn.trim();
		}

		function getSearchStateDescriptiveString(parentdivsuffix) {
			var dReturn = "";
			if (($('#adamdivofsearchbox' + parentdivsuffix).find($('input#keyword_txt' + parentdivsuffix)).val() != undefined) && ($('#adamdivofsearchbox' + parentdivsuffix).find($('input#keyword_txt' + parentdivsuffix)).val().trim() != "")) {
				if (dReturn != "") dReturn += ", ";
				dReturn += "keyword '" + $('#adamdivofsearchbox' + parentdivsuffix).find($('input#keyword_txt' + parentdivsuffix)).val().trim() + "'";
			}
			if (($('#adamdivofsearchbox' + parentdivsuffix).find($('input#property_id_txt' + parentdivsuffix)).val() != undefined) && ($('#adamdivofsearchbox' + parentdivsuffix).find($('input#property_id_txt' + parentdivsuffix)).val().trim() != "")) {
				if (dReturn != "") dReturn += ", ";
				dReturn += "property id '" + $('#adamdivofsearchbox' + parentdivsuffix).find($('input#property_id_txt' + parentdivsuffix)).val().trim() + "'";
			}
			if (!$('#adamdivofsearchbox' + parentdivsuffix).find($('select#select_property_type' + parentdivsuffix)).find("option:selected").text().includes("(Any)")) {
				if (dReturn != "") dReturn += ", ";
				dReturn += "property type is '" + $('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_property_type" + parentdivsuffix)).find("option:selected").text().trim() + "'";
			}
			if (!$('#adamdivofsearchbox' + parentdivsuffix).find($('select#select_status' + parentdivsuffix)).find("option:selected").text().includes("(Any)")) {
				if (dReturn != "") dReturn += ", ";
				dReturn += "property status is '" + $('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_status" + parentdivsuffix)).find("option:selected").text().trim() + "'";
			}
			if (!$('#adamdivofsearchbox' + parentdivsuffix).find($('select#select_bedrooms' + parentdivsuffix)).find("option:selected").text().includes("(Any)")) {
				if (dReturn != "") dReturn += ", ";
				dReturn += "at least " + $('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_bedrooms" + parentdivsuffix)).find("option:selected").text().trim() + " bedroom" + (($('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_bedrooms" + parentdivsuffix)).find("option:selected").text() > 1)?"s":"");
			}
			if (!$('#adamdivofsearchbox' + parentdivsuffix).find($('select#select_bathrooms' + parentdivsuffix)).find("option:selected").text().includes("(Any)")) {
				if (dReturn != "") dReturn += ", ";
				dReturn += "at least " + $('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_bathrooms" + parentdivsuffix)).find("option:selected").text().trim() + " bathroom" + (($('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_bathrooms" + parentdivsuffix)).find("option:selected").text() > 1)?"s":"");
			}
			if (($('#adamdivofsearchbox' + parentdivsuffix).find($('input#min_area' + parentdivsuffix)).val() != undefined) && ($('#adamdivofsearchbox' + parentdivsuffix).find($('input#min_area' + parentdivsuffix)).val().trim() != "")) {
				if (dReturn != "") dReturn += ", ";
				dReturn += "min area of " + $('#adamdivofsearchbox' + parentdivsuffix).find($('input#min_area' + parentdivsuffix)).val().trim() + " sq. m.";
			}
			if (($('#adamdivofsearchbox' + parentdivsuffix).find($('input#max_area' + parentdivsuffix)).val() != undefined) && ($('#adamdivofsearchbox' + parentdivsuffix).find($('input#max_area' + parentdivsuffix)).val().trim() != "")) {
				if (dReturn != "") dReturn += ", ";
				dReturn += "max area of " + $('#adamdivofsearchbox' + parentdivsuffix).find($('input#max_area' + parentdivsuffix)).val().trim() + " sq. m.";
			}
			if (!$('#adamdivofsearchbox' + parentdivsuffix).find($('select#select_min_price' + parentdivsuffix)).find("option:selected").text().includes("(Any)")) {
				if (dReturn != "") dReturn += ", ";
				dReturn += "min price of " + $('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_min_price" + parentdivsuffix)).find("option:selected").text().trim() + " USD";
			}
			if (!$('#adamdivofsearchbox' + parentdivsuffix).find($('select#select_max_price' + parentdivsuffix)).find("option:selected").text().includes("(Any)")) {
				if (dReturn != "") dReturn += ", ";
				dReturn += "max price of " + $('#adamdivofsearchbox' + parentdivsuffix).find($("select#select_max_price" + parentdivsuffix)).find("option:selected").text().trim() + " USD";
			}
			if (!$('#adamdivofsearchbox' + parentdivsuffix).find($('select#Country' + parentdivsuffix)).find("option:selected").text().includes("(Any)")) {
				if (dReturn != "") dReturn += ", ";
				dReturn += "in country '" + $('#adamdivofsearchbox' + parentdivsuffix).find($("select#Country" + parentdivsuffix)).find("option:selected").text().trim() + "'";
			}
			if (!$('#adamdivofsearchbox' + parentdivsuffix).find($('select#State' + parentdivsuffix)).find("option:selected").text().includes("(Any)")) {
				if (dReturn != "") dReturn += ", ";
				dReturn += "state/province '" + $('#adamdivofsearchbox' + parentdivsuffix).find($("select#State" + parentdivsuffix)).find("option:selected").text().trim() + "'";
			}
			if (!$('#adamdivofsearchbox' + parentdivsuffix).find($('select#City' + parentdivsuffix)).find("option:selected").text().includes("(Any)")) {
				if (dReturn != "") dReturn += ", ";
				dReturn += "city '" + $('#adamdivofsearchbox' + parentdivsuffix).find($("select#City" + parentdivsuffix)).find("option:selected").text().trim() + "'";
			}
			var features = [];
			$('[id^="feature'  + parentdivsuffix + '-"]').filter(':checked').each(function(index, element) {
				features.push($("label[for='" + $(this).attr('id') + "']").text().trim());
			});
			for (var i = 0; i < features.length; i++) {
				if ((i == 0) && (dReturn != "")) dReturn += ", with ";
				if (i == 0) {
					if (features.length > 1) {
						dReturn += "features "; 
					} else {
						dReturn += "feature "; 
					}
				}
				if ((i > 0) && (i == (features.length - 1))) {
					dReturn += " and ";
				} else if (i > 0) {
					dReturn += ", ";
				}
				dReturn += "'" + features[i] + "'";
			}
			if (dReturn == "") {
				return undefined;
			} else {
				dReturn = dReturn.substring(0, 1).toUpperCase() + dReturn.substring(1);
			}
			return dReturn;
		}
	</script>
	<div id="adamdivofsearchbox<?php echo $parentdivsuffix;?>" class='parent-container d-flex'>
		<div class='container-fluid'>
			<div class='row'>
				<div class='col'>
					<div class='col-xs-<?php echo $xscells; ?> col-sm-<?php echo $smcells; ?> col-md-<?php echo $mdcells; ?> col-lg-<?php echo $lgcells*2; ?>'<?php echo $addStyle; ?> >
						<?php if (($inspiry_options['inspiry_search_fields']['enabled']['keyword']) || ($inspiry_options['inspiry_search_fields']['default']['keyword'])) { ?>
    						<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'<?php echo ((!$inspiry_options['inspiry_search_fields']['enabled']['keyword'])?'style="display: none;"':$addStyle)?>>
    							<?php ire_get_template_part('includes/forms/search-fields/keyword'); ?>
    						</div>
						<?php } ?>
						<?php if (($inspiry_options['inspiry_search_fields']['enabled']['property-id']) || ($inspiry_options['inspiry_search_fields']['default']['property-id'])) { ?>
    						<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'<?php echo ((!$inspiry_options['inspiry_search_fields']['enabled']['property-id'])?'style="display: none;"':$addStyle)?>>
    							<?php ire_get_template_part('includes/forms/search-fields/property-id'); ?>
    						</div>
						<?php } ?>
						<?php if (($inspiry_options['inspiry_search_fields']['enabled']['type']) || ($inspiry_options['inspiry_search_fields']['default']['type'])) { ?>
    						<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'<?php echo ((!$inspiry_options['inspiry_search_fields']['enabled']['type'])?'style="display: none;"':$addStyle)?>>
    							<?php ire_get_template_part('includes/forms/search-fields/property-type'); ?>
    						</div>
						<?php } ?>
						<?php if (($inspiry_options['inspiry_search_fields']['enabled']['status']) || ($inspiry_options['inspiry_search_fields']['default']['status'])) { ?>
    						<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'<?php echo ((!$inspiry_options['inspiry_search_fields']['enabled']['status'])?'style="display: none;"':$addStyle)?>>
    							<?php ire_get_template_part('includes/forms/search-fields/property-status'); ?>
    						</div>
						<?php } ?>
						<?php if (($inspiry_options['inspiry_search_fields']['enabled']['min-baths']) || ($inspiry_options['inspiry_search_fields']['default']['min-baths'])) { ?>
    						<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'<?php echo ((!$inspiry_options['inspiry_search_fields']['enabled']['min-baths'])?'style="display: none;"':$addStyle)?>>
    							<?php ire_get_template_part('includes/forms/search-fields/baths'); ?>
    						</div>
						<?php } ?>
						<?php if (($inspiry_options['inspiry_search_fields']['enabled']['min-beds']) || ($inspiry_options['inspiry_search_fields']['default']['min-beds'])) { ?>
    						<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'<?php echo ((!$inspiry_options['inspiry_search_fields']['enabled']['min-beds'])?'style="display: none;"':$addStyle)?>>
    							<?php ire_get_template_part('includes/forms/search-fields/beds'); ?>
    						</div>
						<?php } ?>
						<?php if ($inspiry_options['inspiry_search_fields']['enabled']['min-max-area']) { ?>
    						<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'<?php echo $addStyle; ?> >
    							<?php ire_get_template_part('includes/forms/search-fields/min-area'); ?>
    						</div>
    						<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'<?php echo $addStyle; ?> >
    							<?php ire_get_template_part('includes/forms/search-fields/max-area'); ?>
    						</div>
						<?php } ?>
						<?php if ($inspiry_options['inspiry_search_fields']['enabled']['min-max-price']) { ?>
    						<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'<?php echo $addStyle; ?> >
    							<?php ire_get_template_part('includes/forms/search-fields/min-price'); ?>
    						</div>
    						<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'<?php echo $addStyle; ?> >
    							<?php ire_get_template_part('includes/forms/search-fields/max-price'); ?>
    						</div>
						<?php } ?>
					</div>
					<?php if ($inspiry_options['inspiry_search_fields']['enabled']['location']) { ?>
    					<div class='col-xs-<?php echo $xscells; ?> col-sm-<?php echo $smcells; ?> col-md-<?php echo $mdcells; ?> col-lg-<?php echo $lgcells; ?>'<?php echo $addStyle; ?> >
    						<div class='container-fluid'>
    							<div class='row'>
    								<div class='col'>
    									<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'<?php echo $addStyle; ?> >
    										<fieldset class='well'>
    											<legend class='well-legend'>Location</legend>
    											<div class='extrapadding'>
    												<?php ire_get_template_part('includes/forms/search-fields/locations'); ?>
    											</div>
    										</fieldset>
    										<h3 class="panel-title" style="margin-top: 15px;">
												<a id="feature_collapse<?php echo $parentdivsuffix;?>" onclick="myCollapseFeaturesFunction('<?php echo $parentdivsuffix;?>')" role="button" class="stretched-link" style="text-decoration: none; position: relative; cursor: pointer;">Features&nbsp;<i id="features_arrow_icon" class="fa fa-angle-right"></i></a>
											</h3>
    									</div>
    								</div>
    							</div>
    						</div>
    					</div>
					<?php } ?>
				</div>
			</div>
			<div id="featureslist<?php echo $parentdivsuffix;?>" style="display: none;">
				<div class="panel-body">
					<?php
					// Property features
					$features_terms = get_terms('property-feature', array(
							'orderby'    => 'name',
							'order'      => 'ASC',
							'hide_empty' => false,
						)
					);
					if (!empty($features_terms) && ! is_wp_error($features_terms)) {
					    ?>
					    <div class="form-option" style="margin: 25px;">
					    <ul class="features-checkboxes-wrapper list-unstyled clearfix">
					    <?php
						foreach ($features_terms as $feature) {
						    if (!startsWith($feature->name,"priv_")) {
								echo '<li><span class="option-set">';
								echo '<input type="checkbox" onchange="updateFeatureSearchCount(\'' . $parentdivsuffix . '\');' . $useOnChangeString .'" name="features[]" id="feature' . $parentdivsuffix . '-' . $feature->term_id . '" value="' . $feature->term_id . '" />';
								echo '<label for="feature' . $parentdivsuffix . '-' . $feature->term_id . '">' . $feature->name . '</label>';
								echo '</li>';
						    }
						}
						?>
					    </ul>
					    </div>
					    <?php
					}
					?>    						
				</div>
			</div>
			<script>
        			function updateFeatureSearchCount(parentdivsuffix) {
        				var x = document.getElementById("featureslist" + parentdivsuffix);
        				var label = "Features (" + $('#featureslist' + parentdivsuffix + ' input[type="checkbox"]').filter(':checked').length + " of " + $('#featureslist' + parentdivsuffix + ' input[type="checkbox"]').length + ")&nbsp;";
            			$('#feature_collapse'+ parentdivsuffix).html(label + ((x.style.display != "block")?"<i id='features_arrow_icon' class='fa fa-angle-right'></i>":"<i id='features_arrow_icon' class='fa fa-angle-down'></i>"));
        			}
    
        			function myCollapseFeaturesFunction(parentdivsuffix) {
    					var x = document.getElementById("featureslist" + parentdivsuffix);
    					if (x.style.display != "block") {
        					x.style.display = "block";
        					$("#features_arrow_icon").removeClass('fa-angle-right').addClass('fa-angle-down');
        				} else {
        					x.style.display = "none";
        					$("#features_arrow_icon").addClass('fa-angle-right').removeClass('fa-angle-down');
        				}
            		}

        			function internalAdaptLocation(realm, parentdivsuffix) {
//             			if (realm === 'COUNTRY') {
//             				searchChildSelect($('#adamdivofsearchbox' + parentdivsuffix).find($("select#Country")).find("option:selected").attr("id"), $('#' + selector).find($("select#State")), true);
//             			} else {
//             				searchChildSelect($('#adamdivofsearchbox' + parentdivsuffix).find($("select#State")).find("option:selected").attr("id"), $('#' + selector).find($("select#City")), true);
//             			}
        			}
    			</script>
		</div>
	</div>
	<script>
    	$(document).ready(function() {
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('input#keyword_txt')).length > 0) $('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('input#keyword_txt')).attr('id','keyword_txt<?php echo $parentdivsuffix;?>');
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('input#property_id_txt')).length > 0) $('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('input#property_id_txt')).attr('id','property_id_txt<?php echo $parentdivsuffix;?>');
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_property_type')).length > 0) $('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_property_type')).attr('id','select_property_type<?php echo $parentdivsuffix;?>');
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_status')).length > 0) $('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_status')).attr('id','select_status<?php echo $parentdivsuffix;?>');
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_bedrooms')).length > 0) $('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_bedrooms')).attr('id','select_bedrooms<?php echo $parentdivsuffix;?>');
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_bathrooms')).length > 0) $('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_bathrooms')).attr('id','select_bathrooms<?php echo $parentdivsuffix;?>');
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('input#min_area')).length > 0) $('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('input#min_area')).attr('id','min_area<?php echo $parentdivsuffix;?>');
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('input#max_area')).length > 0) $('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('input#max_area')).attr('id','max_area<?php echo $parentdivsuffix;?>');
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_min_price')).length > 0) $('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_min_price')).attr('id','select_min_price<?php echo $parentdivsuffix;?>');
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_max_price')).length > 0) $('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_max_price')).attr('id','select_max_price<?php echo $parentdivsuffix;?>');
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_min_price_rent')).length > 0) $('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_min_price_rent')).attr('id','select_min_price_rent<?php echo $parentdivsuffix;?>');
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_max_price_rent')).length > 0) $('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#select_max_price_rent')).attr('id','select_max_price_rent<?php echo $parentdivsuffix;?>');
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#State')).length > 0) $('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#State')).attr('id','State<?php echo $parentdivsuffix;?>');
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#City')).length > 0) $('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#City')).attr('id','City<?php echo $parentdivsuffix;?>');
			if ($('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#Country')).length > 0) {
    			$('#adamdivofsearchbox<?php echo $parentdivsuffix;?>').find($('select#Country')).attr('id','Country<?php echo $parentdivsuffix;?>');
			}
			$('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($('input#keyword_txt<?php echo $parentdivsuffix; ?>')).attr('onkeyup', ($(this).attr('onkeyup') + '<?php echo $useOnChangeString; ?>').replace('null', ''));
			$('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($('input#property_id_txt<?php echo $parentdivsuffix; ?>')).attr('onkeyup', ($(this).attr('onkeyup') + '<?php echo $useOnChangeString; ?>').replace('null', ''));
			$('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#select_property_type<?php echo $parentdivsuffix; ?>")).attr('onchange', ($(this).attr('onchange') + '<?php echo $useOnChangeString; ?>').replace('null', ''));
			$('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#select_status<?php echo $parentdivsuffix; ?>")).attr('onchange', ($(this).attr('onchange') + '<?php echo $useOnChangeString; ?>').replace('null', ''));
			$('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#select_bathrooms<?php echo $parentdivsuffix; ?>")).attr('onchange', ($(this).attr('onchange') + '<?php echo $useOnChangeString; ?>').replace('null', ''));
			$('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#select_bedrooms<?php echo $parentdivsuffix; ?>")).attr('onchange', ($(this).attr('onchange') + '<?php echo $useOnChangeString; ?>').replace('null', ''));
			$('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($('input#min_area<?php echo $parentdivsuffix; ?>')).attr('onkeyup', ($(this).attr('onkeyup') + '<?php echo $useOnChangeString; ?>').replace('null', ''));
			$('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($('input#max_area<?php echo $parentdivsuffix; ?>')).attr('onkeyup', ($(this).attr('onkeyup') + '<?php echo $useOnChangeString; ?>').replace('null', ''));
			$('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#select_min_price<?php echo $parentdivsuffix; ?>")).attr('onchange', ($(this).attr('onchange') + '<?php echo $useOnChangeString; ?>').replace('null', ''));
			$('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#select_max_price<?php echo $parentdivsuffix; ?>")).attr('onchange', ($(this).attr('onchange') + '<?php echo $useOnChangeString; ?>').replace('null', ''));
			$('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#Country<?php echo $parentdivsuffix; ?>")).attr('onchange', ($(this).attr('onchange') + ' internalAdaptLocation("COUNTRY", "<?php echo $parentdivsuffix; ?>");<?php echo $useOnChangeString; ?>').replace('null', ''));
			$('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#State<?php echo $parentdivsuffix; ?>")).attr('onchange', ($(this).attr('onchange') + ' internalAdaptLocation("STATEORPROVINCE", "<?php echo $parentdivsuffix; ?>");<?php echo $useOnChangeString; ?>').replace('null', ''));
			$('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#City<?php echo $parentdivsuffix; ?>")).attr('onchange', ($(this).attr('onchange') + '<?php echo $useOnChangeString; ?>').replace('null', ''));
    		<?php 
		    if (isset($inspiry_options['inspiry_search_fields']['default']['keyword'])) {
		        ?> $('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("input#keyword_txt<?php echo $parentdivsuffix; ?>")).val('<?php echo $inspiry_options['inspiry_search_fields']['default']['keyword']; ?>'); <?php
            }
		    if (isset($inspiry_options['inspiry_search_fields']['default']['property-id'])) {
		        ?> $('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("input#property_id_txt<?php echo $parentdivsuffix; ?>")).val('<?php echo $inspiry_options['inspiry_search_fields']['default']['property-id']; ?>'); <?php
            }
    		if (isset($inspiry_options['inspiry_search_fields']['default']['type'])) { 
    		    ?> $('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#select_property_type<?php echo $parentdivsuffix; ?> option:selected")).prop("selected", false); $('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#select_property_type<?php echo $parentdivsuffix; ?> option:contains('<?php echo $inspiry_options['inspiry_search_fields']['default']['type']; ?>')")).prop("selected", true); <?php 
            } 
            if (isset($inspiry_options['inspiry_search_fields']['default']['status'])) {
                ?> $('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#select_status<?php echo $parentdivsuffix; ?> option:selected")).prop("selected", false); $('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#select_status<?php echo $parentdivsuffix; ?> option:contains('<?php echo $inspiry_options['inspiry_search_fields']['default']['status']; ?>')")).prop("selected", true); <?php
            } 
            if (isset($inspiry_options['inspiry_search_fields']['default']['min-baths'])) {
                ?> $('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#select_bathrooms<?php echo $parentdivsuffix; ?> option:selected")).prop("selected", false); $('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#select_bathrooms<?php echo $parentdivsuffix; ?> option:contains('<?php echo $inspiry_options['inspiry_search_fields']['default']['min-baths']; ?>')")).prop("selected", true); <?php
            } 
            if (isset($inspiry_options['inspiry_search_fields']['default']['min-beds'])) {
                ?> $('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#select_bedrooms<?php echo $parentdivsuffix; ?> option:selected")).prop("selected", false); $('#adamdivofsearchbox<?php echo $parentdivsuffix; ?>').find($("select#select_bedrooms<?php echo $parentdivsuffix; ?> option:contains('<?php echo $inspiry_options['inspiry_search_fields']['default']['min-beds']; ?>')")).prop("selected", true); <?php
            } 
            echo "featuresArray['" . $parentdivsuffix . "'] = " . json_encode($inspiry_options) . ";";
            ?>
    		updateFeatureSearchCount('<?php echo $parentdivsuffix;?>');
			searchFormResetStates['<?php echo $parentdivsuffix;?>'] = calculateSearchStateJson('<?php echo $parentdivsuffix;?>');
       	});
       	
       	function ensureLocationsLoaded(parentdivsuffix, skipvisiblecheck, initiatordebug) {
           	var debug = false;
        	if ($('#adamdivofsearchbox' + parentdivsuffix).length > 0) {
            	if ($('#adamdivofsearchbox' + parentdivsuffix).find($("select#Country" + parentdivsuffix).length > 0)) {
            		if (debug) console.log("searchChildSelect for " + parentdivsuffix + " called.");
					searchChildSelect("all-real-estate-worldwide", $('#adamdivofsearchbox' + parentdivsuffix).find($("select#Country" + parentdivsuffix)), true, initiatordebug);
            	}
	    	} else {
	    		if (debug) console.log("searchChildSelect for " + parentdivsuffix + " not called (select section not found).");
	    	}
        }
    	</script>
	</form>
	<?php
}

function ire_properties_search_form( $inspiry_options ) {
	// Search Fields
	$search_fields = $inspiry_options[ 'inspiry_search_fields' ][ 'enabled' ];
	$search_page   = $inspiry_options[ 'inspiry_search_page' ];     // search page id

	if ( ( 0 < count( $search_fields ) ) && ( ! empty( $search_page ) ) ) {

		// If WPML is installed then this function will return translated page id for current language against given page id
		$search_page = ire_wpml_translated_page_id( $search_page );

		?>
		<form class="advance-search-form" action="<?php echo get_permalink( $search_page ); ?>" method="get">
			<?php
			foreach ( $search_fields as $key => $val ) {

				switch ( $key ) {

					case 'keyword':
						ire_get_template_part( 'includes/forms/search-fields/keyword' );
						break;

					case 'location':
						ire_get_template_part( 'includes/forms/search-fields/locations' );
						break;

					case 'status':
						ire_get_template_part( 'includes/forms/search-fields/property-status' );
						break;

					case 'type':
						ire_get_template_part( 'includes/forms/search-fields/property-type' );
						break;

					case 'min-beds':
						ire_get_template_part( 'includes/forms/search-fields/beds' );
						break;

					case 'min-baths':
						ire_get_template_part( 'includes/forms/search-fields/baths' );
						break;

					case 'min-max-price':
						ire_get_template_part( 'includes/forms/search-fields/min-price' );
						ire_get_template_part( 'includes/forms/search-fields/max-price' );
						break;

					case 'min-max-area':
						ire_get_template_part( 'includes/forms/search-fields/min-area' );
						ire_get_template_part( 'includes/forms/search-fields/max-area' );
						break;

					case 'property-id':
						ire_get_template_part( 'includes/forms/search-fields/property-id' );
						break;

				}
			}

			// Submit Button
			ire_get_template_part( 'includes/forms/search-fields/submit-btn' );

			// Features
			ire_get_template_part( 'includes/forms/search-fields/features' );
			// if ( $inspiry_options[ 'inspiry_search_features' ] ) {
			// 	ire_get_template_part( 'includes/forms/search-fields/features' );
			// }

			// generated sort by field
			if ( isset( $_GET[ 'sortby' ] ) ) {
				echo '<input type="hidden" name="sortby" value="' . $_GET[ 'sortby' ] . '" />';
			}
			?>
		</form><!-- .advance-search-form -->
		<?php
	}
}


/**
 * @param $inspiry_options
 * @param $number_search_fields
 * @param $field_counter
 */
function ire_header_search_form( $inspiry_options, &$number_search_fields, &$field_counter ) {

	// Search Fields
	$search_fields = $inspiry_options[ 'inspiry_search_fields' ][ 'enabled' ];
	$search_page   = "";
	if ( isset( $inspiry_options[ 'inspiry_search_page' ] ) ) {
		$search_page = $inspiry_options[ 'inspiry_search_page' ];
	}

	// A redux fix
	if ( isset( $search_fields[ 'placebo' ] ) ) {
		unset( $search_fields[ 'placebo' ] );
	}

	$number_search_fields = count( $search_fields );

	// increment the number of search fields as min max has two fields
	if ( isset( $search_fields[ 'min-max-price' ] ) ) {
		$number_search_fields ++;
	}

	// increment the number of search fields as min max has two fields
	if ( isset( $search_fields[ 'min-max-area' ] ) ) {
		$number_search_fields ++;
	}

	if ( ( 0 < $number_search_fields ) && ( ! empty( $search_page ) ) ) {

		// If WPML is installed then this function will return translated page id for current language against given page id
		$search_page = ire_wpml_translated_page_id( $search_page );

		?>
		<form class="advance-search-form" action="<?php echo get_permalink( $search_page ); ?>" method="get">
			<div class="inline-fields clearfix">
				<?php

				$field_counter = 0;

				foreach ( $search_fields as $key => $val ) {

					switch ( $key ) {

						case 'keyword':
							ire_get_template_part( 'includes/forms/search-fields/keyword' );
							$field_counter ++;
							break;

						case 'location':
							ire_get_template_part( 'includes/forms/search-fields/locations' );

							// code to display hidden field separator at right place
							$number_of_location_boxes = ire_get_locations_number();
							if ( $number_of_location_boxes > 1 ) {
								$field_counter += $number_of_location_boxes;
							} else {
								$field_counter ++;
							}
							break;

						case 'status':
							ire_get_template_part( 'includes/forms/search-fields/property-status' );
							$field_counter ++;
							break;

						case 'type':
							ire_get_template_part( 'includes/forms/search-fields/property-type' );
							$field_counter ++;
							break;

						case 'min-beds':
							ire_get_template_part( 'includes/forms/search-fields/beds' );
							$field_counter ++;
							break;

						case 'min-baths':
							ire_get_template_part( 'includes/forms/search-fields/baths' );
							$field_counter ++;
							break;

						case 'min-max-price':
							ire_get_template_part( 'includes/forms/search-fields/min-price' );
							$field_counter ++;

							if ( $field_counter == 3 ) {
								ire_get_template_part( 'partials/search/hidden-fields-separator' );
							}

							ire_get_template_part( 'includes/forms/search-fields/max-price' );
							$field_counter ++;
							break;

						case 'min-max-area':
							ire_get_template_part( 'includes/forms/search-fields/min-area' );
							$field_counter ++;

							if ( $field_counter == 3 ) {
								ire_get_template_part( 'partials/search/hidden-fields-separator' );
							}

							ire_get_template_part( 'includes/forms/search-fields/max-area' );
							$field_counter ++;
							break;

						case 'property-id':
							ire_get_template_part( 'includes/forms/search-fields/property-id' );
							$field_counter ++;
							break;

					}

					if ( ( $field_counter == 3 ) || ( $field_counter < 3 && $field_counter == $number_search_fields ) ) {
						ire_get_template_part( 'includes/forms/search-fields/hidden-fields-separator' );
					}
				}

				if ( $inspiry_options[ 'inspiry_search_features' ] ) {
					ire_get_template_part( 'includes/forms/search-fields/features' );
				}

				if ( $field_counter > 3 ) {
				?>
			</div>
			<?php
			}

			// generated sort by field
			if ( isset( $_GET[ 'sortby' ] ) ) {
				echo '<input type="hidden" name="sortby" value="' . $_GET[ 'sortby' ] . '" />';
			}
			?>
		</form>
		<?php
	}

}

