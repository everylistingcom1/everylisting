<?php
mustBeLoggedCheck();

global $inspiry_options;

$criteria_max = 5;
?>

<style>
.disable-thumbnail-link  a {}
.network-select {
    min-height: 20px;
    width: 100%;
}

.network-select.not-selected {
    background-color: coral;
}

.network-select.selected {
    background-color: green;
}

.network-select-button-action {
    margin: 5px;
    color: black;
}

.prop-selection-card-body {
    min-height: 100px;
    min-width: 140px;
    max-height: 100px;
    max-width: 140px;
    margin-left: 5px;
    margin-right: 5px;
    margin-top: 10px;
    margin-bottom: 10px;
}

.zoom {
  transition: transform .2s;
}

.zoom:hover {
  transform: scale(2);
}

.spinner {
     display: none;
}

.spinning-flex {
    display: flex;
}

.spinning .spinner {
    -animation: spin .7s infinite linear;
    -webkit-animation: spin2 .7s infinite linear;
    display: inline-block;
    vertical-align: middle;
}

.spinning-flex .spinner {
    -animation: spin .7s infinite linear;
    -webkit-animation: spin2 .7s infinite linear;
    display: flex;
    vertical-align: middle;
    margin-right: 10px;
}

@-webkit-keyframes spin2 {
    from { -webkit-transform: rotate(0deg);}
    to { -webkit-transform: rotate(360deg);}
}

@keyframes spin {
    from { transform: scale(1) rotate(0deg);}
    to { transform: scale(1) rotate(360deg);}
}

.cursor-pointer {
  cursor: pointer;
  display: inline;
}

</style>

<script>

function removeProperty(event, elem) {
	var postid;
	if (elem.data_post_id != undefined) {
		postid = elem.data_post_id;
		if (elem.original_elem_id != undefined) {
    		var selector = '#' + elem.original_elem_id;
    		elem = $(selector);
    		if (elem.length == 0) {
    			elem = undefined;
    		}
		} else {
			elem = undefined;
		}
	} else {
		postid = elem.parents("[data-post-id]").attr("data-post-id");
	}
	for (var i = 0; i < propertyselection.length; i++) {
		if ((propertyselection[i] != undefined) && (propertyselection[i].data_post_id == postid)) {
			propertyselection[i] = undefined;
			if (elem != undefined) {
    			elem.parent().addClass('not-selected').removeClass('selected');
    			elem.attr("onclick", "addProperty(event, $(this));").text("Add");
			}
			for (var j = (i+1); j < propertyselection.length; j++) {
				propertyselection[j-1] = propertyselection[j];
				manageOnePropertySelection(j-1);
			}
			propertyselection[propertyselection.length - 1] = undefined;
			if (propertyselection[i] == undefined) manageOnePropertySelection(i);
			break;
		}
	}
	validationCycle(false);
	event.preventDefault();
}

function addPropertyWithContent(id, image, link) {
	var i = getSelectedPropertyIndex(true);
	if (i != undefined) {
		propertyselection[i] = {};
		propertyselection[i].original_elem_id = undefined;
		propertyselection[i].data_post_id = id;
		propertyselection[i].data_img_src = image;
		propertyselection[i].data_a_href = link;
		manageOnePropertySelection(i);
		return true;
	}
	return false;
}

function getSelectedPropertyIndex(forceadd) {
	for (var i = 0; i < propertyselection.length; i++) {
		if (propertyselection[i] == undefined) {
			return i;
		}
	}
	if ((forceadd) || ($("#button_is_limited").prop("checked") == true)) {
		propertyselection.push(undefined);
		return (propertyselection.length-1);
	}
	return undefined;
}

function addProperty(event, elem) {
	var dReturn = false;
	var i = getSelectedPropertyIndex(false);
	if (i != undefined) {
		isPropertiesPageUIDirty = true;
		propertyselection[i] = {original_elem_id: elem.attr('id'), data_post_id: elem.parents("[data-post-id]").attr("data-post-id"), data_img_src: elem.attr("data_img_src"), data_a_href: elem.attr("data_a_href")};
		elem.parent().removeClass('not-selected').addClass('selected');
		elem.attr("onclick", "removeProperty(event, $(this));").text("Remove");
		manageOnePropertySelection(i);
		validationCycle(false);
		dReturn = true;
	}
	event.preventDefault();
	return dReturn;
}

function removeListLinks() {
	var src;
	var href;
	var needsmorevalidation = false;
    $(".disable-thumbnail-link a").not(".enabled-link").each(function() {
    	src = $(this).children("img").attr("src");
    	href = $(this).attr("href");
    	$(this).removeAttr("href");
        if ($(this).parents(".disable-thumbnail-link").find(".stripe-container").find(".network-select").length == 0) {
            var id = Math.random().toString(36).substr(2, 9);
            var isInList = false;
        	if (propertyselection != undefined) for (var i = 0; i < propertyselection.length; i++) {
            	if ((propertyselection[i] != undefined) && (propertyselection[i].data_img_src == src)) {
            		if (hasInit) isPropertiesPageUIDirty = true;
            		isInList = true;
            		id = propertyselection[i].original_elem_id;
            		needsmorevalidation = true;
            		break;
            	}
        	}
        	$(this).parents(".disable-thumbnail-link").find(".stripe-container").prepend("<div class='network-select " + ((isInList)?"selected":"not-selected") + "'><button id='" + id + "' class='network-select-button-action enabled-link' data_img_src='" + src + "' data_a_href='" + href + "' onclick='" + ((isInList)?"removeProperty":"addProperty") + "(event, $(this))'>" + ((isInList)?"Remove":"Add") + "</button></div>");
        }
    });
    if (needsmorevalidation) {
    	validationCycle(false);
    }
}

$('#savebutton2').css("visibility", "hidden");

function extractUIValue(content, variable, defaultval) {
	var replace = "\"" + variable + "\":\"([^\"]*)\"";
	res = content.match(replace);
	if ((res != undefined) && (res.length > 1)) {
		if (res[1].startsWith("[")) {
			replace = "\"" + variable + "\":\"\\[([^\\]]*)\\]\"";
			res = content.match(replace);
			res[1] = "[" + res[1].replaceAll("\\\"", '"') + "]";
		}
	}
	return ((res != undefined) && (res.length > 1))?res[1]:defaultval;
}

function manageUISelectorLoad(selector) {
	if ($(document).find(selector).length > 0) {
		var locHttp = new XMLHttpRequest();
		locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/getUserMeta.php?startwith=ui_");
		locHttp.send();
		locHttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
		    	$('#criteriapill').removeClass("spinning-flex");
		    	$('#addcriteria').prop('disabled', false);
		    	for (var i=0; i < $(document).find(selector).length; i++) {
					var elem = $($(document).find(selector).get(i));
					setTimeout(function(elem, responseText) {
    					var signature = elem.prop('tagName');
    					if ((elem.prop('tagName') == "INPUT") && (elem.attr("type") == "radio")) {
    						signature = "RADIO";
    					} else if ((elem.prop('tagName') == "INPUT") && (elem.attr("type") == "checkbox")) {
    						signature = "CHECKBOX";
    					} else if ((elem.prop('tagName') == "INPUT") && (elem.attr("type") == "hidden")) {
    						signature = "HIDDEN";
    					}
    					var value = extractUIValue(responseText, "ui_" + signature + "_" + elem.attr('id'), undefined);
    					if (value != undefined) {
    						if (elem.attr('id') == "networkpropertyselection") {
    							elem.val(value);
    							var jsonproperties = $('#networkpropertyselection').val();
    							if ((jsonproperties != undefined) && (jsonproperties != "[]")) {
    								$('#criteriapill').addClass("spinning-flex");
    								jsonpropertiesobj = JSON.parse(jsonproperties);
    								var locHttp = new XMLHttpRequest();
    			    				locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/multiplePostInfo.php?ids="+ encodeURIComponent(jsonpropertiesobj) + "&post_type=property&filter=thumbnail_image|permalink|id");
    			    				locHttp.send();
    			    				locHttp.onreadystatechange = function() {
    			    					if (this.readyState == 4 && this.status == 200) {
    			    						var data = JSON.parse(this.responseText);						 
    			    						var html = '';
    			    						if (data.result != undefined) {
    			    							data.result.forEach(function(oneres) {
    			        							addPropertyWithContent(oneres.id, oneres.thumbnail_image, oneres.permalink);
    			        						});
    			    						}
    		        						validationCycle(false);
    			    						$('#criteriapill').removeClass("spinning-flex");
    			    					}
    			    				}
    							}
    						} else {
            		    		switch (signature) {
                		    		case "HIDDEN": {
                    		    			var json = value;
                	    					json = json.replaceAll('\\"', "'");
                	    					json = json.replaceAll("\\'", "'");
                	    			        if (json.startsWith("'")) {
                	    			            json = json.substr(1);
                	    			        }
                	    			        if (json.endsWith("'")) {
                	    			            json = json.substr(0, json.length - 1);
                	    			        }
                	    			        json = json.replaceAll("'", '"');
                		    				elem.val(json);
                    		    			<?php for ($criteria_num = 0; $criteria_num < $criteria_max; $criteria_num++) { ?>
                    		    				setTimeout(function(val) {
                        		    				if (elem.attr('id') == 'search_json_criteria_' + val) {
                            		    				if ((json != '') && (json != "false")) {
                            		    					populateSearchWithJson(json, '_criteria_' + val);
                            		    					criteriaUsedArray[val] = val;
                            		    				} else {
                            		    					searchFormReset("_criteria_" + val);
                            		    					criteriaUsedArray[val] = undefined;
                            		    				}
                        		    				}
                    		    				}, 0, <?php echo $criteria_num; ?>);
                    		    			<?php } ?>
                		    			}
                			    		break;
            						case "CHECKBOX":
            		        		case 'RADIO':
            		        			if ((value == true) || (value == "true")) {
            		        				$("#"+ elem.attr('id')).prop("checked", "true");
            		        			}
            		        			break;
            		        		default:
            		        			elem.val(decodeURIComponent(value));
            		    		}
    						}
        		    	};
					}, 0, elem, this.responseText);
				}
		    	isPropertiesPageUIDirty = false;
		    	isCriteriaDirty = false;
		    	validationCycle(false);
		    	hasInit = true;
			}
		}
    }
}

function ensureMyPropertiesLoaded() {
	if (!propertiesLoaded) {
    	manageUISelectorLoad("[onchange*='manageUIOnChange()'], [onkeyup*='manageUIOnKeyUp()']");
    	propertiesLoaded = true;
	}
}

function manageUIOnChange() {
	if (hasInit) isPropertiesPageUIDirty = true;
	validationCycle(false);
}

function searchBoxValidationCycle() {
	if (hasInit) {
    	isCriteriaDirty = true;
    	isPropertiesPageUIDirty = true;
	}
	validationCycle(false);
}

function agentSearchReset() {
	searchFormReset("_agent");
	$('#agentupdatelist').prop('disabled', true);
	$('#agentreset').prop('disabled', true);
	$('#agentupdatelist').removeClass("btn-primary").addClass("btn-secondary");
	$('#agentstablesection').hide();
}

function agentSearchBoxValidationCycle() {
	var json = calculateSearchStateJson('_agent', true);
	if (json != "{}") {
    	$('#agentupdatelist').removeAttr('disabled');
    	$('#agentreset').removeAttr('disabled');
    	$('#agentupdatelist').addClass("btn-primary").removeClass("btn-secondary");
	} else {
		$('#agentupdatelist').prop('disabled', true);
		$('#agentreset').prop('disabled', true);
		$('#agentupdatelist').removeClass("btn-primary").addClass("btn-secondary");
	}
}

function manageUIOnKeyUp() {
	isPropertiesPageUIDirty = true;
	validationCycle(false);
}

function manageUISelectorSave(selector) {
	var url = "";
	$(document).find(selector).each(function() {
		var signature = $(this).prop('tagName');
		if (($(this).prop('tagName') == "INPUT") && ($(this).attr("type") == "radio")) {
			signature = "RADIO";
		} else if (($(this).prop('tagName') == "INPUT") && ($(this).attr("type") == "checkbox")) {
			signature = "CHECKBOX";
		} else if (($(this).prop('tagName') == "INPUT") && ($(this).attr("type") == "hidden")) {
			signature = "HIDDEN";
		}
		switch (signature) {
			case "CHECKBOX":
			case 'RADIO':
				url += "&" + encodeURIComponent("ui_" + signature +"_" + $(this).attr('id')) + "=" + encodeURIComponent($(this).prop("checked"));
				break;
			default:
				url += "&" + encodeURIComponent("ui_" + signature +"_" + $(this).attr('id')) + "=" + encodeURIComponent($(this).val());
		}
	});
	return url;
}

function manageUISave() {
	// If the user has network properties, it needs to have a created agent. If there was none, at this point we make one.
	var locHttp = new XMLHttpRequest();
	var url = "/wp-content/themes/inspiry-real-places/inc/util/synchUserAndAgent.php?propertieslist=" + encodeURIComponent($('#networkpropertyselection').val());
	locHttp.open("GET", url);
	locHttp.send();
	locHttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			$response = this.responseText;
			if (($response != "") && ($response != "false")) {
				$('#agent_id').val($response);
				var locHttp = new XMLHttpRequest();
				var url = "/wp-content/themes/inspiry-real-places/inc/util/saveUserMeta.php?";
				url += manageUISelectorSave("[onchange*='manageUIOnChange()']");
				url += manageUISelectorSave("[onkeyup*='manageUIOnKeyUp()']");
				locHttp.open("GET", url.replace("?&", "?"));
				locHttp.send();
				locHttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						isPropertiesPageUIDirty = false;
						isCriteriaDirty = false;
						alertmsg = "Save operation succeeded";
						alertclass = 1;
						validationCycle(false);
					}
				}
			} else {
				alertmsg = "Save operation failed";
				alertclass = 3;
				validationCycle(false);
			}
		}
	}
}

function validateForNumbers(evt) {
	var theEvent = evt || window.event;
	// Handle paste
	if (theEvent.type === 'paste') {
		key = event.clipboardData.getData('text/plain');
	} else {
		// Handle key press
		var key = theEvent.keyCode || theEvent.which;
		key = String.fromCharCode(key);
	}
	var regex = /[0-9]|\./;
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		if(theEvent.preventDefault) theEvent.preventDefault();
	}
}

function normalizeJsonForQueryParameter(json, useSingleQuote) {
	var jsononentry = json;
	var useReplaceQuote = useSingleQuote?'"':"'";
	var useReplaceWithQuote = !useSingleQuote?'"':"'";
	json = json.replaceAll('\\"', "'");
	json = json.replaceAll("\\'", "'");
    if (json.startsWith("'")) {
        json = json.substr(1);
    }
    if (json.endsWith("'")) {
        json = json.substr(0, json.length - 1);
    }
    json = json.replaceAll(useReplaceQuote, useReplaceWithQuote);
    return json;
}

function validationCycle(debuginconsole) {
	if (debuginconsole) console.log("validationCycle");
	var alertmsg = undefined;
	var alertclass = 0;
	var dReturn = true;
	removeListLinks();
	if (($("#button_is_limited").prop("checked") == true) && (($("#limitation_number").val() == "") || ($("#limitation_number").val() < 20) || ($("#limitation_number").val() > 500))) {
		$("#limitation_number").addClass("is-invalid").removeClass("is-valid");
		$("#limitation_number").prop('disabled', false);
		dReturn = false;
		setPropertySelectionState("danger");
		$(".not-selected").children("button").hide();
	} else if ($("#button_is_limited").prop("checked") == true) {
		$("#limitation_number").removeClass("is-invalid").addClass("is-valid");
		$("#limitation_number").prop('disabled', false);
		if ($("#limitation_number").val() >= 20) {
			while (propertyselection.length > $("#limitation_number").val()) { propertyselection.pop(); }
			while (propertyselection.length < $("#limitation_number").val()) { propertyselection.push(undefined); }
			managePropertySelection();
		}
		if (countSelectedProperties() < 20) {
    		setPropertySelectionState("danger");
    		$(".not-selected").children("button").show();
		} else if (countSelectedProperties() == $("#limitation_number").val()) {
			setPropertySelectionState("success");
			$(".not-selected").children("button").hide();
		} else {
			setPropertySelectionState("warning");
			$(".not-selected").children("button").show();
		}
	} else {
		$("#limitation_number").removeClass("is-invalid").removeClass("is-valid");
		$("#limitation_number").prop('disabled', true);
		if (countSelectedProperties() < 20) {
    		setPropertySelectionState("danger");
    		$(".not-selected").children("button").show();
		} else {
			setPropertySelectionState("success");
			$(".not-selected").children("button").show();
		}
	}
	if (hasInit) {
    	if ((alertmsg == undefined) && isPropertiesPageUIDirty) {
    		if (dReturn) {
        		alertclass = 2;
        		alertmsg = "<p>Do not forget to save your changes.</p>";
    		} else {
        		alertclass = 3;
        		alertmsg = "<p>There is an error in your data to continue.</p>";
    		}
    	}
    	if (alertmsg != undefined) {
    		var alertdiv = "<div class='alert ";
    		switch (alertclass) {
        		case 1:
        			alertdiv += "alert-info"
            		break;
        		case 2:
        			alertdiv += "alert-warning"
            		break;
        		case 3:
        			alertdiv += "alert-danger"
            		break;
    		}
    		alertdiv += "'>" + alertmsg + "</div>";
    		$("#alertmsg").html(alertdiv);
    		$("#alertblock").show();
    		$("#alertmsg").show();
    	} else {
    		$("#alertblock").hide();
    		$("#alertmsg").hide();
    	}
	}
	$("#savebutton").prop('disabled', (!dReturn) || (!isPropertiesPageUIDirty));
	$("#savebutton2").prop('disabled', (!dReturn) || (!isPropertiesPageUIDirty));
	$('#savebutton2').css("visibility", (!isPropertiesPageUIDirty || !hasInit)?"hidden":"visible");
	var hasonevisible = false;
	var lastVisible = undefined;
	<?php for ($criteria_num = 0; $criteria_num < $criteria_max; $criteria_num++) { ?>
		if (debuginconsole) console.log("validationCycle -> criteriaUsedArray[<?php echo $criteria_num; ?>]: " + criteriaUsedArray[<?php echo $criteria_num; ?>] + ", " + hasInit);
		if (criteriaUsedArray[<?php echo $criteria_num; ?>] != undefined) {
			ensureLocationsLoaded('_criteria_<?php echo $criteria_num; ?>', true, 'validationCycle');
			if (hasInit) {
    			hasonevisible = true;
    			$('#searchblock_<?php echo $criteria_num; ?>').css("display", "block");
            	var searchDescription = getSearchStateDescriptiveString('_criteria_<?php echo $criteria_num; ?>');
            	if (searchDescription != undefined) {
            		$('#searchdescriptor_<?php echo $criteria_num; ?>').text(searchDescription);
            	} else {
            		$('#searchdescriptor_<?php echo $criteria_num; ?>').text("");
            	}
            	if (!hasMoreSearchBlock(<?php echo $criteria_num; ?>)) {
            		$('#separator_<?php echo $criteria_num; ?>').hide();
            	} else {
            		$('#separator_<?php echo $criteria_num; ?>').show();
            	}
            	if (countActiveSearchBoxes() != <?php echo $criteria_max ?>) {
            		lastVisible = <?php echo $criteria_num; ?>;
            	}
            	$('#plusicon_<?php echo $criteria_num; ?>').hide();
    			$('#search_json_criteria_<?php echo $criteria_num; ?>').val(normalizeJsonForQueryParameter(calculateSearchStateJson('_criteria_<?php echo $criteria_num; ?>', false), true));
    			updateFeatureSearchCount('_criteria_<?php echo $criteria_num; ?>');
			}
		} else {
			$('#search_json_criteria_<?php echo $criteria_num; ?>').val('');
			$('#searchblock_<?php echo $criteria_num; ?>').css("display", "none");
		}
	<?php } ?>
	$('#search_json_criteria').val(normalizeJsonForQueryParameter(calculateFullSearchJson(), true));
	if (lastVisible != undefined) {
		$('#plusicon_' + lastVisible).show();
	}
	var selarray = [];
	for (var i = 0; i < propertyselection.length; i++) {
		if ((propertyselection[i] != undefined) && (propertyselection[i].data_post_id != undefined)) {
			selarray.push(propertyselection[i].data_post_id);
		}
	}
	$('#networkpropertyselection').val(normalizeJsonForQueryParameter(JSON.stringify(selarray)));
	if (debuginconsole) console.log("validationCycle -> hasonevisible: " + hasonevisible);
	if (hasonevisible) {
		$('#addcriteria').removeClass("btn-primary").addClass("btn-secondary");
		$('#optionswithcriteria').show();
		if (isCriteriaDirty) {
			$('#updatelist').addClass("btn-primary").removeClass("btn-secondary");
		} else {
			$('#updatelist').removeClass("btn-primary").addClass("btn-secondary");
		}
	} else {
		$('#addcriteria').addClass("btn-primary").removeClass("btn-secondary");
		$('#optionswithcriteria').hide();
	}
	$('#nosearchblock').css("display", ((countActiveSearchBoxes() == 0)?"block":"none"));
	managePropertySelection();
	return dReturn;
}

function isOnlySearchBlock(number) {
	for (var i = 0; i < <?php echo $criteria_max; ?>; i++) {
		if ((i != number) && (criteriaUsedArray[i] != undefined)) {
			return false;
		}
	}
	return true;
}

function hasMoreSearchBlock(number) {
	for (var i = number + 1; i < <?php echo $criteria_max; ?>; i++) {
		if (criteriaUsedArray[i] != undefined) {
			return true;
		}
	}
	return false;
}

function nextCriteriaSlotAvailable() {
	for (var i = 0; i < <?php echo $criteria_max; ?>; i++) {
		if ((criteriaUsedArray[i] == undefined) || (criteriaUsedArray[i] == null)) {
    		var available = undefined;
    		for (var j = 0; ((j < <?php echo $criteria_max; ?>) && (available == undefined)); j++) {
        		for (var k = 0; ((k < <?php echo $criteria_max; ?>) && (available == undefined)); k++) {
            		if (criteriaUsedArray[k] === j) {
            			break;
            		}
            		if (k == (<?php echo $criteria_max; ?> - 1)) {
            			available = j;
            		}
        		}
    		}
    		if (available != undefined) {
    			criteriaUsedArray[i] = available;
    			searchFormReset("_criteria_" + available);
    			if (hasInit) isCriteriaDirty = true;
    			validationCycle(true);
    		    // location.href = "#searchblock_" + available;
    			return;
    		}
		}
	}
}

function allSearchesReset() {
	for (var i = 0; i < <?php echo $criteria_max; ?>; i++) {
		searchFormReset("_criteria_" + i);
		criteriaUsedArray[i] = undefined;
	}
	$("#aftersearchshow").html("");
	$('#updatelistbuttonname').text("View Properties List");
	setInfoContent("");
	validationCycle(false);
}

function calculateFullSearchJson() {
	var json = "{}";
	for (var i = 0; i < <?php echo $criteria_max; ?>; i++) {
		if (criteriaUsedArray[i] != undefined) {
			if (json == "{}") {
				json = calculateSearchStateJson("_criteria_" + i, false);
			} else if (json.startsWith("{'multiple': [")) {
				json = json.replace("{'multiple': [", "{'multiple': [" + calculateSearchStateJson("_criteria_" + i, false) + ", ");
			} else {
				json = "{'multiple': [" + calculateSearchStateJson("_criteria_" + i, false) + ", " + json + "]}";
			}
		}
	}
	return json;
}

<?php for ($criteria_num = 0; $criteria_num < $criteria_max; $criteria_num++) { ?>
    $('body').on('DOMSubtreeModified', '#adamdivofsearchbox_criteria_<?php echo $criteria_num; ?> > select#Country_criteria_<?php echo $criteria_num; ?>', function() {
    	var searchDescription = getSearchStateDescriptiveString('_criteria_<?php echo $criteria_num; ?>');
    	if (searchDescription != undefined) {
    		$('#searchdescriptor_<?php echo $criteria_num; ?>').text(searchDescription);
    	} else {
    		$('#searchdescriptor_<?php echo $criteria_num; ?>').text("");
    	}
    });
    $('body').on('DOMSubtreeModified', '#adamdivofsearchbox_criteria_<?php echo $criteria_num; ?> > select#State_criteria_<?php echo $criteria_num; ?>', function() {
    	var searchDescription = getSearchStateDescriptiveString('_criteria_<?php echo $criteria_num; ?>');
    	if (searchDescription != undefined) {
    		$('#searchdescriptor_<?php echo $criteria_num; ?>').text(searchDescription);
    	} else {
    		$('#searchdescriptor_<?php echo $criteria_num; ?>').text("");
    	}
    });
    $('body').on('DOMSubtreeModified', '#adamdivofsearchbox_criteria_<?php echo $criteria_num; ?> > select#City_criteria_<?php echo $criteria_num; ?>', function() {
    	var searchDescription = getSearchStateDescriptiveString('_criteria_<?php echo $criteria_num; ?>');
    	if (searchDescription != undefined) {
    		$('#searchdescriptor_<?php echo $criteria_num; ?>').text(searchDescription);
    	} else {
    		$('#searchdescriptor_<?php echo $criteria_num; ?>').text("");
    	}
    });
<?php } ?>

$('body').on('DOMSubtreeModified', '#aftersearchshow', function() {
	if (($('.alm-load-more-btn').attr('data-showingtotal') != undefined) && ($('.alm-load-more-btn').attr('data-total') != undefined) && ($('.alm-load-more-btn').attr('data-total') > 0)) {
		if ($('.alm-load-more-btn').attr('data-showingtotal') > $('.alm-load-more-btn').attr('data-total')) {
			$('.alm-load-more-btn').attr('data-showingtotal', $('.alm-load-more-btn').attr('data-total'));
		}
		if ($('.alm-load-more-btn').attr('data-showingtotal') == $('.alm-load-more-btn').attr('data-total')) {
			if ($('.alm-load-more-btn').attr('data-total') == 1) {
	    		setInfoContent("Showing the only property found.");
			} else {
	    		setInfoContent("Showing all " + $('.alm-load-more-btn').attr('data-total') + " properties in the list.");
			}
		} else {
    		setInfoContent("Showing " + $('.alm-load-more-btn').attr('data-showingtotal') + " of " + $('.alm-load-more-btn').attr('data-total') + " properties in the list.");
		}
		$('#selectedproperties').show();
	} else {
		$('#selectedproperties').hide();
		setInfoContent("");
	}
	removeListLinks();
	validationCycle(false);
});

function agentUpdateList() {
	var json = calculateSearchStateJson("_agent", false);
	var filter = "&filter=id|permalink|title|IDXGenerator_listing_agents_id";
	var query = "?post_type=property";
	var querycount = 0;
	$('#agentupdatelist').addClass('spinning-flex');
	query += "&json=" + encodeURIComponent(json);
	var locHttp = new XMLHttpRequest();
	$('#agentstablebody tr').remove();
	$('#agentstablesection').hide();
	locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/multiplePostInfo.php"+ query + filter);
	locHttp.send();
	locHttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			$('#agentupdatelist').removeClass('spinning-flex');
			if (this.responseText != "{}") {
				agents = [];
				var propertiesid = [];
				var response = JSON.parse(this.responseText);
				if (response.result != undefined) response.result.forEach(function(elem) {
					propertiesid.push(elem.id);
					if (elem.IDXGenerator_listing_agents_id != undefined) {
						var tokens = elem.IDXGenerator_listing_agents_id.split("|");
						tokens.forEach(function(token) {
    						if ((token.length > 0) && (agents[token] == undefined)) {
    							agents[token] = {};
    							agents[token].matchcount = 1;
    							agents[token].matchedpropertiesid = [];
    						} else if (token.length > 0) {
    							agents[token].matchcount++;
    						}
    						if (token.length > 0) agents[token].matchedpropertiesid.push(elem.id);
						});
					}
				});
				if (agents.length > 0) {
    				query = "?post_type=agent&relation=OR";
    				filter = "&filter=id|IDXGenerator_realEstateBrokerId|IDXGenerator_name|IDXGenerator_phone|IDXGenerator_email|IDXGenerator_firstDetected|IDXGenerator_lastDetected|IDXGenerator_valuation|IDXGenerator_duplicateCount|IDXGenerator_notDuplicateCount|IDXGenerator_urls|CallCenterSecretUrl|CallCenterNotes|CallCenterAssignedAgent|CallCenterAssignedDate|CallCenterCorrectedName|CallCenterCorrectedPhone|CallCenterCorrectedEmail|CallCenterCorrectedWhatsapp|CallCenterLogs|IDXGenerator_language|CallCenter_prefill_profile_image_id|CallCenter_prefill_profile_image_url|CallCenter_prefill_profile_bio|CallCenter_prefill_profile_first_name|CallCenter_prefill_profile_last_name|CallCenter_prefill_profile_user_name|CallCenter_prefill_profile_display_name|CallCenter_prefill_profile_phone|CallCenter_prefill_profile_email|CallCenter_prefill_profile_office_number|CallCenter_prefill_profile_facebook_url|CallCenter_prefill_profile_instagram_url|CallCenter_prefill_profile_linkedin_url|CallCenter_prefill_profile_twitter_url";
    				querycount = 0;
    				for (var key in agents) {
    			    	query += "&meta_key"+ querycount + "=IDXGenerator_listing_agents_id&meta_value"+ querycount + "=" + encodeURIComponent(key);
    			    	querycount++;
    				}
    				var locHttp = new XMLHttpRequest();
    				locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/multiplePostInfo.php"+ query + filter);
    				locHttp.send();
    				$('#agentupdatelist').addClass('spinning-flex');
    				locHttp.onreadystatechange = function() {
    					if (this.readyState == 4 && this.status == 200) {
    						var data = JSON.parse(this.responseText);						 
    						$('#agentstablebody tr').remove();
    						var html = '';
    						if (data.result != undefined) {
    							data.result.forEach(function(oneres) {
            						if (agents[oneres.IDXGenerator_realEstateBrokerId] != undefined) {
            							agents[oneres.IDXGenerator_realEstateBrokerId].data = oneres;
            							agents[oneres.IDXGenerator_realEstateBrokerId].idMatchCount = Math.random().toString(36).substr(2, 9);
            							agents[oneres.IDXGenerator_realEstateBrokerId].idPropertiesCount = Math.random().toString(36).substr(2, 9);
            							html += '<tr><td>' + ((oneres.CallCenterCorrectedName != undefined)?oneres.CallCenterCorrectedName:(oneres.IDXGenerator_name != undefined)?oneres.IDXGenerator_name:'N/A') + '<a data-toggle="modal" data-target="#agentinformationmodal" class="cursor-pointer" onclick="populateAgentModalInfo(' + oneres.IDXGenerator_realEstateBrokerId + ')"><i class="fa fa-info-circle fa-2x pull-right"/></a>' + '</td><td>' 
            								+ ((oneres.CallCenterCorrectedPhone != undefined)?oneres.CallCenterCorrectedPhone:(oneres.IDXGenerator_phone != undefined)?oneres.IDXGenerator_phone:'N/A') + '</td><td>' 
            								+ ((oneres.CallCenterCorrectedEmail != undefined)?oneres.CallCenterCorrectedEmail:(oneres.IDXGenerator_email != undefined)?oneres.IDXGenerator_email:'N/A') + '</td><td><div>' 
            								+ ((agents[oneres.IDXGenerator_realEstateBrokerId].matchcount != undefined)?agents[oneres.IDXGenerator_realEstateBrokerId].matchcount:'N/A') + '<a id=' + agents[oneres.IDXGenerator_realEstateBrokerId].idMatchCount + ' class="cursor-pointer" onclick="populateAgentMatchedProperties(' + oneres.IDXGenerator_realEstateBrokerId + ')"><span class="d-flex flex-nowrap pull-right"><i class="spinner fa fa-spinner fa-lg"></i><i class="fa fa-home fa-2x"/></span></a>' + '</div></td><td>'
            								+ oneres.IDXGenerator_notDuplicateCount + '<a id=' + agents[oneres.IDXGenerator_realEstateBrokerId].idPropertiesCount + ' class="cursor-pointer" onclick="populateAgentProperties(' + oneres.IDXGenerator_realEstateBrokerId + ')"><span class="d-flex flex-nowrap pull-right"><i class="spinner fa fa-spinner fa-lg"></i><i class="fa fa-home fa-2x pull-right"/></span></a>' + '</td>'
            								+ '</tr>';
            						}
        						});
        						$('#agentstablebody').html(html);
        						$('#agentstable').DataTable();
        						$('.dataTables_length').addClass('bs-select');
        						$('#agentstablesection').show();
    						}
    						// setAgentInfoContent(json);
    						$('#agentupdatelist').removeClass('spinning-flex');
    					}
    				}
    				if (propertiesid.length > 0) {
    					jQuery.ajax({
    			              method:'POST',
    			              url: '/wp-admin/admin-ajax.php',
    			              data: {action: 'folder_contents', json:json, addclassthumbnail:'disable-thumbnail-link'},
    			              success: function(res) {
    			              	$("#aftersearchshow").html(res);
    			              }
    			        });
    				}
    			}
    		}
		}
	};
}

function populateAgentProperties(agentid) {
	var id = agents[agentid].idPropertiesCount;
	var json = JSON.stringify({agent_id:agentid});
	$('#' + id).addClass("spinning-flex");
	agents[agentid].idPropertiesCount;
	jQuery.ajax({
          method:'POST',
          url: '/wp-admin/admin-ajax.php',
          data: {action: 'folder_contents', json:json, addclassthumbnail:'disable-thumbnail-link'},
          success: function(res) {
            	$("#aftersearchshow").html(res);
            	$('#' + id).removeClass("spinning-flex");
          }
    });
}

function populateAgentMatchedProperties(agentid) {
	var id = agents[agentid].idMatchCount;
	var propertiesid = agents[agentid].matchedpropertiesid;
	if ((propertiesid != undefined) && (propertiesid.length > 0)) {
		$('#' + id).addClass("spinning-flex");
		var json = JSON.stringify(propertiesid);
		jQuery.ajax({
              method:'POST',
              url: '/wp-admin/admin-ajax.php',
              data: {action: 'folder_contents', properties_id_json:json, addclassthumbnail:'disable-thumbnail-link'},
              success: function(res) {
                	$("#aftersearchshow").html(res);
                	$('#' + id).removeClass("spinning-flex");
              }
        });
	}
}

function populateAgentModalInfo(agentid) {
	var agent = agents[agentid].data;
	$('#agent_wells_Name').text(((agent.CallCenterCorrectedName != undefined)?agent.CallCenterCorrectedName:(agent.IDXGenerator_name != undefined)?agent.IDXGenerator_name:'N/A'));
	$('#agent_wells_Phone').text(((agent.CallCenterCorrectedPhone != undefined)?agent.CallCenterCorrectedPhone:(agent.IDXGenerator_phone != undefined)?agent.IDXGenerator_phone:'N/A'));
	$('#agent_wells_EMail').text(((agent.CallCenterCorrectedEmail != undefined)?agent.CallCenterCorrectedEmail:(agent.IDXGenerator_email != undefined)?agent.IDXGenerator_email:'N/A'));
	$('#agent_wells_FirstDetected').text(agent.IDXGenerator_firstDetected);
	$('#agent_wells_LastDetected').text(agent.IDXGenerator_lastDetected);
	$('#agent_wells_NonDuplicateCount').text(agent.IDXGenerator_notDuplicateCount);
	$('#agent_wells_DuplicateCount').text(agent.IDXGenerator_duplicateCount);
	$('#agent_wells_valuationtext').text(agent.IDXGenerator_valuation);
	$('#agent_wells_urlstext').html("<notextile></notextile><span>" + agent.IDXGenerator_urls + "</span>");
	$('#agent_wells_urlstext').prop("onclick", undefined).off("click"); 
}

function updateList() {
	isCriteriaDirty = false;
	$('#updatelist').addClass('spinning-flex');
	var value = calculateFullSearchJson();
	value = (value != "{}")?value:"";
	if (value != "") {
    	jQuery.ajax({
              method:'POST',
              url: '/wp-admin/admin-ajax.php',
              data: {action: 'folder_contents', json:value, addclassthumbnail:'disable-thumbnail-link'},
              success: function(res) {
            	  $('#updatelist').removeClass('spinning-flex');
            	  	$('#updatelistbuttonname').text("Update Properties List");
                	$("#aftersearchshow").html(res);
              }
        });
	}
}

function countActiveSearchBoxes() {
	var count = 0;
	for (var i = 0; i < <?php echo $criteria_max; ?>; i++) {
		if (criteriaUsedArray[i] != undefined) {
			count++;
		}
	}
	return count;
}

function setAgentInfoContent(text) {
	if ((text != undefined) && (text.length > 0)) {
		$('#agentinfocontent').html('<em>' + text + '</em>');
		$('#agentinfocontent').show();
	} else {
		$('#agentinfocontent').html('');
		$('#agentinfocontent').hide();
	}
}

function setInfoContent(text) {
	if ((text != undefined) && (text.length > 0)) {
		$('#infocontent').html('<em>' + text + '</em>');
		$('#infocontent').show();
	} else {
		$('#infocontent').html('');
		$('#infocontent').hide();
	}
}

function setPropertySelectionState(style) {
	if ((style == "success") || (style == "warning") || (style == "danger")) {
		$('#propertyselectioncontainer').removeClass("bg-success").removeClass("bg-warning").removeClass("bg-danger").addClass("bg-"+style);
	}
}

function getSelectionEmptyCode(i, bodyOnly) {
	if (bodyOnly) {
		return 'Property #' + (i+1);
	} else {
		return '<div id="selcardslot' + i + '" data-index="' + i + '" class="card prop-selection-card-body">' + getSelectionEmptyCode(i, true)  + '</div>';
	}
}

function countSelectedProperties() {
	var dReturn = 0;
	if (propertyselection != undefined) {
		for (var i = 0; i < propertyselection.length; i++) {
	    	if (propertyselection[i] != undefined) {
	    		dReturn++;
	    	}
		}
	}
	return dReturn;
}

function manageOnePropertySelection(i) {
	var emptycode = getSelectionEmptyCode(i, true);
	if ($('#selcardslot' + i).length > 0) {
    	if (propertyselection[i] != undefined) {
     		var filledcode = '<img src="' + propertyselection[i].data_img_src + '" class="img-thumbnail zoom"><a href="#" onclick="removeProperty(event, propertyselection[' + i + ']);"><i style="position: absolute; left: 10px; top: 10px; background: white; padding: 3px; border: 1px solid black;" class="fa fa-trash fa-lg"/></a></img>';
			// console.log("filledcode: " + filledcode);
     		$('#selcardslot' + i).html(filledcode);
    	} else {
    		$('#selcardslot' + i).html(emptycode);
    	}
    }
}

function managePropertySelection() {
	if (propertyselection != undefined) {
		for (var i = 0; i < propertyselection.length; i++) {
			var emptycode = getSelectionEmptyCode(i, false);
			if ($('#propertyselectioncontainer').find('[data-index="' + i + '"]').length == 0) {
				if (i == 0) {
					$('#propertyselectioncontainer').html(emptycode);
				} else if ($('#propertyselectioncontainer > [data-index="' + (i-1) + '"]').length > 0) {
					$('#propertyselectioncontainer > [data-index="' + (i-1) + '"]').after(emptycode);
				}
			}
		}
		var todelete = propertyselection.length;
		while ($('#selcardslot' + todelete).length > 0) {
			$('#selcardslot' + todelete).remove();
			todelete++;
		}
    	for (var i = 0; i < propertyselection.length; i++) {
    		manageOnePropertySelection(i);
    	}
	}
}
var agents = [];
var isPropertiesPageUIDirty = false;
var isCriteriaDirty = false;
var hasInit = false;
var criteriaUsedArray = [];
var propertyselection = new Array(20);
var propertiesLoaded = false;
for (var i = 0; i < <?php echo $criteria_max; ?>; i++) {
	criteriaUsedArray[i] = undefined;
}
</script>

<div id="limitscomponent">
	<form class="container">
		<div class="row">
			<div id="alertblock" class="col-xs-12 col-sm-12 col-md-8 col-lg-10">
				<div id="alertmsg">
					<div class="alert alert-danger" role="alert">
						<p>Please wait while your data sets-up.</p>
					</div>
				</div>
			</div>
			<div id="savetop" class="col-xs-12 col-sm-12 col-md-4 col-lg-2">
				<div class="float-right">
					<button id="savebutton2" type="button"
						class="btn btn-primary btn-lg" onclick="manageUISave();" disabled
						style="visibility: hidden;">Save</button>
				</div>
			</div>
		</div>
		<input type="hidden"
    		id="networkpropertyselection"
    		onchange="manageUIOnChange();"/>
		<div class="row">
			<div class="container">
				<ul class="nav nav-pills tabNav pull-right">
					<li class="nav-item"><a id="criteriapill" class="nav-link active" data-toggle="pill" href="#criteria"><i class="spinner fa fa-spinner fa-lg"></i>By Property Search</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="pill" href="#agent" onclick="ensureLocationsLoaded('_agent', true, 'agentpill');">By Agent Search</a></li>
				</ul>
				<div class="tab-content tabConts"
					style="border-left: 1px solid #778899; border-right: 1px solid #778899; border-bottom: 1px solid #778899; border-top: 0.5px solid #778899">
					<div id="limitblock" class="form-check form-check-inline">
						<input type="checkbox" class="form-check-input align-middle"
							id="button_is_limited" name="button_is_limited"
							onchange="manageUIOnChange();"> <label
							class="custom-control-label align-middle" for="button_is_limited">Limit
							the amount of properties matched to </label> <input
							id="limitation_number" class="form-control input-sm" type="text"
							style="max-height: 24px; max-width: 60px; margin-left: 10px;"
							onkeyup="manageUIOnKeyUp();" value="20"
							onkeypress="validateForNumbers(event)" maxlength="5" />
						&nbsp;&nbsp;
						<div class="invalid-feedback">Please provide a number higher than
							20 and lower than 500.</div>
						<div class="valid-feedback">A number higher than 20 is provided.</div>
					</div>
					<hr/>
    					<div class="container-fluid">
                            <h4 class="font-weight-light">Your Promotable Properties Selection</h2>
                            <div id="propertyselectioncontainer" class="d-flex flex-row flex-nowrap" style="overflow-x: scroll; overflow-y: hidden;"></div>
                        </div>
					<hr />
					<div class="tab-pane container active" id="criteria">
						<div class="container-fluid well" style="margin-top: 15px;">
							<script>
							function myCollapseSearchBox(number) {
            					if ($('#searchbox_criteria_' + number).css("display") != "block") {
            						$('#searchbox_criteria_' + number).css("display", "block");
                					$("#searchbox_arrow_icon_" + number).removeClass('fa-angle-right').addClass('fa-angle-down');
                				} else {
                					$('#searchbox_criteria_' + number).css("display", "none");
                					$("#searchbox_arrow_icon_" + number).addClass('fa-angle-right').removeClass('fa-angle-down');
                				}
                    		}
						</script>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
								id="nosearchblock">
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-10">
									<div id="alertmsg">
										<div class="alert alert-info" role="alert">
											<p>You have no search criteria set-up to pick your
												Network-Properties so far.</p>
										</div>
									</div>
								</div>
								<div id="savetop" class="col-xs-12 col-sm-12 col-md-4 col-lg-2">
									<div class="float-right">
										<button id="addcriteria" type="button" href="#"
											onclick="nextCriteriaSlotAvailable();"
											class="btn btn-primary btn-lg" disabled>Add Criteria</button>
									</div>
								</div>
							</div>
                		<?php for ($criteria_num = 0; $criteria_num < $criteria_max; $criteria_num++) { ?>
                    		<div
								class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<a name="searchblock_<?php echo $criteria_num; ?>" />
								<div id="searchblock_<?php echo $criteria_num; ?>"
									style="display: none;">
									<a id="plusicon_<?php echo $criteria_num; ?>" href="#"
										onclick="nextCriteriaSlotAvailable();"><i
										class='fa fa-plus fa-lg'
										onmouseover="this.style.opacity = '1'"
										onmouseout="this.style.opacity = '0.3'" style="opacity: 0.3;"></i></a>&nbsp;&nbsp;<a
										onclick="criteriaUsedArray[<?php echo $criteria_num; ?>] = undefined; searchFormReset('_criteria_<?php echo $criteria_num; ?>'); validationCycle(false);"><i
										id="trashicon_<?php echo $criteria_num; ?>"
										class='fa fa-trash-o fa-lg'
										onmouseover="this.style.opacity = '1'"
										onmouseout="this.style.opacity = '0.3'" style="opacity: 0.3;"></i></a>&nbsp;&nbsp;
									<button type="button" class="btn btn-secondary btn-sm"
										data-parentdivsuffix="_criteria_<?php echo $criteria_num; ?>"
										data-toggle="modal"
										data-target="#searchListPreviewModal_criteria_<?php echo $criteria_num; ?>"
										onmouseover="this.style.opacity = '1'"
										onmouseout="this.style.opacity = '0.3'" style="opacity: 0.3;">Preview List</button>
									&nbsp; <a
										onclick="myCollapseSearchBox(<?php echo $criteria_num; ?>);"
										style="cursor: pointer;"><span
										id="searchdescriptor_<?php echo $criteria_num; ?>"></span><i
										id='searchbox_arrow_icon_<?php echo $criteria_num; ?>'
										class='fa fa-angle-down pull-right fa-2x'></i></a>
									<div id="searchbox_criteria_<?php echo $criteria_num; ?>">
										<input type="hidden"
											id="search_json_criteria_<?php echo $criteria_num; ?>"
											onchange="manageUIOnChange();" />
        							<?php
                                    $inspiry_options_copy = array_merge(array(), $inspiry_options);
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['keyword'] = esc_html__( 'Keyword', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['location'] = esc_html__( 'Location', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['type'] = esc_html__( 'Type', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['min-beds'] = esc_html__( 'Min Beds', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['min-baths'] = esc_html__( 'Min Baths', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['min-max-price'] = esc_html__( 'Min Max Price', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['min-max-area'] = esc_html__( 'Min Max Area', 'inspiry' );
                                    unset($inspiry_options_copy['inspiry_search_fields']['enabled']['status']);
                                    unset($inspiry_options_copy['inspiry_search_fields']['enabled']['property-id']);
                                    $inspiry_options_copy['inspiry_search_fields']['default']['status'] = "For Sale";
                                    ire_properties_search_parameters_form($inspiry_options_copy, "_criteria_" . $criteria_num, "searchBoxValidationCycle()");
                                    ?>
        							</div>
									<hr id='separator_<?php echo $criteria_num; ?>' />
								</div>
							</div>
    					<?php } ?>
    						<input type="hidden" id="search_json_criteria"
								onchange="manageUIOnChange();"/>
							<input type="hidden" id="agent_id"
								onchange="manageUIOnChange();"/>
							<div id="optionswithcriteria"
								class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<button id="resetall" type="button" href="#"
									onclick="allSearchesReset();"
									class="btn btn-secondary btn-lg float-left"
									style="margin-top: 10px;">Reset</button>
								<button id="updatelist" type="button" href="#"
									onclick="updateList();"
									class="btn btn-secondary btn-lg float-right"
									style="margin-top: 10px;"><i class="spinner fa fa-spinner fa-lg"></i><span id="updatelistbuttonname">View Properties List</span></button>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div id="infocontent" class="text-center"></div>
						</div>
					</div>
					<div class="tab-pane container" id="agent">
						<div class='alert alert-info'>Find agents that have properties of given criteria here.</div>
						<div class="container well">
							<div class="dflex">
    							<?php
                                    $inspiry_options_copy = array_merge(array(), $inspiry_options);
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['keyword'] = esc_html__( 'Keyword', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['location'] = esc_html__( 'Location', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['type'] = esc_html__( 'Type', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['min-beds'] = esc_html__( 'Min Beds', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['min-baths'] = esc_html__( 'Min Baths', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['min-max-price'] = esc_html__( 'Min Max Price', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['min-max-area'] = esc_html__( 'Min Max Area', 'inspiry' );
                                    unset($inspiry_options_copy['inspiry_search_fields']['enabled']['status']);
                                    unset($inspiry_options_copy['inspiry_search_fields']['enabled']['property-id']);
                                    $inspiry_options_copy['inspiry_search_fields']['default']['status'] = "For Sale";
                                    $inspiry_options_copy['post_target'] = "agent";
                                    ire_properties_search_parameters_form($inspiry_options_copy, "_agent", "agentSearchBoxValidationCycle()");
                                ?>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    								<button id="agentreset" type="button" href="#"
    									onclick="agentSearchReset();"
    									class="btn btn-secondary btn-lg float-left"
    									style="margin-top: 10px;" disabled>Reset</button>
    								<button id="agentupdatelist" type="button" href="#"
    									onclick="agentUpdateList();"
    									class="btn btn-secondary btn-lg float-right"
    									style="margin-top: 10px;" disabled><i class="spinner fa fa-spinner fa-lg"></i><span id="agentupdatelistbuttonname">Update Agents List</span></button>
    							</div>
    						</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div id="agentinfocontent" class="text-center" style="margin-bottom: 12px"></div>
						</div>
						<div id="agentstablesection" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none">
    						<div class="tblCont">
    							<table id="agentstable" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                        		    <thead>
                            		    <tr>
                                		    <th class="th-sm">Name</th>
                                		    <th class="th-sm">Phone</th>
                                		    <th class="th-sm">E-mail</th>
                                		    <th class="th-sm">Match Count</th>
                                		    <th class="th-sm">Total Properties</th>
                            		    </tr>
                        		    </thead>
                        		    <tbody id ="agentstablebody">
                        		    </tbody>
                    		    </table>
                		    </div>
						</div>
					</div>
					<div id="selectedproperties" style="display:none;">
						<hr/>
						<script>
    						var value = calculateFullSearchJson();
    						value = (value != "{}")?value:"";
    						if (value != "") {
        						jQuery.ajax({
              		                method:'POST',
              		                url: '/wp-admin/admin-ajax.php',
              		              	data: {action: 'folder_contents', json:value, addclassthumbnail:'disable-thumbnail-link'},
              		                success: function(res) {
              		                  	$("#aftersearchshow").html(res);
              		                }
              		            });
    						} else {
    							$("#aftersearchshow").html("");
    						}
          		    	</script>
						<div id="homeProperties" class="property-listing-two">
							<div class="newContainer" id="aftersearchshow"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 12px;">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="float-right">
					<button id="savebutton" type="button"
						class="btn btn-primary btn-lg" onclick="manageUISave();" disabled>Save</button>
				</div>
			</div>
		</div>
	</form>
    <?php
        for ($criteria_num = 0; $criteria_num < $criteria_max; $criteria_num ++) {
            ire_properties_search_modal("_criteria_" . $criteria_num);
        }
    ?>
    <div class="modal fade" id="agentinformationmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 100000 !important" style="min-width: 80%; min-height: 70%">
    	<div class="modal-dialog" role="document" style="min-width: 80%; height: 100%;">
    		<div class="modal-content" style="min-width: 80%; height: 100%;">
    			<div class="modal-content" style="min-width: 80%; height: 100%;">
    				<div class="modal-header">
    					<h5 class="modal-title" id="exampleModalLabel">Agent Information</h5>
    					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    						<span aria-hidden="true">&times;</span>
    					</button>
    				</div>
    				<div class="modal-body">
						<div class="pre-scrollable" style="min-height: 95%; display: flex; flex-direction: column; flex-grow: 1;">
        					<div class="wells">
            					<div class="col-xs-12 col-sm-6 col-md-3 fieldCont"><label for="agent_wells_Name">Name</label><span id="agent_wells_Name" name="agent_wells_Name"/></div>
            					<div class="col-xs-12 col-sm-6 col-md-3 fieldCont"><label for="agent_wells_Phone">Phone</label><span id="agent_wells_Name" name="agent_wells_Phone"/></div>
            					<div class="col-xs-12 col-sm-6 col-md-3 fieldCont"><label for="agent_wells_EMail">E-mail</label><span id="agent_wells_EMail" name="agent_wells_EMail"/></div>
            					<div class="col-xs-12 col-sm-6 col-md-3 fieldCont"><label for="agent_wells_FirstDetected">First detected</label><span id="agent_wells_FirstDetected" name="agent_wells_FirstDetected"/></div>
                				<div class="col-xs-12 col-sm-6 col-md-3 fieldCont"><label for="agent_wells_LastDetected">Last detected</label><span id="agent_wells_LastDetected" name="agent_wells_LastDetected"/></div>
                				<div class="col-xs-12 col-sm-6 col-md-3 fieldCont"><label for="agent_wells_NonDuplicateCount">Non-duplicate listings count</label><span id="agent_wells_NonDuplicateCount" name="agent_wells_NonDuplicateCount"/></div>
                				<div class="col-xs-12 col-sm-6 col-md-3 fieldCont"><label for="agent_wells_DuplicateCount">Duplicate listings count</label><span id="agent_wells_DuplicateCount" name="agent_wells_DuplicateCount"/></div>
                				<div class="col-xs-12 col-sm-12 col-md-12"
                				style="margin-bottom: 15px;">
                				<a id="agent_wells_cuslinks" class="text-warning stretched-link" style="cursor: pointer;" onclick="$('#agent_wells_valuationdiv').toggle();">Listing Agent Valuation Information (prices are in USD$)</a>
                					<div id="agent_wells_valuationdiv">
                						<pre id="agent_wells_valuationtext"></pre>
                					</div>
                				</div>
                				<div class="col-xs-12 col-sm-12 col-md-12"
                					style="margin-bottom: 15px;">
                					<a id="agent_wells_urllinks" class="text-warning" style="cursor: pointer;" onclick="$('#agent_wells_urlsdiv').toggle();">Urls (prices are in USD$)</a>
                					<div id="agent_wells_urlsdiv">
                						<pre id="agent_wells_urlstext"></pre>
                					</div>
                				</div>
            				</div>
        				</div>
    				</div>
    				<div class="modal-footer">
    					<button type="button" class="btn btn-secondary"
    						data-dismiss="modal">Close</button>
    				</div>
    				<span id="json<?php echo $parentdivsuffix; ?>"
    					style="display: none;"></span>
    			</div>
    		</div>
    	</div>
    </div>
</div>
<script>
$('#criteriapill').addClass("spinning-flex");
</script>