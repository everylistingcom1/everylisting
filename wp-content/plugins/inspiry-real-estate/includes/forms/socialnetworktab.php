<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap-switch-button@1.1.0/css/bootstrap-switch-button.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap-switch-button@1.1.0/dist/bootstrap-switch-button.min.js"></script>
<form class="container">
	<div class="row">
		<div class="container">
			<ul class="nav nav-pills tabNav pull-right">
				<li class="nav-item"><a id="socialpill" class="nav-link active"
					data-toggle="pill" href="#socialtab">Connections</a></li>
				<li class="nav-item"><a class="nav-link" data-toggle="pill" href="#postingtab" onclick="ensurePropertiesContextLoaded(); initAutoPostingScreen();">Automated Posting</a></li>
			</ul>
			<div class="tab-content tabConts"
				style="border-left: 1px solid #778899; border-right: 1px solid #778899; border-bottom: 1px solid #778899; border-top: 0.5px solid #778899">
				<div class="tab-pane container active" id="socialtab">
                    <?php
                    if (in_array('referee', (array) $current_user->roles)) {
                        ?>
                    	<div class="refContainer" style="margin-top: 30px;">
                    	<?php
                        global $wpdb;
                        $current_user = wp_get_current_user();
                        // print_r($current_user->ID);
                        // $aResult = array();
                        $aResult = $wpdb->get_results("SELECT * from wp_wslusersprofiles where user_id=$current_user->ID", ARRAY_A);
                        // print_r($aResult);
                        // Ends here
                        if (! empty($aResult)) {
                            ?>
                    			<h2>Refered By</h2>
                    						<div class="tblCont" style="overflow-x: auto;">
                    							<table>
                    								<thead>
                    									<tr>
                    										<th>Name</th>
                    										<th>Contacts</th>
                    										<th>Other Info</th>
                    									</tr>
                    								</thead>
                    								<tbody>
                    					<?php
                            foreach ($aResult as $rows) {
                                // $rows[profile_url] = 'https://web.facebook.com/public/Brett-Henry?_rdc=1&_rdr';
                                ?>
                    							<tr>
                    										<td><?php echo $rows['firstname']." ".$rows['lastname']; ?></td>
                    										<td><?php echo $rows['email']; ?></td>
                    										<td><?php echo $rows['profileurl']; ?></td>
                    									</tr>
                    					<?php
                            }
                            ?>
                    					</tbody>
                    							</table>
                    						</div>
                    	<?php
                        }
                        ?>
                    	</div>
                    <?php
                    }
                    if ((in_array('referrer', (array) $current_user->roles))) {
                        ?>
                    	<div class="alert alert-success" role="alert">
                    						<h4 class="alert-heading">Why connecting my social-network here?</h4>
                    						<p>In real-estate, maximal visibility of all social-networks is
                    							key. Connecting your social-networks allows EveryListing.com to
                    							periodically display your properties to your contacts.</p>
                    					</div>
                    <?php
                    }
                    if ((in_array('referee', (array) $current_user->roles)) && (! (in_array('referrer', (array) $current_user->roles)))) {
                        ?>
                    	<div class="alert alert-success" role="alert">
                    						<h4 class="alert-heading">Why connecting my social-network here?</h4>
                    						<p>To better serve you, EveryListing.com uses your social-networks
                    							to detect real-estate agents that are part of them. Since these
                    							contacts are part of your social-network, we figure they are
                    							people you trust. Should you connect your social-networks, we
                    							will prioritize their corresponding listings in your feed so that
                    							you can further consider their properties first.</p>
                    					</div>
                    <?php
                    }
                    ?>
					<div><?php do_action('wordpress_social_login', array('mode' => 'link', 'enable_providers' => 'facebook|linkedin|twitter|instagram')); ?></div>
                    <?php if (in_array('referrer', (array)$current_user->roles)) { ?>
                    	<h2>References</h2>
                    					<div class="tblCont">
                    	<?php
                        global $wpdb;
                        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
                        // Check connection
                        if (mysqli_connect_errno()) {
                            echo "Failed to connect to MySQL: " . mysqli_connect_error();
                            die();
                        }
                        $sql = "SELECT * FROM wp_wsluserscontacts WHERE user_id = " . $current_user->ID;
                        $res_data = mysqli_query($conn, $sql);
                        while ($row = mysqli_fetch_array($res_data)) {
                            // here goes the data
                        }
                        mysqli_close($conn);
                    
                        $current_user = wp_get_current_user();
                        // print_r($current_user->ID);
                    
                        // $aResult = array();
                        $aResult = $wpdb->get_results("SELECT * FROM `wp_wsluserscontacts` WHERE user_id = $current_user->ID", ARRAY_A);
                    
                        // print_r($aResult);
                        // Ends here
                        if (! empty($aResult)) {
                            ?>
                    			<div style="overflow-x: auto" class="tblCont">
                    							<table id="dtReferrer"
                    								class="table table-striped table-bordered table-sm"
                    								cellspacing="0" width="100%">
                    								<thead>
                    									<tr>
                    										<th class="th-sm">Name</th>
                    										<th class="th-sm">Contacts</th>
                    										<th class="th-sm">Other Info</th>
                    									</tr>
                    								</thead>
                    								<tbody> <?php
                            foreach ($res_data as $rows) {
                                ?><tr><?php
                                if (strpos($rows['profile_url'], "facebook") !== false) {
                                    ?>
                    					<td><span style="float: left;"><i class="fa fa-facebook"></i></span>&nbsp;&nbsp;<?php echo $rows['full_name']; ?></td>
                    										<td><?php echo $rows['email']; ?><br /><?php //echo $row[phone]; ?></td>
                    										<td><?php echo $rows['profile_url']; ?><br></td>
                    				<?php
                                } else if (strpos($rows['profile_url'], "twitter") !== false) {
                                    ?>
                    					<td><span style="float: left;"><i class="fa fa-twitter"></i></span>&nbsp;&nbsp;<?php echo $rows['full_name']; ?></td>
                    										<td><?php echo $rows['email']; ?><br /><?php //echo $row[phone]; ?></td>
                    										<td><?php echo $rows['profile_url']; ?><br></td>
                    				<?php
                                } else if (strpos($rows['profile_url'], "instagram") !== false) {
                                    ?>
                    					<td><span style="float: left;"><i class="fa fa-instagram"></i></span>&nbsp;&nbsp;<?php echo $rows['full_name']; ?></td>
                    										<td><?php echo $rows['email']; ?><br /><?php //echo $row[phone]; ?></td>
                    										<td><?php echo $rows['profile_url']; ?><br></td>
                    				<?php
                                } else if (strpos($rows['profile_url'], "linkedin") !== false) {
                                    ?>
                    					<td><span style="float: left;"><i class="fa fa-linkedin"></i></span>&nbsp;&nbsp;<?php echo $rows['full_name']; ?></td>
                    										<td><?php echo $rows['email']; ?><br /><?php //echo $row[phone]; ?></td>
                    										<td><?php echo $rows['profile_url']; ?><br></td>
                    				<?php
                                }
                                ?></tr><?php
                            }
                            ?>
                    		    </tbody>
                    							</table>
                    						</div>
                    						<script>
                    			$(document).ready(function () {
                    			  $('#dtReferrer').DataTable();
                    			  $('.dataTables_length').addClass('bs-select');
                    			});
                    			</script>
                    		    <?php
                        } else {
                            ?>
                    			No records found.
                    		<?php
                        }
                        ?>
                    	</div>
                    <?php
                    }
                    ?>
				</div>
			<script>
            	var IDXGenerator_realEstateBrokerId = undefined;
            	var propertyIdsForVideos = [];
            	var refreshed_propertyIdsForVideos = undefined;
            	var refreshed_valuestr = undefined;
            	var IDXGenerator_realEstateBrokerId_properties = [];
            	var agentAvatar = "<?php echo get_avatar_url(get_current_user_id()); ?>";
            	var agentDisplayName = "<?php echo wp_get_current_user()->nickname; ?>";
            	var userId = "<?php echo get_current_user_id(); ?>";
            	var imgtogglecount = 0;
            	var videotogglecount = 0;
            	var imgtoggletotalcount = 0;
            	var videotoggletotalcount = 0;
				var logohandled = undefined;
				var autopostingscreenhasinit = false;

				function initAutoPostingScreen() {
					setprogressbarstate();
					ensureSelectValuesSet();
					validatePreviewUI();
				}
				
            	<?php 
            	$IDXGenerator_realEstateBrokerId = get_user_meta(get_current_user_id(), "IDXGenerator_realEstateBrokerId", true);
            	if ($IDXGenerator_realEstateBrokerId != false) {
            	    echo 'IDXGenerator_realEstateBrokerId = ' . $IDXGenerator_realEstateBrokerId . ';';
            	    $args = array('post_type' => 'property', 'meta_query' => array(
            	        'relation' => 'OR',
            	        'left_clause' => array(
            	            'key' => 'IDXGenerator_listing_agents_id',
            	            'value'   => $IDXGenerator_realEstateBrokerId . '|',
            	            'compare' => 'LIKE'
            	        ),
            	        'center_clause' => array(
            	            'key' => 'IDXGenerator_listing_agents_id',
            	            'value'   => '|' . $IDXGenerator_realEstateBrokerId . '|',
            	            'compare' => 'LIKE'
            	        ),
            	        'right_clause' => array(
            	            'key' => 'IDXGenerator_listing_agents_id',
            	            'value'   => '|'. $IDXGenerator_realEstateBrokerId,
            	            'compare' => 'LIKE'
            	        ),
            	    ));
            	    $agent_properties_query = new WP_Query($args);
            	    if ($agent_properties_query->found_posts > 1) {
                		while ($agent_properties_query->have_posts()) {
                			$agent_properties_query->the_post();
                			?>
                			IDXGenerator_realEstateBrokerId_properties.push(<?php echo get_the_ID()?>);
                			<?php
                		}
                		wp_reset_postdata();
            	    }
            	    wp_reset_postdata();
            	}
            	?>
            	function ensurePropertiesContextLoaded() {
                	var locHttp = new XMLHttpRequest();
                	locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/getUserMeta.php?startwith=ui_HIDDEN_networkpropertyselection");
                	locHttp.send();
                	locHttp.onreadystatechange = function() {
                		if (this.readyState == 4 && this.status == 200) {
                			var jsonpropertiesobj = JSON.parse(this.responseText.replaceAll("\\\"", "").replace("\"[", "[").replace("]\"", "]"));
                			propertyIdsForVideos = IDXGenerator_realEstateBrokerId_properties;
                			if (jsonpropertiesobj.ui_HIDDEN_networkpropertyselection != undefined) {
                				var buildPropertyIdsForVideos = jsonpropertiesobj.ui_HIDDEN_networkpropertyselection;
                				if (buildPropertyIdsForVideos.length > 0) {
                        			buildPropertyIdsForVideos.forEach(function(value) {
                        				propertyIdsForVideos.push(value);
                        			});
                				}
                				if (buildPropertyIdsForVideos.length == 0) {
                					$('#togglevideobuttons').hide();
                					$('calculationsprogressrow').hide();
                				}
                				refreshPreviewsIfNeeded();
                			} else {
                				$('#togglevideobuttons').hide();
                				$('calculationsprogressrow').hide();
                			}
                		}
                	}
            	}
            	function valueToLocation(value) {
            		switch (value) {
                		case "1":
                    		return "eTopLeft";
                		case "2":
                			return "eTopRight";
                		case "3":
                			return "eBottomLeft";
                		case "4":
                			return "eBottomRight";
                		case "5":
                    		return "eCenterOnLastSequence";
                		case "6":
                    		return "eLeft";
                		case "7":
                    		return "eRight";
                		default:
                    		return "";
            		}
            	}
            	function ensureSelectValuesSet() {
            		var locHttp = new XMLHttpRequest();
            		locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/getUserMeta.php?startwith=socialscreenui_");
            		locHttp.send();
            		locHttp.onreadystatechange = function() {
            			if (this.readyState == 4 && this.status == 200) {
                			if ((this.responseText == "{}") || (this.responseText == "")) {
                				$('#price').val("2").change();
                				$('#location').val("4").change();
                				$('#agentinfo').val("1").change();
                			} else {
                    			var json = JSON.parse(this.responseText);
                				for (var k in json) {
                    				if ((k.substring("socialscreenui_".length)).startsWith("switch_")) {
                        				if (json[k] == 'true') {
                    						$('#' + k.substring("socialscreenui_".length)).get(0).switchButton('on');
                        				} else {
                        					$('#' + k.substring("socialscreenui_".length)).get(0).switchButton('off');
                        				}
                    				} else {
                    					$('#' + k.substring("socialscreenui_".length)).val(json[k]).change();
                    				}
                				}
                			}
                			validatePreviewUI();
                			autopostingscreenhasinit = true;
            			}
            		}
            	}
            	function manageUISelectOnChange(elem) {
                	if (!autopostingscreenhasinit) return;
                	var value = elem.val();
                	if (elem.attr('id').startsWith("switch_")) {
                    	value = elem.prop('checked');
                	}
					var storeurl = "/wp-content/themes/inspiry-real-places/inc/util/saveUserMeta.php?socialscreenui_" + elem.attr('id') + "=" + value;
					var locHttp = new XMLHttpRequest();
					locHttp.open("GET", storeurl);
					locHttp.send();
            	}
            	var delayedRefresh = undefined;
            	function refreshDelayedPreviewsIfNeeded() {
            		var statestr = $("#price").val() + "|" + $("#onelineinfo").val() + "|" + $("#location").val() + "|" + $("#agentinfo").val();
            		if ((JSON.stringify(refreshed_propertyIdsForVideos) != JSON.stringify(propertyIdsForVideos)) || (refreshed_valuestr != statestr)) {
            			if (delayedRefresh != undefined) {
		    				clearTimeout(delayedRefresh);
		    			}
            			delayedRefresh = setTimeout(refreshPreviewsIfNeeded, 500);
    					imgtogglecount = 0;
    					videotogglecount = 0;
    					imgtoggletotalcount = 0;
    					videotoggletotalcount = 0;
    					setprogressbarstate();
            		}
            	}
            	function refreshPreviewsIfNeeded() {
            		var statestr = $("#price").val() + "|" + $("#onelineinfo").val() + "|" + $("#location").val() + "|" + $("#agentinfo").val();
            		if ((JSON.stringify(refreshed_propertyIdsForVideos) != JSON.stringify(propertyIdsForVideos)) || (refreshed_valuestr != statestr)) {
            			refreshed_valuestr = statestr;
            			refreshed_propertyIdsForVideos = JSON.stringify(propertyIdsForVideos);
            			var locHttp = new XMLHttpRequest();
            			locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/multiplePostInfo.php?ids="+ encodeURIComponent(propertyIdsForVideos) + "&post_type=property&filter=id|permalink|title|REAL_HOMES_property_address|IDXGenerator_nlp_price|REAL_HOMES_property_images|delayed_images");
            			locHttp.send();
            			locHttp.onreadystatechange = function() {
            				if (this.readyState == 4 && this.status == 200) {
                				if (this.responseText == "{}") {
                					$('#togglevideobuttons').hide();
                					$('calculationsprogressrow').hide();
                				} else {
                					$('#togglevideobuttons').show();
                					$('calculationsprogressrow').show();
                					var imagehtml = "";
                					var videohtml = "";
                					var curproperty = 0;
                					imgtogglecount = 0;
                					videotogglecount = 0;
                					imgtoggletotalcount = 0;
                					videotoggletotalcount = 0;
                					setprogressbarstate();
                					var data = JSON.parse(this.responseText);
                					$('#previewstatimagecarousel').html();
                					$('#fullvideoimagecarousel').html();
                					if (data.result != undefined) data.result.forEach(function(entry) {
                						curproperty++;
                						var location;
                						var json = "{'animation': {'className':'AnimationBuilder','dimension':{'width':600,'height':600},'fileformat':'gif'},'superposers': [";
                						location = logohandled;
                						if ((location != undefined) && (location != "")) {
                							if (!json.endsWith("[")) {
                								json += ",";
                							}
                							json += "{'location':'" + valueToLocation(location) + "','size':30,'padding':15,'className':'EveryListingLogoSuperposer'}";
                						}
                						location = valueToLocation($("#price").val());
                						if ((location != "") && (entry.IDXGenerator_nlp_price != undefined)) {
                							if (!json.endsWith("[")) {
                								json += ",";
                							}
                							json += "{'text':'" + entry.IDXGenerator_nlp_price + "','location':'" + location + "','size':40,'padding':15,'color':'black','className':'TextSuperposer'}";
                						}
                						location = valueToLocation($("#onelineinfo").val());
                						if ((location != "") && (entry.title != undefined)) {
                							if (!json.endsWith("[")) {
                								json += ",";
                							}
                							json += "{'text':'" + entry.title + "','location':'" + location + "','size':40,'padding':15,'color':'black','className':'TextSuperposer'}";
                						}
                						location = valueToLocation($("#location").val());
                						if ((location != "") && (entry.REAL_HOMES_property_address != undefined)) {
                							if (!json.endsWith("[")) {
                								json += ",";
                							}
                							json += "{'text':'" + entry.REAL_HOMES_property_address + "','location':'" + location + "','size':40,'padding':15,'color':'black','className':'TextSuperposer'}";
                						}
                						location = valueToLocation($("#agentinfo").val());
                						if ((location != "") && (agentDisplayName != undefined)) {
                							if (!json.endsWith("[")) {
                								json += ",";
                							}
                							json += "{'profileimageurl':'" + agentAvatar + "','name':'" + agentDisplayName + "','location':'" + location + "','size':50,'padding':15,'className':'AgentSuperposer'}";
                						}
                						json += "],'sequence':[";
                						var images = [];
                						if (entry.delayed_images != undefined) {
                							images = entry.delayed_images.split("|");
                						}
                						if (entry.REAL_HOMES_property_images != undefined) {
                							images.unshift(entry.REAL_HOMES_property_images);
                						}
                						var cur = 0;
                						var curnocycle = 0;
                						images.forEach(function(value) {
                							if (value != "") {
                    							if (!json.endsWith("[")) {
                    								json += ",";
                    							}
                    							json += "{'className':'Sequence','animationEndClass':'";
                    							switch (cur) {
                        							case 0:
                            							json += "ShrinkToTopEffect";
                        								break;
                        							case 1:
                            							json += "AlphaDissolveEffect";
                        								break;
                        							case 2:
                            							json += "PixelDissolveEffect";
                        								break;
                        							case 3:
                            							json += "CheckerboardEffect";
                        								break;
                        							case 4:
                            							json += "BlindsEffect";
                        								break;
                        							case 5:
                            							json += "ScrollLeftEffect";
                        								break;
                        							case 6:
                            							json += "ScrollRightEffect";
                        								break;
                        							case 7:
                            							json += "ScrollUpEffect";
                        								break;
                        							case 8:
                            							json += "ScrollDownEffect";
                        								break;
                        							case 9:
                            							json += "WipeLeftEffect";
                        								break;
                        							case 10:
                            							json += "WipeRightEffect";
                        								break;
                        							case 11:
                            							json += "WipeUpEffect";
                        								break;
                        							case 12:
                            							json += "WipeDownEffect";
                        								break;
                        							case 13:
                            							json += "ZoomOutEffect";
                        								break;
                        							case 14:
                            							json += "ZoomInEffect";
                        								break;
                        							case 15:
                            							json += "IrisOpenEffect";
                        								break;
                        							case 16:
                            							json += "IrisCloseEffect";
                        								break;
                        							case 17:
                            							json += "BarnDoorOpenEffect";
                        								break;
                        							case 18:
                            							json += "BarnDoorCloseEffect";
                        								break;
                        							case 19:
                            							json += "ShrinkToBottomEffect";
                        								break;
                        							case 20:
                            							json += "ShrinkToTopEffect";
                        								break;
                        							case 21:
                            							json += "ShrinkToCenterEffect";
                        								break;
                        							case 22:
                            							json += "StretchFromBottomEffect";
                        								break;
                        							case 23:
                            							json += "StretchFromCenterEffect";
                            							cur = 0;
                        								break;
                    							}
                    							cur++;
                    							curnocycle++;
                    							json += "','url':'" + value + "','fps':30,'fixedTime':2000,'msTransitionTime':250}";
                							}
                						});
                						json += "]}";
                						json = json.replaceAll("'", "\"");
                						imagehtml += "<div class='carousel-item" + ((imagehtml == "")?" active":"") + "'><img id='loading_" + curproperty + "' data-property-id='" + entry.id + "' data-toggle='request=previewnameonly&jobsdir=<?php echo urlencode(IMAGES_JOBS_DIR) ?>&json=" + encodeURI(json) + "' class='d-block w-100' src='/mediacode/images/loading.gif'></div>";
                						videohtml += "<div class='carousel-item" + ((videohtml == "")?" active":"") + "'><img id='video_" + curproperty + "' data-property-id='" + entry.id + "' data-progress-bar='progress_vid_bar_" + curproperty + "' data-progress='progress_vid_" + curproperty + "' data-toggle='request=create&jobsdir=<?php echo urlencode(IMAGES_JOBS_DIR) ?>&json=" + encodeURI(json) + "' class='d-block w-100' src='/mediacode/images/loading.gif'><div class='progress' id='progress_vid_bar_" + curproperty + "' style='height: 40px; display: none;'><div id='progress_vid_" + curproperty + "' class='progress-bar' role='progressbar' style='width: 25%;' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100'>25%</div></div></div>";
                						imgtogglecount++;
                						videotogglecount++;
                					});
                					imgtoggletotalcount = imgtogglecount;
                					videotoggletotalcount = videotogglecount;
                					$('#previewstatimagecarousel').html(imagehtml);
                					$('#fullvideoimagecarousel').html(videohtml);
                					if (imgtogglecount > 0) {
                						setTimeout(manageImageToggle, 0);
                					}
                					if (videotogglecount > 0) {
                		    			if (curvideotimeout != undefined) {
                		    				clearTimeout(curvideotimeout);
                		    			}
                		    			curvideotimeout = setTimeout(manageVideoToggle, 0);
                					}
                					setprogressbarstate();
                				}
            				}
            			}
            		}
            	}
            	var curvideotimeout = undefined;
            	var curimagetimeout = undefined;
            	var curcontrolseq = undefined;
            	var queuedVideosRequests = 0;
            	function manageVideoToggle() {
            		if (videotogglecount > 0) {
                		if (($("img[id^=video_]:first").length > 0) && (queuedVideosRequests == 0)) {
                			var locHttp2 = new XMLHttpRequest();
                			locHttp2.open("POST", "http://<?php echo IMAGES_POST_SERVLET ?>/ImageGeneratorServlet/Process");
                			locHttp2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                			curcontrolseq = Math.floor((Math.random()) * 0x10000).toString(16);
                			locHttp2.send("controlseq="+ curcontrolseq + "&" + $("img[id^=video_]:first").attr('data-toggle'));
                			queuedVideosRequests++;
                			locHttp2.onreadystatechange = function() {
                				queuedVideosRequests = Math.max(0, queuedVideosRequests-1);
                				if (this.readyState == 4 && this.status == 200) {
                		    		try {
                    		    		var json = JSON.parse(this.responseText);
                    					if (json.renderingJobProgress.state == "successfullycompleted") {
                     						$("img[id^=video_]:first").attr('src', "http://<?php echo IMAGES_POST_SERVLET ?>/ImageGeneratorServlet/Process?request=get&jobsdir=<?php echo urlencode(IMAGES_JOBS_DIR) ?>&payload="+ json.renderingJobProgress.finalFileName);
                     						var storeurl = "/wp-content/themes/inspiry-real-places/inc/util/savePostMeta.php?id=" + $("img[id^=video_]:first").attr("data-property-id") + "&postvideo_userid_" + userId + "=" + json.renderingJobProgress.finalFileName;
                     						$('#' + $("img[id^=video_]:first").attr('data-progress-bar')).hide();
                     						$("img[id^=video_]:first").removeAttr("data-toggle");
                     						$("img[id^=video_]:first").removeAttr("id");
                     						videotogglecount--;
                     						var locHttp = new XMLHttpRequest();
                     						locHttp.open("GET", storeurl);
                     						locHttp.send();
                    					} else {
                    						if ((json.renderingJobProgress.current != undefined) && (json.renderingJobProgress.total != undefined)) {
                    							var percent = json.renderingJobProgress.current/json.renderingJobProgress.total * 100;
                    							$('#' + $("img[id^=video_]:first").attr('data-progress')).attr('style', 'width: ' + percent + '%');
                    							$('#' + $("img[id^=video_]:first").attr('data-progress')).attr('aria-valuenow', percent);
                    							$('#' + $("img[id^=video_]:first").attr('data-progress')).attr('aria-valuemax', '100');
                    							$('#' + $("img[id^=video_]:first").attr('data-progress')).html('<span class="justify-content-center d-flex position-absolute w-100">' + (json.renderingJobProgress.oneLinerCustomerFacingStatus != undefined)?(json.renderingJobProgress.oneLinerCustomerFacingStatus + ' (' + json.renderingJobProgress.current + ' / ' + json.renderingJobProgress.total + ')'):(percent + '%') + '</span>');
                    							$('#' + $("img[id^=video_]:first").attr('data-progress-bar')).show();
                    						} else {
                    							$('#' + $("img[id^=video_]:first").attr('data-progress-bar')).hide();
                    						}
                    					}
                		    		} catch (err) {}
                		    		setprogressbarstate();
                				}
                				if (curvideotimeout != undefined) {
        		    				clearTimeout(curvideotimeout);
        		    				curvideotimeout = undefined;
        		    			}
        		    			curvideotimeout = setTimeout(manageVideoToggle, 250);
                			}
                		}
                		if (curvideotimeout != undefined) {
		    				clearTimeout(curvideotimeout);
		    				curvideotimeout = undefined;
		    			}
		    			curvideotimeout = setTimeout(manageVideoToggle, 250);
            		}
            		setprogressbarstate();
            	}
            	function setprogressbarstate() {
                	var total = imgtoggletotalcount + videotoggletotalcount;
                	if (total == 0) {
                    	$('#imagesprogress').css('width', '0%');
                    	$('#videosprogress').css('width', '0%');
                    	$('#calculationsspinner').hide();
                    	$('#calculationslabel').text('Calculations Progress');
                	} else {
                		$('#imagesprogress').css('width', (((imgtoggletotalcount - imgtogglecount)/total)*100) + '%');
                    	$('#videosprogress').css('width', (((videotoggletotalcount - videotogglecount)/total)*100) + '%');
                    	if ((imgtogglecount > 0) || (videotogglecount > 0)) {
                    		$('#calculationsspinner').show();
                    		var text = 'Calculations Progress (' + Math.floor((((imgtoggletotalcount - imgtogglecount) + (videotoggletotalcount - videotogglecount))/ total) * 100) + '%)' + '&nbsp;<i id="calculationsspinner" class="fa fa-spinner fa-spin fa-lg"></i>';
                    		if (text != $('#calculationslabel').html()) {
                    			$('#calculationslabel').html(text);
                    		}
                    	} else {
                    		$('#calculationsspinner').hide();
                    		$('#calculationslabel').text('Calculations Progress (Done)');
                    	}
                	}
            	}
            	var queuedImageRequests = 0;
            	function manageImageToggle() {
            		if (imgtogglecount > 0) {
            			if (($("img[id^=loading_]:first").length > 0) && (queuedImageRequests == 0)) {
            				var locHttp2 = new XMLHttpRequest();
            				locHttp2.open("POST", "http://<?php echo IMAGES_POST_SERVLET ?>/ImageGeneratorServlet/Process");
            				locHttp2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            				locHttp2.send($("img[id^=loading_]:first").attr('data-toggle'));
            				queuedImageRequests++;
            				locHttp2.onreadystatechange = function() {
            					queuedImageRequests = Math.max(0, queuedImageRequests-1);
            					if (this.readyState == 4 && this.status == 200) {
            						$("img[id^=loading_]:first").attr('src', "http://<?php echo IMAGES_POST_SERVLET ?>/ImageGeneratorServlet/Process?request=get&jobsdir=<?php echo urlencode(IMAGES_JOBS_DIR) ?>&payload="+ this.response);
             						var storeurl = "/wp-content/themes/inspiry-real-places/inc/util/savePostMeta.php?id=" + $("img[id^=video_]:first").attr("data-property-id") + "&postimage_userid_" + userId + "=" + this.response;
            						$("img[id^=loading_]:first").removeAttr("data-toggle");
            						imgtogglecount--;
            						$("img[id^=loading_]:first").removeAttr("id");
            						var locHttp = new XMLHttpRequest();
             						locHttp.open("GET", storeurl);
             						locHttp.send();
            					}
            					if (curimagetimeout != undefined) {
        		    				clearTimeout(curimagetimeout);
        		    				curimagetimeout = undefined;
        		    			}
            					if (imgtogglecount > 0) curimagetimeout = setTimeout(manageImageToggle, 100);
            					setprogressbarstate();
            				}
            			}
            			if (curimagetimeout != undefined) {
		    				clearTimeout(curimagetimeout);
		    				curimagetimeout = undefined;
		    			}
    					if (imgtogglecount > 0) curimagetimeout = setTimeout(manageImageToggle, 100);	
            		}
            		setprogressbarstate();
            	}
            </script>
			<div class="tab-pane container" id="postingtab">
            	<h2 style="margin-bottom: 10px;">Postings Format and Content</h2>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
					<img style="border: 1px dashed; padding: 10px; width: 100%; height: auto;" id="guideimage" src="http://<?php echo IMAGES_POST_SERVLET ?>/ImageGeneratorServlet/Process?request=tutor&json=%7B%22topleft%22%3A1%2C%22topright%22%3A0%2C%22center%22%3A0%2C%22leftbanner%22%3A0%2C%22rightbanner%22%3A1%2C%22bottomleft%22%3A1%2C%22bottomright%22%3A1%7D" />
					<div id="previewstatimage" class="carousel slide" data-ride="carousel" style="display: none; border: 1px dashed; padding: 10px; width: 100%; height: auto;">
                      <div class="carousel-inner" id="previewstatimagecarousel">
                        <div class="carousel-item active">
                          <img class="d-block w-100" alt="First slide">
                        </div>
                        <div class="carousel-item">
                          <img class="d-block w-100" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                          <img class="d-block w-100" alt="Third slide">
                        </div>
                      </div>
                    </div>
                    <div id="fullvideoimage" class="carousel slide" data-ride="false" style="display: none; border: 1px dashed; padding: 10px; width: 100%; height: auto;">
                      <div class="carousel-inner" id="fullvideoimagecarousel">
                        <div class="carousel-item active">
                          <img class="d-block w-100" alt="First slide">
                        </div>
                        <div class="carousel-item">
                          <img class="d-block w-100" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                          <img class="d-block w-100" alt="Third slide">
                        </div>
                      </div>
                      <a class="carousel-control-prev" href="#fullvideoimage" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#fullvideoimage" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
					<div class="btn-group btn-group-toggle" data-toggle="buttons" id="togglevideobuttons" style="display: none;">
						<label class="btn btn-secondary active"> <input type="radio"
							name="options" id="guide" autocomplete="off" checked onchange="$('#guideimage').show(); $('#previewstatimage').hide(); $('#fullvideoimage').hide();"> Guide
						</label> <label class="btn btn-secondary"> <input type="radio"
							name="options" id="preview" autocomplete="off" onchange="$('#guideimage').hide(); $('#previewstatimage').show(); $('#fullvideoimage').hide();"> Preview Image
						</label> <label class="btn btn-secondary"> <input type="radio"
							name="options" id="video" autocomplete="off" onchange="$('#guideimage').hide(); $('#previewstatimage').hide(); $('#fullvideoimage').show();"> Full Video
						</label>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4"
					style="margin-top: 20px;">
					<table border="0">
					<tr>
					<td><div class="form-group">
                    	<label for="price">Price</label>
                    	<select class="form-control" id="price" onchange="aloneWithValue($(this)); manageUISelectOnChange($(this)); validatePreviewUI(); refreshDelayedPreviewsIfNeeded();">
                      		<option value="0">None</option>
                      		<option value="1">Top-Left</option>
                      		<option value="2">Top-Right</option>
                      		<option value="3">Bottom-Left</option>
                      		<option value="4">Bottom-Right</option>
                      		<option value="5">Center</option>
                      		<option value="6">Left-Banner</option>
                      		<option value="7">Right-Banner</option>
                    	</select>
                  	</div>
					</td></tr><tr>
					<td><div class="form-group">
                    	<label for="location">Location</label>
                    	<select class="form-control" id="location" onchange="aloneWithValue($(this)); manageUISelectOnChange($(this)); validatePreviewUI(); refreshDelayedPreviewsIfNeeded();">
                      		<option value="0">None</option>
                      		<option value="1">Top-Left</option>
                      		<option value="2">Top-Right</option>
                      		<option value="3">Bottom-Left</option>
                      		<option value="4">Bottom-Right</option>
                      		<option value="5">Center</option>
                      		<option value="6">Left-Banner</option>
                      		<option value="7">Right-Banner</option>
                    	</select>
                  	</div>
					</td></tr><tr>
					<td><div class="form-group">
                    	<label for="agentinfo">Agent Name and Profile Image</label>
                    	<select class="form-control" id="agentinfo" onchange="aloneWithValue($(this)); manageUISelectOnChange($(this)); validatePreviewUI(); refreshDelayedPreviewsIfNeeded();">
                      		<option value="0">None</option>
                      		<option value="1">Top-Left</option>
                      		<option value="2">Top-Right</option>
                      		<option value="3">Bottom-Left</option>
                      		<option value="4">Bottom-Right</option>
                      		<option value="5">Center</option>
                      		<option value="6">Left-Banner</option>
                      		<option value="7">Right-Banner</option>
                    	</select>
                  	</div>
					</td></tr><tr>
					<td><div class="form-group">
                    	<label for="onelineinfo">One-line Property Info</label>
                    	<select class="form-control" id="onelineinfo" onchange="aloneWithValue($(this)); manageUISelectOnChange($(this)); validatePreviewUI(); refreshDelayedPreviewsIfNeeded();">
                      		<option value="0">None</option>
                      		<option value="1">Top-Left</option>
                      		<option value="2">Top-Right</option>
                      		<option value="3">Bottom-Left</option>
                      		<option value="4">Bottom-Right</option>
                      		<option value="5">Center</option>
                      		<option value="6">Left-Banner</option>
                      		<option value="7">Right-Banner</option>
                    	</select>
                  	</div></td></tr><tr>
					</tr>
					<tr id="calculationsprogressrow" style="display: none;"><td>
						<label id="calculationslabel" for="calculationsprogressbar">Calculations Progress&nbsp;<i id="calculationsspinner" class="fa fa-spinner fa-spin fa-lg"></i></label>
						<div id="calculationsprogressbar" class="progress" style="height: 40px;">
							<div id="imagesprogress" class="progress-bar" role="progressbar" style="width: 0%; background-color: green; color: black;">Images</div>
							<div id="videosprogress" class="progress-bar" role="progressbar" style="width: 0%; background-color: yellow; color: black;">Videos</div>
						</div>
					</td></tr>
					</table>
				</div>
				<h2>Social-Networks</h2>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="container-fluid">
						<?php 
						$socialmedia = get_social_media_array(false);
						foreach ($socialmedia as $onesocialmedia) {
						    if ($onesocialmedia['issocial']) {
    						    ?>
    						    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="padding:15px;">
            						<input onchange="manageUISelectOnChange($(this));" data-width="150" type="checkbox" <?php echo $onesocialmedia['default']?'checked':''; ?> data-size="lg" data-toggle="switchbutton" id="switch_<?php echo $onesocialmedia['tech_name']; ?>" data-onlabel="<i <?php echo (($onesocialmedia['color'] != null)?("style='color:" . $onesocialmedia['color'] . "';"):""); ?> class='<?php echo (($onesocialmedia['fa'] == null)?("fa fa-" . $onesocialmedia['tech_name']):$onesocialmedia['fa']); ?>'></i>&nbsp;&nbsp;<b><?php echo $onesocialmedia['customer_facing']; ?> ON</b>" data-offlabel="<i <?php echo (($onesocialmedia['color'] != null)?("style='color:" . $onesocialmedia['color'] . "';"):"") ?> class='<?php echo (($onesocialmedia['fa'] == null)?("fa fa-" . $onesocialmedia['tech_name']):$onesocialmedia['fa']); ?>'></i>&nbsp;&nbsp;<?php echo $onesocialmedia['customer_facing']; ?> OFF" data-onstyle="light" data-offstyle="light"/>
    						    </div>
    						    <?php
						    }
						}
						?>
					</div>
				</div>
			<script>
					function validatePreviewUI() {
						logohandled = undefined;
						var previewjson = "{";
    						previewjson += "\"bottomright\":" + ((($("#onelineinfo").val() == "4") || ($("#agentinfo").val() == "4") || ($("#location").val() == "4") || ($("#price").val() == "4"))?"1":"0") + ",";
    						previewjson += "\"bottomrightlabel\":";
    						if ($("#onelineinfo").val() == "4") previewjson += ("\"" + $("label[for='onelineinfo']").text() + "\"")
    						else if ($("#agentinfo").val() == "4") previewjson += ("\"" + $("label[for='agentinfo']").text() + "\"")
    						else if ($("#location").val() == "4") previewjson += ("\"" + $("label[for='location']").text() + "\"")
    						else if ($("#price").val() == "4") previewjson += ("\"" + $("label[for='price']").text() + "\"")
    						else if (logohandled == undefined) { logohandled = "4"; previewjson += "\"EveryListing.com logo\"" }
    						else previewjson += "\"Bottom-Right\"";
    						previewjson += ",";
							previewjson += "\"bottomleft\":" + ((($("#onelineinfo").val() == "3") || ($("#agentinfo").val() == "3") || ($("#location").val() == "3") || ($("#price").val() == "3"))?"1":"0") + ",";
							previewjson += "\"bottomleftlabel\":";
							if ($("#onelineinfo").val() == "3") previewjson += ("\"" + $("label[for='onelineinfo']").text() + "\"")
							else if ($("#agentinfo").val() == "3") previewjson += ("\"" + $("label[for='agentinfo']").text() + "\"")
							else if ($("#location").val() == "3") previewjson += ("\"" + $("label[for='location']").text() + "\"")
							else if ($("#price").val() == "3") previewjson += ("\"" + $("label[for='price']").text() + "\"")
    						else if (logohandled == undefined) { logohandled = "3"; previewjson += "\"EveryListing.com logo\"" }
							else previewjson += "\"Bottom-Left\"";
							previewjson += ",";
							previewjson += "\"topright\":" + ((($("#onelineinfo").val() == "2") || ($("#agentinfo").val() == "2") || ($("#location").val() == "2") || ($("#price").val() == "2"))?"1":"0") + ",";
							previewjson += "\"toprightlabel\":";
							if ($("#onelineinfo").val() == "2") previewjson += ("\"" + $("label[for='onelineinfo']").text() + "\"")
							else if ($("#agentinfo").val() == "2") previewjson += ("\"" + $("label[for='agentinfo']").text() + "\"")
							else if ($("#location").val() == "2") previewjson += ("\"" + $("label[for='location']").text() + "\"")
							else if ($("#price").val() == "2") previewjson += ("\"" + $("label[for='price']").text() + "\"")
    						else if (logohandled == undefined) { logohandled = "2"; previewjson += "\"EveryListing.com logo\"" }
							else previewjson += "\"Top-Right\"";
							previewjson += ",";
							previewjson += "\"topleft\":" + ((($("#onelineinfo").val() == "1") || ($("#agentinfo").val() == "1") || ($("#location").val() == "1") || ($("#price").val() == "1"))?"1":"0") + ",";
							previewjson += "\"topleftlabel\":";
							if ($("#onelineinfo").val() == "1") previewjson += ("\"" + $("label[for='onelineinfo']").text() + "\"")
							else if ($("#agentinfo").val() == "1") previewjson += ("\"" + $("label[for='agentinfo']").text() + "\"")
							else if ($("#location").val() == "1") previewjson += ("\"" + $("label[for='location']").text() + "\"")
							else if ($("#price").val() == "1") previewjson += ("\"" + $("label[for='price']").text() + "\"")
    						else if (logohandled == undefined) { logohandled = "1"; previewjson += "\"EveryListing.com logo\"" }
							else previewjson += "\"Top-Left\"";
							previewjson += ",";
							previewjson += "\"center\":" + ((($("#onelineinfo").val() == "5") || ($("#agentinfo").val() == "5") || ($("#location").val() == "5") || ($("#price").val() == "5"))?"1":"0") + ",";
							previewjson += "\"centerlabel\":";
							if ($("#onelineinfo").val() == "5") previewjson += ("\"" + $("label[for='onelineinfo']").text() + "\"")
							else if ($("#agentinfo").val() == "5") previewjson += ("\"" + $("label[for='agentinfo']").text() + "\"")
							else if ($("#location").val() == "5") previewjson += ("\"" + $("label[for='location']").text() + "\"")
							else if ($("#price").val() == "5") previewjson += ("\"" + $("label[for='price']").text() + "\"")
    						else if (logohandled == undefined) { logohandled = "5"; previewjson += "\"EveryListing.com logo\"" }
							else previewjson += "\"Center\"";
							previewjson += ",";
							previewjson += "\"leftbanner\":" + ((($("#onelineinfo").val() == "6") || ($("#agentinfo").val() == "6") || ($("#location").val() == "6") || ($("#price").val() == "6"))?"1":"0") + ",";
							previewjson += "\"leftbannerlabel\":";
							if ($("#onelineinfo").val() == "6") previewjson += ("\"" + $("label[for='onelineinfo']").text() + "\"")
							else if ($("#agentinfo").val() == "6") previewjson += ("\"" + $("label[for='agentinfo']").text() + "\"")
							else if ($("#location").val() == "6") previewjson += ("\"" + $("label[for='location']").text() + "\"")
							else if ($("#price").val() == "6") previewjson += ("\"" + $("label[for='price']").text() + "\"")
							else previewjson += "\"Left-Banner\"";
							previewjson += ",";
							previewjson += "\"rightbanner\":" + ((($("#onelineinfo").val() == "7") || ($("#agentinfo").val() == "7") || ($("#location").val() == "7") || ($("#price").val() == "7"))?"1":"0") + ",";
							previewjson += "\"rightbannerlabel\":";
							if ($("#onelineinfo").val() == "7") previewjson += ("\"" + $("label[for='onelineinfo']").text() + "\"")
							else if ($("#agentinfo").val() == "7") previewjson += ("\"" + $("label[for='agentinfo']").text() + "\"")
							else if ($("#location").val() == "7") previewjson += ("\"" + $("label[for='location']").text() + "\"")
							else if ($("#price").val() == "7") previewjson += ("\"" + $("label[for='price']").text() + "\"")
							else previewjson += "\"Right-Banner\"";
							previewjson += "}";
						$("#guideimage").attr("src", "http://<?php echo IMAGES_POST_SERVLET ?>/ImageGeneratorServlet/Process?request=tutor&json=" + encodeURI(previewjson));
					}
					function aloneWithValue(elem) {
						if (elem.val() != "0") {
							var ids = ["onelineinfo", "agentinfo", "location", "price"];
							for (var id in ids) {
								if (ids[id] !== elem.attr("id")) {
									if (elem.val() === $("#" + ids[id]).val()) {
										$("#" + ids[id]).val("0").change();
									}
								}
							}
						}
					}
					</script>
				</div>
			</div>
		</div>
	</div>
</form>