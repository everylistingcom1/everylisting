<style>
.margin20 { margin-left:20px;}
.paddingform { padding-top: 8px; padding-bottom: 8px; }
</style>
<form>
	<div class="container-fluid">
	
    	<div class="form-check paddingform"><input type="checkbox" class="form-check-input" id="buyerproperties" onchange="manageRolesUIOnChange(); dirtyUp();"/><label class="form-check-label" for="buyerproperties">I am a <strong>buyer</strong> looking at properties.</label></div>
    	
    	<hr/>
    	
        <div class="form-group paddingform"><span style="white-space: nowrap"><div class="form-check"><input type="checkbox" class="form-check-input" id="propertyowner" onchange="manageRolesUIOptions(); manageRolesUIOnChange(); dirtyUp();"/><label class="form-check-label" for="propertyowner" onclick="manageRolesUIOptions(); dirtyUp();">I am a &nbsp;<select id="propertytype" class="form-control" onchange="manageRolesUIOptions(this); manageRolesUIOnChange(); dirtyUp();" onclick="dirtyUp(); return false;"><option value="1">for sale</option><option value="2">for rent</option><option value="3">for vacational use</option></select>&nbsp; <strong>property owner</strong>.</label></div></span></div>
    	
    		<div class="margin20">
        		<div class="form-group paddingform"><span style="white-space: nowrap"><div class="form-check"><input type="checkbox" class="form-check-input" id="commissioncheck" onchange="manageRolesUIOptions(); manageRolesUIOnChange(); dirtyUp();"/><label class="form-check-label" for="commissioncheck" onclick="manageRolesUIOptions(); manageRolesUIOnChange(); dirtyUp();">I accept other agents promotion on their network against a commission.</label></div></span></div>
        		<div id="salecommissiondiv"><select id="salecommission" class="form-control margin20 paddingform" onchange="manageRolesUIOptions(); manageRolesUIOnChange(); dirtyUp();"><option value="fixed_500">$500</option><option value="fixed_1000">$1000</option><option value="fixed_2000">$2000</option><option value="percent_1">1%</option><option value="percent_2">2%</option><option value="percent_3">3%</option><option value="percent_4">4%</option><option value="percent_5">5%</option><option value="percent_6">6%</option><option value="percent_7">7%</option><option value="percent_8">8%</option><option value="percent_9">9%</option><option value="percent_10">10%</option></select></div><div id="leasecommissiondiv"><select id="leasecommission" class="form-control margin20 paddingform" onchange="manageRolesUIOptions(); manageRolesUIOnChange(); dirtyUp();"><option value="fixed_250">$250</option><option value="fixed_500">$500</option><option value="lease_1_month">1 month rent</option><option value="lease_2_month">2 months rent</option></select></div><div id="rentcommissiondiv"><select id="rentcommission" class="form-control margin20 paddingform" onchange="manageRolesUIOptions(); manageRolesUIOnChange(); dirtyUp();"><option value="per_night_5">$5 per night</option><option value="per_night_10">$10 per night</option><option value="per_night_15">$15 per night</option><option value="percent_5">5%</option><option value="percent_10">10%</option><option value="percent_15">15%</option><option value="percent_20">20%</option><option value="percent_25">25%</option><option value="percent_30">30%</option></select></div>
    		</div>
    		
    	<hr style="margin-top: 15px;"/>
    	
    	<div class="form-check paddingform"><input type="checkbox" class="form-check-input" id="realestateagent" onchange="manageRolesUIOptions(); manageRolesUIOnChange(); dirtyUp();"/><label class="form-check-label" for="realestateagent">I am a <strong>real-estate agent</strong>.</label></div>
    
    		<div class="margin20">
        		<div class="form-group margin20">
                	<input type="text" class="form-control" id="websiteusedentry" aria-describedby="webHelp" placeholder="Enter web-site url" onkeyup="manageRolesUIOnKeyUp(); dirtyUp();">
                	<small id="webHelp" class="form-text text-muted">We will bring the attention of our artificial-intelligence process to your web-site if not already done.</small>
              	</div>
              	<div class="form-group margin20">
                	<input type="text" class="form-control" id="phoneusedentry" aria-describedby="phoneHelp" placeholder="Enter phone number used for business" onkeyup="manageRolesUIOnKeyUp(); dirtyUp();">
                	<small id="phoneHelp" class="form-text text-muted">Enter the phone number you use for business so our artificial intelligence process can find your properties.</small>
              	</div>
          	</div>
          	<div class="form-group paddingform margin20"><span style="white-space: nowrap"><div class="form-check"><input type="checkbox" class="form-check-input" id="moreproperties" onchange="manageRolesUIOptions(); manageRolesUIOnChange(); dirtyUp();"/><label class="form-check-label" for="moreproperties">I want to acquire more properties to promote at my network.</label></div></span></div>
         
        <hr/> 
          	
    	<div class="form-check paddingform"><input type="checkbox" class="form-check-input" id="influencer" onchange="manageRolesUIOptions(); manageRolesUIOnChange(); dirtyUp();"/><label class="form-check-label" for="influencer">I am an <strong>influencer on social-media</strong> interested in promoting properties to build-up some revenue.</label></div>

		<button id="userrolepagerevert" class="btn btn-secondary pull-left" onclick="rolesSettingsLoaded = false; rolesHasInit = false; ensureRolesSettingsLoaded();" disabled>Revert</button>
		<button id="userrolepagesave" class="btn btn-primary pull-right" onclick="manageRolesUISave();" disabled>Save</button>

	</div>
</form>
<script>
function dirtyUp() {
	if (!rolesHasInit) return;
	isAdvancedUserPageDirty = true;
	$('#userrolepagesave').prop("disabled", false);
	$('#userrolepagerevert').prop("disabled", false);
}
function manageRolesUIOptions(event) {
	if (($('#propertyowner').prop("checked")) && ($('#commissioncheck').prop("checked"))) {
		switch ($('#propertytype').val()) {
    		case '1':
        		$('#salecommissiondiv').show();
        		$('#leasecommissiondiv').hide();
        		$('#rentcommissiondiv').hide();
        		break
    		case '2':
        		$('#salecommissiondiv').hide();
	    		$('#leasecommissiondiv').show();
        		$('#rentcommissiondiv').hide();
        		break
    		case '3':
        		$('#salecommissiondiv').hide();
	    		$('#leasecommissiondiv').hide();
	    		$('#rentcommissiondiv').show();
        		break
		}
	} else {
		$('#salecommissiondiv').hide();
		$('#leasecommissiondiv').hide();
		$('#rentcommissiondiv').hide();
	}
	if (!$('#propertyowner').prop("checked")) {
		$('#commissioncheck').prop("disabled", true);
		$('#commissioncheck').prop("checked", false);
	} else {
		$('#commissioncheck').prop("disabled", false);
	}
	if (!$('#realestateagent').prop("checked")) {
		$('#websiteusedentry').prop("disabled", true);
		$('#phoneusedentry').prop("disabled", true);
		$('#moreproperties').prop("disabled", true);
		$('#moreproperties').prop("checked", false);
	} else {
		$('#websiteusedentry').prop("disabled", false);
		$('#phoneusedentry').prop("disabled", false);
		$('#moreproperties').prop("disabled", false);
	}
	return false;
}

function ensureRolesSettingsLoaded() {
	if (!rolesSettingsLoaded) {
		$('#advanceduseroptionsspinner').show();
		manageRolesUISelectorLoad("[onclick*='manageRolesUIOnChange()'], [onchange*='manageRolesUIOnChange()'], [onkeyup*='manageRolesUIOnKeyUp()']");
    	rolesSettingsLoaded = true;
	}
}

function manageRolesUIOnChange() {
	if (!rolesHasInit) return;
}

function manageRolesUIOnKeyUp() {
	if (!rolesHasInit) return;
}

function manageRolesUISave() {
	var locHttp = new XMLHttpRequest();
	var url = "/wp-content/themes/inspiry-real-places/inc/util/saveUserMeta.php?";
	url += manageRolesUISelectorSave("[onchange*='manageRolesUIOnChange()']");
	url += manageRolesUISelectorSave("[onkeyup*='manageRolesUIOnKeyUp()']");
	if (($('#influencer').prop("checked")) || (($('#realestateagent').prop("checked")))) {
		url += "&" + encodeURIComponent("user_role_referrer") + "=" + encodeURIComponent("true");
	}
	if ((typeof userRoles['active.agent___active'] == undefined) && ((($('#realestateagent').prop("checked")) && ($('#moreproperties').prop("checked"))) || ($('#influencer').prop("checked")))) {
		url += "&" + encodeURIComponent("user_role_agent_-_inactive") + "=" + encodeURIComponent("true");
	}
	if ($('#buyerproperties').prop("checked")) {
		url += "&" + encodeURIComponent("user_role_referee") + "=" + encodeURIComponent("true");
	}
	if ($('#propertyowner').prop("checked")) {
		if ($('#commissioncheck').prop("checked")) {
			switch ($('#propertytype').val()) {
        		case '1':
        			url += "&" + encodeURIComponent("commission_model") + "=" + encodeURIComponent($('#salecommissiondiv').val());
            		break
        		case '2':
        			url += "&" + encodeURIComponent("commission_model") + "=" + encodeURIComponent($('#leasecommissiondiv').val());
            		break
        		case '3':
        			url += "&" + encodeURIComponent("commission_model") + "=" + encodeURIComponent($('#rentcommissiondiv').val());
            		break
    		}
		} else {
			url += "&" + encodeURIComponent("commission_model") + "=" + encodeURIComponent("undefined");
		}
	} else {
		url += "&" + encodeURIComponent("commission_model") + "=" + encodeURIComponent("undefined");
	}
	locHttp.open("GET", url.replace("?&", "?"));
	locHttp.send();
	locHttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var locHttp2 = new XMLHttpRequest();
			url = "/wp-content/themes/inspiry-real-places/inc/util/getUserMeta.php?startwith=user_role_";
			locHttp2.open("GET", url.replace("?&", "?"));
			locHttp2.send();
			locHttp2.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
    				console.log("getUserMeta res: " + this.responseText);
    				userRoles = JSON.parse(this.responseText);
    				pillsShowHideCycle();
				}
			}
		}
	}
	isAdvancedUserPageDirty = false;
	$('#userrolepagesave').prop("disabled", true);
	$('#userrolepagerevert').prop("disabled", true);
}

function manageRolesUISelectorSave(selector) {
	var url = "";
	$(document).find(selector).each(function() {
		var signature = $(this).prop('tagName');
		if (($(this).prop('tagName') == "INPUT") && ($(this).attr("type") == "radio")) {
			signature = "RADIO";
		} else if (($(this).prop('tagName') == "INPUT") && ($(this).attr("type") == "checkbox")) {
			signature = "CHECKBOX";
		} else if (($(this).prop('tagName') == "INPUT") && ($(this).attr("type") == "hidden")) {
			signature = "HIDDEN";
		} else if ($(this).prop('tagName') == "SELECT") {
			signature = "SELECT";
		}
		if ($(this).attr('id') != undefined) {
    		switch (signature) {
    			case "CHECKBOX":
    			case 'RADIO':
    				url += "&" + encodeURIComponent("roles_ui_" + signature +"_" + $(this).attr('id')) + "=" + encodeURIComponent($(this).prop("checked"));
    				break;
    			default:
    				url += "&" + encodeURIComponent("roles_ui_" + signature +"_" + $(this).attr('id')) + "=" + encodeURIComponent($(this).val());
    		}
		}
	});
	return url;
}

function manageRolesUISelectorLoad(selector) {
	if ($(document).find(selector).length > 0) {
		var locHttp = new XMLHttpRequest();
		locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/getUserMeta.php?startwith=roles_ui_");
		locHttp.send();
		locHttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
		    	for (var i=0; i < $(document).find(selector).length; i++) {
					var elem = $($(document).find(selector).get(i));
					setTimeout(function(elem, responseText, islast) {
    					var signature = elem.prop('tagName');
    					if ((elem.prop('tagName') == "INPUT") && (elem.attr("type") == "radio")) {
    						signature = "RADIO";
    					} else if ((elem.prop('tagName') == "INPUT") && (elem.attr("type") == "checkbox")) {
    						signature = "CHECKBOX";
    					} else if ((elem.prop('tagName') == "INPUT") && (elem.attr("type") == "hidden")) {
    						signature = "HIDDEN";
    					} else if (elem.prop('tagName') == "SELECT") {
    						signature = "SELECT";
    					}
    					if (elem.attr('id') != undefined) {
        					var value = extractUIValue(responseText, "roles_ui_" + signature + "_" + elem.attr('id'), undefined);
        					if (value != undefined) {
        						switch (signature) {
            						case "SELECT":
            							elem.val(decodeURIComponent(value)).change();
                						break;
            						case "CHECKBOX":
            		        		case 'RADIO':
            		        			if ((value == true) || (value == "true")) {
            		        				$("#"+ elem.attr('id')).prop("checked", "true");
            		        			}
            		        			break;
            		        		default:
            		        			elem.val(decodeURIComponent(value));
            		    		}
            		    	}
    					}
    					if (islast) {
        					manageRolesUIOptions();
        					rolesHasInit = true;
        					$('#advanceduseroptionsspinner').hide();
    					}
					}, 0, elem, this.responseText, (i == $(document).find(selector).length - 1));
				}
			}
		}
    }
}
var rolesSettingsLoaded = false;
var rolesHasInit = false;
var isAdvancedUserPageDirty = false;
manageRolesUIOptions();
</script>