<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');

$issecreturl = isset($_COOKIE['secret-url-agent-id']);
$currentuser = (get_current_user_id() != 0)?wp_get_current_user():null;
if ($currentuser == null) {
    unset($currentuser);
}
// echo "currentuser: " . (isset($currentuser))?print_r($currentuser, true):"not set";
$isnewregistration = !isset($currentuser);

//Cookie for email is created here
setCookie("emailreg", $_POST['prefill-email'], time() + 60);
if (!filter_var($_POST['prefill-email'], FILTER_VALIDATE_EMAIL )) {
    echo 'ERROR: '  . 'The email entered is not Valid !!!';
} else if (($isnewregistration) && ($_POST['password'] != $_POST['password2'])) {
	echo 'ERROR: '  . 'The Passwords does not match !!!';
} else if ((($isnewregistration) && (!email_exists($_POST['prefill-email'])) && (!username_exists($_POST['prefill-user-name']))) || (get_current_user_id() != 0)) {
	$WP_array = array (
		'user_login'    =>  $_POST['prefill-user-name'],
		'user_email'    =>  $_POST['prefill-email'],
	    'display_name'  =>  $_POST['prefill-display-name'],
		'first_name'    =>  $_POST['prefill-first-name'],
		'last_name'     =>  $_POST['prefill-last-name'],
		'nickname'      =>  $_POST['prefill-display-name'],
		'description'   =>  $_POST['prefill-bio-text']
	);
	if ($isnewregistration) {
	    $WP_array['user_pass'] = $_POST['password'];
		$id = wp_insert_user($WP_array);
		wp_update_user(array('ID' => $id, 'role' => 'agent_-_inactive'));
		$fuser = get_userdata($id);
		$fuser->add_role('subscriber');	
	} else {
		$id = get_current_user_id();
		array_push($WP_array, 'ID', $id);
	    wp_update_user($WP_array);
	}
	update_user_meta($id, 'first_name', $_POST['prefill-first-name']);
	update_user_meta($id, 'last_name', $_POST['prefill-last-name']);
	update_user_meta($id, 'display_name', $_POST['prefill-display-name']);
	update_user_meta($id, 'user_login', $_POST['prefill-user-name']);
	update_user_meta($id, 'user_email', $_POST['prefill-email']);
	update_user_meta($id, 'nickname', $_POST['prefill-display-name']);
	update_user_meta($id, 'description', $_POST['prefill-bio-text']);
	update_user_meta($id, 'mobile_number', $_POST['prefill-mobile-number']);
	update_user_meta($id, 'office_number', $_POST['prefill-office-number']);
	update_user_meta($id, 'REAL_HOMES_agent_email', $_POST['prefill-email']);
	update_user_meta($id, 'facebook', $_POST['prefill-facebook-url']);
	update_user_meta($id, 'twitter', $_POST['prefill-twitter-url']);
	update_user_meta($id, 'linkedin', $_POST['prefill-linkedin-url']);
	update_user_meta($id, 'instagram', $_POST['prefill-instagram-url']);
	update_user_meta($id, 'facebook_url', $_POST['prefill-facebook-url']);
	update_user_meta($id, 'twitter_url', $_POST['prefill-twitter-url']);
	update_user_meta($id, 'linkedin_url', $_POST['prefill-linkedin-url']);
	update_user_meta($id, 'instagram_url', $_POST['prefill-instagram-url']);
	update_user_meta($id, 'user_id', get_current_user_id());
	
	if ($issecreturl) {
	    update_user_meta($id, "agent_id", $_COOKIE['secret-url-agent-id']);
	    $postmeta = get_post_meta($_COOKIE['secret-url-agent-id']);
	    if (isset($postmeta['IDXGenerator_realEstateBrokerId'][0])) {
	        update_user_meta($id, "IDXGenerator_realEstateBrokerId", $postmeta['IDXGenerator_realEstateBrokerId'][0]);
	    }
	}
	echo 'OK';
} else if ($isnewregistration) {  
    echo 'ERROR: '  . 'The email or username already exists !!!';
}	
?>