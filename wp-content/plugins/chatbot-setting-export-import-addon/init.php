<?php
/**
 * Plugin Name: Chatbot Settings Export Import
 * Plugin URI: https://wordpress.org/plugins/woowbot-woocommerce-chatbot/
 * Description: An addon plugin for chatbot settings export import.
 * Donate link: http://www.quantumcloud.com
 * Version: 0.9.0
 * @author    QuantumCloud
 * Author: QunatumCloud
 * Author URI: https://www.quantumcloud.com/
 * Requires at least: 4.6
 * Tested up to: 5.3
 * Text Domain: qccsi
 * Domain Path: /languages
 * License: GPL2
 */
 
if (!defined('ABSPATH')) exit; // Exit if accessed directly
if (!defined('QCLD_WPSEI_VERSION')) define('QCLD_WPSEI_VERSION', '1.0.0');
if (!defined('QCLD_WPSEI_VERSION')) define('QCLD_WPSEI_VERSION', '1.0.0');
if (!defined('QCLD_WPSEI_PLUGIN_DIR_PATH')) define('QCLD_WPSEI_PLUGIN_DIR_PATH', plugin_dir_path(__FILE__));
if (!defined('QCLD_WPSEI_PLUGIN_URL')) define('QCLD_WPSEI_PLUGIN_URL', plugin_dir_url(__FILE__));

/**
 *
 * Function to load translation files.
 *
 */
function qc_wpsei_addon_lang_init() {
    load_plugin_textdomain( 'qccsi', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'qc_wpsei_addon_lang_init');


add_action( 'admin_menu', 'wpsei_admin_menu');

function wpsei_admin_menu(){
	if ( current_user_can( 'publish_posts' ) ){
		add_menu_page( 'Bot - Settings Export/Import', 'Bot - Settings Export/Import', 'manage_options', 'wpsei-settings-export-import', 'qc_wpsei_export_callback', '
dashicons-controls-repeat', '9' );
	}
}

function qc_wpsei_export_callback(){
?>
<div class="qchero_sliders_list_wrapper">
	<div class="sld_menu_title">
		<h2><?php echo esc_html__('Export/Import All Settings for Chatbot & Bot Woocommerce', 'qccsi') ?></h2>
	</div>
		<br>
		<div class="content">
			<h4><?php echo esc_html__('Export Settings', 'qccsi') ?></h4>
			<p>
				<?php echo esc_html('Export button will create a downloadable CSV file with all of your existing settings for Chatbot & Bot Woocommerce.', 'qccsi'); ?>
			</p>
			<a class="button-primary" href="<?php echo admin_url( 'admin-post.php?action=wpwbotsettings.csv' ); ?>"><?php echo esc_html__('Export Settings', 'qccsi') ?></a>
			
			<div>
			<br>
			<h4>
				<?php echo esc_html__('Upload your backup settings CSV file', 'qccsi'); ?>
			</h4>

			<form name="uploadfile" id="uploadfile_form" method="POST" enctype="multipart/form-data" action="" accept-charset="utf-8">
				
				<p>
					<?php echo esc_html__('Select file to upload', 'qccsi') ?>
					<input type="file" name="wp_csv_upload" id="csv_upload" size="35" class="uploadfiles" required/>
				</p>
				
				<p>
					<input class="button-primary" type="submit" name="wp_upload_csv" id="" value="<?php echo esc_html__('Import Settings', 'qccsi') ?>"/>

				</p>

			</form>				
			<?php if(isset($_GET['msg']) && $_GET['msg']=='success'): ?>
			<p style="color:green;font-weight:bold;"><?php echo esc_html__('Settings imported successfully!', 'qccsi'); ?></p>
			<?php endif; ?>
			</div>
		</div>
</div>
<?php
}

//===========settings export ==============//

function qc_wpsei_download_send_headers($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    /*header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");*/

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}

function qc_wpsei_array2csv(array &$array)
{
   if (count($array) == 0) {
     return null;
   }

   ob_start();

   $df = fopen("php://output", 'w');

   $titles = array('key', 'value');

   fputcsv($df, $titles);

   foreach ($array as $row) {
      fputcsv($df, $row);
   }

   fclose($df);

   return ob_get_clean();
}

add_action( 'admin_post_wpwbotsettings.csv', 'qc_wpsei_export_print_csv' );

function qc_wpsei_export_print_csv()
{    
	$childArray = array();
	$childArray[0] = array('key'=>'disable_wp_chatbot', 'value'=>get_option('disable_wp_chatbot'));
	$childArray[1] = array('key'=>'disable_wp_no_image_product', 'value'=>get_option('disable_wp_no_image_product'));
	$childArray[2] = array('key'=>'disable_wp_chatbot_icon_animation', 'value'=>get_option('disable_wp_chatbot_icon_animation'));
	$childArray[3] = array('key'=>'disable_wp_chatbot_on_mobile', 'value'=>get_option('disable_wp_chatbot_on_mobile'));
	$childArray[4] = array('key'=>'skip_wp_greetings', 'value'=>get_option('skip_wp_greetings'));
	$childArray[5] = array('key'=>'ask_email_wp_greetings', 'value'=>get_option('ask_email_wp_greetings'));
	$childArray[6] = array('key'=>'limit_msg_show', 'value'=>get_option('limit_msg_show'));
	$childArray[7] = array('key'=>'enable_wp_chatbot_gdpr_compliance', 'value'=>get_option('enable_wp_chatbot_gdpr_compliance'));
	$childArray[8] = array('key'=>'woobot_gdpr_text', 'value'=>get_option('woobot_gdpr_text'));
	$childArray[9] = array('key'=>'disable_wp_chatbot_product_search', 'value'=>get_option('disable_wp_chatbot_product_search'));
	$childArray[10] = array('key'=>'disable_wp_chatbot_catalog', 'value'=>get_option('disable_wp_chatbot_catalog'));
	$childArray[11] = array('key'=>'disable_wp_chatbot_order_status', 'value'=>get_option('disable_wp_chatbot_order_status'));
	$childArray[12] = array('key'=>'disable_wp_chatbot_support', 'value'=>get_option('disable_wp_chatbot_support'));
	$childArray[13] = array('key'=>'disable_wp_chatbot_notification', 'value'=>get_option('disable_wp_chatbot_notification'));
	$childArray[14] = array('key'=>'disable_wp_chatbot_notification_mobile', 'value'=>get_option('disable_wp_chatbot_notification_mobile'));
	$childArray[15] = array('key'=>'enable_wp_chatbot_rtl', 'value'=>get_option('enable_wp_chatbot_rtl'));
	$childArray[16] = array('key'=>'enable_wp_chatbot_disable_allicon', 'value'=>get_option('enable_wp_chatbot_disable_allicon'));
	$childArray[17] = array('key'=>'enable_wp_chatbot_disable_helpicon', 'value'=>get_option('enable_wp_chatbot_disable_helpicon'));
	$childArray[18] = array('key'=>'enable_wp_chatbot_disable_producticon', 'value'=>get_option('enable_wp_chatbot_disable_producticon'));
	$childArray[19] = array('key'=>'enable_wp_chatbot_disable_carticon', 'value'=>get_option('enable_wp_chatbot_disable_carticon'));
	$childArray[20] = array('key'=>'enable_wp_chatbot_disable_supporticon', 'value'=>get_option('enable_wp_chatbot_disable_supporticon'));
	$childArray[21] = array('key'=>'enable_wp_chatbot_disable_chaticon', 'value'=>get_option('enable_wp_chatbot_disable_chaticon'));
	$childArray[22] = array('key'=>'enable_wp_chatbot_mobile_full_screen', 'value'=>get_option('enable_wp_chatbot_mobile_full_screen'));
	$childArray[23] = array('key'=>'disable_wp_chatbot_cart_item_number', 'value'=>get_option('disable_wp_chatbot_cart_item_number'));
	$childArray[24] = array('key'=>'disable_wp_chatbot_featured_product', 'value'=>get_option('disable_wp_chatbot_featured_product'));
	$childArray[25] = array('key'=>'disable_wp_chatbot_sale_product', 'value'=>get_option('disable_wp_chatbot_sale_product'));
	$childArray[26] = array('key'=>'wp_chatbot_open_product_detail', 'value'=>get_option('wp_chatbot_open_product_detail'));
	$childArray[27] = array('key'=>'qlcd_wp_chatbot_product_orderby', 'value'=>get_option('qlcd_wp_chatbot_product_orderby'));
	$childArray[28] = array('key'=>'qlcd_wp_chatbot_product_order', 'value'=>get_option('qlcd_wp_chatbot_product_order'));
	$childArray[29] = array('key'=>'qlcd_wp_chatbot_ppp', 'value'=>get_option('qlcd_wp_chatbot_ppp'));
	$childArray[30] = array('key'=>'wp_chatbot_show_parent_category', 'value'=>get_option('wp_chatbot_show_parent_category'));
	$childArray[31] = array('key'=>'wp_chatbot_show_sub_category', 'value'=>get_option('wp_chatbot_show_sub_category'));
	$childArray[32] = array('key'=>'wp_chatbot_exclude_stock_out_product', 'value'=>get_option('wp_chatbot_exclude_stock_out_product'));
	$childArray[33] = array('key'=>'wp_chatbot_hide_product_details_add_to_cart', 'value'=>get_option('wp_chatbot_hide_product_details_add_to_cart'));
	$childArray[34] = array('key'=>'wp_chatbot_show_home_page', 'value'=>get_option('wp_chatbot_show_home_page'));
	$childArray[35] = array('key'=>'wp_chatbot_show_posts', 'value'=>get_option('wp_chatbot_show_posts'));
	$childArray[36] = array('key'=>'wp_chatbot_show_pages', 'value'=>get_option('wp_chatbot_show_pages'));
	$childArray[37] = array('key'=>'wp_chatbot_exitintent_show_pages', 'value'=>get_option('wp_chatbot_exitintent_show_pages'));
	$childArray[38] = array('key'=>'wp_chatbot_show_pages_list', 'value'=>get_option('wp_chatbot_show_pages_list'));
	$childArray[39] = array('key'=>'wp_chatbot_exclude_post_list', 'value'=>get_option('wp_chatbot_exclude_post_list'));
	$childArray[40] = array('key'=>'wp_chatbot_exclude_pages_list', 'value'=>get_option('wp_chatbot_exclude_pages_list'));
	$childArray[41] = array('key'=>'wp_chatbot_exitintent_show_pages_list', 'value'=>get_option('wp_chatbot_exitintent_show_pages_list'));
	$childArray[42] = array('key'=>'wp_chatbot_show_woocommerce', 'value'=>get_option('wp_chatbot_show_woocommerce'));
	$childArray[43] = array('key'=>'wp_chatbot_show_cart', 'value'=>get_option('wp_chatbot_show_cart'));
	$childArray[44] = array('key'=>'wp_chatbot_show_checkout', 'value'=>get_option('wp_chatbot_show_checkout'));
	$childArray[45] = array('key'=>'qlcd_wp_chatbot_stop_words_name', 'value'=>get_option('qlcd_wp_chatbot_stop_words_name'));
	$childArray[46] = array('key'=>'qlcd_wp_chatbot_stop_words', 'value'=>get_option('qlcd_wp_chatbot_stop_words'));
	$childArray[47] = array('key'=>'qlcd_wp_chatbot_order_user', 'value'=>get_option('qlcd_wp_chatbot_order_user'));
	$childArray[48] = array('key'=>'wp_chatbot_icon', 'value'=>get_option('wp_chatbot_icon'));
	$childArray[49] = array('key'=>'wp_chatbot_agent_image', 'value'=>get_option('wp_chatbot_agent_image'));
	$childArray[50] = array('key'=>'qcld_wp_chatbot_theme', 'value'=>get_option('qcld_wp_chatbot_theme'));
	$childArray[51] = array('key'=>'qcld_wp_chatbot_custom_icons', 'value'=>get_option('qcld_wp_chatbot_custom_icons'));
	$childArray[52] = array('key'=>'qcld_wp_chatbot_custom_icon_help', 'value'=>get_option('qcld_wp_chatbot_custom_icon_help'));
	$childArray[53] = array('key'=>'qcld_wp_chatbot_custom_icon_support', 'value'=>get_option('qcld_wp_chatbot_custom_icon_support'));
	$childArray[54] = array('key'=>'qcld_wp_chatbot_custom_icon_recent', 'value'=>get_option('qcld_wp_chatbot_custom_icon_recent'));
	$childArray[55] = array('key'=>'qcld_wp_chatbot_custom_icon_cart', 'value'=>get_option('qcld_wp_chatbot_custom_icon_cart'));
	$childArray[56] = array('key'=>'qcld_wp_chatbot_custom_icon_chat', 'value'=>get_option('qcld_wp_chatbot_custom_icon_chat'));
	$childArray[57] = array('key'=>'qcld_wp_chatbot_change_bg', 'value'=>get_option('qcld_wp_chatbot_change_bg'));
	$childArray[58] = array('key'=>'wp_chatbot_custom_css', 'value'=>get_option('wp_chatbot_custom_css'));
	$childArray[59] = array('key'=>'qlcd_wp_chatbot_host', 'value'=>get_option('qlcd_wp_chatbot_host'));
	$childArray[60] = array('key'=>'qlcd_wp_chatbot_agent', 'value'=>get_option('qlcd_wp_chatbot_agent'));
	$childArray[61] = array('key'=>'qlcd_wp_chatbot_yes', 'value'=>get_option('qlcd_wp_chatbot_yes'));
	$childArray[62] = array('key'=>'qlcd_wp_chatbot_no', 'value'=>get_option('qlcd_wp_chatbot_no'));
	$childArray[63] = array('key'=>'qlcd_wp_chatbot_or', 'value'=>get_option('qlcd_wp_chatbot_or'));
	$childArray[64] = array('key'=>'qlcd_wp_chatbot_hello', 'value'=>get_option('qlcd_wp_chatbot_hello'));
	$childArray[65] = array('key'=>'qlcd_wp_chatbot_sorry', 'value'=>get_option('qlcd_wp_chatbot_sorry'));
	$childArray[66] = array('key'=>'qlcd_wp_chatbot_chat', 'value'=>get_option('qlcd_wp_chatbot_chat'));
	$childArray[67] = array('key'=>'qlcd_wp_chatbot_agent_join', 'value'=>get_option('qlcd_wp_chatbot_agent_join'));
	$childArray[68] = array('key'=>'qlcd_wp_chatbot_welcome', 'value'=>get_option('qlcd_wp_chatbot_welcome'));
	$childArray[69] = array('key'=>'qlcd_wp_chatbot_back_to_start', 'value'=>get_option('qlcd_wp_chatbot_back_to_start'));
	$childArray[70] = array('key'=>'qlcd_wp_chatbot_hi_there', 'value'=>get_option('qlcd_wp_chatbot_hi_there'));
	$childArray[71] = array('key'=>'qlcd_wp_chatbot_welcome_back', 'value'=>get_option('qlcd_wp_chatbot_welcome_back'));
	$childArray[72] = array('key'=>'qlcd_wp_chatbot_asking_name', 'value'=>get_option('qlcd_wp_chatbot_asking_name'));
	$childArray[73] = array('key'=>'qlcd_wp_chatbot_asking_emailaddress', 'value'=>get_option('qlcd_wp_chatbot_asking_emailaddress'));
	$childArray[74] = array('key'=>'qlcd_wp_chatbot_got_email', 'value'=>get_option('qlcd_wp_chatbot_got_email'));
	$childArray[75] = array('key'=>'qlcd_wp_chatbot_email_ignore', 'value'=>get_option('qlcd_wp_chatbot_email_ignore'));
	$childArray[76] = array('key'=>'qlcd_wp_email_subscription', 'value'=>get_option('qlcd_wp_email_subscription'));
	$childArray[77] = array('key'=>'do_you_want_to_subscribe', 'value'=>get_option('do_you_want_to_subscribe'));
	$childArray[78] = array('key'=>'do_you_want_to_unsubscribe', 'value'=>get_option('do_you_want_to_unsubscribe'));
	$childArray[79] = array('key'=>'you_have_successfully_unsubscribe', 'value'=>get_option('you_have_successfully_unsubscribe'));
	$childArray[80] = array('key'=>'we_do_not_have_your_email', 'value'=>get_option('we_do_not_have_your_email'));
	$childArray[81] = array('key'=>'qlcd_wp_email_subscription_success', 'value'=>get_option('qlcd_wp_email_subscription_success'));
	$childArray[82] = array('key'=>'qlcd_wp_email_already_subscribe', 'value'=>get_option('qlcd_wp_email_already_subscribe'));
	$childArray[83] = array('key'=>'qlcd_wp_chatbot_name_greeting', 'value'=>get_option('qlcd_wp_chatbot_name_greeting'));
	$childArray[84] = array('key'=>'qlcd_wp_chatbot_i_am', 'value'=>get_option('qlcd_wp_chatbot_i_am'));
	$childArray[85] = array('key'=>'qlcd_wp_chatbot_wildcard_msg', 'value'=>get_option('qlcd_wp_chatbot_wildcard_msg'));
	$childArray[86] = array('key'=>'qlcd_wp_chatbot_empty_filter_msg', 'value'=>get_option('qlcd_wp_chatbot_empty_filter_msg'));
	$childArray[87] = array('key'=>'qlcd_wp_chatbot_wildcard_product', 'value'=>get_option('qlcd_wp_chatbot_wildcard_product'));
	$childArray[88] = array('key'=>'qlcd_wp_chatbot_wildcard_catalog', 'value'=>get_option('qlcd_wp_chatbot_wildcard_catalog'));
	$childArray[89] = array('key'=>'qlcd_wp_chatbot_featured_products', 'value'=>get_option('qlcd_wp_chatbot_featured_products'));
	$childArray[90] = array('key'=>'qlcd_wp_chatbot_sale_products', 'value'=>get_option('qlcd_wp_chatbot_sale_products'));
	$childArray[91] = array('key'=>'qlcd_wp_chatbot_wildcard_support', 'value'=>get_option('qlcd_wp_chatbot_wildcard_support'));
	$childArray[92] = array('key'=>'qlcd_wp_chatbot_messenger_label', 'value'=>get_option('qlcd_wp_chatbot_messenger_label'));
	$childArray[93] = array('key'=>'qlcd_wp_chatbot_product_success', 'value'=>get_option('qlcd_wp_chatbot_product_success'));
	$childArray[94] = array('key'=>'qlcd_wp_chatbot_product_fail', 'value'=>get_option('qlcd_wp_chatbot_product_fail'));
	$childArray[95] = array('key'=>'qlcd_wp_chatbot_product_asking', 'value'=>get_option('qlcd_wp_chatbot_product_asking'));
	$childArray[96] = array('key'=>'qlcd_wp_chatbot_product_suggest', 'value'=>get_option('qlcd_wp_chatbot_product_suggest'));
	$childArray[97] = array('key'=>'qlcd_wp_chatbot_product_infinite', 'value'=>get_option('qlcd_wp_chatbot_product_infinite'));
	$childArray[98] = array('key'=>'qlcd_wp_chatbot_load_more', 'value'=>get_option('qlcd_wp_chatbot_load_more'));
	$childArray[99] = array('key'=>'qlcd_wp_chatbot_wildcard_order', 'value'=>get_option('qlcd_wp_chatbot_wildcard_order'));
	$childArray[100] = array('key'=>'qlcd_wp_chatbot_order_welcome', 'value'=>get_option('qlcd_wp_chatbot_order_welcome'));
	$childArray[101] = array('key'=>'qlcd_wp_chatbot_order_username_asking', 'value'=>get_option('qlcd_wp_chatbot_order_username_asking'));
	$childArray[102] = array('key'=>'qlcd_wp_chatbot_order_username_password', 'value'=>get_option('qlcd_wp_chatbot_order_username_password'));
	$childArray[103] = array('key'=>'qlcd_wp_chatbot_support_welcome', 'value'=>get_option('qlcd_wp_chatbot_support_welcome'));
	$childArray[104] = array('key'=>'qlcd_wp_chatbot_support_email', 'value'=>get_option('qlcd_wp_chatbot_support_email'));
	$childArray[105] = array('key'=>'qlcd_wp_chatbot_asking_email', 'value'=>get_option('qlcd_wp_chatbot_asking_email'));
	$childArray[106] = array('key'=>'qlcd_wp_chatbot_asking_msg', 'value'=>get_option('qlcd_wp_chatbot_asking_msg'));
	$childArray[107] = array('key'=>'qlcd_wp_chatbot_admin_email', 'value'=>get_option('qlcd_wp_chatbot_admin_email'));
	$childArray[108] = array('key'=>'qlcd_wp_chatbot_email_sub', 'value'=>get_option('qlcd_wp_chatbot_email_sub'));
	$childArray[109] = array('key'=>'qlcd_wp_chatbot_email_sent', 'value'=>get_option('qlcd_wp_chatbot_email_sent'));
	$childArray[110] = array('key'=>'qlcd_wp_chatbot_support_phone', 'value'=>get_option('qlcd_wp_chatbot_support_phone'));
	$childArray[111] = array('key'=>'qlcd_wp_chatbot_asking_phone', 'value'=>get_option('qlcd_wp_chatbot_asking_phone'));
	$childArray[112] = array('key'=>'qlcd_wp_chatbot_thank_for_phone', 'value'=>get_option('qlcd_wp_chatbot_thank_for_phone'));
	$childArray[113] = array('key'=>'qlcd_wp_chatbot_sys_key_help', 'value'=>get_option('qlcd_wp_chatbot_sys_key_help'));
	$childArray[114] = array('key'=>'qlcd_wp_email_unsubscription', 'value'=>get_option('qlcd_wp_email_unsubscription'));
	$childArray[115] = array('key'=>'qlcd_wp_chatbot_sys_key_product', 'value'=>get_option('qlcd_wp_chatbot_sys_key_product'));
	$childArray[116] = array('key'=>'qlcd_wp_chatbot_sys_key_catalog', 'value'=>get_option('qlcd_wp_chatbot_sys_key_catalog'));
	$childArray[117] = array('key'=>'qlcd_wp_chatbot_sys_key_order', 'value'=>get_option('qlcd_wp_chatbot_sys_key_order'));
	$childArray[118] = array('key'=>'qlcd_wp_chatbot_sys_key_support', 'value'=>get_option('qlcd_wp_chatbot_sys_key_support'));
	$childArray[119] = array('key'=>'qlcd_wp_chatbot_sys_key_reset', 'value'=>get_option('qlcd_wp_chatbot_sys_key_reset'));
	$childArray[120] = array('key'=>'qlcd_wp_chatbot_order_username_not_exist', 'value'=>get_option('qlcd_wp_chatbot_order_username_not_exist'));
	$childArray[121] = array('key'=>'qlcd_wp_chatbot_order_username_thanks', 'value'=>get_option('qlcd_wp_chatbot_order_username_thanks'));
	$childArray[122] = array('key'=>'qlcd_wp_chatbot_order_password_incorrect', 'value'=>get_option('qlcd_wp_chatbot_order_password_incorrect'));
	$childArray[123] = array('key'=>'qlcd_wp_chatbot_order_not_found', 'value'=>get_option('qlcd_wp_chatbot_order_not_found'));
	$childArray[124] = array('key'=>'qlcd_wp_chatbot_order_found', 'value'=>get_option('qlcd_wp_chatbot_order_found'));
	$childArray[125] = array('key'=>'qlcd_wp_chatbot_order_email_support', 'value'=>get_option('qlcd_wp_chatbot_order_email_support'));
	$childArray[126] = array('key'=>'qlcd_wp_chatbot_support_option_again', 'value'=>get_option('qlcd_wp_chatbot_support_option_again'));
	$childArray[127] = array('key'=>'qlcd_wp_chatbot_invalid_email', 'value'=>get_option('qlcd_wp_chatbot_invalid_email'));
	$childArray[128] = array('key'=>'qlcd_wp_chatbot_shopping_cart', 'value'=>get_option('qlcd_wp_chatbot_shopping_cart'));
	$childArray[129] = array('key'=>'qlcd_wp_chatbot_add_to_cart', 'value'=>get_option('qlcd_wp_chatbot_add_to_cart'));
	$childArray[130] = array('key'=>'qlcd_wp_chatbot_cart_link', 'value'=>get_option('qlcd_wp_chatbot_cart_link'));
	$childArray[131] = array('key'=>'qlcd_wp_chatbot_checkout_link', 'value'=>get_option('qlcd_wp_chatbot_checkout_link'));
	$childArray[132] = array('key'=>'qlcd_wp_chatbot_cart_welcome', 'value'=>get_option('qlcd_wp_chatbot_cart_welcome'));
	$childArray[133] = array('key'=>'qlcd_wp_chatbot_featured_product_welcome', 'value'=>get_option('qlcd_wp_chatbot_featured_product_welcome'));
	$childArray[134] = array('key'=>'qlcd_wp_chatbot_viewed_product_welcome', 'value'=>get_option('qlcd_wp_chatbot_viewed_product_welcome'));
	$childArray[135] = array('key'=>'qlcd_wp_chatbot_latest_product_welcome', 'value'=>get_option('qlcd_wp_chatbot_latest_product_welcome'));
	$childArray[136] = array('key'=>'qlcd_wp_chatbot_cart_title', 'value'=>get_option('qlcd_wp_chatbot_cart_title'));
	$childArray[137] = array('key'=>'qlcd_wp_chatbot_cart_quantity', 'value'=>get_option('qlcd_wp_chatbot_cart_quantity'));
	$childArray[138] = array('key'=>'qlcd_wp_chatbot_cart_price', 'value'=>get_option('qlcd_wp_chatbot_cart_price'));
	$childArray[139] = array('key'=>'qlcd_wp_chatbot_cart_total', 'value'=>get_option('qlcd_wp_chatbot_cart_total'));
	$childArray[140] = array('key'=>'qlcd_wp_chatbot_no_cart_items', 'value'=>get_option('qlcd_wp_chatbot_no_cart_items'));
	$childArray[141] = array('key'=>'qlcd_wp_chatbot_cart_updating', 'value'=>get_option('qlcd_wp_chatbot_cart_updating'));
	$childArray[142] = array('key'=>'qlcd_wp_chatbot_cart_removing', 'value'=>get_option('qlcd_wp_chatbot_cart_removing'));
	$childArray[143] = array('key'=>'qlcd_wp_chatbot_email_fail', 'value'=>get_option('qlcd_wp_chatbot_email_fail'));
	$childArray[144] = array('key'=>'support_query', 'value'=>get_option('support_query'));
	$childArray[145] = array('key'=>'support_ans', 'value'=>get_option('support_ans'));
	$childArray[146] = array('key'=>'qlcd_wp_chatbot_notification_interval', 'value'=>get_option('qlcd_wp_chatbot_notification_interval'));
	$childArray[147] = array('key'=>'qlcd_wp_chatbot_notifications', 'value'=>get_option('qlcd_wp_chatbot_notifications'));
	$childArray[148] = array('key'=>'qlcd_wp_chatbot_search_option', 'value'=>get_option('qlcd_wp_chatbot_search_option'));
	$childArray[149] = array('key'=>'wp_chatbot_index_count', 'value'=>get_option('wp_chatbot_index_count'));
	$childArray[150] = array('key'=>'wp_chatbot_app_pages', 'value'=>get_option('wp_chatbot_app_pages'));
	$childArray[151] = array('key'=>'enable_wp_chatbot_custom_search', 'value'=>get_option('enable_wp_chatbot_custom_search'));
	$childArray[152] = array('key'=>'enable_wp_chatbot_custom_intent', 'value'=>get_option('enable_wp_chatbot_custom_intent'));
	$childArray[153] = array('key'=>'enable_wp_chatbot_rich_response', 'value'=>get_option('enable_wp_chatbot_rich_response'));
	$childArray[154] = array('key'=>'enable_wp_chatbot_messenger', 'value'=>get_option('enable_wp_chatbot_messenger'));
	$childArray[155] = array('key'=>'enable_wp_chatbot_messenger_floating_icon', 'value'=>get_option('enable_wp_chatbot_messenger_floating_icon'));
	$childArray[156] = array('key'=>'qlcd_wp_chatbot_fb_app_id', 'value'=>get_option('qlcd_wp_chatbot_fb_app_id'));
	$childArray[157] = array('key'=>'qlcd_wp_chatbot_fb_page_id', 'value'=>get_option('qlcd_wp_chatbot_fb_page_id'));
	$childArray[158] = array('key'=>'qlcd_wp_chatbot_fb_color', 'value'=>get_option('qlcd_wp_chatbot_fb_color'));
	$childArray[159] = array('key'=>'qlcd_wp_chatbot_fb_in_msg', 'value'=>get_option('qlcd_wp_chatbot_fb_in_msg'));
	$childArray[160] = array('key'=>'qlcd_wp_chatbot_fb_out_msg', 'value'=>get_option('qlcd_wp_chatbot_fb_out_msg'));
	$childArray[161] = array('key'=>'enable_wp_chatbot_skype_floating_icon', 'value'=>get_option('enable_wp_chatbot_skype_floating_icon'));
	$childArray[162] = array('key'=>'enable_wp_chatbot_skype_id', 'value'=>get_option('enable_wp_chatbot_skype_id'));
	$childArray[163] = array('key'=>'enable_wp_chatbot_whats', 'value'=>get_option('enable_wp_chatbot_whats'));
	$childArray[164] = array('key'=>'qlcd_wp_chatbot_whats_label', 'value'=>get_option('qlcd_wp_chatbot_whats_label'));
	$childArray[165] = array('key'=>'enable_wp_chatbot_floating_whats', 'value'=>get_option('enable_wp_chatbot_floating_whats'));
	$childArray[166] = array('key'=>'qlcd_wp_chatbot_whats_num', 'value'=>get_option('qlcd_wp_chatbot_whats_num'));
	$childArray[167] = array('key'=>'enable_wp_chatbot_floating_viber', 'value'=>get_option('enable_wp_chatbot_floating_viber'));
	$childArray[168] = array('key'=>'qlcd_wp_chatbot_viber_acc', 'value'=>get_option('qlcd_wp_chatbot_viber_acc'));
	$childArray[169] = array('key'=>'enable_wp_chatbot_floating_phone', 'value'=>get_option('enable_wp_chatbot_floating_phone'));
	$childArray[170] = array('key'=>'qlcd_wp_chatbot_phone', 'value'=>get_option('qlcd_wp_chatbot_phone'));
	$childArray[171] = array('key'=>'enable_wp_chatbot_floating_link', 'value'=>get_option('enable_wp_chatbot_floating_link'));
	$childArray[172] = array('key'=>'qlcd_wp_chatbot_weblink', 'value'=>get_option('qlcd_wp_chatbot_weblink'));
	$childArray[173] = array('key'=>'enable_wp_chatbot_floating_livechat', 'value'=>get_option('enable_wp_chatbot_floating_livechat'));
	$childArray[174] = array('key'=>'enable_wp_custom_intent_livechat_button', 'value'=>get_option('enable_wp_custom_intent_livechat_button'));
	$childArray[175] = array('key'=>'qlcd_wp_chatbot_livechatlink', 'value'=>get_option('qlcd_wp_chatbot_livechatlink'));
	$childArray[176] = array('key'=>'qlcd_wp_livechat_button_label', 'value'=>get_option('qlcd_wp_livechat_button_label'));
	$childArray[177] = array('key'=>'wp_custom_icon_livechat', 'value'=>get_option('wp_custom_icon_livechat'));
	$childArray[178] = array('key'=>'qlcd_wp_chatbot_ret_greet', 'value'=>get_option('qlcd_wp_chatbot_ret_greet'));
	$childArray[179] = array('key'=>'enable_wp_chatbot_exit_intent', 'value'=>get_option('enable_wp_chatbot_exit_intent'));
	$childArray[180] = array('key'=>'wp_chatbot_exit_intent_msg', 'value'=>get_option('enable_wp_chatbot_exit_intent'));
	$childArray[181] = array('key'=>'wp_chatbot_exit_intent_once', 'value'=>get_option('wp_chatbot_exit_intent_once'));
	$childArray[182] = array('key'=>'enable_wp_chatbot_scroll_open', 'value'=>get_option('enable_wp_chatbot_scroll_open'));
	$childArray[183] = array('key'=>'wp_chatbot_scroll_open_msg', 'value'=>get_option('wp_chatbot_scroll_open_msg'));
	$childArray[184] = array('key'=>'wp_chatbot_scroll_percent', 'value'=>get_option('wp_chatbot_scroll_percent'));
	$childArray[185] = array('key'=>'wp_chatbot_scroll_once', 'value'=>get_option('wp_chatbot_scroll_once'));
	$childArray[186] = array('key'=>'enable_wp_chatbot_auto_open', 'value'=>get_option('enable_wp_chatbot_auto_open'));
	$childArray[187] = array('key'=>'enable_wp_chatbot_ret_sound', 'value'=>get_option('enable_wp_chatbot_ret_sound'));
	$childArray[188] = array('key'=>'enable_wp_chatbot_sound_initial', 'value'=>get_option('enable_wp_chatbot_sound_initial'));
	$childArray[189] = array('key'=>'disable_wp_chatbot_feedback', 'value'=>get_option('disable_wp_chatbot_feedback'));
	$childArray[190] = array('key'=>'disable_email_subscription', 'value'=>get_option('disable_email_subscription'));
	$childArray[191] = array('key'=>'qlcd_wp_chatbot_feedback_label', 'value'=>get_option('qlcd_wp_chatbot_feedback_label'));
	$childArray[192] = array('key'=>'enable_wp_chatbot_meta_title', 'value'=>get_option('enable_wp_chatbot_meta_title'));
	$childArray[193] = array('key'=>'qlcd_wp_chatbot_meta_label', 'value'=>get_option('qlcd_wp_chatbot_meta_label'));
	$childArray[194] = array('key'=>'wp_chatbot_auto_open_msg', 'value'=>get_option('wp_chatbot_auto_open_msg'));
	$childArray[195] = array('key'=>'wp_chatbot_auto_open_time', 'value'=>get_option('wp_chatbot_auto_open_time'));
	$childArray[196] = array('key'=>'wp_chatbot_auto_open_once', 'value'=>get_option('wp_chatbot_auto_open_once'));
	$childArray[197] = array('key'=>'wp_chatbot_inactive_once', 'value'=>get_option('wp_chatbot_inactive_once'));
	$childArray[198] = array('key'=>'wp_chatbot_proactive_bg_color', 'value'=>get_option('wp_chatbot_proactive_bg_color'));
	$childArray[199] = array('key'=>'wp_chatbot_proactive_text_color', 'value'=>get_option('wp_chatbot_proactive_text_color'));
	$childArray[200] = array('key'=>'wp_chatbot_bot_msg_bg_color', 'value'=>get_option('wp_chatbot_bot_msg_bg_color'));
	$childArray[201] = array('key'=>'wp_chatbot_bot_msg_text_color', 'value'=>get_option('wp_chatbot_bot_msg_text_color'));
	$childArray[202] = array('key'=>'wp_chatbot_user_msg_bg_color', 'value'=>get_option('wp_chatbot_user_msg_bg_color'));
	$childArray[203] = array('key'=>'wp_chatbot_user_msg_text_color', 'value'=>get_option('wp_chatbot_user_msg_text_color'));
	$childArray[204] = array('key'=>'wp_chatbot_buttons_bg_color', 'value'=>get_option('wp_chatbot_buttons_bg_color'));
	$childArray[205] = array('key'=>'wp_chatbot_buttons_text_color', 'value'=>get_option('wp_chatbot_buttons_text_color'));
	$childArray[206] = array('key'=>'enable_wp_chatbot_custom_color', 'value'=>get_option('enable_wp_chatbot_custom_color'));
	$childArray[207] = array('key'=>'wp_chatbot_text_color', 'value'=>get_option('wp_chatbot_text_color'));
	$childArray[208] = array('key'=>'wp_chatbot_link_color', 'value'=>get_option('wp_chatbot_link_color'));
	$childArray[209] = array('key'=>'wp_chatbot_link_hover_color', 'value'=>get_option('wp_chatbot_link_hover_color'));
	$childArray[210] = array('key'=>'qlcd_wp_chatbot_phone_sent', 'value'=>get_option('qlcd_wp_chatbot_phone_sent'));
	$childArray[212] = array('key'=>'qlcd_wp_chatbot_phone_fail', 'value'=>get_option('qlcd_wp_chatbot_phone_fail'));
	$childArray[213] = array('key'=>'disable_wp_chatbot_call_gen', 'value'=>get_option('disable_wp_chatbot_call_gen'));
	$childArray[214] = array('key'=>'disable_wp_chatbot_call_sup', 'value'=>get_option('disable_wp_chatbot_call_sup'));
	$childArray[215] = array('key'=>'enable_wp_chatbot_ret_user_show', 'value'=>get_option('enable_wp_chatbot_ret_user_show'));
	$childArray[216] = array('key'=>'enable_wp_chatbot_inactive_time_show', 'value'=>get_option('enable_wp_chatbot_inactive_time_show'));
	$childArray[217] = array('key'=>'wp_chatbot_inactive_time', 'value'=>get_option('wp_chatbot_inactive_time'));
	$childArray[218] = array('key'=>'wp_chatbot_checkout_msg', 'value'=>get_option('wp_chatbot_checkout_msg'));
	$childArray[219] = array('key'=>'qlcd_wp_chatbot_shopper_demo_name', 'value'=>get_option('qlcd_wp_chatbot_shopper_demo_name'));
	$childArray[220] = array('key'=>'qlcd_wp_chatbot_shopper_call_you', 'value'=>get_option('qlcd_wp_chatbot_shopper_call_you'));
	$childArray[221] = array('key'=>'qlcd_wp_chatbot_is_typing', 'value'=>get_option('qlcd_wp_chatbot_is_typing'));
	$childArray[222] = array('key'=>'qlcd_wp_chatbot_send_a_msg', 'value'=>get_option('qlcd_wp_chatbot_send_a_msg'));
	$childArray[223] = array('key'=>'qlcd_wp_chatbot_choose_option', 'value'=>get_option('qlcd_wp_chatbot_choose_option'));
	$childArray[224] = array('key'=>'qlcd_wp_chatbot_viewed_products', 'value'=>get_option('qlcd_wp_chatbot_viewed_products'));
	$childArray[225] = array('key'=>'qlcd_wp_chatbot_help_welcome', 'value'=>get_option('qlcd_wp_chatbot_help_welcome'));
	$childArray[226] = array('key'=>'qlcd_wp_chatbot_help_msg', 'value'=>get_option('qlcd_wp_chatbot_help_msg'));
	$childArray[227] = array('key'=>'qlcd_wp_chatbot_reset', 'value'=>get_option('qlcd_wp_chatbot_reset'));
	$childArray[228] = array('key'=>'enable_wp_chatbot_opening_hour', 'value'=>get_option('enable_wp_chatbot_opening_hour'));
	$childArray[229] = array('key'=>'wpwbot_hours', 'value'=>get_option('wpwbot_hours'));
	$childArray[230] = array('key'=>'enable_wp_chatbot_dailogflow', 'value'=>get_option('enable_wp_chatbot_dailogflow'));
	$childArray[231] = array('key'=>'qlcd_wp_chatbot_dialogflow_client_token', 'value'=>get_option('qlcd_wp_chatbot_dialogflow_client_token'));
	$childArray[232] = array('key'=>'qlcd_wp_chatbot_dialogflow_defualt_reply', 'value'=>get_option('qlcd_wp_chatbot_dialogflow_defualt_reply'));
	$childArray[234] = array('key'=>'qlcd_wp_chatbot_dialogflow_agent_language', 'value'=>get_option('qlcd_wp_chatbot_dialogflow_agent_language'));
	$childArray[235] = array('key'=>'custom_intent_names', 'value'=>get_option('custom_intent_names'));
	$childArray[236] = array('key'=>'custom_intent_labels', 'value'=>get_option('custom_intent_labels'));
	$childArray[237] = array('key'=>'custom_intent_kewords', 'value'=>get_option('custom_intent_kewords'));
	
	//bot woocommerce settings
	
	$childArray[238] = array('key'=>'enable_wp_chatbot_disable_producticon', 'value'=>get_option('enable_wp_chatbot_disable_producticon'));
	$childArray[239] = array('key'=>'enable_wp_chatbot_disable_carticon', 'value'=>get_option('enable_wp_chatbot_disable_carticon'));
	$childArray[240] = array('key'=>'disable_wp_chatbot_product_search', 'value'=>get_option('disable_wp_chatbot_product_search'));
	$childArray[241] = array('key'=>'disable_wp_chatbot_catalog', 'value'=>get_option('disable_wp_chatbot_catalog'));
	$childArray[242] = array('key'=>'disable_wp_chatbot_order_status', 'value'=>get_option('disable_wp_chatbot_order_status'));
	$childArray[243] = array('key'=>'disable_wp_chatbot_featured_product', 'value'=>get_option('disable_wp_chatbot_featured_product'));
	$childArray[244] = array('key'=>'disable_wp_chatbot_sale_product', 'value'=>get_option('disable_wp_chatbot_sale_product'));
	$childArray[245] = array('key'=>'qlcd_wp_chatbot_viewed_products', 'value'=>get_option('qlcd_wp_chatbot_viewed_products'));
	$childArray[246] = array('key'=>'qlcd_wp_chatbot_shopping_cart', 'value'=>get_option('qlcd_wp_chatbot_shopping_cart'));
	$childArray[247] = array('key'=>'qlcd_wp_chatbot_add_to_cart', 'value'=>get_option('qlcd_wp_chatbot_add_to_cart'));
	$childArray[248] = array('key'=>'qlcd_wp_chatbot_cart_link', 'value'=>get_option('qlcd_wp_chatbot_cart_link'));
	$childArray[249] = array('key'=>'qlcd_wp_chatbot_checkout_link', 'value'=>get_option('qlcd_wp_chatbot_checkout_link'));
	$childArray[250] = array('key'=>'qlcd_wp_chatbot_cart_welcome', 'value'=>get_option('qlcd_wp_chatbot_cart_welcome'));
	$childArray[251] = array('key'=>'qlcd_wp_chatbot_featured_product_welcome', 'value'=>get_option('qlcd_wp_chatbot_featured_product_welcome'));
	$childArray[252] = array('key'=>'qlcd_wp_chatbot_viewed_product_welcome', 'value'=>get_option('qlcd_wp_chatbot_viewed_product_welcome'));
	$childArray[253] = array('key'=>'qlcd_wp_chatbot_latest_product_welcome', 'value'=>get_option('qlcd_wp_chatbot_latest_product_welcome'));
	$childArray[254] = array('key'=>'qlcd_wp_chatbot_cart_title', 'value'=>get_option('qlcd_wp_chatbot_cart_title'));
	$childArray[255] = array('key'=>'qlcd_wp_chatbot_cart_quantity', 'value'=>get_option('qlcd_wp_chatbot_cart_quantity'));
	$childArray[256] = array('key'=>'qlcd_wp_chatbot_cart_price', 'value'=>get_option('qlcd_wp_chatbot_cart_price'));
	$childArray[257] = array('key'=>'qlcd_wp_chatbot_no_cart_items', 'value'=>get_option('qlcd_wp_chatbot_no_cart_items'));
	$childArray[258] = array('key'=>'qlcd_wp_chatbot_cart_updating', 'value'=>get_option('qlcd_wp_chatbot_cart_updating'));
	$childArray[259] = array('key'=>'qlcd_wp_chatbot_cart_removing', 'value'=>get_option('qlcd_wp_chatbot_cart_removing'));
	$childArray[260] = array('key'=>'qlcd_wp_chatbot_cart_total', 'value'=>get_option('qlcd_wp_chatbot_cart_total'));
	$childArray[261] = array('key'=>'qlcd_wp_chatbot_wildcard_product', 'value'=>get_option('qlcd_wp_chatbot_wildcard_product'));
	$childArray[262] = array('key'=>'qlcd_wp_chatbot_wildcard_catalog', 'value'=>get_option('qlcd_wp_chatbot_wildcard_catalog'));
	$childArray[263] = array('key'=>'qlcd_wp_chatbot_featured_products', 'value'=>get_option('qlcd_wp_chatbot_featured_products'));
	$childArray[264] = array('key'=>'qlcd_wp_chatbot_sale_products', 'value'=>get_option('qlcd_wp_chatbot_sale_products'));
	$childArray[265] = array('key'=>'qlcd_wp_chatbot_product_asking', 'value'=>get_option('qlcd_wp_chatbot_product_asking'));
	$childArray[266] = array('key'=>'qlcd_wp_chatbot_product_success', 'value'=>get_option('qlcd_wp_chatbot_product_success'));
	$childArray[267] = array('key'=>'qlcd_wp_chatbot_product_fail', 'value'=>get_option('qlcd_wp_chatbot_product_fail'));
	$childArray[268] = array('key'=>'qlcd_wp_chatbot_product_suggest', 'value'=>get_option('qlcd_wp_chatbot_product_suggest'));
	$childArray[269] = array('key'=>'qlcd_wp_chatbot_product_infinite', 'value'=>get_option('qlcd_wp_chatbot_product_infinite'));
	$childArray[270] = array('key'=>'qlcd_wp_chatbot_load_more', 'value'=>get_option('qlcd_wp_chatbot_load_more'));
	$childArray[271] = array('key'=>'qlcd_wp_chatbot_wildcard_order', 'value'=>get_option('qlcd_wp_chatbot_wildcard_order'));
	$childArray[272] = array('key'=>'qlcd_wp_chatbot_order_welcome', 'value'=>get_option('qlcd_wp_chatbot_order_welcome'));
	$childArray[273] = array('key'=>'qlcd_wp_chatbot_order_username_asking', 'value'=>get_option('qlcd_wp_chatbot_order_username_asking'));
	$childArray[274] = array('key'=>'qlcd_wp_chatbot_order_username_not_exist', 'value'=>get_option('qlcd_wp_chatbot_order_username_not_exist'));
	$childArray[275] = array('key'=>'qlcd_wp_chatbot_order_username_thanks', 'value'=>get_option('qlcd_wp_chatbot_order_username_thanks'));
	$childArray[276] = array('key'=>'qlcd_wp_chatbot_order_username_password', 'value'=>get_option('qlcd_wp_chatbot_order_username_password'));
	$childArray[277] = array('key'=>'qlcd_wp_chatbot_order_password_incorrect', 'value'=>get_option('qlcd_wp_chatbot_order_password_incorrect'));
	$childArray[278] = array('key'=>'qlcd_wp_chatbot_order_not_found', 'value'=>get_option('qlcd_wp_chatbot_order_not_found'));
	$childArray[279] = array('key'=>'qlcd_wp_chatbot_order_found', 'value'=>get_option('qlcd_wp_chatbot_order_found'));
	$childArray[280] = array('key'=>'qlcd_wp_chatbot_order_email_support', 'value'=>get_option('qlcd_wp_chatbot_order_email_support'));
	$childArray[281] = array('key'=>'qlcd_wp_chatbot_order_email', 'value'=>get_option('qlcd_wp_chatbot_order_email'));
	$childArray[282] = array('key'=>'qlcd_wp_chatbot_order_id', 'value'=>get_option('qlcd_wp_chatbot_order_id'));
	$childArray[283] = array('key'=>'qlcd_wp_chatbot_sys_key_product', 'value'=>get_option('qlcd_wp_chatbot_sys_key_product'));
	$childArray[284] = array('key'=>'qlcd_wp_chatbot_sys_key_catalog', 'value'=>get_option('qlcd_wp_chatbot_sys_key_catalog'));
	$childArray[285] = array('key'=>'qlcd_wp_chatbot_sys_key_order', 'value'=>get_option('qlcd_wp_chatbot_sys_key_order'));
	$childArray[286] = array('key'=>'disable_wp_chatbot_cart_item_number', 'value'=>get_option('disable_wp_chatbot_cart_item_number'));
	$childArray[287] = array('key'=>'wp_chatbot_open_product_detail', 'value'=>get_option('wp_chatbot_open_product_detail'));
	$childArray[288] = array('key'=>'wp_chatbot_exclude_stock_out_product', 'value'=>get_option('wp_chatbot_exclude_stock_out_product'));
	$childArray[289] = array('key'=>'wp_chatbot_show_parent_category', 'value'=>get_option('wp_chatbot_show_parent_category'));
	$childArray[290] = array('key'=>'wp_chatbot_show_sub_category', 'value'=>get_option('wp_chatbot_show_sub_category'));
	$childArray[291] = array('key'=>'wp_chatbot_order_status_without_login', 'value'=>get_option('wp_chatbot_order_status_without_login'));

	
	qc_wpsei_download_send_headers("chatbot_settings_" . date("Y-m-d") . ".csv");
	$result = qc_wpsei_array2csv($childArray);
	print $result;
}


add_action('init', 'qc_wpsei_settings_upload_handle');

function qc_wpsei_settings_upload_handle(){
	
	if(isset($_POST['wp_upload_csv'])){
		//First check if the uploaded file is valid
		$valid = true;
		
		$allowedTypes = array(
                        			'application/vnd.ms-excel',
                        			'text/comma-separated-values', 
                        			'text/csv', 
                        			'application/csv', 
                        			'application/excel', 
                        			'application/vnd.msexcel', 
                        			'text/anytext',
                        			'application/octet-stream',
                        		);
		//echo $_FILES['csv_upload']['type'];exit;
		if( !in_array($_FILES['wp_csv_upload']['type'], $allowedTypes) ){
			$valid = false;
		}

		if( ! $valid ){
			echo "Status: Invalid file type.";
		}
		
		
		$tmpName = $_FILES['wp_csv_upload']['tmp_name'];
								
		if( $tmpName != "" )
		{
		
			$file = fopen($tmpName, "r");
			$flag = true;
			
			//Reading file and building our array
			
			$baseData = array();

			
			//Read fields from CSV file and dump in $baseData
			while(($data = fgetcsv($file)) !== FALSE) 
			{
				
				if ($flag) {
					$flag = false;
					continue;
				}
				
				update_option($data[0], $data[1]);
		
			}
		}
		wp_redirect(admin_url('admin.php?page=wpbot_settings_export_import&msg=success'));die();
	}
	
}
