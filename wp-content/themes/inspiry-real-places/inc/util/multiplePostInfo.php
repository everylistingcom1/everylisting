<?php

unset($multiplePostInfoQuery);

require_once('../../../../../wp-load.php');

if (!function_exists('argsprocesswithsuffix')) :
function argsprocessmetawithsuffix(&$args, $post_type, $meta, $value, $compare) {
    if ((isset($meta)) && (isset($value))) {
        $candidate = array();
        $candidate['post_type'] = $post_type;
        $candidate['meta_key'] = $meta;
        $candidate['meta_value'] = urldecode($value);
        if (isset($compare)) {
            $candidate['meta_compare'] = urldecode($compare);
        } else {
            $candidate['meta_compare'] = '=';
        }
        if (count($args) > 0) {
           $multiple = array();
           $multiple['post_type'] = $candidate['post_type'];
           unset($candidate['post_type']);
           unset($args['post_type']);
           $multiple['meta_query'] = array();
           array_push($multiple['meta_query'], $args);
           array_push($multiple['meta_query'], $candidate);
           array_splice($args, 0);
           $args = array_merge($args, $multiple);
        } else {
           $args = array_merge($args, $candidate);
        }
        return true;
    }
    return false;
}
endif;

if (isset($_GET['json'])) {
    $args = array();
    $args['post_type'] = $_GET['post_type'];
    $json = $_GET['json'];
    $json = str_replace('\\"', "'", $json);
    $json = str_replace("\\'", "'", $json);
    if (startsWith($json, "'")) {
        $json = substr($json, 1);
    }
    if (endsWith($json, "'")) {
        $json = substr($json, 0, $json.length - 1);
    }
    $json = str_replace("'", '"', $json);
        
    $decoded = json_decode($json, true);
        
    // echo '<script>console.log(`$decoded json: ' . print_r($decoded, true) . ' from ' . $json . '`);</script>';
    
    $values = json_search_contents($decoded);
    // print_r($values);
    $args['meta_query'] = getMetaQueryArray($values);
    $args['tax_query'] = getTaxQueryArray($values);
    // print_r($args);
    unset($_GET['json']);
} else if (isset($_GET['ids'])) {
    $args = array();
    $args['post_type'] = $_GET['post_type'];
    $ids = explode( ',', urldecode($_GET['ids']));
    $args['post__in'] = $ids;
    $args['ignore_sticky_posts'] = 1;
    $args['posts_per_page'] = -1;
    $args['offset'] = 0;
} else {
    $args = array();
    argsprocessmetawithsuffix($args, $_GET['post_type'], $_GET['meta_key'], $_GET['meta_value'], $_GET['meta_compare']);
    $i = 0;        
    while (argsprocessmetawithsuffix($args, $_GET['post_type'], $_GET['meta_key'.$i], $_GET['meta_value'.$i], $_GET['meta_compare'.$i])) {
        $i++;
    }
    if (isset($_GET['keyword'])) {
        $args['s'] = urldecode($_GET['keyword']);
    }
}
if (isset($args['meta_query'])) {
    if (isset($_GET['relation'])) {
        $args['meta_query']['relation'] = $_GET['relation'];
    } else if (count($args['meta_query']) > 1) {
        $args['meta_query']['relation'] = 'AND';
    }
}
if ((isset($_GET['sortasc'])) && ($_GET['sortasc'] != "")) {
    $args['orderby'] = $_GET['sortasc'];
    $args['order'] = 'ASC';
}
if ((isset($_GET['sortdesc'])) && ($_GET['sortdesc'] != "")) {
    $args['orderby'] = $_GET['sortdesc'];
    $args['order'] = 'DESC';
}

// echo '<script>console.log(`$args: ' . print_r($args, true) . '`);</script>';

$multiplePostInfoQuery = new WP_Query($args);
unset($_GET['post_type']);
unset($_GET['meta_key']);
unset($_GET['meta_value']);

require_once('postInfo.php');

if (!function_exists('multiplePostInfo')) :
function multiplePostInfo($the_query, $filter, $ids) {    
    $count = 0;
    $return = "{";
    // echo '<script>console.log(`count: ' . $the_query->found_posts . '`);</script>';
    if ($the_query->have_posts()) {
        while ($the_query->have_posts()) {
            if ($count > 0) {
                $return .= ", ";
            } else {
                $return .= "\"result\": [";
            }
            $return .= onePostInfo($the_query, $filter);
            $count++;
        }
        if ($count > 0) {
            $return .= "]";
        }
    }
    $return .= "}";
    wp_reset_postdata();
    return $return;
}
endif;

if (isset($multiplePostInfoQuery)) {
    echo multiplePostInfo($multiplePostInfoQuery, $_GET['filter'], $ids);
    unset($ids);
}



