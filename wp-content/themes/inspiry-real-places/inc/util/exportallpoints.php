<?php
define('WP_USE_THEMES', false);  
require_once('../../../../../wp-load.php');

	$properties_search_arg = array(
	    'post_type' => 'property',
	    'offset' => 0,
	    'posts_per_page' => 5000
	    // 'posts_per_page' => 1000
	);

	$properties_search_query = new WP_Query($properties_search_arg);

    if ($properties_search_query->have_posts()) { 
            $properties_count = 1;
            while ($properties_search_query->have_posts()) {
                $properties_search_query->the_post();
                $home_property = new Inspiry_Property(get_the_ID());
                echo get_the_ID();
                echo '\\t';
                echo $home_property->get_property_meta('IDXGenerator_Country');
                echo '\\t';
                echo $home_property->get_property_meta('REAL_HOMES_property_latitude');
                echo '\\t';
                echo $home_property->get_property_meta('REAL_HOMES_property_longitude');
                echo '\\n';
                $properties_count++;
            }
    }
    wp_reset_postdata();
?>
