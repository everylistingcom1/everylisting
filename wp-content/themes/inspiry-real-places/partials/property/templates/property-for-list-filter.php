<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/property/templates/property-for-list-filter.php'); ?>

<?php
if ( ! class_exists( 'Inspiry_Real_Estate' ) ) {
	return;
}

global $inspiry_options;
global $property_list_counter;
global $post;

$list_property = new Inspiry_Property( get_the_ID() );

$custom = get_post_custom();

/*
 * Even / Odd Class
 */
$even_odd_class = 'listing-post-odd';
if ( $property_list_counter % 2 == 0) {
    $even_odd_class = 'listing-post-even';
}

/*
 * Price title
 */
$price_title = esc_html__( 'Price', 'inspiry' );
if ( !empty( $inspiry_options[ 'inspiry_property_card_price_title' ] ) ) {
    $price_title = $inspiry_options[ 'inspiry_property_card_price_title' ];
}

/*
 * Description title
 */
$desc_title = esc_html__( 'Description', 'inspiry' );
if ( !empty( $inspiry_options[ 'inspiry_property_card_desc_title' ] ) ) {
    $desc_title = $inspiry_options[ 'inspiry_property_card_desc_title' ];
}

/*
 * Button Text
 */
$button_text = esc_html__( 'Show Details', 'inspiry' );
if ( !empty( $inspiry_options[ 'inspiry_property_card_button_text' ] ) ) {
    $button_text = $inspiry_options[ 'inspiry_property_card_button_text' ];
}
?>

 
     <div class="homepropBoxes thumbnail-size">
     		 <?php new_property_thumbnail_insert($list_property->get_post_ID()); ?>    
             <div class="new-property-description">
                 <a href="<?php the_permalink(); ?>" rel="bookmark"></a>
                 <div class="new-property-info">
				        <?php
                        /*
                         * Property meta
                         */
                        inspiry_property_meta( $list_property, array( 'meta' => array('type', 'status' ) ) );
                        ?>
				</div>
				<div class="new-property-city">
				    <?php $list_property_address = $list_property->get_address(); ?>
                    <p class="property-address visible-lg"><i class="fa fa-map-marker"></i><?php 
                        if (isset($custom['IDXGenerator_directions'])) {
                            echo esc_html($custom['IDXGenerator_directions'][0]);
                        } else {
                            echo esc_html( $list_property_address ); 
                        }
                    ?></p>
        							
        		</div>
        		<!--sse-->
        		<div class="new-price-and-status">
                    <?php 
                    if (isset($custom['IDXGenerator_nlp_price'])) {
                        echo esc_html($custom['IDXGenerator_nlp_price'][0]);
                    } else {
                        echo esc_html( price_format($list_property->get_price_without_postfix()) );  
                        $price_postfix = $list_property->get_price_postfix();
                        if (!empty( $price_postfix)) {
                            ?><?php echo ' ' . $price_postfix; ?><?php
                        }
                    }
                    ?>
                </div>
                <!--/sse-->
             </div>
      </div>
