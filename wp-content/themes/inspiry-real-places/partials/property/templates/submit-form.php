<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/property/templates/submit-form.php'); ?>

<?php

global $inspiry_options;
if ( function_exists( 'ire_property_submit_form' ) ) {
	ire_property_submit_form( $inspiry_options );
}
