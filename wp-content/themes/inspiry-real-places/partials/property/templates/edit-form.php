<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/property/templates/edit-form.php'); ?>

<?php

global $inspiry_options;
if ( function_exists( 'ire_property_edit_form' ) ) {
	ire_property_edit_form( $inspiry_options );
}