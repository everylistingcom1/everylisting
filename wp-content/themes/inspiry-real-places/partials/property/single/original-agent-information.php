<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/property/single/original-agent-information.php'); ?>

<style>
.listing-agent {margin-bottom:25px}
.fixedCloseBoxOnScreen {
    position: fixed;
    right: 5%;
    width: 50px;
    top: 5%;
    height: 50px;
    float: right;
    z-index: 3000;
}
</style>
<div class="listing-agent">
<?php
        global $inspiry_options;
        global $wpdb;
        $title = "Listing Agent Information";
?>      <a onclick="toggleDisplay()" style="cursor: pointer;"><h4 class="fancy-title"><?php echo esc_html($title); ?>&nbsp;<i id="arrowIcon" class="fa fa-angle-right fa-lg"></i></a></h4>
<div id="agentInfo" style="display:none;">
<?php 

$urls = explode('|', get_post_meta(get_the_ID(), 'IDXGenerator_urls', TRUE)); 

$urls = array_filter($urls, function($value) { return !is_null($value) && $value !== '' && trim($value, '"') !== ''; });

if (count($urls) > 0) {
	echo '<h5 class="fancy-title">Original Url(s)</h5>';
	echo '<ul>';
	foreach($urls as $url) {
		echo '<li><a target="_blank" href="' . trim($url, '"') . '">' . trim($url, '"') . '</a></li>';
	}
	echo '</ul>';
}

$ids = explode('|', get_post_meta(get_the_ID(), 'IDXGenerator_listing_agents_id', TRUE)); 

$ids = array_filter($ids, function($value) { return !is_null($value) && $value !== ''; });

$count = 1;
if (count($ids) > 0) {
	if (count($ids) > 1) echo '<ol>';
	foreach($ids as $id) {
		if (count($ids) > 1) echo '<li>Agent #' . $count . '</li>';
		
		$results = $wpdb->get_row("select post_id from $wpdb->postmeta where (meta_value = '".$id."') and (meta_key = 'IDXGenerator_realEstateBrokerId')");
		if (!empty($results)) {
			$post_id = $results->post_id;
			$the_query = new WP_Query(array('post_type' => 'agent','p'=>$post_id)); 
			$agent_data = $the_query->posts;
			$agent_data[0]->post_content = htmlentities($agent_data[0]->post_content);
			$agent = (array)$agent_data[0];
			$agent_meta = get_post_meta($post_id);
?>
			<h5 class="fancy-title">Information</h5>
			<ul>
<?php if (strlen(get_post_meta($post_id, IDXGenerator_name, TRUE)) > 0) { ?>
			<li><span class="label">Name:&nbsp;</span><span class="meta-item-value"><b><?php echo get_post_meta($post_id, IDXGenerator_name, TRUE); ?></b></span></li>
<?php } if (strlen(get_post_meta($post_id, IDXGenerator_phone, TRUE)) > 0) { ?>
			<li><span class="label">Phone number:&nbsp;</span><span class="meta-item-value"><b><?php echo get_post_meta($post_id, IDXGenerator_phone, TRUE); ?></b></span></li>
<?php } if (strlen(get_post_meta($post_id, IDXGenerator_email, TRUE)) > 0) { ?>
			<li><span class="label">e-mail:&nbsp;</span><span class="meta-item-value"><b><?php echo get_post_meta($post_id, IDXGenerator_email, TRUE); ?></b></span></li>
<?php } if (strlen(get_post_meta($post_id, IDXGenerator_firstDetected, TRUE)) > 0) { ?>
			<li><span class="label">First detected:&nbsp;</span><span class="meta-item-value"><?php echo get_post_meta($post_id, IDXGenerator_firstDetected, TRUE); ?></span></li>
<?php } if (strlen(get_post_meta($post_id, IDXGenerator_lastDetected, TRUE)) > 0) { ?>
			<li><span class="label">Last detected:&nbsp;</span><span class="meta-item-value"><?php echo get_post_meta($post_id, IDXGenerator_lastDetected, TRUE); ?></span></li>
<?php } if (strlen(get_post_meta($post_id, IDXGenerator_notDuplicateCount, TRUE)) > 0) { ?>
			<li><span class="label">Not duplicate listing count:&nbsp;</span><span class="meta-item-value"><?php echo get_post_meta($post_id, IDXGenerator_notDuplicateCount, TRUE); ?></span></li>
<?php } if (strlen(get_post_meta($post_id, IDXGenerator_duplicateCount, TRUE)) > 0) { ?>
			<li><span class="label">Duplicate listing count:&nbsp;</span><span class="meta-item-value"><?php echo get_post_meta($post_id, IDXGenerator_duplicateCount, TRUE); ?></span></li>
<?php } ?>
    </ul>
<?php
if (strlen(get_post_meta($post_id, IDXGenerator_valuation, TRUE)) > 0) { ?>
			<h5 class="fancy-title">Valuation&nbsp;<a onclick="requestValuationFullScreen()"><img height="24" width="24" src="/wp-content/themes/inspiry-real-places/images/full-screen.png"/></a></h5>
			<pre id="valuationText" style="max-height: 300px;"><a onclick="closeFullScreen()"><img id="closeboximage1" class="fixedCloseBoxOnScreen" width="24px" height="24px" style="visibility: hidden;" src="/wp-content/themes/inspiry-real-places/images/close-button.png"></a><?php echo get_post_meta($post_id, IDXGenerator_valuation, TRUE); ?></pre>
<?php } if (strlen(get_post_meta($post_id, IDXGenerator_urls, TRUE)) > 0) { ?>
			<h5 class="fancy-title">All agent listings&nbsp;<a onclick="requestAllAgentListingsFullScreen()"><img height="24" width="24" src="/wp-content/themes/inspiry-real-places/images/full-screen.png"/></a></h5>
			<pre id="allAgentListings" style="max-height: 300px;"><a onclick="closeFullScreen()"><img id="closeboximage2" class="fixedCloseBoxOnScreen" width="24px" height="24px" style="visibility: hidden;" src="/wp-content/themes/inspiry-real-places/images/close-button.png"><?php echo get_post_meta($post_id, IDXGenerator_urls, TRUE); ?></pre>
<?php }
		} else {
			echo '<div class="warning">No listing agent information for id ' . $id . '.</div>';
		}	
		$count++;
	}
	if (count($ids) > 1) echo '</ol>';
} else {
	echo '<br/>NOT count > 0';
	echo '<div class="warning">There is no stored listing agent for this property.</div>';
}

?>
		<script>
			var isFullScreen = (isFullScreen === undefined)?false:isFullScreen;
			document.addEventListener('fullscreenchange', handleFullScreen, false);
 			document.addEventListener('mozfullscreenchange', handleFullScreen, false);
 			document.addEventListener('MSFullscreenChange', handleFullScreen, false);
 			document.addEventListener('webkitfullscreenchange', handleFullScreen, false);
			function handleFullScreen() {
				isFullScreen = !isFullScreen;
				if (isFullScreen) {
			  		document.getElementById('closeboximage1').style.visibility = "visible";
			  		document.getElementById('closeboximage2').style.visibility = "visible";
			 	} else {
			 		console.log("Exited full-screen");
			 		document.getElementById('closeboximage1').style.visibility = "hidden";
			 		document.getElementById('closeboximage2').style.visibility = "hidden";
			 	}
			}
			function closeFullScreen() {
				if (document.exitFullscreen) {
			    	document.exitFullscreen();
			  	} else if (document.mozCancelFullScreen) { /* Firefox */
			    	document.mozCancelFullScreen();
			  	} else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
			    	document.webkitExitFullscreen();
			  	} else if (document.msExitFullscreen) { /* IE/Edge */
			    	document.msExitFullscreen();
			  	}
			}
			function requestValuationFullScreen() {
				requestFullScreen(document.getElementById("valuationText"));
			}
			function requestAllAgentListingsFullScreen() {
				requestFullScreen(document.getElementById("allAgentListings"));
			}
			function requestFullScreen(elem) {
			    // Supports most browsers and their versions.
			    var requestMethod = elem.requestFullScreen || elem.webkitRequestFullScreen || elem.mozRequestFullScreen || elem.msRequestFullScreen;
			    if (requestMethod) { // Native full screen.
			        requestMethod.call(elem);
			    } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
			        var wscript = new ActiveXObject("WScript.Shell");
			        if (wscript !== null) {
			            wscript.SendKeys("{F11}");
			        }
			    }
			}
			
			function toggleDisplay() {
				var x = document.getElementById("agentInfo");
				if (x.style.display === "none") {
			    	x.style.display = "block";
			    	$('#arrowIcon').removeClass('fa-angle-right').addClass('fa-angle-down');
			  	} else {
			    	x.style.display = "none";
			    	$('#arrowIcon').addClass('fa-angle-right').removeClass('fa-angle-down');
			  	}
			}
		</script>
	</div>
</div>