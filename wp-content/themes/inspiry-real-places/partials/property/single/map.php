<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/property/single/map.php'); ?>

<?php if ( inspiry_has_google_maps_api_key() ) : ?>
    <section class="property-location-section clearfix">
		<?php
		global $inspiry_options;
		if ( ! empty( $inspiry_options['inspiry_property_map_title'] ) ) {
			?>
            <h4 class="fancy-title"><?php echo esc_html( $inspiry_options['inspiry_property_map_title'] ); ?></h4><?php
		}

		global $inspiry_single_property;
		$property_marker = array();

		$property_marker['lat']  = $inspiry_single_property->get_latitude();
		$property_marker['lang'] = $inspiry_single_property->get_longitude();

		$marker_slug        = 'single-family-home'; // Default Icon Slug
		$marker_slug_retina = 'single-family-home'; // Default Retina Icon Slug
		$base_icon_path     = get_template_directory() . '/images/map/';
		$base_icon_uri      = get_template_directory_uri() . '/images/map/';

		// Property Map Icon Based on Property Type
		$property_type_slugs = wp_get_post_terms( $inspiry_single_property->get_post_ID(), 'property-type' );
		foreach ( $property_type_slugs as $type_slug ) {
			$type_slugs_array[] = $type_slug->slug;
		}

		if ( ! empty( $type_slugs_array ) ) {
			foreach ( $type_slugs_array as $type_slug ) {
				if ( file_exists( $base_icon_path . $type_slug . '-map-icon.png' ) ) {
					$marker_slug = $type_slug; // icon slug
					if ( file_exists( $base_icon_path . $type_slug . '-map-icon@2x.png' ) ) {
						$marker_slug_retina = $type_slug;   // retina icon slug
					}
					break;
				}
			}
		}

		$property_marker['icon']       = $base_icon_uri . $marker_slug . '-map-icon.png';
		$property_marker['retinaIcon'] = $base_icon_uri . $marker_slug_retina . '-map-icon@2x.png';

		wp_enqueue_script( 'property-google-map' );
		wp_localize_script( 'property-google-map', 'propertyMapData', $property_marker );
		?>
        <div id="property-map"></div>
    </section>
<?php endif; ?>