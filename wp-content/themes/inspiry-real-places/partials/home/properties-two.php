<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/home/properties-two.php'); ?>

<?php
/*
 * Properties for homepage
 */

global $inspiry_options;

$number_of_properties = intval( $inspiry_options[ 'inspiry_home_properties_number_2' ] );
if( !$number_of_properties ) {
    $number_of_properties = 8;
}

$home_properties_args = array(
    'post_type' => 'property',
    'posts_per_page' => $number_of_properties,
);

$home_properties_query = new WP_Query( apply_filters( 'inspiry_home_properties', $home_properties_args ) );

// Homepage Properties Loop
if ( $home_properties_query->have_posts() ) :

?>
<div class="property-listing-two fade-in-up">
    <div class="container">
        <div class="row zero-horizontal-margin">
            <?php
            while ( $home_properties_query->have_posts() ) :
                $home_properties_query->the_post();
                property_list_insert(get_the_ID());
            endwhile;
            ?>
        </div>
        <!-- .row -->
    </div>
    <!-- .container -->
</div><!-- .property-listing-home -->
<?php
endif;

wp_reset_postdata();
