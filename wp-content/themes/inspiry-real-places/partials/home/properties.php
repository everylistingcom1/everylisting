<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/home/properties.php'); ?>

<?php
/*
 * Properties for homepage
 */

global $inspiry_options;

$number_of_properties = intval( $inspiry_options[ 'inspiry_home_properties_number_1' ] );
if( !$number_of_properties ) {
    $number_of_properties = 4;
}

$home_properties_args = array(
    'post_type' => 'property',
    'posts_per_page' => $number_of_properties,
);

$home_properties_query = new WP_Query( apply_filters( 'inspiry_home_properties', $home_properties_args ) );

// Homepage Properties Loop
if ( $home_properties_query->have_posts() ) :
    $properties_count = 1;
    $gallery_enabled = $inspiry_options[ 'inspiry_home_properties_gallery' ];
    $gallery_limit = 3;
    if ( $gallery_enabled ) {
        $gallery_limit =  intval( $inspiry_options[ 'inspiry_home_properties_gallery_limit' ] );
    }
    ?>
    <div class="container fade-in-up <?php echo inspiry_animation_class(); ?>">
        <div class="row row-odd zero-horizontal-margin">
        <?php
        while ( $home_properties_query->have_posts() ) :
            $home_properties_query->the_post();
            $even_odd_class = 'property-post-odd';
            if ( $properties_count % 2 == 0) {
                $even_odd_class = 'property-post-even';
            }
            ?>
            <div class="col-xs-6 custom-col-xs-12">
                <article class="row hentry property-listing-home property-listing-one meta-item-half <?php echo esc_attr( $even_odd_class ); ?>">
                	<?php property_list_insert(get_the_ID(), 'zero-horizontal-padding col-lg-6', ' clearfix col-lg-6'); ?>
                </article>
                <!-- .property-post-odd -->
            </div>
            <?php
            /*
             * odd and even rows
             */
            if ( $number_of_properties > $properties_count ) {
                if ( 0 == ( $properties_count % 4 ) ) {
                    ?></div><div class="row row-odd zero-horizontal-margin"><?php
                } elseif ( 0 == ( $properties_count % 2 ) ) {
                    ?></div><div class="row row-even zero-horizontal-margin"><?php
                }
            }
            $properties_count++;
        endwhile;
        ?>
        </div>
        <!-- end of row -->
    </div>
    <?php
endif;

wp_reset_postdata();