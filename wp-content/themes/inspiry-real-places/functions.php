<?php
require 'inc/util/lat-long-cluster-cache.php';
/**
 * The current version of the theme.
 */
define('INSPIRY_THEME_VERSION', '1.4.308');
define('CONTEXT_IN_HTML', true);

load_theme_textdomain('inspiry', get_template_directory() . '/languages');

if (! function_exists('inspiry_theme_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function inspiry_theme_setup()
    {

        // Set the default content width.
        $GLOBALS['content_width'] = 710;

        add_theme_support('automatic-feed-links');

        add_theme_support('title-tag');

        add_theme_support('custom-logo');

        add_theme_support('custom-background');

        add_theme_support('post-thumbnails');

        set_post_thumbnail_size(850, 570, true);

        /*
         * Used on:
         * Home page ( Properties and Featured Properties )
         * Properties list pages ( both list and grid layout )
         */
        add_image_size('inspiry-grid-thumbnail', 660, 600, true);

        /*
         * Used on:
         * Agent single page
         * Property single page
         */
        add_image_size('inspiry-agent-thumbnail', 220, 220, true);

        /*
         * Theme theme uses wp_nav_menu in one location.
         */
        register_nav_menus(array(
            'primary' => esc_html__('Primary Menu', 'inspiry')
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption'
        ));

        add_theme_support('post-formats', array(
            'image',
            'video',
            'gallery'
        ));

        // To support default block styles
        add_theme_support('wp-block-styles');

        add_theme_support('align-wide');

        // editor style
        add_theme_support('editor-styles');

        // editor style
        add_editor_style(get_template_directory_uri() . '/css/editor-style.css');

        // Add support for responsive embedded content.
        add_theme_support('responsive-embeds');
    }

    add_action('after_setup_theme', 'inspiry_theme_setup');

    update_option('users_can_register', true);

endif;

    // inspiry_theme_setup

if (! function_exists('console')) :

    function console($debug, $asString = false)
    {
        $dReturn = str_replace('"', "'", preg_replace('/\s+/', ' ', $debug));
        if ((isset($asString)) && ($asString == true)) {
            return $dReturn;
        } else {
            echo '<script>console.log("' . $dReturn . '")</script>';
        }
    }
endif;

if (! function_exists('console_array')) :

    function console_array($debug, $asString)
    {
        $dReturn = str_replace('"', "'", trim(preg_replace('/\s+/', ' ', print_r($debug, true))));
        if ((isset($asString)) && ($asString == true)) {
            return $dReturn;
        } else {
            echo '<script>console.log("' . $dReturn . '")</script>';
        }
    }
endif;

if (! function_exists('filter_agent_ids')) :

    function filter_agent_ids($propertypostid, $agentids)
    {
        // console("into filter_agent_ids");
        $showEveryListingAgent = true;
        $showOriginalListingAgent = true;
        if ($showEveryListingAgent) {
            // console("Added Brett");
            array_push($agentids, "511584"); // Brett Henry
                                             // console_array($agentids);
        }
        if ($showOriginalListingAgent) {
            // console("Property post id is " . $propertypostid);
            $val = get_post_meta($propertypostid, "IDXGenerator_listing_agents_id", true);
            if (isset($val)) {
                // console("Listing agent(s): " . $val);
                $splitarray = explode("|", $val);
                if ((isset($splitarray)) && (count($splitarray) > 0)) {
                    // console("Has split array results");
                    foreach ($splitarray as $value) {
                        if (strlen($value) > 0) {
                            $args = array(
                                'post_status' => 'private',
                                'post_type' => 'agent',
                                'meta_query' => array(
                                    array(
                                        'key' => 'IDXGenerator_realEstateBrokerId',
                                        'value' => $value,
                                        'compare' => '='
                                    )
                                )
                            );
                            $query = new WP_Query($args);
                            // console("Did query: ". $value);
                            if ($query->have_posts()) {
                                // console("Got result.");
                                $query->the_post();
                                $custom = get_post_custom();
                                if ((isset($custom['IDXGenerator_email'])) || (isset($custom['IDXGenerator_phone']))) {
                                    array_push($agentids, get_the_ID());
                                }
                            } else {
                                // console("No result: " . $query->request);
                            }
                            wp_reset_postdata();
                        }
                    }
                }
            } else {
                // console("Did not detect listing agent");
            }
        }
        return array_unique($agentids);
    }
endif;

if (! function_exists('commentPHPContext')) :

    function commentPHPContext($path)
    {
        if (CONTEXT_IN_HTML) {
            echo '<!-- PHP Context: ' . $path . ' -->';
        }
    }
endif;

if (! function_exists('property_list_insert')) :

    function property_list_insert($id, $addclassthumbnail = '', $addclassdescription = '')
    {
        $home_property = new Inspiry_Property($id);
        ?>
<!-- HERE 1 -->
<div
	class="property-thumbnail <?php echo inspiry_animation_class() . ' ' . $addclassthumbnail; ?>">
	<div class="stripe-container">
    	<?php
        inspiry_thumbnail();
        echo inspiry_property_meta($home_property, array(
            'meta' => array(
                'area',
                'beds',
                'baths',
                'garages'
            )
        ), 'overlay-on-thumbnail-stripe');
        ?>
    	   </div>
</div>
<div class="property-description <?php echo $addclassdescription; ?>">
	<header class="entry-header">
		<h4 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo get_inspiry_custom_excerpt(get_the_title(), 7); ?></a>
		</h4>
		<!--sse-->
		<div class="price-and-status">
			<span class="price"><?php echo esc_html(price_format($home_property->get_price_without_postfix())); ?></span>
                <?php
        $first_status_term = $home_property->get_taxonomy_first_term('property-status', 'all');
        if ($first_status_term) {
            ?>
                    <a
				href="<?php echo esc_url( get_term_link($first_status_term)); ?>"> <span
				class="property-status-tag <?php echo esc_html($first_status_term->slug); ?>"><?php echo esc_html($first_status_term->name); ?></span>
			</a>
                    <?php
        }
        ?>
            </div>
		<!--/sse-->
	</header>
</div>
<?php
    }
endif;

    // property_list_insert

if (! function_exists('inspiry_thumbnail_with_metas')) :

    /**
     * Display thumbnail
     *
     * @param string $size
     */
    function inspiry_thumbnail_with_metas($id, $size = 'inspiry-grid-thumbnail')
    {
        $property_meta = get_post_custom($id);
        if ((!isset($property_meta['REAL_HOMES_property_address'])) && ((isset($property_meta['IDXGenerator_Country'])) || (isset($property_meta['IDXGenerator_Province'])) || (isset($property_meta['IDXGenerator_City'])) || (isset($property_meta['IDXGenerator_Sector'])))) {
            $nAddress = '';
            if (isset($property_meta['IDXGenerator_Country'])) {
                $nAddress .= $property_meta['IDXGenerator_Country'];
            }
            if (isset($property_meta['IDXGenerator_Province'])) {
                if ($nAddress != '') {
                    $nAddress .= ' > ';
                }
                $nAddress .= $property_meta['IDXGenerator_Province'];
            }
            if (isset($property_meta['IDXGenerator_City'])) {
                if ($nAddress != '') {
                    $nAddress .= ' > ';
                }
                $nAddress .= $property_meta['IDXGenerator_City'];
            }
            if (isset($property_meta['IDXGenerator_Sector'])) {
                if ($nAddress != '') {
                    $nAddress .= ' > ';
                }
                $nAddress .= $property_meta['IDXGenerator_Sector'];
            }
            update_post_meta($id, 'REAL_HOMES_property_address', $nAddress);
        }
        $dReturn = false;
        ?>
<a href="<?php the_permalink($id); ?>">
			<?php
        if (has_post_thumbnail($id)) {
            $val = get_the_post_thumbnail($id, $size, array(
                'class' => 'img-thumbnail-responsive noobjectfit'
            ));
            $dReturn = true;
        } else {
            $val = get_inspiry_image_placeholder($size, 'img-thumbnail-responsive noobjectfit');
        }
        echo ("<!-- before regex: " . $val . " -->");
        $pattern = '/\(max-width:[ ]?[0-9]*px[;]?\)/i';
        $val = preg_replace($pattern, "", $val);
        $pattern = '/max-width:[ ]?[0-9]*px[;]?/i';
        $val = preg_replace($pattern, "", $val);
        // $pattern = $pattern = '/width="[0-9]*"/i';
        // $val = preg_replace($pattern, "width=\"100%\"", $val);
        echo ("<!-- after regex: " . $val . " -->");
        echo $val;
        $home_property = new Inspiry_Property($id);
        echo inspiry_property_meta($home_property, array(
            'meta' => array(
                'area',
                'beds',
                'baths',
                'garages'
            )
        ), 'overlay-on-thumbnail-stripe', true);
        ?>
		</a>
<?php
        return $dReturn;
    }
endif;

if (! function_exists('new_property_thumbnail_insert')) :

    function new_property_thumbnail_insert($id, $addclassthumbnail = '')
    {
        $home_property = new Inspiry_Property($id);
        ?>
<div class="new-property-thumbnail <?php echo $addclassthumbnail; ?>" data-post-id="<?php echo $id ?>">
	<div class="stripe-container">
    		<?php inspiry_thumbnail_with_metas($id, 'inspiry-grid-thumbnail'); ?>
    	</div>
</div>
<?php
    }
endif;

    // new_property_list_insert

if (! function_exists('new_property_list_insert')) :

    function new_property_list_insert($id, $addclassthumbnail = '', $addclassdescription = '')
    {
        $home_property = new Inspiry_Property($id);
        $custom = get_post_custom($id);
        $address = $home_property->get_address();
        $country = $home_property->get_property_meta('IDXGenerator_Country');
        $first_type_term = $home_property->get_taxonomy_first_term('property-type', 'all');
        $first_status_term = $home_property->get_taxonomy_first_term('property-status', 'all');
        ?>
        <?php new_property_thumbnail_insert(get_the_ID(), $addclassthumbnail); ?>
<div
	class="new-property-description <?php echo $addclassdescription; ?>" data-post-id="<?php echo $id ?>">
	<div class="new-property-info">
            <?php
        if ((isset($custom['IDXGenerator_directions'])) && (strlen($custom['IDXGenerator_directions'][0]) > 0)) {
            echo esc_html($custom['IDXGenerator_directions'][0]);
            echo ' - ';
        } else if (strlen($address) > 0) {
            echo $address;
            echo ' - ';
        } else {
            $address = '';
            if ((isset($custom['IDXGenerator_Country'])) && (count($custom['IDXGenerator_Country']) > 0) && (strlen($custom['IDXGenerator_Country'][0]) > 0)) {
                $address .= $custom['IDXGenerator_Country'][0];
            }
            if ((isset($custom['IDXGenerator_Province'])) && (count($custom['IDXGenerator_Province']) > 0) && (strlen($custom['IDXGenerator_Province'][0]) > 0)) {
                if ($address != '') {
                    $address .= ' > ';
                }
                $address .= $custom['IDXGenerator_Province'][0];
            }
            if ((isset($custom['IDXGenerator_City'])) && (count($custom['IDXGenerator_City']) > 0) && (strlen($custom['IDXGenerator_City'][0]) > 0)) {
                if ($address != '') {
                    $address .= ' > ';
                }
                $address .= $custom['IDXGenerator_City'][0];
            }
            if ((isset($custom['IDXGenerator_Sector'])) && (count($custom['IDXGenerator_Sector']) > 0) && (strlen($custom['IDXGenerator_Sector'][0]) > 0)) {
                if ($address != '') {
                    $address .= ' > ';
                }
                $address .= $custom['IDXGenerator_Sector'][0];
            }
            if (strlen($address) > 0) {
                echo $address;
                echo ' - ';
            }
        }
        if ((isset($custom['IDXGenerator_PropertyTypeDescriptor'])) && (strlen($custom['IDXGenerator_PropertyTypeDescriptor'][0]) > 0)) {
            echo $custom['IDXGenerator_PropertyTypeDescriptor'][0];
        } else {
            $first_type_term = $home_property->get_taxonomy_first_term('property-type', 'all');
            $first_status_term = $home_property->get_taxonomy_first_term('property-status', 'all');
            $first_city_term = $home_property->get_taxonomy_first_term('property-city', 'all');
            echo esc_html($first_type_term->name);
            echo ' ';
            echo esc_html($first_status_term->name);
        }
        ?>
            </div>
	<!--sse-->
	<div class="new-price-and-status">
                <?php
        if (isset($custom['IDXGenerator_nlp_price'])) {
            echo esc_html($custom['IDXGenerator_nlp_price'][0]);
        } else {
            echo esc_html(price_format($home_property->get_price_without_postfix()));
            $price_postfix = $home_property->get_price_postfix();
            if (! empty($price_postfix)) {
                ?><?php echo ' ' . $price_postfix; ?><?php
            }
        }
        ?>
            </div>
	<!--/sse-->
</div>
<?php
    }
endif;

    // new_property_list_insert

if (! function_exists('compare_title')) :
    function compare_title($a, $b)
    {
        return strnatcmp(get_the_title($a), get_the_title($b));
    }
endif;

if (!function_exists('hasMyProperties')) :
function hasMyProperties() {
    return false;
}
endif;

if (!function_exists('hasFavoriteProperties')) :
function hasFavoriteProperties() {
    return false;
}
endif;

if (!function_exists('if_menu_has_favorites')) :
function if_menu_has_favorites($conditions) {
    $conditions[] = array(
        'id'        =>  'has-favorites', 
        'name'    =>  'If there are favorites', // name of the condition
        'condition' =>  function($item) {          // callback - must return TRUE or FALSE
            return hasFavoriteProperties();
        }
    );
    return $conditions;
}
add_filter('if_menu_conditions', 'if_menu_has_favorites');
endif;

    // compare_title

if (! function_exists('processChatbotWebHook')) :

    // Call this from qcld_wpbot_dfwebhookcallback into qcld-df-webhook.php
    
    // Call is this way: echo processChatbotWebHook($req, $intent, $parameters);
    

    function processChatbotWebHook($request, $intent, $parameters)
    {
        global $inspiry_options;
        global $post;
        $response = "";
        $debuginfo = "";
        $req = $request->get_params();
        $originalIntent = $intent;
        $grabbedparams = false;
        if ((isset($req['queryResult']['action'])) && (startsWith($req['queryResult']['action'], "Property_Search"))) {
            $intent = "Property_Search";
            if ((isset($req['queryResult']['outputContexts'])) && (isset($req['queryResult']['outputContexts'][0])) && (isset($req['queryResult']['outputContexts'][0]['parameters'])) && (count($req['queryResult']['outputContexts'][0]['parameters']) > count($parameters))) {
                $parameters = $req['queryResult']['outputContexts'][0]['parameters'];
                $grabbedparams = true;
            }
        }
        if (! $grabbedparams) {
            if ((isset($req['queryResult']['outputContexts'])) && (isset($req['queryResult']['outputContexts'][0])) && (isset($req['queryResult']['outputContexts'][0]['parameters']))) {
                $parameters['Property_Type.original'] = $req['queryResult']['outputContexts'][0]['parameters']['Property_Type.original'];
            }
        }
        $debuginfo .= '$intent: ' . $intent;
        if ($intent == "Property_Search") {
            $propertyType = $parameters["Property_Type"];
            $originalPropertyType = $parameters["Property_Type.original"];
            if (! isset($originalPropertyType)) {
                $originalPropertyType = $propertyType;
            }
            $transactionType = $parameters["Transaction_Type"];
            $country = $parameters["Jurisdiction_Country"];
            $province = $parameters["Jurisdiction_Province"];
            $city = $parameters["Jurisdiction_City"];
            $sector = $parameters["Jurisdiction_Sector"];
            $features = $parameters["Property_Features"];
            $lifestyle = $parameters["Lifestyle"];
            $minprice = $parameters["price_minimum"];
            $maxprice = $parameters["price_maximum"];
            $tracer = $parameters["Tracer_start"];
            if ($tracer > 0) {
                $tracer --;
            }
            $number_of_properties = 5;
            // Basic query arguments
            $properties_search_arg = array(
                'post_type' => 'property',
                'posts_per_page' => $number_of_properties,
                'offset' => $tracer,
                'paged' => 1,
                'meta_query' => array(
                    array(
                        'key' => '_thumbnail_id',
                        'compare' => 'EXISTS'
                    )
                )
            );
            $_GET = [];
            if ((isset($lifestyle)) && (strlen($lifestyle) > 0))
                $_GET['keyword'] = $lifestyle;
            if (isset($propertyType))
                $_GET['type'] = $propertyType;
            if (isset($transactionType))
                $_GET['status'] = $transactionType;
            if ((isset($features)) && (count($features) > 0))
                $_GET['features'] = $features;
            if ((isset($minprice)) && (strlen($minprice) > 0))
                $_GET['min-price'] = $minprice;
            if ((isset($maxprice)) && (strlen($maxprice) > 0))
                $_GET['max-price'] = $maxprice;
            $locations = [];
            if ((isset($country)) && (strlen($country) > 0))
                array_push($locations, $country);
            if ((isset($province)) && (strlen($province) > 0))
                array_push($locations, $province);
            if ((isset($city)) && (strlen($city) > 0))
                array_push($locations, $city);
            if ((isset($sector)) && (strlen($sector) > 0))
                array_push($locations, $sector);
            if (count($locations) > 0)
                $_GET['location'] = $locations;
            // Apply search filter
            $properties_search_arg = apply_filters('inspiry_property_search', $properties_search_arg);
            // Apply sorting filter
            $properties_search_arg = apply_filters('inspiry_sort_properties', $properties_search_arg);
            // Create custom query
            $properties_search_query = new WP_Query($properties_search_arg);
            $compoundIds = [];
            $count = 0;
            unset($json_platform_messages);
            if ($properties_search_query->have_posts()) {
                $json_platform_messages = '';
                while ($properties_search_query->have_posts()) {
                    $properties_search_query->the_post();
                    unset($price);
                    if (isset(get_post_custom()['IDXGenerator_price_nlp'])) {
                        $price = get_post_custom()['IDXGenerator_price_nlp'][0];
                    }
                    if ((! isset($price)) && (isset(get_post_custom()['REAL_HOMES_property_price']))) {
                        $price = price_format(number_format(get_post_custom()['REAL_HOMES_property_price'][0], 2, '.', ','));
                        if ((isset($price)) && (isset(get_post_custom()['REAL_HOMES_property_price_postfix']))) {
                            $price .= " " . get_post_custom()['REAL_HOMES_property_price_postfix'][0];
                        }
                    }
                    $subtitle = isset($price) ? $price : '';
                    $url = get_the_permalink();
                    if (has_post_thumbnail()) {
                        $image = get_the_post_thumbnail_url();
                        if ((isset($image)) && ((endsWith($image, "undefined")) || ($image == false))) {
                            unset($image);
                        }
                    }
                    if (!isset($image)) {
                        $image = get_post_meta(get_The_ID(), '_thumbnail_id', true);
                        if ((isset($image)) && ((endsWith($image, "undefined")) || ($image == false))) {
                            unset($image);
                        }
                    }
                    if (! isset($image)) {
                        $image = get_inspiry_image_placeholder_url('inspiry-grid-thumbnail');
                        if ((isset($image)) && ((endsWith($image, "undefined")) || ($image == false))) {
                            unset($image);
                        }
                    }
                    if (isset(get_post_custom()['REAL_HOMES_property_address'])) {
                        $address = get_post_custom()['REAL_HOMES_property_address'][0];
                        if ((strlen($address) == 0) || ($address == "undefined")) {
                            unset($address);
                        }
                    }
                    $title = get_the_title();
                    if ((! isset($title)) || ($title == 'undefined') || ($title === 'undefined')) {
                        $title = '';
                    }
                    if (isset(get_post_custom()['REAL_HOMES_property_id'])) {
                        if (strlen($title) > 0) {
                            $title = get_post_custom()['REAL_HOMES_property_id'][0] . ': ' . $title;
                        } else {
                            $title = get_post_custom()['REAL_HOMES_property_id'][0];
                        }
                    }
                    if ((! isset($address)) || ($address == 'undefined') || ($address === 'undefined')) {
                        $address = ' ';
                    }
                    if ((! isset($subtitle)) || ($subtitle == 'undefined') || ($subtitle === 'undefined')) {
                        $subtitle = '';
                    }
                    if ($count > 0) {
                        $json_platform_messages .= ", ";
                    }
                    if ((count($locations) == 0) && ($address == ' ')) {
                        if (isset(get_post_custom()['IDXGenerator_Country'])) {
                            if (strlen($subtitle) > 0) {
                                $subtitle .= " in " . get_post_custom()['IDXGenerator_Country'][0];
                            } else {
                                $subtitle .= get_post_custom()['IDXGenerator_Country'][0];
                            }
                        }
                    }
                    // $queryNlpResponse .= '<script>console.log(\"' . console($address, true) . '\")</script>';
                    $json_platform_messages .= '{
        				"card" : {
	        				"buttons": [
						      {
						        "postback": "' . $url . '",
						        "text": "' . $title . '"
						      }
						    ],
						    ' . ((isset($image)) ? ('"imageUri": "' . $image . '",') : '') . '
						    ' . ((isset($price)) ? ('"subtitle": "' . $subtitle . '",') : '') . '
						    ' . (((isset($address)) && ($address != 'undefined') && ($address !== 'undefined')) ? ('"title": "' . $address . '",') : '') . '
						    "type": 1
						},
						"platform": "FACEBOOK"
					}';
                    array_push($compoundIds, get_post_custom()['compound_id'][0]);
                    $count ++;
                }
                if (($count > 0) && (isset($json_platform_messages))) {
                    $json_platform_messages .= ', {
					    "platform": "FACEBOOK",
					    "quickReplies": {
					    	"title": "Do you want to follow-up on these?",
					    	"quickReplies": [
						      "Show me ' . $number_of_properties . ' other ' . strtolower($originalPropertyType) . ' (' . ($tracer + $count + 1) . ' to ' . ($tracer + $count + $number_of_properties) . ')...",
						      "Leave your number. We will call you back!",
						      "Send us Email"
						    ]
						}
				  	}';
                }
            }
            wp_reset_postdata();
            $queryNlpResponse = (($count > 1) ? ($count . " first properties shown") : (($count == 1) ? ("One Property Found") : "No Property Found"));
            if (($count < $properties_search_query->found_posts) && ($count > 0)) {
                $queryNlpResponse .= " from " . $properties_search_query->found_posts;
            }
            $queryNlpResponse .= '.';
            $queryNlpResponse .= '<script>console.log(\"' . console_array($properties_search_arg, true) . '\")</script>';
            // $queryNlpResponse .= '<script>console.log(\"BURP\")</script>';
            $locationNlp = ((strlen($sector) > 0) ? $sector : "");
            $locationNlp = $locationNlp . ((strlen($city) > 0) ? ((strlen($locationNlp) > 0) ? (', ' . $city) : $city) : '');
            $locationNlp = $locationNlp . ((strlen($province) > 0) ? ((strlen($locationNlp) > 0) ? (', ' . $province) : $province) : "");
            $locationNlp = $locationNlp . ((strlen($country) > 0) ? ((strlen($locationNlp) > 0) ? (', ' . $country) : $country) : "");
            $featuresNlp = implode(", ", $features);
            if ($originalIntent === "Refine_Location") {
                $response = "Your search now takes the location " . $locationNlp . " in consideration.";
            } else if ($originalIntent === "Refine_Search_Max_Price") {
                $response = "The maximum price of " . $maxprice . " is considered.";
            } else if ($originalIntent === "Refine_Search_Min_Price") {
                $response = "The minimum price of " . $minprice . " is understood.";
            } else if ($originalIntent === "Redefine_Property_Type") {
                $response = "Looking for " . strtolower($originalPropertyType) . " instead.";
            } else if ($originalIntent === "Refine_Lifestyle") {
                $response = "These are the properties for the " . strtolower($lifestyle) . " lifestyle.";
            } else if ($originalIntent === "Refine_Features") {
                $response = "Here are options with the " . strtolower($featuresNlp) . " features.";
            } else if ($tracer == 0) {
                $response = "OK, got it! Here is what I have in terms of " . strtolower($originalPropertyType) . " " . strtolower($transactionType) . ".";
            } else {
                $response = ucfirst($originalPropertyType) . " " . ($tracer + 1) . " to " . ($tracer + $count) . ":";
                unset($queryNlpResponse);
            }
            if ((isset($queryNlpResponse)) && (isset($response)))
                $response .= (" " . $queryNlpResponse);
        }
        if ($response == "") {
            $json_reponse = '
	        {
	            "fulfillmentText": "This is from webhook_test response from processChatbotWebHook",
	            "fulfillmentMessages": [
	                {
	                    "text": {
	                        "text": [
	                            "This is from webhook_test response from processChatbotWebHook"
	                        ]
	                    }
	                }
	            ],
	            "debuginfo": "' . $debuginfo . '"
	        }';
        } else {
            $json_reponse = '
	        {
	            "fulfillmentText": "' . $response . '",
	            "fulfillmentMessages": [
	            	{
	                    "text": {
	                        "text": [
	                            "' . $response . '"
	                        ]
	                    }
	                }
	        		' . ((isset($json_platform_messages)) ? (', ' . $json_platform_messages) : '') . '
	            ],
	            "debuginfo": "' . $debuginfo . '"
	        }';
        }
        return $json_reponse;
    }
endif;

if (! function_exists('countries_list')) :

    function countries_list($withpermalinks, $useJurisdiction)
    {
        $return = [];
        if ((isset($useJurisdiction)) && $useJurisdiction) {
            $jurisdictions_country__args = array(
                'post_type' => 'jurisdiction',
                'posts_per_page' => - 1,
                'meta_query' => array(
                    array(
                        'key' => 'IDXGenerator_jurisdiction_type',
                        'value' => 'COUNTRY',
                        'compare' => '=',
                        'type' => 'text'
                    )
                )
            );
            $countries_query = new WP_Query(apply_filters('inspiry_home_properties', $jurisdictions_country__args));
            usort($countries_query->posts, 'compare_title');
            while ($countries_query->have_posts()) {
                $countries_query->the_post();
                array_push($return, get_the_title());
                if ($withpermalinks) {
                    array_push($return, get_permalink());
                }
            }
            wp_reset_postdata();
        } else {
            $the_query = get_categories('taxonomy=country&type=property');
            foreach ($the_query as $value) {
                array_push($return, $value->name);
                $category_link = get_category_link($value->cat_ID);
                if ($withpermalinks) {
                    array_push($return, $category_link);
                }
            }
        }
        return $return;
    }
endif;

    // add_action('wp_footer', 'countries_list');

if (! function_exists('representing_image')) :

    function representing_image()
    {
        $thumb = get_the_post_thumbnail(get_the_ID());
        if (! empty($thumb))
            $image = $thumb;
        else {
            $image = '<img src="';
            $image .= catch_that_image();
            $image .= '" alt="" />';
        }
        return $image;
    }
endif;

if (! function_exists('startsWith')) :

    function startsWith($string, $startString)
    {
        $len = strlen($startString);
        return (substr($string, 0, $len) === $startString);
    } 
endif;

if (! function_exists('endsWith')) :

    function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }
        return (substr($haystack, - $length) === $needle);
    }
endif;

if (! function_exists('price_format')) :

    function price_format($price)
    {
        $price = str_replace(".00 ", "", $price);
        if (endsWith($price, '.00')) {
            return substr($price, 0, - strlen('.00'));
        } else {
            $split = explode(" ", $price, 2);
            if (count($split) == 2) {
                return price_format($split[0]) . ' ' . price_format($split[1]);
            } else {
                return $price;
            }
        }
    }
endif;

if (! function_exists('catch_that_image')) :

    function catch_that_image()
    {
        global $post, $posts;
        $first_img = '';
        ob_start();
        ob_end_clean();
        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
        $first_img = $matches[1][0];

        if (empty($first_img)) { // Defines a default image
            $first_img = "/images/default.jpg";
        }
        return $first_img;
    }
endif;

if (! function_exists('inspiry_content_width')) :

    /**
     * Set the content width in pixels, based on the theme's design and stylesheet.
     *
     * Priority 0 to make it available to lower priority callbacks.
     *
     * @global int $content_width
     */
    function inspiry_content_width()
    {
        $content_width = $GLOBALS['content_width'];

        if (is_page_template('page-templates/full-width.php')) {
            $content_width = 1004;
        }

        $GLOBALS['content_width'] = apply_filters('inspiry_content_width', $content_width);
    }

    add_action('template_redirect', 'inspiry_content_width', 0);

endif;

/**
 * Set google reCAPTCHA display to false by default
 */
if (! isset($google_reCAPTCHA_counter)) {
    $google_reCAPTCHA_counter = 1;
}

/**
 * Theme based options declaration
 *
 * Note: I have set forced_dev_mode_off to avoid debug mode notices and links
 */
require_once (get_template_directory() . '/inc/theme-options/options-config.php');

/*
 * Meta boxes configuration
 */
require_once (get_template_directory() . '/inc/meta-boxes/config.php');

/*
 * TGM plugin activation
 */
require_once (get_template_directory() . '/inc/tgm/inspiry-required-plugins.php');

/*
 * One Click Demo Import
 */
require_once get_template_directory() . '/inc/demo-import/demo-import.php';

/*
 * Utility functions
 */
require_once (get_template_directory() . '/inc/util/basic-functions.php');
require_once (get_template_directory() . '/inc/util/lat-long-cluster-cache.php');
require_once (get_template_directory() . '/inc/util/header-functions.php');
require_once (get_template_directory() . '/inc/util/footer-functions.php');
require_once (get_template_directory() . '/inc/util/payment-functions.php');
require_once (get_template_directory() . '/inc/util/breadcrumbs-functions.php');
require_once (get_template_directory() . '/inc/util/favorites-functions.php');
require_once (get_template_directory() . '/inc/util/real-estate-functions.php');

require_once ABSPATH . 'wp-content/themes/inspiry-real-places/inc/util/postings-scroll.php';

if (! function_exists('inspiry_google_fonts')) :

    /**
     * Google fonts enqueue url
     */
    function inspiry_google_fonts()
    {
        $fonts_url = '';

        /*
         * Translators: If there are characters in your language that are not
         * supported by Varela Round, translate this to 'off'. Do not translate
         * into your own language.
         */

        $varela_round = _x('on', 'Varela Round font: on or off', 'inspiry');

        if ('off' !== $varela_round) {
            $font_families = array();

            if ('off' !== $varela_round) {
                $font_families[] = 'Varela Round';
            }

            $query_args = array(
                'family' => urlencode(implode('|', $font_families)),
                'subset' => urlencode('latin,latin-ext')
            );

            $fonts_url = add_query_arg($query_args, '//fonts.googleapis.com/css');
        }

        return esc_url_raw($fonts_url);
    }
endif;

if ( ! function_exists( 'wpjquery_enqueue_script' ) ) :
    function wpjquery_enqueue_script() {
        wp_deregister_script('jquery');
        wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"), false, false);
    }
    add_action('wp_enqueue_scripts', 'wpjquery_enqueue_script');
endif;

if (! function_exists('wpbootstrap_enqueue_styles')) :

    function wpbootstrap_enqueue_styles()
    {
        wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css');
        wp_enqueue_style('my-style', get_template_directory_uri() . '/style.css');
    }
    add_action('wp_enqueue_scripts', 'wpbootstrap_enqueue_styles');
endif;

add_action( 'admin_enqueue_scripts', 'wp_enqueue_media' );

if (! function_exists('custom_user_login')) :
    function custom_user_login() {
        
        // make sure regisration form is submitted
        if ($_SERVER['REQUEST_METHOD'] != 'POST')
            return;
            
            // base of user_login, change it according to ur needs
            $ulogin = $_POST['user_login'];
            
            // make user_login unique so WP will not return error
            $check = username_exists($ulogin);
            if (!empty($check)) {
                $suffix = 2;
                while (!empty($check)) {
                    $alt_ulogin = $ulogin . '-' . $suffix;
                    $check = username_exists($alt_ulogin);
                    $suffix++;
                }
                $ulogin = $alt_ulogin;
            }
            
            $_POST['user_login'] = $ulogin;
    }
    add_action('login_form_register', 'custom_user_login');
endif;

if (!function_exists('mustBeLoggedCheck')) :
    function mustBeLoggedCheck() {
        if (get_current_user_id() == 0) {
            echo 'Error: not logged in.';
            die();
        }
    }
endif;

if (! function_exists('inspiry_enqueue_styles')) :

    /**
     * Enqueue required styles for front end
     *
     * @since 1.0.0
     * @return void
     */
    function inspiry_enqueue_styles()
    {
        if (! is_admin()) :

            $inspiry_template_directory_uri = get_template_directory_uri();
            wp_enqueue_style('style_table', '//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css', array(), 1, 'all');

            wp_enqueue_style('style_toggle', 'https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css', array(), 1, 'all');

            // Google Font
            wp_enqueue_style('google-varela-round', inspiry_google_fonts(), array(), INSPIRY_THEME_VERSION);

            // flexslider
            wp_enqueue_style('flexslider', $inspiry_template_directory_uri . '/js/flexslider/flexslider.css', array(), '2.4.0');

            // lightslider
            wp_enqueue_style('lightslider', $inspiry_template_directory_uri . '/js/lightslider/css/lightslider.min.css', array(), '1.1.2');

            // owl carousel
            wp_enqueue_style('owl-carousel', $inspiry_template_directory_uri . '/js/owl.carousel/owl.carousel.css', array(), INSPIRY_THEME_VERSION);

            // swipebox
            wp_enqueue_style('swipebox', $inspiry_template_directory_uri . '/js/swipebox/css/swipebox.min.css', array(), '1.3.0');

            // select2
            wp_enqueue_style('select2', $inspiry_template_directory_uri . '/js/select2/select2.css', array(), '4.0.0');

            // font awesome
            wp_enqueue_style('font-awesome', $inspiry_template_directory_uri . '/css/font-awesome.min.css', array(), '4.3.0');

            // animate css
            wp_enqueue_style('animate', $inspiry_template_directory_uri . '/css/animate.css', array(), INSPIRY_THEME_VERSION);

            // magnific popup css
            wp_enqueue_style('magnific-popup', $inspiry_template_directory_uri . '/js/magnific-popup/magnific-popup.css', array(), '1.0.0');

            if (is_singular('property')) {
                // entypo fonts.
                wp_enqueue_style('entypo-fonts', $inspiry_template_directory_uri . '/css/entypo.min.css', array(), INSPIRY_THEME_VERSION, 'all');
            }

            // main styles
            wp_enqueue_style('inspiry-main', $inspiry_template_directory_uri . '/css/main.css', array(
                'font-awesome',
                'animate'
            ), INSPIRY_THEME_VERSION);

            // theme styles
            wp_enqueue_style('inspiry-theme', $inspiry_template_directory_uri . '/css/theme.css', array(
                'inspiry-main'
            ), INSPIRY_THEME_VERSION);

            // parent theme style.css
            wp_enqueue_style('inspiry-parent-default', get_stylesheet_uri(), array(
                'inspiry-main'
            ), INSPIRY_THEME_VERSION);

            // if rtl is enabled
            if (is_rtl()) {

                wp_enqueue_style('inspiry-main-rtl', $inspiry_template_directory_uri . '/css/main-rtl.css', array(
                    'inspiry-main'
                ), INSPIRY_THEME_VERSION);

                wp_enqueue_style('inspiry-theme-rtl', $inspiry_template_directory_uri . '/css/theme-rtl.css', array(
                    'inspiry-main',
                    'inspiry-theme',
                    'inspiry-main-rtl'
                ), INSPIRY_THEME_VERSION);
            }

            // parent theme css/custom.css
            wp_enqueue_style('inspiry-parent-custom', $inspiry_template_directory_uri . '/css/custom.css', array(
                'inspiry-parent-default'
            ), INSPIRY_THEME_VERSION);

            // multiselect

            wp_enqueue_style('jquery-multiselect-css', get_template_directory_uri() . "/css/jquery-multiselect.css", array(), INSPIRY_THEME_VERSION);

            // custom css

            wp_enqueue_style('custom-css-bitsclan', get_template_directory_uri() . "/css/bitsclan.css", array(), INSPIRY_THEME_VERSION);

            wp_add_inline_style('inspiry-parent-custom', apply_filters('realplaces_custom_css', ''));

		endif;

    }

endif;

    // inspiry_enqueue_styles

add_action('wp_enqueue_scripts', 'inspiry_enqueue_styles');

if (! function_exists('inspiry_admin_styles')) :

    /**
     * Enqueue styles for admin related things.
     */
    function inspiry_admin_styles()
    {

        // Google Font
        wp_enqueue_style('google-varela-round', inspiry_google_fonts(), array(), INSPIRY_THEME_VERSION);

        wp_enqueue_style('inspiry-admin-styles', get_template_directory_uri() . '/css/admin.css');
    }

    add_action('admin_enqueue_scripts', 'inspiry_admin_styles');
endif;

if (! function_exists('inspiry_enqueue_scripts')) :

    /**
     * Enqueue required java scripts for front end
     *
     * @since 1.0.0
     * @return void
     */
    function inspiry_enqueue_scripts()
    {
        if (! is_admin()) :

            global $inspiry_options;
            $inspiry_template_directory_uri = get_template_directory_uri();

            wp_enqueue_script('jquery_datatables', 'https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js', array(), 1, 1, 1);

            wp_enqueue_script('jquery_datatables', 'https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js', array(), 1, 1, 1);

            // flex slider
            wp_enqueue_script('flexslider', $inspiry_template_directory_uri . '/js/flexslider/jquery.flexslider-min.js', array(
                'jquery'
            ), '2.4.0', true);

            if (is_singular('property')) {
                // light slider
                wp_enqueue_script('lightslider', $inspiry_template_directory_uri . '/js/lightslider/js/lightslider.min.js', array(
                    'jquery'
                ), '1.1.2', true);
            }

            if (is_singular('property') || is_page_template('page-templates/home.php')) {
                // owl carousel
                wp_enqueue_script('owl-carousel', $inspiry_template_directory_uri . '/js/owl.carousel/owl.carousel.min.js', array(
                    'jquery'
                ), INSPIRY_THEME_VERSION, true);
            }

            // swipebox
            wp_enqueue_script('swipebox', $inspiry_template_directory_uri . '/js/swipebox/js/jquery.swipebox.min.js', array(
                'jquery'
            ), '1.3.0', true);

            // select2
            wp_enqueue_script('select2', $inspiry_template_directory_uri . '/js/select2/select2.min.js', array(
                'jquery'
            ), '4.0.0', true);

            // hoverIntent
            wp_enqueue_script('hoverIntent');

            // transition
            wp_enqueue_script('transition', $inspiry_template_directory_uri . '/js/transition.js', array(
                'jquery'
            ), '3.3.1', true);

            // appear
            wp_enqueue_script('appear', $inspiry_template_directory_uri . '/js/jquery.appear.js', array(
                'jquery'
            ), '0.3.4', true);

            // modal
            wp_enqueue_script('modal', $inspiry_template_directory_uri . '/js/modal.js', array(
                'jquery'
            ), '3.3.4', true);

            // mean menu
            wp_enqueue_script('meanmenu', $inspiry_template_directory_uri . '/js/meanmenu/jquery.meanmenu.min.js', array(
                'jquery'
            ), '2.0.8', true);

            // Placeholder
            wp_enqueue_script('placeholder', $inspiry_template_directory_uri . '/js/jquery.placeholder.min.js', array(
                'jquery'
            ), '2.1.2', true);

            // multiselect jquery
            wp_enqueue_script('multiselect', $inspiry_template_directory_uri . '/js/jquery-multiselect.js', array(
                'jquery'
            ), '2.4.17', true);

            if (is_page_template('page-templates/2-columns-gallery.php') || is_page_template('page-templates/3-columns-gallery.php') || is_page_template('page-templates/4-columns-gallery.php')) {

                // isotope
                wp_enqueue_script('isotope', $inspiry_template_directory_uri . '/js/isotope.pkgd.min.js', array(
                    'jquery'
                ), '2.2.0', true);
            }

            // jQuery
            wp_enqueue_script('jquery');

            // jQuery UI
            wp_enqueue_script('jquery-ui-core');
            wp_enqueue_script('jquery-ui-autocomplete');
            wp_enqueue_script('jquery-ui-sortable');
            wp_enqueue_script('jquery-ui-tooltip');

            if (is_singular('property') || is_page_template('page-templates/contact.php') || is_page_template('page-templates/properties-search.php') || is_page_template('page-templates/home.php') || is_page_template('page-templates/properties-list.php') || is_page_template('page-templates/properties-list-with-sidebar.php') || is_page_template('page-templates/properties-grid.php') || is_page_template('page-templates/properties-grid-with-sidebar.php') || is_page_template('page-templates/properties-list-half-map.php') || is_page_template('page-templates/submit-property.php') || is_tax('property-city') || is_tax('property-status') || is_tax('property-type') || is_tax('property-feature') || is_post_type_archive('property')) {

                if (inspiry_has_google_maps_api_key()) {
                    /* default map query arguments */
                    $google_map_arguments = array();

                    // Google Map API
                    wp_enqueue_script('google-map-api', esc_url_raw(add_query_arg(apply_filters('inspiry_google_map_arguments', $google_map_arguments), '//maps.google.com/maps/api/js')), array(), '3.21', true);
                }
            }

            if (is_page_template('page-templates/properties-search.php') || is_page_template('page-templates/home.php') || is_page_template('page-templates/properties-list.php') || is_page_template('page-templates/properties-list-with-sidebar.php') || is_page_template('page-templates/properties-grid.php') || is_page_template('page-templates/properties-grid-with-sidebar.php') || is_page_template('page-templates/properties-list-half-map.php') || is_tax('property-city') || is_tax('property-status') || is_tax('property-type') || is_tax('property-feature') || is_post_type_archive('property')) {

                wp_enqueue_script('google-map-info-box', $inspiry_template_directory_uri . '/js/infobox.js', array(
                    'google-map-api'
                ), '1.1.9', true);

                wp_enqueue_script('google-map-marker-clusterer', $inspiry_template_directory_uri . '/js/markerclusterer.js', array(
                    'google-map-api'
                ), '2.1.1', true);
            }

            wp_register_script('lat-long-cluster-cache', $inspiry_template_directory_uri . '/js/lat-long-cluster-cache.js', array(
                'google-map-api'
            ), INSPIRY_THEME_VERSION, true);

            wp_register_script('properties-google-map', $inspiry_template_directory_uri . '/js/properties-google-map.js', array(
                'google-map-api'
            ), INSPIRY_THEME_VERSION, true);

            if (is_singular('property')) {
                wp_register_script('property-google-map', $inspiry_template_directory_uri . '/js/property-google-map.js', array(
                    'google-map-api'
                ), INSPIRY_THEME_VERSION, true);
            }

            // Comment reply script
            if (is_singular() && comments_open() && get_option('thread_comments')) {
                wp_enqueue_script('comment-reply');
            }

            // favorites
            if (is_singular('property') || is_page_template('page-templates/favorites.php')) {

                wp_enqueue_script('inspiry-favorites', $inspiry_template_directory_uri . '/js/inspiry-favorites.js', array(
                    'jquery'
                ), INSPIRY_THEME_VERSION, true);
            }

            // Property search form script
            wp_enqueue_script('inspiry-search-form', $inspiry_template_directory_uri . '/js/inspiry-search-form.js', array(
                'jquery'
            ), INSPIRY_THEME_VERSION, true);

            $localized_search_params = array();
            if (isset($inspiry_options['inspiry_status_for_rent']) && ! empty($inspiry_options['inspiry_status_for_rent'])) {
                $status_term = get_term_by('id', intval($inspiry_options['inspiry_status_for_rent']), 'property-status');
                $localized_search_params['rent_slug'] = is_object($status_term) ? $status_term->slug : '';
            }

            wp_localize_script('inspiry-search-form', 'SearchForm', $localized_search_params);

            // edit profile
            if (is_page_template('page-templates/edit-profile')) {

                wp_enqueue_script('plupload');

                wp_enqueue_script('inspiry-edit-profile', $inspiry_template_directory_uri . '/js/inspiry-edit-profile.js', array(
                    'jquery',
                    'plupload'
                ), INSPIRY_THEME_VERSION, true);
			
			

                $edit_profile_data = array(
                    'ajaxURL' => admin_url('admin-ajax.php'),
                    'uploadNonce' => wp_create_nonce('inspiry_allow_upload'),
                    'fileTypeTitle' => esc_html__('Valid file formats', 'inspiry')
                );

                wp_localize_script('inspiry-edit-profile', 'editProfile', $edit_profile_data);
            }

            // Property submit
            if (is_page_template('page-templates/submit-property.php')) {

                wp_enqueue_script('plupload');
                wp_enqueue_script('jquery-ui-sortable');

                wp_enqueue_script('inspiry-property-submit', $inspiry_template_directory_uri . '/js/inspiry-property-submit.js', array(
                    'jquery',
                    'plupload',
                    'jquery-ui-sortable'
                ), INSPIRY_THEME_VERSION, true);

                $property_submit_data = array(
                    'ajaxURL' => admin_url('admin-ajax.php'),
                    'uploadNonce' => wp_create_nonce('inspiry_allow_upload'),
                    'fileTypeTitle' => esc_html__('Valid file formats', 'inspiry')
                );
                wp_localize_script('inspiry-property-submit', 'propertySubmit', $property_submit_data);
            }

            // Property Single
            if (is_singular('property')) {

                wp_enqueue_script('magnific-popup', $inspiry_template_directory_uri . '/js/magnific-popup/jquery.magnific-popup.min.js', array(
                    'jquery'
                ), '1.0.0', true);

                wp_enqueue_script('imagesloaded-js', $inspiry_template_directory_uri . '/js/imagesloaded.pkgd.min.js', array(
                    'jquery'
                ), INSPIRY_THEME_VERSION, true);

                wp_enqueue_script('sly-js', $inspiry_template_directory_uri . '/js/sly.min.js', array(
                    'jquery'
                ), INSPIRY_THEME_VERSION, true);

                wp_enqueue_script('property-horizontal-scrolling', $inspiry_template_directory_uri . '/js/property-horizontal-scrolling.js', array(
                    'jquery'
                ), INSPIRY_THEME_VERSION, true);
            }

            // contact
            if (is_page_template('page-templates/contact.php')) {

                // Get all custom post meta information related to contact page
                $contact_meta_data = get_post_custom(get_the_ID());

                // Google Map
                if (isset($contact_meta_data['inspiry_map_address']) && isset($contact_meta_data['inspiry_map_location'])) {

                    if (! empty($contact_meta_data['inspiry_map_address'][0]) && ! empty($contact_meta_data['inspiry_map_location'][0])) {

                        $lat_lng = explode(',', $contact_meta_data['inspiry_map_location'][0]);

                        if (is_array($lat_lng) && isset($lat_lng[0]) && isset($lat_lng[1])) {

                            $lat = $lat_lng[0];
                            $lng = $lat_lng[1];
                            $map_zoom = 15;

                            if (isset($contact_meta_data['inspiry_map_zoom'])) {
                                $map_zoom = intval($contact_meta_data['inspiry_map_zoom'][0]);
                            }

                            wp_enqueue_script('contact-google-map', $inspiry_template_directory_uri . '/js/contact-google-map.js', array(
                                'google-map-api'
                            ), INSPIRY_THEME_VERSION, true);

                            $contact_map_data = array(
                                'lat' => $lat,
                                'lng' => $lng,
                                'zoom' => $map_zoom,
                                'icon' => get_template_directory_uri() . '/images/svg/map-marker.svg'
                            );
                            wp_localize_script('contact-google-map', 'contactMapData', $contact_map_data);
                        }
                    }
                }
            }

            if (! is_user_logged_in()) {
                wp_enqueue_script('login-modal', $inspiry_template_directory_uri . '/js/login-modal.js', array(
                    'jquery'
                ), INSPIRY_THEME_VERSION, true);
            }

            // Main js
            wp_enqueue_script('custom', $inspiry_template_directory_uri . '/js/custom.js', array(
                'jquery'
            ), INSPIRY_THEME_VERSION, true);
                        
            wp_add_inline_script('custom', apply_filters('realplaces_custom_js', ''));

		endif;

    }

endif;

    // inspiry_enqueue_scripts

add_action('wp_enqueue_scripts', 'inspiry_enqueue_scripts');

if (! function_exists('inspiry_enqueue_admin_scripts')) :

    /**
     * Enqueue admin side scripts
     *
     * @param
     *            $hook
     */
    function inspiry_enqueue_admin_scripts($hook)
    {
        if ($hook == 'post.php' || $hook == 'post-new.php') {
            global $post_type;
            if (('post' == $post_type) || ('page' == $post_type)) {
                wp_enqueue_script('post-meta-box-switcher', get_template_directory_uri() . '/js/admin.js', array(
                    'jquery'
                ), INSPIRY_THEME_VERSION);
            }
        }
    }

    add_action('admin_enqueue_scripts', 'inspiry_enqueue_admin_scripts');
endif;

if (! function_exists('inspiry_theme_sidebars')) :

    /**
     * Register theme sidebars
     *
     * @since 1.0.0
     */
    function inspiry_theme_sidebars()
    {

        // Location: Default Sidebar
        register_sidebar(array(
            'name' => esc_html__('Default Sidebar', 'inspiry'),
            'id' => 'default-sidebar',
            'description' => esc_html__('Widget area for default sidebar on news and post pages.', 'inspiry'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));

        // Location: Sidebar Pages
        register_sidebar(array(
            'name' => esc_html__('Pages Sidebar', 'inspiry'),
            'id' => 'default-page-sidebar',
            'description' => esc_html__('Widget area for default page template sidebar.', 'inspiry'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));

        // Location: Sidebar Properties List
        register_sidebar(array(
            'name' => esc_html__('Properties List Sidebar', 'inspiry'),
            'id' => 'properties-list',
            'description' => esc_html__('Widget area for properties list template with sidebar support.', 'inspiry'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));

        // Location: Sidebar Properties Grid
        register_sidebar(array(
            'name' => esc_html__('Properties Grid Sidebar', 'inspiry'),
            'id' => 'properties-grid',
            'description' => esc_html__('Widget area for properties grid template with sidebar support.', 'inspiry'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));

        // Location: Sidebar Property
        register_sidebar(array(
            'name' => esc_html__('Property Sidebar', 'inspiry'),
            'id' => 'property-sidebar',
            'description' => esc_html__('Widget area for property detail page sidebar.', 'inspiry'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));

        // Location: Sidebar dsIDX
        register_sidebar(array(
            'name' => esc_html__('dsIDXpress Sidebar', 'inspiry'),
            'id' => 'dsidx-sidebar',
            'description' => esc_html__('Widget area for dsIDX related pages.', 'inspiry'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));

        // Location: Footer First Column
        register_sidebar(array(
            'name' => esc_html__('Footer First Column', 'inspiry'),
            'id' => 'footer-first-column',
            'description' => esc_html__('Widget area for first column in footer.', 'inspiry'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));

        // Location: Footer Second Column
        register_sidebar(array(
            'name' => esc_html__('Footer Second Column', 'inspiry'),
            'id' => 'footer-second-column',
            'description' => esc_html__('Widget area for second column in footer.', 'inspiry'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));

        // Location: Footer Third Column
        register_sidebar(array(
            'name' => esc_html__('Footer Third Column', 'inspiry'),
            'id' => 'footer-third-column',
            'description' => esc_html__('Widget area for third column in footer.', 'inspiry'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));

        // Location: Footer Fourth Column
        register_sidebar(array(
            'name' => esc_html__('Footer Fourth Column', 'inspiry'),
            'id' => 'footer-fourth-column',
            'description' => esc_html__('Widget area for fourth column in footer.', 'inspiry'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));
    }

    add_action('widgets_init', 'inspiry_theme_sidebars');

endif;

if (! function_exists('inspiry_apply_google_maps_arguments')) :

    /**
     * This function adds google maps arguments to admins side maps displayed in meta boxes
     */
    function inspiry_apply_google_maps_arguments($google_maps_url)
    {

        /* default map query arguments */
        $google_map_arguments = array();

        return esc_url_raw(add_query_arg(apply_filters('inspiry_google_map_arguments', $google_map_arguments), $google_maps_url));
    }

    add_filter('rwmb_google_maps_url', 'inspiry_apply_google_maps_arguments');
endif;

if (! function_exists('inspiry_has_google_maps_api_key')) :

    /**
     * Checks if Google Maps API Key exists
     */
    function inspiry_has_google_maps_api_key()
    {
        $inspiry_map_option = get_option('inspiry_map_option');

        if (isset($inspiry_map_option['inspiry_google_map_api_key']) && ! empty($inspiry_map_option['inspiry_google_map_api_key'])) {
            return true;
        }

        return false;
    }
endif;

if (! function_exists('inspiry_google_maps_api_key')) :

    /**
     * This function adds API key ( if provided in settings ) to google maps arguments
     */
    function inspiry_google_maps_api_key($google_map_arguments)
    {

        /* Get Google Maps API Key if available */
        if (inspiry_has_google_maps_api_key()) {
            $inspiry_map_option = get_option('inspiry_map_option');
            $google_map_arguments['key'] = urlencode($inspiry_map_option['inspiry_google_map_api_key']);
        }

        return $google_map_arguments;
    }

    add_filter('inspiry_google_map_arguments', 'inspiry_google_maps_api_key');
endif;

if (! function_exists('inspiry_google_maps_language')) :

    /**
     * This function add current language to google maps arguments
     */
    function inspiry_google_maps_language($google_map_arguments)
    {
        $inspiry_map_option = get_option('inspiry_map_option');

        /* Localise Google Map if related theme options is set */
        if (isset($inspiry_map_option['inspiry_google_map_auto_lang']) && '1' === $inspiry_map_option['inspiry_google_map_auto_lang']) {
            if (function_exists('wpml_object_id_filter')) {
                $google_map_arguments['language'] = urlencode(ICL_LANGUAGE_CODE);
            } else {
                $google_map_arguments['language'] = urlencode(get_locale());
            }
        }

        return $google_map_arguments;
    }

    add_filter('inspiry_google_map_arguments', 'inspiry_google_maps_language');
endif;

if (! function_exists('inspiry_post_classes')) :

    function inspiry_post_classes($classes)
    {
        $classes[] = 'clearfix';

        if (is_page_template('page-templates/template-compare.php')) {
            $classes[] = 'remove-overflow';
        } elseif (is_page_template(array(
            'page-templates/properties-grid.php',
            'page-templates/properties-grid-with-sidebar.php',
            'page-templates/properties-list.php',
            'page-templates/properties-list-with-sidebar.php',
            'page-templates/properties-list-half-map.php'
        ))) {
            $classes[] = 'property-content';
        } elseif (is_singular('post') || is_home() || is_archive() || is_search()) {

            $post_type = get_post_type(get_the_ID());

            if ('post' == $post_type) {
                $classes[] = 'blog-post';
            }

            if (is_home() || is_archive() || is_search()) {
                $classes[] = 'blog-listing-post';
            }

            // header margin fix in case of no thumbnail for a blog post
            if ('post' == $post_type && ! has_post_thumbnail()) {
                $classes[] = ' entry-header-margin-fix';
            }
        }

        return $classes;
    }

    add_filter('post_class', 'inspiry_post_classes');
endif;

/**
 * Functions: Compare.
 *
 * This file contain functions related to compare properties.
 *
 * @since 1.6.3
 */
if (! function_exists('inspiry_add_to_compare')) {

    /**
     * Add a property to list of compare.
     */
    function inspiry_add_to_compare()
    {

        /* Store the property id in the list of compare cookie */
        if (isset($_POST['property_id'])) {

            // Get the property id from form.
            $property_id = intval($_POST['property_id']);
            // Check if the property id is valid.
            if ($property_id > 0) {

                $property_img = wp_get_attachment_image_src(get_post_meta($property_id, '_thumbnail_id', true), 'inspiry-grid-thumbnail');

                if (empty($property_img)) {
                    $property_img[0] = get_inspiry_image_placeholder_url('inspiry-grid-thumbnail');
                }

                $inspiry_compare = array();
                if (isset($_COOKIE['inspiry_compare'])) {
                    $inspiry_compare = unserialize($_COOKIE['inspiry_compare']);
                }
                $inspiry_compare[] = $property_id;

                if (setcookie('inspiry_compare', serialize($inspiry_compare), time() + (60 * 60 * 24 * 30), '/')) {
                    echo json_encode(array(
                        'success' => true,
                        'message' => esc_html__('Added to Compare', 'inspiry'),
                        'img' => (isset($property_img[0])) ? $property_img[0] : false,
                        'img_width' => (isset($property_img[1])) ? $property_img[1] : false,
                        'img_height' => (isset($property_img[2])) ? $property_img[2] : false,
                        'property_id' => $property_id,
                        'ajaxURL' => admin_url('admin-ajax.php')
                    ));
                } else {
                    echo json_encode(array(
                        'success' => false,
                        'message' => esc_html__('Failed!', 'inspiry')
                    ));
                }
            }
        } else {
            echo json_encode(array(
                'success' => false,
                'message' => esc_html__('Invalid Parameters', 'inspiry')
            ));
        }
        die();
    }

    add_action('wp_ajax_inspiry_add_to_compare', 'inspiry_add_to_compare');
    add_action('wp_ajax_nopriv_inspiry_add_to_compare', 'inspiry_add_to_compare');
}

if (! function_exists('inspiry_is_added_to_compare')) {

    /**
     * Check if a property is already added to compare list.
     *
     * @param
     *            $property_id
     *            
     * @return bool
     */
    function inspiry_is_added_to_compare($property_id)
    {
        if ($property_id > 0) {
            /* check cookies for property id */
            if (isset($_COOKIE['inspiry_compare'])) {
                $inspiry_compare = unserialize($_COOKIE['inspiry_compare']);
                if (in_array($property_id, $inspiry_compare)) {
                    return true;
                }
            }
        }

        return false;
    }
}

if (! function_exists('inspiry_remove_from_compare')) {

    /**
     * Remove from compare
     */
    function inspiry_remove_from_compare()
    {
        if (isset($_POST['property_id'])) {

            $property_id = intval($_POST['property_id']);

            if ($property_id > 0) {

                if (isset($_COOKIE['inspiry_compare'])) {

                    $inspiry_compare = unserialize($_COOKIE['inspiry_compare']);
                    $target_index = array_search($property_id, $inspiry_compare);

                    if ($target_index >= 0 && $target_index !== false) {
                        unset($inspiry_compare[$target_index]);
                        setcookie('inspiry_compare', serialize($inspiry_compare), time() + (60 * 60 * 24 * 30), '/');

                        echo json_encode(array(
                            'success' => true,
                            'property_id' => $property_id,
                            'message' => esc_html__("Removed Successfully!", 'inspiry')
                        ));
                        die();
                    } else {
                        echo json_encode(array(
                            'success' => false,
                            'message' => esc_html__("Failed to remove!", 'inspiry')
                        ));
                        die();
                    }
                }
            }
        }

        echo json_encode(array(
            'success' => false,
            'message' => esc_html__("Invalid Parameters!", 'inspiry')
        ));
        die();
    }

    add_action('wp_ajax_inspiry_remove_from_compare', 'inspiry_remove_from_compare');
    add_action('wp_ajax_nopriv_inspiry_remove_from_compare', 'inspiry_remove_from_compare');
}

if (! function_exists('inspiry_theme_options_set')) {

    /**
     * Return true if ID is set
     *
     * @param
     *            $id
     *            
     * @return bool
     * @since 1.7.0
     */
    function inspiry_theme_options_set($id)
    {
        global $inspiry_options;

        if (isset($inspiry_options[$id])) {
            return true;
        }

        return false;
    }
}

if (! function_exists('realplaces_content_width')) {

    /**
     * Adds css class to body when related sidebar is not active
     */
    function realplaces_content_width($classes)
    {
        if (is_home() && ! inspiry_is_active_custom_sidebar('default-sidebar')) {

            $classes[] = 'realplaces-content-fullwidth blog-content-fullwidth';
        } elseif (is_singular('post') && ! inspiry_is_active_custom_sidebar('default-sidebar')) {

            $classes[] = 'realplaces-content-fullwidth single-content-fullwidth';
        } elseif (is_page() && ! is_page_template() && ! inspiry_is_active_custom_sidebar('default-page-sidebar')) {

            $classes[] = 'realplaces-content-fullwidth page-content-fullwidth';
        } elseif (is_page_template('page-templates/properties-grid-with-sidebar.php') && ! inspiry_is_active_custom_sidebar('properties-grid')) {

            $classes[] = 'realplaces-content-fullwidth properties-grid-content-fullwidth';
        } elseif (is_page_template('page-templates/properties-list-with-sidebar.php') && ! inspiry_is_active_custom_sidebar('properties-list')) {

            $classes[] = 'realplaces-content-fullwidth properties-list-content-fullwidth';
        }

        return $classes;
    }
    add_filter('body_class', 'realplaces_content_width');
}


if (! function_exists('property_saved')) {
    function property_saved($id)
    {
        $post_type = get_post_type($id);
        if ("property" != $post_type)
            return;
        $meta = get_post_meta($id);
        $lat = get_post_meta($id, 'property_latitude', true);
        $lng = get_post_meta($id, 'property_longitude', true);
        $country = get_post_meta($id, 'IDXGenerator_Country', true);
        $listingkey = get_post_meta($id, 'REAL_HOMES_property_id', true);
        cluster_cache_update_property($id, $lat, $lng, false, $country, $listingkey);
    }
    add_action('pmxi_saved_post', 'property_saved', 10, 1);
}

if (! function_exists('register_session')) {
    function register_session()
    {
        if ( ! session_id() ) {
    		session_start();
    	}
    }
    add_action('init', 'register_session');
}

if (! function_exists('json_search_contents')) {
    function json_search_contents($content) {
        $return = [];
        $current_url = "//" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        if (!is_array($content)) {
            $datavalue = json_decode($content, true);
        } else {
            $datavalue = $content;
        }
        $Property_status_mapping = array(
            'For Sale' => array('1','4','7','9','11','13','16','17','19','21','22','23','25'),
            'For Rent' => array('2','5','8','10','12','14','18','20','24','26'),
            'For Vacation Rental Use' => array('3','6','15')
        );
        $Property_types_mapping = array(
            'Apartments' => array('4', '5', '6', '19', '20'),
            'Buildings' => array('27'),
            'Houses' => array('1', '2', '3', '18'),
            'Land' => array('11', '12', '17'),
            'Locals' => array('7', '8'),
            'Offices' => array('9', '10'),
            'Wharehouses' => array('25', '26'),
            'Country-houses' => array('13', '14', '15'),
            'Multi-Familial Houses' => array('21', '22'),
            'Others' => array('23', '24')
        );
                    
        if (isset($datavalue['keyword'])) {
            $return['keyword'] = $datavalue['keyword'];
        }
        
        if (isset($datavalue['country'])) {
            $return['country'] = $datavalue['country'];
        }
        
        if (isset($datavalue['state'])) {
            $return['state'] = $datavalue['state'];
        }
        
        if (isset($datavalue['city'])) {
            $return['city'] = $datavalue['city'];
        }
        
        if (isset($datavalue['sector'])) {
            $return['sector'] = $datavalue['sector'];
        }
        
        if (isset($datavalue['beds'])) {
            $return['beds'] = $datavalue['beds'];
        }
        
        if (isset($datavalue['baths'])) {
            $return['baths'] = $datavalue['baths'];
        }
        
        if (isset($datavalue['min_price'])) {
            $return['min_price'] = $datavalue['min_price'];
        }
        
        if (isset($datavalue['max_price'])) {
            $return['max_price'] = $datavalue['max_price'];
        }
        
        if (isset($datavalue['min_area'])) {
            $return['min_area'] = $datavalue['min_area'];
        }
        
        if (isset($datavalue['max_area'])) {
            $return['max_area'] = $datavalue['max_area'];
        }
        
        if (isset($datavalue['agent_id'])) {
            $return['agent_id'] = $datavalue['agent_id'];
        }
        
        if (isset($datavalue['features'])) {
            $return['features'] = $datavalue['features'];
        }
        
        if (isset($datavalue['property_status'])) {
            $return['catid'] = $Property_status_mapping[$datavalue['property_status']];
        }
        
        if (isset($datavalue['property_type'])) {
            if ((!isset($datavalue['property_status'])) || (count($return['catid']) == 0) || ($return['catid'] == null)) {
                $return['catid'] = $Property_types_mapping[$datavalue['property_type']];
            } else {
                $return['catid'] = array_intersect($Property_types_mapping[$datavalue['property_type']],  $return['catid']);
            }
        }
        
        if ((isset($datavalue['catid'])) && (!isset($return['catid']))) {
            $return['catid'] = $datavalue['catid'];
        }
        
        return $return;
    }
}

if (! function_exists('getMetaQueryArray')) {
    function getMetaQueryArray($values) {
        
        $country = $values['country'];
        $state = $values['state'];
        $city = $values['city'];
        $sector = $values['sector'];
        $beds = $values['beds'];
        $agent_id = $values['agent_id'];
        $baths = $values['baths'];
        $min_price = $values['min_price'];
        $max_price = $values['max_price'];
        $min_area = $values['min_area'];
        $max_area = $values['max_area'];
        $valueoftype = $values['propertytype'];
        $valueofstatus = $values['statustype'];
        $features = $values['features'];
        
        unset($metaquery);
        if ($country === 'Worldwide') {
            $country = '';
        }
        if ((isset($country)) && (strlen($country) > 0)) {
            $queryportion = array(
                array(
                    'key' => 'IDXGenerator_Country',
                    'value' => $country
                )
            );
            if ((isset($metaquery)) && (count($metaquery) > 0) && (count($queryportion) > 0)) {
                $metaquery['relation'] = 'AND';
                array_push($metaquery, $queryportion);
            } else {
                $metaquery = $queryportion;
            }
        }
        if ((isset($state)) && (strlen($state) > 0)) {
            $queryportion = array(
                array(
                    'key' => 'IDXGenerator_Province',
                    'value' => $state
                )
            );
            if ((isset($metaquery)) && (count($metaquery) > 0) && (count($queryportion) > 0)) {
                $metaquery['relation'] = 'AND';
                array_push($metaquery, $queryportion);
            } else {
                $metaquery = $queryportion;
            }
        }
        if ((isset($city)) && (strlen($city) > 0)) {
            $queryportion = array(
                array(
                    'key' => 'IDXGenerator_City',
                    'value' => $city
                )
            );
            if ((isset($metaquery)) && (count($metaquery) > 0) && (count($queryportion) > 0)) {
                $metaquery['relation'] = 'AND';
                array_push($metaquery, $queryportion);
            } else {
                $metaquery = $queryportion;
            }
        }
        if ((isset($sector)) && (strlen($sector) > 0)) {
            $queryportion = array(
                array(
                    'key' => 'IDXGenerator_Sector',
                    'value' => $sector
                )
            );
            if ((isset($metaquery)) && (count($metaquery) > 0) && (count($queryportion) > 0)) {
                $metaquery['relation'] = 'AND';
                array_push($metaquery, $queryportion);
            } else {
                $metaquery = $queryportion;
            }
        }
        if ((isset($agent_id)) && (strlen($agent_id) > 0)) {
            $queryportion = array(
                array(
                    'key' => 'IDXGenerator_listing_agents_id',
                    'value' => $agent_id,
                    'compare' => 'LIKE'
                )
            );
            if ((isset($metaquery)) && (count($metaquery) > 0) && (count($queryportion) > 0)) {
                $metaquery['relation'] = 'AND';
                array_push($metaquery, $queryportion);
            } else {
                $metaquery = $queryportion;
            }
        }
        if ((isset($beds)) && (strlen($beds) > 0)) {
            $queryportion = array(
                array(
                    'key' => 'REAL_HOMES_property_bedrooms',
                    'value' => $beds,
                    'compare' => '>='
                )
            );
            if ((isset($metaquery)) && (count($metaquery) > 0) && (count($queryportion) > 0)) {
                $metaquery['relation'] = 'AND';
                array_push($metaquery, $queryportion);
            } else {
                $metaquery = $queryportion;
            }
        }
        if ((isset($baths)) && (strlen($baths) > 0)) {
            $queryportion = array(
                array(
                    'key' => 'REAL_HOMES_property_bathrooms',
                    'value' => $baths,
                    'compare' => '>='
                )
            );
            if ((isset($metaquery)) && (count($metaquery) > 0) && (count($queryportion) > 0)) {
                $metaquery['relation'] = 'AND';
                array_push($metaquery, $queryportion);
            } else {
                $metaquery = $queryportion;
            }
        }
        if ((isset($min_price)) && (strlen($min_price) > 0)) {
            $queryportion = array(
                array(
                    'key' => 'REAL_HOMES_property_price',
                    'value' => str_replace(',', '', str_replace('$', '', preg_replace('~\.0+$~','', $min_price))),
                    'compare' => '>='
                )
            );
            if ((isset($metaquery)) && (count($metaquery) > 0) && (count($queryportion) > 0)) {
                $metaquery['relation'] = 'AND';
                array_push($metaquery, $queryportion);
            } else {
                $metaquery = $queryportion;
            }
        }
        if ((isset($max_price)) && (strlen($max_price) > 0)) {
            $queryportion = array(
                array(
                    'key' => 'REAL_HOMES_property_price',
                    'value' => str_replace(',', '', str_replace('$', '', preg_replace('~\.0+$~','', $max_price))),
                    'compare' => '<='
                )
            );
            if ((isset($metaquery)) && (count($metaquery) > 0) && (count($queryportion) > 0)) {
                $metaquery['relation'] = 'AND';
                array_push($metaquery, $queryportion);
            } else {
                $metaquery = $queryportion;
            }
        }
        if ((isset($min_area)) && (strlen($min_area) > 0)) {
            $queryportion = array(
                array(
                    'key' => 'REAL_HOMES_property_size',
                    'value' => str_replace(',', '', preg_replace('~\.0+$~','', $min_area)),
                    'compare' => '>='
                )
            );
            if ((isset($metaquery)) && (count($metaquery) > 0) && (count($queryportion) > 0)) {
                $metaquery['relation'] = 'AND';
                array_push($metaquery, $queryportion);
            } else {
                $metaquery = $queryportion;
            }
        }
        if ((isset($max_area)) && (strlen($max_area) > 0)) {
            $queryportion = array(
                array(
                    'key' => 'REAL_HOMES_property_size',
                    'value' => str_replace(',', '', preg_replace('~\.0+$~','', $max_area)),
                    'compare' => '<='
                )
            );
            if ((isset($metaquery)) && (count($metaquery) > 0) && (count($queryportion) > 0)) {
                $metaquery['relation'] = 'AND';
                array_push($metaquery, $queryportion);
            } else {
                $metaquery = $queryportion;
            }
        }    
        if ((isset($values['catid'])) && ((is_array($values['catid'])) && (count($values['catid']) > 0)) || ((!is_array($values['catid'])) && ($values['catid'] != ''))) {
            if ((count($values['catid']) == 1) || (!is_array($values['catid']))) {
                $queryportion = array(
                    array(
                        'key' => 'IDXGenerator_category_id',
                        'value' => (!is_array($values['catid']))?$values['catid'][0]:$values['catid'],
                        'compare' => '='
                    )
                );
                if ((isset($metaquery)) && (count($metaquery) > 0) && (count($queryportion) > 0)) {
                    $metaquery['relation'] = 'AND';
                    array_push($metaquery, $queryportion);
                } else {
                    $metaquery = $queryportion;
                }
            } else {
                $queryportion = array(
                    array(
                        'key' => 'IDXGenerator_category_id',
                        'value' => $values['catid'],
                        'compare' => 'IN'
                    )
                );
                if ((isset($metaquery)) && (count($metaquery) > 0) && (count($queryportion) > 0)) {
                    $metaquery['relation'] = 'AND';
                    array_push($metaquery, $queryportion);
                } else {
                    $metaquery = $queryportion;
                }
            }
            unset($values['catid']);
        }
        return $metaquery;
    }
}

if (! function_exists('getTaxQueryArray')) {
    function getTaxQueryArray($values) {
        
        $country = $values['country'];
        $state = $values['state'];
        $city = $values['city'];
        $sector = $values['sector'];
        $beds = $values['beds'];
        $baths = $values['baths'];
        $min_price = $values['min_price'];
        $max_price = $values['max_price'];
        $min_area = $values['min_area'];
        $max_area = $values['max_area'];
        $valueoftype = $values['propertytype'];
        $valueofstatus = $values['statustype'];
        $features = $values['features'];
        
        unset($taxquery);
        if ((isset($values['catid'])) && ($values['catid'] != "")) {
            $Property_type = array(
                '1' => 'House',
                '2' => 'House',
                '3' => 'House',
                '4' => 'Apartment',
                '5' => 'Apartment',
                '6' => 'Apartment',
                '7' => 'Local',
                '8' => 'Local',
                '9' => 'Office',
                '10' => 'Office',
                '11' => 'Land',
                '12' => 'Land',
                '13' => 'Country-House',
                '14' => 'Country-House',
                '15' => 'Country-House',
                '16' => 'Building',
                '17' => 'Land',
                '18' => 'House',
                '19' => 'Apartment',
                '20' => 'Apartment',
                '21' => 'Multi-Familial House',
                '22' => 'Multi-Familial House',
                '23' => 'Other',
                '24' => 'Other',
                '25' => 'Warehouse',
                '26' => 'Warehouse',
                '27' => 'Building',
                '28' => 'Uncategorized'
            );
            $Property_status = array(
                '1' => 'For Sale',
                '2' => 'For Rent',
                '3' => 'For Vacation Rental Use',
                '4' => 'For Sale',
                '5' => 'For Rent',
                '6' => 'For Vacation Rental Use',
                '7' => 'For Sale',
                '8' => 'For Rent',
                '9' => 'For Sale',
                '10' => 'For Rent',
                '11' => 'For Sale',
                '12' => 'For Rent',
                '13' => 'For Sale',
                '14' => 'For Rent',
                '15' => 'For Vacation Rental Use',
                '16' => 'For Sale',
                '17' => 'For Sale',
                '18' => 'For Rent',
                '19' => 'For Sale',
                '20' => 'For Rent',
                '21' => 'For Sale',
                '22' => 'For Sale',
                '23' => 'For Sale',
                '24' => 'For Rent',
                '25' => 'For Sale',
                '26' => 'For Rent'
            );
            if (!isset($valueoftype)) {
                if (is_array($values['catid'])) {
                    $valueoftype = array();
                    foreach ($values['catid'] as $catid) {
                        if ((array_key_exists($catid, $Property_type)) && (!in_array($Property_type[$catid], $valueoftype))) {
                            array_push($valueoftype, $Property_type[$catid]);
                        }
                    }
                } else {
                    if (array_key_exists($values['catid'], $Property_type)) {
                        $valueoftype = $Property_type[$values['catid']];
                    }
                }
            }
            if (!isset($valueofstatus)) {
                if (is_array($values['catid'])) {
                    $valueofstatus = array();
                    foreach ($values['catid'] as $catid) {
                        if ((array_key_exists($catid, $Property_status)) && (!in_array($Property_status[$catid], $valueofstatus))) {
                            array_push($valueofstatus, $Property_status[$catid]);
                        }
                    }
                } else {
                    if (array_key_exists($values['catid'], $Property_status)) {
                        $valueofstatus = $Property_status[$values['catid']];
                    }
                }
            }
        }
//         if ((isset($valueoftype)) && ($valueoftype != '')) {
//             $queryportion = array(
//                 array(
//                     'taxonomy' => 'property-type',
//                     'field' => 'slug',
//                     'terms' => $valueoftype
//                 )
//             );
//             if ((isset($taxquery)) && (count($taxquery) > 0)) {
//                 $taxquery['relation'] = 'AND';
//                 array_push($taxquery, $queryportion);
//             } else {
//                 $taxquery = $queryportion;
//             }
//         }
        if ((isset($features)) && (is_array($features)) && (count($features) > 0)) {
            $queryportion = array(
                array(
                    'taxonomy' => 'property-feature',
                    'field' => 'slug',
                    'terms' => $features
                )
            );
            if ((isset($taxquery)) && (count($taxquery) > 0) && (count($queryportion) > 0)) {
                $taxquery['relation'] = 'AND';
                array_push($taxquery, $queryportion);
            } else {
                $taxquery = $queryportion;
            }
        }
//         if ((isset($valueofstatus)) && ($valueofstatus != '')) {
//             $queryportion = array(
//                 array(
//                     'taxonomy' => 'property-status',
//                     'field' => 'slug',
//                     'terms' => $valueofstatus
//                 )
//             );
//             if ((isset($taxquery)) && (count($taxquery) > 0)) {
//                 $taxquery['relation'] = 'AND';
//                 array_push($taxquery, $queryportion);
//             } else {
//                 $taxquery = $queryportion;
//             }
//         }
        return $taxquery;
    }
}

if (!function_exists('_remove_empty_internal')) {
    function _remove_empty_internal($value) {
        return !empty($value) || $value === 0;
    }
}

if (!function_exists('folder_contents')) {
    function folder_contents() {
        
        $allownothumbnail = false;
        
        $values = array_merge(array(), $_POST);
        $country = $_POST['country'];
        unset($metaquery);
        unset($taxquery);
        unset($multivalues);
        unset($jsontofoward);
        
        if ((isset($_POST['json'])) && ($_POST['json'] != "")) {
            
            $json = $_POST['json'];
            $json = str_replace('\\"', "'", $json);
            $json = str_replace("\\'", "'", $json);
            if (startsWith($json, "'")) {
                $json = substr($json, 1);
            }
            if (endsWith($json, "'")) {
                $json = substr($json, 0, $json.length - 1);
            }
            $json = str_replace("'", '"', $json);
            
            $jsontofoward = $json;
            
            $decoded = json_decode($json, true);
            
            // echo '<script>console.log(`$decoded json: ' . print_r($decoded, true) . ' from ' . $json . '`);</script>';
            
            if (isset($decoded['multiple'])) {
                $multivalues = array();
                foreach ($decoded['multiple'] as $keyfor => $valuefor) {
                    array_push($multivalues, json_search_contents($valuefor));
                }
                unset($values);
            } else {
                $values = json_search_contents($json);
            }
        } else if ((isset($_POST['properties_id_json'])) && ($_POST['properties_id_json'] != "")) {
            
            $json = $_POST['properties_id_json'];
            $json = str_replace('\\"', "'", $json);
            $json = str_replace("\\'", "'", $json);
            if (startsWith($json, "'")) {
                $json = substr($json, 1);
            }
            if (endsWith($json, "'")) {
                $json = substr($json, 0, $json.length - 1);
            }
            $json = str_replace("'", '"', $json);
            
            $jsonpropertiesidstofoward = $json;
            
            $properties_id_decoded = json_decode($json, true);
        }
            
        $current_url = "//" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        
        if (isset($_POST['iteration'])) {
            $iteration = (int) $_POST['iteration'];
            $iteration++;
        } else {
            $iteration = 1;
        }
            
        $postsperpage = 16;
    
        if (isset($_POST['offset'])) {
            $offset = $_POST['offset'];
            $passOffset = $iteration * $postsperpage;
        } else {
            $offset = '';
            $passOffset = $postsperpage;
        }
        
        $aftersearchshowid = $_POST['aftersearchshowid'];
        $showingtotal = (isset($_POST['showingtotal']))?$_POST['showingtotal']:0;
        
        // echo '<script>console.log(`$values: ' . print_r($values, true) . '`);</script>';
        
        if ((!isset($multivalues)) && (isset($values))) {
            $values = array_filter($values, '_remove_empty_internal');
            $multivalues = array();
            $multivalues['unique'] = $values;
            unset($values);
        }
        
        // echo '<script>console.log(`$multivalues: ' . print_r($multivalues, true) . '`);</script>';
          
        $multiresults = array();
        foreach($multivalues as $keyfor => $valuefor) {
            $metaquery = getMetaQueryArray($valuefor);
            $taxquery = getTaxQueryArray($valuefor);
            $keyword = "";
            if (isset($valuefor['keyword']) && $valuefor['keyword'] != '') {
                $keyword = $valuefor['keyword'];
            }
            $properties_search_arg = array(
                'post_type' => 'property',
                'posts_per_page' => (count($multivalues) > 1)?-1:$postsperpage,
                'offset' => (count($multivalues) > 1)?0:((isset($offset)) && ($offset != ''))?$offset:0,
                'orderby' => (((isset($_POST['sortasc'])) && ($_POST['sortasc'] != ""))?$_POST['sortasc']:((isset($_POST['sortdesc'])) && ($_POST['sortdesc'] != ""))?$_POST['sortdesc']:'ID'),
                'order' => (((isset($_POST['sortasc'])) && ($_POST['sortasc'] != ""))?'ASC':'DESC'),
                'fields' => (count($multivalues) > 1)?'ids':'all'
            );
//             if (isset($valuefor['category__in'])) {
//                 $properties_search_arg['cat'] = '';
//                 foreach ($valuefor['category__in'] as $value) {
//                     if ($properties_search_arg['cat'] != '') {
//                         $properties_search_arg['cat'] .= ',';
//                     }
//                     $properties_search_arg['cat'] .= $value;
//                 }
//             }
            if ((isset($_POST['properties_id_json'])) && ($_POST['properties_id_json'] != "")) {
                $properties_search_arg['post__in'] = $properties_id_decoded;
            }
            if ((isset($keyword)) && ($keyword != "")) {
                $properties_search_arg['s'] = $keyword;
            }
            
//             if (isset($valuefor['catid'])) {
//                 if (is_array($valuefor['catid'])) {
//                     $properties_search_arg['category__in'] = $valuefor['catid'];
//                 } else {
//                     $properties_search_arg['category'] = $valuefor['catid'];
//                 }
//             }
            
            $metaquerynothumb = array_merge(array(), $metaquery);
            if (!$allownothumbnail) {
                $queryportion = array(
                    array(
                        'key' => '_thumbnail_id',
                        'compare' => 'EXISTS'
                    )
                );
            }
            if (isset($metaquery) && (count($queryportion) > 0)) {
                $metaquery['relation'] = 'AND';
                array_push($metaquery, $queryportion);
            } else {
                $metaquery = $queryportion;
            }
            if (isset($metaquery)) {
                $properties_search_arg['meta_query'] = $metaquery;
            }
            if (isset($taxquery)) {
                $properties_search_arg['tax_query'] = $taxquery;
            }
            $properties_search_arg = apply_filters('inspiry_property_search', $properties_search_arg);
            $properties_search_arg_str = print_r($properties_search_arg, TRUE);
            if (count($multivalues) > 1) {
                $post_ids = get_posts($properties_search_arg);
                // echo '<script>console.log(`$properties_search_arg: ' . $properties_search_arg_str . ' found ' . print_r($post_ids, true) . '`);</script>';
                array_push($multiresults, $post_ids);
            } else {
                $properties_search_query = new WP_Query($properties_search_arg);
                // echo '<script>console.log(`$properties_search_arg: ' . $properties_search_arg_str . ', $multivalues : ' . print_r($multivalues, true) . '`);</script>';
                // echo '<script>console.log(`$properties_search_arg: ' . $properties_search_arg_str . ' found ' . $properties_search_query->post_count . '`);</script>';
                array_push($multiresults, $properties_search_query);
            }
            if (count($multivalues) > 1) {
                wp_reset_query();
            }
        }
        
        if (count($multiresults) > 1) {
            $array = array();
            foreach($multiresults as $oneresult) {
                $array = array_merge($array, $oneresult);
            }
            $resarray = array_unique($array, SORT_REGULAR);
            $properties_search_query = new WP_Query(array(
                'post_type' => 'property',
                'post__in'      => $resarray,
                'posts_per_page' => $postsperpage,
                'offset' => ((isset($offset)) && ($offset != ''))?$offset:0,
                'orderby' => 'ID',
                'order' => 'DESC',
            ));
            // echo '<script>console.log(`total found ' . $properties_search_query->post_count . '`);</script>';
        }
        
        $totalcount = 0;
        
        echo '<!-- DEBUGQUERYRES: ';
        echo "Found ' . $properties_search_query->found_posts . ' posts.";
        echo ' -->';
        
        $totalcount = $properties_search_query->found_posts;
    
        if ((!$properties_search_query->have_posts()) && ((isset($country)) && (strlen($country) > 0)) && (($offset == '') || ($offset == 0))) {
            wp_reset_postdata();
            $properties_search_arg['meta_query'] = $metaquerynothumb;
            
            $finalargs = apply_filters('inspiry_property_search', $properties_search_arg);
    //         $myString = print_r($finalargs, TRUE);
    //         $myarray = explode("\n", $myString);
    //         echo '<script>console.log("1965. args ' . $myString . '");</script>';
            
            $properties_search_query = new WP_Query($finalargs);
            $allownothumbnail = true;
        }
    
        // echo '<script>console.log("2044. Found ' . $properties_search_query->found_posts . ' posts for country ' . $country . ((($keyword != ''))?(', keyword: '. $keyword):'') . '.");</script>';
    
        if ($properties_search_query->have_posts()) :
            
            for ($pass = 0; $pass <= 1; $pass ++) {
    
                $shownpropertiescount = 0;
                $properties_count = 1;
                $columns_count = 4; // todo: add the ability to change number of columns between 2, 3, 4
    
                while ($properties_search_query->have_posts()) :
                    $properties_search_query->the_post();
                    $lastid = get_the_ID();
                    if ((has_post_thumbnail()) || ($allownothumbnail)) {
                        $shownpropertiescount ++;
                        // echo '<script>console.log("Displaying post #' .$properties_count . '");</script>';
                        ?>
    					<div class='col-lg-4'>
    					<div class="homepropBoxes thumbnail-size">
                	    <?php
                	    $showingtotal++;
                	    new_property_list_insert(get_the_ID(), $_POST['addclassthumbnail'], $_POST['addclassdescription']);
                        ?>
                	    </div>
    					</div>
    					<?php
                    } else {
                        echo '<script>console.log("Post #' . $properties_count . ' has no thumbnail.");</script>';
                    }
                    $properties_count ++;
                endwhile;
    
                if ($shownpropertiescount > 0) {
                    break;
                }
                if ($pass == 0) {
                    $allownothumbnail = true;
                    rewind_posts();
                }
            }
            ?>
    
    <div class="col-md-12 loadmorediv" style="text-align: center;">
    	<div class="alm-btn-wrap">
    		<button class="alm-load-more-btn more loadmore"
    			style="background: #293542 !important;"
                <?php if (isset($jsonpropertiesidstofoward)) { ?>
                	data-properties_id_json="<?php echo str_replace('"', "'", $jsonpropertiesidstofoward) ?>"
                <?php } else if (isset($jsontofoward)) { ?>
                	data-json="<?php echo str_replace('"', "'", $jsontofoward) ?>"
                <?php } else { ?>
        			data-statustype='<?php echo $statustype; ?>'
        			data-propertytype='<?php echo $propertytype; ?>'
        			data-catid="<?php echo $_POST['catid'] ?>"
        			data-country="<?php echo $country; ?>" data-keyword="<?php echo $keyword; ?>" 
        		<?php } ?>
        		<?php if (isset($aftersearchshowid)) { ?>
        			data-aftersearchshowid="<?=$aftersearchshowid?>"
        		<?php } ?>
    			data-offset="<?=$passOffset?>"
    			data-total="<?=$totalcount?>"
    			data-showingtotal="<?=$showingtotal?>"
    			data-iteration="<?=$iteration?>" rel="next">See More Properties</button>
    	</div>
    </div>
    
    
    <?php
        else :
            ?>
    <div class="col-md-12 " style="text-align: center;">
    	<div class="alm-btn-wrap">
    		<button class="alm-load-more-btn more "
    			data-total="<?=$totalcount?>"
    			data-showingtotal="<?=$showingtotal?>"
    			style="background: #293542 !important;">No Properties Found</button>
    	</div>
    </div>
    <?php
        endif;
        // echo '<div class = "col-md-12 loadmorediv" style="text-align: center;"><button class="form-submit-btn loadmore" data-country = "'.$country.'" data-offset = "'.$passOffset.'" data-iteration = "'.$iteration.'" >Load More</button></div>';
        wp_reset_postdata();
        die();
    }
    
    add_action('wp_ajax_folder_contents', 'folder_contents');
    add_action('wp_ajax_nopriv_folder_contents', 'folder_contents');
}

if (! function_exists('custom_query_join')) {
    function custom_query_join($join)
    {
        $join .= 'Put here join to table';
        return $join;
    }
}

if (! function_exists('register_jurisdiction_post_type')) {
    function register_jurisdiction_post_type()
    {
        $labels = array(
            'name' => _x('Jurisdictions', 'post type general name'),
            'singular_name' => _x('Jurisdiction', 'post type singular name'),
            'add_new' => _x('Add New', 'activity'),
            'add_new_item' => __('Add New Jurisdiction'),
            'edit_item' => __('Edit Jurisdiction'),
            'new_item' => __('New Jurisdiction'),
            'view_item' => __('View Jurisdiction'),
            'view_items' => __('View Jurisdictions'),
            'search_items' => __('Search Jurisdictions'),
            'not_found' => __('No Jurisdictions found'),
            'not_found_in_trash' => __('Nothing found in Trash'),
            'parent_item_colon' => ''
        );
        $args = array(
            'labels' => $labels,
            'menu_icon' => 'dashicons-book',
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 7,
            'public' => true,
            'publicly_queryable' => true,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'jurisdictions',
                'with_front' => false
            ),
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => true,
            'show_in_rest' => true,
            'rest_base' => 'jurisdiction',
            'rest_controller_class' => 'WP_REST_Posts_Controller',
            'supports' => array(
                'title',
                'editor',
                'author',
                'thumbnail'
            )
        );
        register_post_type('jurisdiction', $args);
        register_taxonomy('jurisdiction_category', 'jurisdiction', array(
            'hierarchical' => true,
            'rewrite' => array(
                'slug' => 'jurisdiction-categories'
            ),
            'show_in_rest' => true
        ));
        register_taxonomy_for_object_type('jurisdiction_category', 'jurisdiction');
        register_taxonomy('jurisdiction_tag', 'jurisdiction', array(
            'rewrite' => array(
                'slug' => 'jurisdiction-tags'
            ),
            'show_in_rest' => true
        ));
        register_taxonomy_for_object_type('jurisdiction_tag', 'jurisdiction');
    }
    
    add_action('init', 'register_jurisdiction_post_type');
}

if (! function_exists('build_jurisdiction_table')) {
    function build_jurisdiction_table($atts, $customFieldData, $customFieldTitle, $checkboxid, $tableid) {
        $debug = false;
        if (!isset($post_id)) {
            $post_id = get_the_ID();
        }
        if (!isset($post_id)) {
            if ($debug) echo 'build_jurisdiction_table: post_id not set<br/>';
            return;
        }
        if (isset($atts['jurs_id'])) {
            global $wpdb;
            if (!isset($wpdb)) {
                if ($debug) echo 'build_jurisdiction_table: $wpdb not set<br/>';
                return;
            }
            $tbl = $wpdb->prefix . 'postmeta';
            $prepare_guery = $wpdb->prepare("SELECT post_id FROM $tbl where meta_key ='IDXGenerator_jurisdiction_id' and meta_value=" . $atts['jurs_id']);
            $get_values = $wpdb->get_col($prepare_guery);
            $post_id = $get_values[0];
        } else {
            $post_id = $atts['post_id'];
        }
        
        if ($debug) {
            echo $post_id . '<br/>';
            echo '$atts: ';
            print_r($atts);
            echo '<br/>';
            echo '$_GET: ';
            print_r($_GET);
            echo '<br/>';
        }
        
        $json = get_post_meta($post_id, $customFieldData);
        
        // ==================== purcahses table =====================
        
        $json = htmlspecialchars_decode($json[0], ENT_QUOTES);
        
        $datavalue = json_decode($json, true);
        if ($debug) {
            echo '$json: ';
            print_r($json);
            echo '<br/>';
            echo '$datavalue: ';
            print_r($datavalue);
            echo '<br/>';
        }
        $array = array();
        $header_value = "";
        $body_value = "";
        
        $datavaluenew_row = array();
        $count = 0;
        if ((isset($_COOKIE['country'])) && (isset($atts['jurs_id'])) && ($atts['jurs_id'] == 0)) {
            if ($debug) {
                echo 'Going into block 1' . '<br/>';
            }
            $countrykey = array_search('Country', $datavalue['row'][0]['elem']);
            foreach ($datavalue['row'] as $key => $value) {
                if ($key != 0) {
                    if ($value[elem][$countrykey] == $_COOKIE['country']) {
                        // unset($datavalue->row[$key]);
                        $datavaluenew_row[$count] = $value;
                        $count ++;
                    }
                } else {
                    // unset($datavalue->row[$key]);
                    $datavaluenew_row[$count] = $value;
                    $count ++;
                }
            }
            $datavalue = $datavaluenew_row;
        } else {
            if ($debug) {
                echo 'Not going into block 1' . '<br/>';
            }
        }
        
        $datavaluecategory = array();
        $countcat = 0;
        if ((isset($_GET['category'])) && ($_GET['category'] != '') && ($datavalue['row'] != '')) {
            if ($debug) {
                echo 'Going into block 2' . '<br/>';
            }
            $countrykey = ($datavalue['row'][0]['elem'] != null)?array_search('hide_categoryid', $datavalue['row'][0]['elem']):null;
            foreach ($datavalue['row'] as $key => $value) {
                if (($key != 0) && ($countrykey != null)) {
                    if ($value['elem'][$countrykey] == $_GET['category']) {
                        // unset($datavalue->row[$key]);
                        $datavaluecategory['row'][$countcat] = $value;
                        $countcat ++;
                    }
                } else {
                    // unset($datavalue->row[$key]);
                    $datavaluecategory['row'][$countcat] = $value;
                    $countcat ++;
                }
            }
            $datavalue = $datavaluecategory;
        } else {
            if ($debug) {
                echo 'Not going into block 2' . '<br/>';
            }
        }
        
        // get the first index
        $first_index = $datavalue['row'][0]['elem'];
        if ($debug) {
            echo '$first_index: ';
            print_r($first_index);
            echo '<br/>';
        }
        
        if ($first_index == '') {
            return '';
        }
        
        // delete the first index from main array
        unset($datavalue['row'][0]);
        
        // get all the index which got $ sign
        $columns_with_dollar = array();
        foreach ($first_index as $key => $value) {
            if (strpos($value, '$') !== false) {
                $columns_with_dollar[] = $key;
            }
        }
        
        // sort the main array
        foreach ($datavalue['row'] as $key => $value) {
            $datavalue['row'][$key] = $datavalue['row'][$key]['elem'];
        }
        
        if (count($datavalue['row']) == 0) {
            return '';
        }
        
        // remove empty and duplicate
        $count = count($datavalue['row'][1]);
        $tmp = array_keys($datavalue['row'][1]);
        $lastKeyP = end($tmp);
        
        $rmv_indexs = array();
        
        for ($i = 0; $i < $count; $i ++) {
            
            // remove empty
            if (empty(array_filter(array_column($datavalue['row'], $i)))) {
                
                $rmv_indexs[] = $i;
                
                // since all are empty
                // use reference and unset
                foreach ($datavalue['row'] as &$item) {
                    unset($item[$i]);
                }
                
                // unset reference
                unset($item);
            }
            
            if (count($datavalue['row']) != 1 && $lastKeyP != $i) {
                // remove duplicate except numerical values
                if (! in_array($i, $columns_with_dollar)) {
                    if (count(array_unique(array_column($datavalue['row'], $i))) == 1) {
                        
                        $rmv_indexs[] = $i;
                        
                        // since all are same
                        // use reference and unset
                        foreach ($datavalue['row'] as &$item) {
                            unset($item[$i]);
                        }
                        // unset reference
                        unset($item);
                    }
                }
            }
        }
        
        // remove the indexes from header
        foreach ($rmv_indexs as $key => $value) {
            
            unset($first_index[$value]);
        }
        
        // merge the header index in main array
        $datavalue['row'][0] = $first_index;
        
        // reset the keys
        $datavalue['row'] = array_map('array_values', $datavalue['row']);
        // get the laskey for link
        $tmp = array_keys($datavalue['row'][0]);
        $lastKeyData = end($tmp);
        //
        
        $countrykey = array_search('Country', $datavalue['row'][0]);
        $header_value_data = '';
        
        foreach ($datavalue['row'][0] as $key => $dataheaderrow) {
            $val = explode("_", $dataheaderrow);
            
            if ($val[0] != 'hide') {
                
                if ($key != $lastKeyData) {
                    
                    $header_value_data .= "<th>" . $dataheaderrow . "</th>";
                    $array_data[$key] = 'show';
                }
            }
        }
        
        foreach ($datavalue['row'] as $key => $rowdata) {
            if ($key != 0) {
                
                $href = "";
                if ($rowdata[$lastKeyData] != "") {
                    
                    $stringlink = $rowdata[$lastKeyData];
                    $val1 = explode("?", $stringlink);
                    if (count($val1) > 1) {
                        $val2 = explode("#", $val1[0]);
                        $equlval = str_replace("u003d", "=", $val1[1]);
                        
                        // $href = "data-href='/jurisdictions".$val2[0]."/?".$equlval."#".$val2[1]."' style='cursor:pointer;color: #0c74d6;'";
                        if ($val2[0] != "" && ! is_numeric($val2[0]) && count($val2) > 1) {
                            $href = "data-href='/jurisdictions" . $val2[0] . "/?" . $equlval . "#" . $val2[1] . "' class='hoveronlink' style='cursor:pointer;color: #0c74d6; '";
                        }
                    }
                }
                $body_value .= "<tr " . $href . " >";
                foreach ($rowdata as $key => $value2) {
                    if (array_key_exists($key, $array_data)) {
                        $body_value .= "<td>" . $value2 . "</td>";
                    }
                }
                
                $body_value .= "</tr>";
            }
        }
        
        if (! empty($body_value)) {
            
            if (isset($atts['title'])) {
                if (!isset($atts['is_set_search'])) {
                    $title = get_post_meta($post_id, $customFieldTitle, true);
                }
                if (!(isset($atts['tableonly']) && ($atts['tableonly']))) {
                    if (isset($title)) {
                        $title = $atts['title'] . ' - ' . $title;
                    } else {
                        $title = $atts['title'];
                    }
                    $table = '<h3 style="display:inline">' . $title . '</h3>';
                } else {
                    $table = '';
                }
            }
            
            if (!(isset($atts['tableonly']) && ($atts['tableonly']))) {
                $table .= '<label style="float: right; margin-bottom: 10px;" class="switch"><input type="checkbox" id="' . $checkboxid . '" checked ><div class="slider round"></div></label><br/>';
                $table .= '<div>';
            }
            $table .= '<table style="display:none" class="table table-bordered table-striped table-responsive-stack" id="' . $tableid .'"><thead><tr>' . $header_value_data . '</tr><tbody>' . $body_value . '</tbody></table>';
            if (!(isset($atts['tableonly']) && ($atts['tableonly']))) {
                $table .= '</div>';
                $table = '<div style="margin-bottom: 15px;">' . $table . '</div>';
            }
            return $table;
        } else {
            return '';
        }
    }
}

// https://mdbootstrap.com/docs/jquery/javascript/accordion/

if (! function_exists('jurisdiction_one_accordion')) {
    function jurisdiction_one_accordion($atts, $content, $customFieldTitle, $id, $cookie) {
        $title = get_post_meta($atts['post_id'], $customFieldTitle, true);
        if (isset($title)) {
            $title = $atts['title'] . ' - ' . $title;
        } else {
            $title = $atts['title'];
        }
        $return = '<div class="panel-heading">';
        $return .= '<h3 id="panel_' . $id .'" class="panel-title" style= "margin-top: 15px; cursor: pointer;" aria-expanded="true">';
        $return .= '<a id="collapse_' . $id .'" onclick="myCollapse'. $id .'Function()" role="button" data-toggle="collapse" class="stretched-link" style="text-decoration: none;" aria-controls="' . $id . '">'. $title . '</a>';
        $return .= '</h3>';
        $return .= '</div>';
        $return .= '<div id="'. $id .'" class="panel-collapse collapse" style="display: block;">';
        $return .= '<div class="panel-body">';
        $return .= $content;
        $return .= '</div>';
        $return .= '</div>';
        $return .= '<script>';
        $return .= 'function myCollapse'. $id .'Function() {
          var x = document.getElementById("'. $id .'");
          if (x.style.display === "none") {
            x.style.display = "block";
            document.getElementById("panel_'. $id .'").setAttribute("aria-expanded", "false");
          } else {
            x.style.display = "none";
            document.getElementById("panel_'. $id .'").setAttribute("aria-expanded", "true");
          }
          }';
        $return .= '</script>';
        return $return;
    }
}

if (! function_exists('secret_url_entry_point_handling')) {
    function secret_url_entry_point_handling($atts) {
        $secretUrlGood = false;
        echo "<script>";
        if ((isset($_GET['key'])) && (isset($_GET['secret']))) {
            ?>
            function setcookie(name, value, days) {
      			console.log("setting cookie '" + name + "' at " + value);
      	        var expires = "";
      	        if (days) {
      	            var date = new Date();
      	            date.setTime(date.getTime() + (days*24*60*60*1000));
      	            expires = "; expires=" + date.toUTCString();
      	        }
      	        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
      	    }
            <?php
            $url = '/member-overview-page?key=' . $_GET['key'] . '&secret=' . $_GET['secret'];
            $args = array(
                'post_type' => 'agent',
                'posts_per_page' => 1,
                'meta_query' => array(
                    array(
                        'key' => 'CallCenterSecretUrl',
                        'value' => $url,
                        'compare' => '=',
                        'type' => 'text'
                    )
                )
            );
            $secret_url_query = new WP_Query($args);
            if ($secret_url_query->have_posts()) {
                $secret_url_query->the_post();
                echo 'setcookie("secret-url-agent-id", "' . get_the_ID() . '", ' . (time() + (24 * 60 * 60)) . ');';
                echo 'setcookie("secret-url", "' . $url . '", ' . (time() + (24 * 60 * 60)) . ');';
                $secretUrlGood = true;
            }
            wp_reset_postdata();
        }    
        if (!$secretUrlGood) {
            if (isset($_COOKIE['secret-url-agent-id'])) {
                unset($_COOKIE['secret-url-agent-id']);
                echo 'setcookie("secret-url-agent-id", "", ' . (time() - (15 * 60)) . ');';
            }
            if (isset($_COOKIE['secret-url'])) {
                unset($_COOKIE['secret-url']);
                echo 'setcookie("secret-url", "", ' . (time() - (15 * 60)) . ');';
            }
            echo 'window.location.replace("' . home_url() .'");';
        } else {
            if (is_user_logged_in()) {
                wp_logout();
            }
            echo 'window.location.replace("' . home_url("/edit-profile/#") .'");';
        }
        echo "</script>";
    }
    add_shortcode('secret-url', 'secret_url_entry_point_handling');
}

if (! function_exists('idx_generator_map')) {
    function idx_generator_map($atts) {
        /* default map query arguments */
        $google_map_arguments = array();
        // Google Map API
        wp_enqueue_script('google-map-api', esc_url_raw(add_query_arg(apply_filters('inspiry_google_map_arguments', $google_map_arguments), '//maps.google.com/maps/api/js')), array(), '3.21', true);
        wp_enqueue_script('google-map-info-box', $inspiry_template_directory_uri . '/wp-content/themes/inspiry-real-places/js/infobox.js', array(
            'google-map-api'
        ), '1.1.9', true);
        wp_enqueue_script('google-map-marker-clusterer', $inspiry_template_directory_uri . '/wp-content/themes/inspiry-real-places/js/markerclusterer.js', array(
            'google-map-api'
        ), '2.1.1', true);
        wp_enqueue_script('properties-google-map', $inspiry_template_directory_uri . '/wp-content/themes/inspiry-real-places/js/properties-google-map.js', array(
            'google-map-api'
        ), INSPIRY_THEME_VERSION, true);
        $ids = $atts['ids'];
        if (isset($ids)) {
            $properties_data = explode(",", $ids);
        }
        if ((isset($properties_data)) && (count($properties_data) > 0)) {
            $file = cluster_cache_create_ref_file($properties_data, '2d');
            $bounds = cluster_cache_ids_bounds($file);
            if (isset($bounds) && (startsWith($bounds, '{}'))) {
                unset($bounds);
            }
            unset($properties_data);
        }
        
        if (((isset($properties_data)) && (count($properties_data, 1) > 0)) || (isset($bounds))) {
            $properties_map_data = array(
                'properties'  => $properties_data,
                'properties_bounds' => (isset($bounds)) ? $bounds : null,
                'closeIcon'   => get_template_directory_uri() . '/images/map/close.png',
                'clusterIcon' => get_template_directory_uri() . '/images/map/cluster-icon.png'
            );
        } else {
            $properties_map_data = array(
                'closeIcon'   => get_template_directory_uri() . '/images/map/close.png',
                'clusterIcon' => get_template_directory_uri() . '/images/map/cluster-icon.png',
                'zoom'        => file_get_contents(cluster_cache_json_location() . '/zoom.json', FALSE, null),
                'countries'   => file_get_contents(cluster_cache_json_location() . '/all.json', FALSE, null)
            );
        }
        print_r($properties_data);
        $in_footer = true;
        wp_localize_script('properties-google-map', 'propertiesMapData', $properties_map_data);
        ?>
        <div id="map-div">
    		<img id="map-image">
    		<div id="listing-map"></div>
    	</div>
        <?php
    }
    add_shortcode('idx_generator_map', 'idx_generator_map');
}

if (! function_exists('get_social_media_array')) {
    function get_social_media_array($setvalues) {
        unset($post_meta);
        if (($setvalues) && (get_current_user_id() != 0)) {
            $post_meta = get_user_meta(get_current_user_id());
        }
        $socialmedia = array();
        $tech_name = 'facebook'; $key = 'socialscreenui_switch_' . $tech_name; $default = true; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => '#3b5998', 'customer_facing' => 'Facebook', 'fa' => null, 'default' => $default, 'issocial' => true, 'value' => $value]);
        $tech_name = 'twitter'; $key = 'socialscreenui_switch_' . $tech_name; $default = true; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => '#1da1f2', 'customer_facing' => 'Twitter', 'fa' => null, 'default' => $default, 'issocial' => true, 'value' => $value]);
        $tech_name = 'linkedin'; $key = 'socialscreenui_switch_' . $tech_name; $default = true; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => '#007bb5', 'customer_facing' => 'LinkedIn', 'fa' => null, 'default' => $default, 'issocial' => true, 'value' => $value]);
        $tech_name = 'pinterest'; $key = 'socialscreenui_switch_' . $tech_name; $default = true; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => '#bd081c', 'customer_facing' => 'Pinterest', 'fa' => null, 'default' => $default, 'issocial' => true, 'value' => $value]);
        $tech_name = 'messenger'; $key = 'socialscreenui_switch_' . $tech_name; $default = true; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => '#1877f2', 'customer_facing' => 'Messenger', 'fa' => 'fa fa-facebook-square', 'default' => $default, 'issocial' => true, 'value' => $value]);
        $tech_name = 'whatsapp'; $key = 'socialscreenui_switch_' . $tech_name; $default = true; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => '#25d366', 'customer_facing' => 'WhatsApp', 'fa' => null, 'default' => $default, 'issocial' => true, 'value' => $value]);
        $tech_name = 'telegram'; $key = 'socialscreenui_switch_' . $tech_name; $default = false; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => '#0088cc', 'customer_facing' => 'Telegram', 'fa' => null, 'default' => $default, 'issocial' => true, 'value' => $value]);
        $tech_name = 'tumblr'; $key = 'socialscreenui_switch_' . $tech_name; $default = false; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => '#2c4762', 'customer_facing' => 'Tumblr', 'fa' => null, 'default' => $default, 'issocial' => true, 'value' => $value]);
        $tech_name = 'reddit'; $key = 'socialscreenui_switch_' . $tech_name; $default = false; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => '#ff5700', 'customer_facing' => 'Reddit', 'fa' => null, 'default' => $default, 'issocial' => true, 'value' => $value]);
        $tech_name = 'digg'; $key = 'socialscreenui_switch_' . $tech_name; $default = false; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => null, 'customer_facing' => 'Digg', 'fa' => null, 'default' => $default, 'issocial' => true, 'value' => $value]);
        $tech_name = 'delicious'; $key = 'socialscreenui_switch_' . $tech_name; $default = false; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => null, 'customer_facing' => 'Delicious', 'fa' => null, 'default' => $default, 'issocial' => true, 'value' => $value]);
        $tech_name = 'vkontakte'; $key = 'socialscreenui_switch_' . $tech_name; $default = false; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => null, 'customer_facing' => 'Vkontakte', 'fa' => 'fa fa-vk', 'default' => $default, 'issocial' => true, 'value' => $value]);
        $tech_name = 'odnoklassniki'; $key = 'socialscreenui_switch_' . $tech_name; $default = false; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => null, 'customer_facing' => 'Odnoklassniki', 'fa' => null, 'default' => $default, 'issocial' => true, 'value' => $value]);
        $tech_name = 'link'; $key = 'socialscreenui_switch_' . $tech_name; $default = true; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => null, 'customer_facing' => 'Link', 'img_url' => null, 'default' => $default, 'issocial' => false, 'value' => $value]);
        $tech_name = 'email'; $key = 'socialscreenui_switch_' . $tech_name; $default = true; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => null, 'customer_facing' => 'Email', 'fa' => 'fa fa-envelope-square', 'default' => $default, 'issocial' => true, 'value' => $value]);
        $tech_name = 'download'; $key = 'socialscreenui_switch_' . $tech_name; $default = false; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => null, 'customer_facing' => 'Download', 'fa' => null, 'default' => $default, 'issocial' => false, 'value' => $value]);
        $tech_name = 'embed'; $key = 'socialscreenui_switch_' . $tech_name; $default = false; $value = (isset($post_meta) && (isset($post_meta[$key])))?$post_meta[$key]:$default;
        array_push($socialmedia, ['tech_name' => $tech_name, 'color' => null, 'customer_facing' => 'Embed', 'fa' => 'fa fa-anchor', 'default' => $default, 'issocial' => false, 'value' => $value]);
        return $socialmedia;
    }
}

if (! function_exists('jurisdiction_accordion')) {
    function jurisdiction_accordion($atts) {
        $count = 0;
        if (!isset($post_id)) {
            $post_id = get_the_ID();
        }
        if (!isset($post_id)) {
            return '';
        }
        $atts['post_id'] = $post_id;
        $return = '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>';
        $return = '<style>.panel-title {
          position: relative;
        }
        .panel-title::after {
        	content: "\f107";
        	color: #333;
        	top: -2px;
        	right: 0px;
        	position: absolute;
            font-family: "FontAwesome"
        }
        .panel-title[aria-expanded="false"]::after {
        	content: "\f106";
        }
        </style>';
        $return .= '<div class="container" style="padding-top: 20px;">';
        $return .= '<form>';
        $return .= '<fieldset class="well">';
        $return .= '<legend class="well-legend">' . $atts['accordiontitle'] . '</legend>';
        $return .= '<div class="panel-group" id="accordion">';
        $atts['title'] = "Purchase";
        $atts['tableonly'] = true;
        $purchase = jurisdiction_table_purchases($atts);
        if ((isset($purchase)) && ($purchase != '')) {
            $count++;
            $return .= jurisdiction_one_accordion($atts, $purchase, 'IDXGenerator_purchase_valuation_title', 'collapsepurchase', 'collapsepurchasestatecookie');
        }
        $atts['title'] = "Lease";
        $atts['tableonly'] = true;
        $leases = jurisdiction_table_leases($atts);
        if ((isset($leases)) && ($leases != '')) {
            $count++;
            $return .= jurisdiction_one_accordion($atts, $leases, 'IDXGenerator_lease_valuation_title', 'collapselease', 'collapseleasestatecookie');
        }
        $atts['title'] = "Rent";
        $atts['tableonly'] = true;
        $rents = jurisdiction_table_rents($atts);
        if ((isset($rents)) && ($rents != '')) {
            $count++;
            $return .= jurisdiction_one_accordion($atts, $rents, 'IDXGenerator_rent_valuation_title', 'collapserent', 'collapserentstatecookie');
        }
        $return .= '</div>';
        $return .= '</fieldset>';
        $return .= '</form>';
        $return .= '</div>';
        return ($count > 0)?$return:'';
    }
    add_shortcode('juris-accordion', 'jurisdiction_accordion');
}



if (! function_exists('jurisdiction_table_rents')) {
    function jurisdiction_table_rents($atts) {
        return build_jurisdiction_table($atts, 'IDXGenerator_rent_valuation', 'IDXGenerator_rent_valuation_title', 'rent', 'rent_table');
    }
    add_shortcode('table-juris-rents', 'jurisdiction_table_rents');
}

if (! function_exists('jurisdiction_table_leases')) {
    function jurisdiction_table_leases($atts) {
        return build_jurisdiction_table($atts, 'IDXGenerator_lease_valuation', 'IDXGenerator_lease_valuation_title', 'lease', 'lease_table');
    }
    add_shortcode('table-juris-leases', 'jurisdiction_table_leases');
}

if (! function_exists('jurisdiction_table_purchases')) {
    function jurisdiction_table_purchases($atts) {
        return build_jurisdiction_table($atts, 'IDXGenerator_purchase_valuation', 'IDXGenerator_purchase_valuation_title', 'purchase', 'purchases_table');
    }
    add_shortcode('table-juris-purchases', 'jurisdiction_table_purchases');
}

if (! function_exists('load_table_short_code')) {
    function load_table_short_code()
    {
        echo "<br/>";
        echo do_shortcode("[table-juris-purchases jurs_id='0' title='Purchases']");
        echo "<br/>";
        echo do_shortcode("[table-juris-rents jurs_id='0' title='Vacation Rentals (per day)']");
        echo "<br/>";
        echo do_shortcode("[table-juris-leases jurs_id='0' title='Leases (per month)']");
        wp_die();
    }
    add_action('wp_ajax_load_table_short_code', 'load_table_short_code');
    add_action('wp_ajax_nopriv_load_table_short_code', 'load_table_short_code');
}

if (! function_exists('everyl_scripts')) {
    function everyl_scripts()
    {
        // Register the script
        wp_register_script('custom-script', esc_url(IRE_PLUGIN_URL) . 'public/js/profile.js', array(
            'jquery'
        ), false, true);
    
        // Localize the script with new data
        $script_data_array = array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'security' => wp_create_nonce('load_states')
        );
        wp_localize_script('custom-script', 'blog', $script_data_array);
    
        // Enqueued script with localized data.
    
        wp_enqueue_script('custom-script');
    }
}

if (! function_exists('disable_emojis')) {
    /**
     * Disable the emoji's
     */
    function disable_emojis()
    {
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action('admin_print_styles', 'print_emoji_styles');
        remove_filter('the_content_feed', 'wp_staticize_emoji');
        remove_filter('comment_text_rss', 'wp_staticize_emoji');
        remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
        add_filter('tiny_mce_plugins', 'disable_emojis_tinymce');
        add_filter('wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2);
    }
    add_action('init', 'disable_emojis');
}

if (! function_exists('disable_emojis_tinymce')) {
    /**
     * Filter function used to remove the tinymce emoji plugin.
     *
     * @param array $plugins
     * @return array Difference betwen the two arrays
     */
    function disable_emojis_tinymce($plugins)
    {
        if (is_array($plugins)) {
            return array_diff($plugins, array(
                'wpemoji'
            ));
        } else {
            return array();
        }
    }
}

if (! function_exists('disable_emojis_remove_dns_prefetch')) {
    /**
     * Remove emoji CDN hostname from DNS prefetching hints.
     *
     * @param array $urls
     *            URLs to print for resource hints.
     * @param string $relation_type
     *            The relation type the URLs are printed for.
     * @return array Difference betwen the two arrays.
     */
    function disable_emojis_remove_dns_prefetch($urls, $relation_type)
    {
        if ('dns-prefetch' == $relation_type) {
            /**
             * This filter is documented in wp-includes/formatting.php
             */
            $emoji_svg_url = apply_filters('emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/');
    
            $urls = array_diff($urls, array(
                $emoji_svg_url
            ));
        }
    
        return $urls;
    }
}

if (! function_exists('get_jurisdisction')) {
    function get_jurisdisction()
    {
    
        /*
         * global $wpdb;
         * check_ajax_referer('load_states', 'security');
         * $country = $_POST['country'];
         * $aStates = $wpdb->get_results( $wpdb->prepare( "SELECT id, NAME FROM el_states WHERE country_id = %d", $country ) );
         */
    	 
        if ($_POST[jurisdisction_type] == "Country") {
            $jurisdictions_country__args = array(
                'post_type' => 'jurisdiction',
                'posts_per_page' => - 1,
                'meta_query' => array(
                    array(
                        'key' => 'IDXGenerator_jurisdiction_type',
                        'value' => 'COUNTRY',
                        'compare' => '=',
                        'type' => 'text'
                    )
                )
            );
            $countries_query = new WP_Query(apply_filters('inspiry_home_properties', '$jurisdictions_country__args'));
            usort($countries_query->posts, 'compare_title');
            while ($countries_query->have_posts()) {
                $countries_query->the_post();
                echo "<option value=" . get_post_meta(get_the_ID(), "IDXGenerator_jurisdiction_id", true) . ">" . get_the_title() . "</option>";
            }
            wp_reset_postdata();
        } elseif (($_POST[jurisdisction_type] == "State") || ($_POST[jurisdisction_type] == "City") || ($_POST[jurisdisction_type] == "Sector")) {
            $jurisdictions_country__args = array(
                'post_type' => 'jurisdiction',
                'posts_per_page' => - 1,
                'meta_query' => array(
                    array(
                        'key' => 'IDXGenerator_jurisdiction_parent_id',
                        'value' => $_POST[jurisdisction_value],
                        'compare' => '=',
                        'type' => 'text'
                    )
                )
            );
            $countries_query = new WP_Query(apply_filters('inspiry_home_properties', $jurisdictions_country__args));
            if ($countries_query) {
                usort($countries_query->posts, 'compare_title');
                while ($countries_query->have_posts()) {
                    $countries_query->the_post();
                    echo "<option value=" . get_post_meta(get_the_ID(), "IDXGenerator_jurisdiction_id", true) . ">" . get_the_title() . "</option>";
                }
            } else {
    
                echo "<option value='' >  No options to select </option>";
            }
            wp_reset_postdata();
        }
        wp_die();
    }
    
    add_action('wp_ajax_get_jurisdisction', 'get_jurisdisction');
    //add_action('wp_ajax_nopriv_get_jurisdisction', 'get_jurisdisction');
}


