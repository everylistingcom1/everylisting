<?php commentPHPContext('/wp-content/themes/Real-Spaces-Child/footer-back.php'); ?>

<?php
global $inspiry_options;

$footer_variation = $inspiry_options['inspiry_footer_variation'];
if( !$footer_variation ) {
    $footer_variation = 'one';
}

?>
<footer class="newFooter">

    <div class="newContainer" style="align-items:flex-start;">

        <div class="footerBoxes">
            <?php  
            // if ( is_active_sidebar( 'footer-first-column' ) ) {
            //     dynamic_sidebar( 'footer-first-column' );
            //  } 
                ?>
                <section id="lc_taxonomy-4" class="widget clearfix widget_lc_taxonomy">
                    <div id="lct-widget-country-container" class="list-custom-taxonomy-widget">
                        <h3 class="widget-title">Find Listings By Country</h3>
                        <ul id="lct-widget-country">
                            <?php
                            
                            $jurisdictions_country__args = array(
                                'post_type' => 'jurisdiction',
                                'posts_per_page' => -1,
                                'meta_query' => array(
                                    array(
                                        'key' => 'IDXGenerator_jurisdiction_type',
                                        'value' => 'COUNTRY',
                                        'compare' => '=',
                                        'type'  => 'text'
                                    )
                                )
                            );
                            
                            $countries_query = new WP_Query(apply_filters('inspiry_home_properties', $jurisdictions_country__args));
                            usort($countries_query->posts, 'compare_title');
                            while ($countries_query->have_posts()) {
                                $countries_query->the_post();
                                echo "<li class='cat-item cat-item-".$cat_item.'><a href='" . get_permalink() . "'>" . get_the_title() . "</a></li>";
                            }
                            ?>
                        </ul>
                    </div>
                </section>
                
                <img src="/wp-content/themes/inspiry-real-places/images/FooterLogo.webp"/>
                
                <div class="copyrightBox">

            <?php

                /*
                 * Footer copyright text
                 */
                if ( !empty( $inspiry_options['inspiry_copyright_html'] ) ) {
                    ?>
                    <p class="copyright-text"><?php echo wp_kses(
                        $inspiry_options['inspiry_copyright_html'],
                        array(
                            'a' => array(
                                'href' => array(),
                                'title' => array(),
                                'target' => array(),
                            ),
                            'em' => array(),
                            'strong' => array(),
                            'br' => array(),
                            ) ); ?></p>
                    <?php
                }
                ?>
            </div>

        </div>
        <div class="footerBoxes fRight" >
            <?php
            if ( is_active_sidebar( 'footer-second-column' ) ) {
               dynamic_sidebar ( 'footer-second-column' );
           }
           ?>
           
           <?php
           if ( is_active_sidebar( 'footer-third-column' ) ) {
             dynamic_sidebar( 'footer-third-column' );
         }
         ?>

         <?php
         if ( is_active_sidebar( 'footer-fourth-column' ) ) {
             dynamic_sidebar( 'footer-fourth-column' );
         }
         ?> 

         


        </div> 

    </div>

</footer> <!-- .footer -->

</div><!-- end page -->

<a href="#top" id="scroll-top"><i class="fa fa-chevron-up"></i></a>

<?php wp_footer(); ?>

</body>
</html>