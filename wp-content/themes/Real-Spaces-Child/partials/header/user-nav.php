<?php commentPHPContext('/wp-content/themes/Real-Spaces-Child/partials/header/user-nav.php'); ?>

<?php
/*
 * User navigation for header
 */
global $inspiry_options;
$is_header_variation_three = ( $inspiry_options[ 'inspiry_header_variation' ] == '3' ) ? true : false ;
$user_nav_in_header = $inspiry_options[ 'inspiry_header_user_nav' ];
if ( $user_nav_in_header ) {

    $submit_property_url = "";
    if ( isset( $inspiry_options[ 'inspiry_submit_property_page' ] ) && !empty( $inspiry_options[ 'inspiry_submit_property_page' ] ) ) {
        $submit_property_url    = get_permalink( ire_wpml_translated_page_id ( $inspiry_options[ 'inspiry_submit_property_page' ] ) );
    }

    ?>
    <ul class="user-nav">
	<?php
		//Search bar menu 
	?>	
	<script type="text/javascript" src="/wp-content/themes/Real-Spaces-Child/partials/header/user.js"></script>
		
		<?php include 'home.css';
			
		?>
<li>
    <div class="container-fluid">
    <input type="text" id='mainsearch' " onkeyup='submitSearchUserNav(this.value);' name="mainsearch" class="form-control" placeholder="Search">
<script>
console.log("bootstrap version: " + typeof $.fn.typeahead !== 'undefined' ? '2.3.2' : '3.0.0');
var pendingtimeout = undefined;
function submitSearchUserNav(item) {
	if (pendingtimeout != undefined) {
		clearTimeout(pendingtimeout);
		pendingtimeout = undefined;
	}
	pendingtimeout = setTimeout(function() {
		var value = "{}";
		if ((item != undefined) && (item != "")) {
			value = "{'keyword': '" + item+ "'}";
		}
		console.log("*** request:  " + value);
		if (value != "") {
			jQuery.ajax({
				  method:'POST',
				  url: '/wp-admin/admin-ajax.php',
				  data: {action: 'folder_contents', json:value},
				  success: function(res) {
						console.log("*** result:  " + res);
						$("#aftersearchshow").html(res);					
				  }
			});
		}
		
	}, 500);
}
</script>
			
    </div>
	</li>
		
		<?php
        /*
         * Header Email
         */
        if( $is_header_variation_three && !empty( $inspiry_options['inspiry_header_email'] ) ) {
            $inspiry_header_email = is_email( $inspiry_options['inspiry_header_email'] );
            if ( $inspiry_header_email ) {
                ?>
                <li class="email">
                    <a href="mailto:<?php echo antispambot( $inspiry_header_email ); ?>">
                        <?php
                        include( get_template_directory() . '/images/svg/icon-email-two.svg' );
                        echo antispambot( $inspiry_header_email );
                        ?>
                    </a>
                </li>
                <?php
            }
        }

        /*
         * Favorites
         */
        if ( isset( $inspiry_options['inspiry_favorites_page'] ) && ! empty( $inspiry_options['inspiry_favorites_page'] ) ) {
            $favorites_url = get_permalink( ire_wpml_translated_page_id( $inspiry_options['inspiry_favorites_page'] ) );
            if ( ! empty( $favorites_url ) ) {
                ?>
                <li>
                    <a href="<?php echo esc_url( $favorites_url ); ?>">
                        <i class="fa fa-star"></i>
                        <?php echo ! empty( $inspiry_options['inspiry_header_user_nav_favorites'] ) ?
                            esc_html( $inspiry_options['inspiry_header_user_nav_favorites'] ) :
                            esc_html__( 'Favorite', 'inspiry' ); ?></a>
                </li>
                <?php
            }
        }


        if ( is_user_logged_in() ) {

            $edit_profile_url = "";
            $my_properties_url = "";
            $favorites_url = "";
			$messaging_url = "/messaging";

			$inspiry_options[ 'inspiry_edit_profile_page' ] = "137";
            if ( isset( $inspiry_options[ 'inspiry_edit_profile_page' ] ) && !empty( $inspiry_options[ 'inspiry_edit_profile_page' ] ) ) {
                $edit_profile_url = get_permalink( ire_wpml_translated_page_id ( $inspiry_options[ 'inspiry_edit_profile_page' ] ) );
            }

            if ( isset( $inspiry_options[ 'inspiry_my_properties_page' ] ) && !empty( $inspiry_options[ 'inspiry_my_properties_page' ] ) ) {
                $my_properties_url = get_permalink ( ire_wpml_translated_page_id ( $inspiry_options[ 'inspiry_my_properties_page' ] ) );
            }
            
	        /*
	         * Logout
	         */
            ?>
			
		  
          <li>
                <a href="<?php echo wp_logout_url( esc_url( home_url( '/' ) ) ); ?>">
                    <i class="fa fa-sign-out"></i>
                    <?php echo ! empty( $inspiry_options['inspiry_header_user_nav_logout'] ) ?
                        esc_html( $inspiry_options['inspiry_header_user_nav_logout'] ) :
                        esc_html__( 'Logout', 'inspiry' ); ?></a>
            </li>
            <?php

	        /*
	         * Profile
	         */
            if (!empty($edit_profile_url)) {
                ?>
                <li>
                <a href="<?php echo esc_url( $edit_profile_url ); ?>">
                    <i class="fa fa-user"></i>
                    <?php echo ! empty( $inspiry_options['inspiry_header_user_nav_profile'] ) ?
                        esc_html( $inspiry_options['inspiry_header_user_nav_profile'] ) :
                        esc_html__( 'Profile', 'inspiry' ); ?></a>
                </li>
                <?php
            }
			
			/* Messaging box
			*/
			if (!empty($messaging_url)) {
                ?>
                <li>
                <a href="<?php echo esc_url( $messaging_url ); ?>">
                    <i class="fa fa-envelope"></i><?php if (do_shortcode("[fep_shortcode_new_message_count]") != 0) { ?><span class="badge badge-danger" style="height: 12px;"><?php echo do_shortcode("[fep_shortcode_new_message_count]"); ?></span><?php } ?></a>
                </li>
                <?php
            }
			

	        /*
	         * My Properties
	         */
            if ((!empty($my_properties_url)) && (hasMyProperties())) {
                ?>
                <li>
                <a href="<?php echo esc_url( $my_properties_url ); ?>">
                    <i class="fa fa-th-list"></i>
                    <?php echo ! empty( $inspiry_options['inspiry_header_user_nav_properties'] ) ?
                        esc_html( $inspiry_options['inspiry_header_user_nav_properties'] ) :
                        esc_html__( 'My Properties', 'inspiry' ); ?></a>
                </li>
                <?php
            }

	        /*
	         * Submit
	         */
            if ((!empty( $submit_property_url)) && (false)) {
                ?>
                <li>
                <a class="submit-property-link" href="<?php echo esc_url( $submit_property_url ); ?>">
                    <i class="fa fa-plus-circle"></i>
                    <?php echo ! empty( $inspiry_options['inspiry_header_user_nav_submit'] ) ?
                        esc_html( $inspiry_options['inspiry_header_user_nav_submit'] ) :
                        esc_html__( 'Submit', 'inspiry' ); ?></a>
                </li>
                <?php
            }

        } else {
            
            $cururl = home_url(add_query_arg(array(), $wp->request));
            if (!((isset($_COOKIE['secret-url-agent-id'])) && (strpos($cururl, "/edit-profile") != false))) {
                if (isset($_COOKIE['secret-url-agent-id'])) {
                    echo '<a href="/edit-profile"><i class="fa fa-sign-in" style="color: red;"></i>&nbsp;<span style="color: red;">Sign-up here</span></a>';
                } else {
                ?>
                <li>
                    <a id="loginhyperlink" class="login-register-link" href="#login-modal" data-toggle="modal">
                        <?php
                        if ($is_header_variation_three) {
                            include(get_template_directory() . '/images/svg/icon-lock.svg');
                        } else {
                            echo '<i class="fa fa-sign-in"></i>';
                        }
                        esc_html_e('Login / Sign up', 'inspiry'); ?>
                    </a>
                </li>
                <?php
                }
                if (! empty($submit_property_url) && ! ($inspiry_options['inspiry_submit_for_registered_only'])) {
                    ?><li><a class="submit-property-link" href="<?php echo esc_url( $submit_property_url ); ?>"><i class="fa fa-plus-circle"></i><?php esc_html_e( 'Submit Property', 'inspiry' ); ?></a></li><?php
                }
            }

        }
        ?>
		
    </ul><!-- .user-nav -->
    <?php
}


/*
 * WPML Language Switcher
 */
if ( $inspiry_options['inspiry_display_wpml_flags'] ) {
    if ( function_exists( 'icl_object_id' ) ) {
        $languages = apply_filters( 'wpml_active_languages', NULL, array( 'skip_missing' => 0 ) );
        if ( !empty( $languages ) ) {
            echo '<div id="inspiry_language_list"><ul class="clearfix">';
            foreach( $languages as $language ){
                $active = ( $language['active'] ) ? 'active' : '';
                echo '<li class="' . $active . '">';
                    $native_name = $language['active'] ? strtoupper( $language['native_name'] ) : $language['native_name'];
                    if( !$language['active'] ) {
                        echo '<a href="' . $language['url'] . '">';
                    }
                    if ( $language['country_flag_url'] ) {
                        echo '<img src="' . $language['country_flag_url'] . '" height="12" alt="' . $language['language_code'] . '" width="18">';
                    }
                    echo esc_html( $native_name . ' ' );
                    if( !$language['active'] ) {
                        echo '</a>';
                    }
                echo '</li>';
            }
            echo '</ul></div>';
        }
    }
}

?>

