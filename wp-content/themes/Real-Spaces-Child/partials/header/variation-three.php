<?php commentPHPContext('/wp-content/themes/Real-Spaces-Child/partials/header/variation-three.php'); ?>

<header class="newHeader">
	
	<div class="newContainer" style="padding-top: 10px;">
		<div id="headerLeft">
			<div class="newHeader-bottom">
                <?php
                get_template_part('partials/header/menu-left');
                ?>
			</div>
		</div>
		<!-- /headerLeft -->
		<div id="headerRight">
			<div id="new-mobile-header">
				<script>
					jQuery(document).ready(function($){
                	$('.mobileMenu').click(function(){
                		$('#mobilenavBG').show();
                		$('#new-mobile-header-nav').show();
                	    $('#site-main-nav').show();
                	    $('#site-main-nav-left').show();
                	});
                	$('#closemobileMenu img').click(function(){
                		$('#mobilenavBG').hide();
                		$('#new-mobile-header-nav').hide();
                	    $('#site-main-nav').hide();
                	    $('#site-main-nav-left').hide();
                	});
                });
	            </script>
				<div id="mobileMenuIcon">
					<a style="margin-right: 10px; color: #888;"
						class='advancesearchLink menu-item menu-item-type-custom menu-item-object-custom menu-item-459374'><i
						class="fa fa-search" aria-hidden="true"></i></a> <a href="#"
						class="mobileMenu"><img src="/mediacode/images/menu.png"
						width="30" alt="nav menu" /></a>
				</div>
				<!-- /mobileMenuIcon -->
			</div>
			<div id="mobilenavBG"></div>
			<div id="new-mobile-header-nav">
				<div id="mobilemenuTop" class="newContainer">
					<div id="closemobileMenu">
						<img src="/mediacode/images/smallx.png" alt="close menu" />
					</div>
					<img src="/mediacode/logos/retired/header_bar_logo.jpg" />
				</div>
				<div class="newContainer">
                    <?php
                   get_template_part('partials/header/menu-left');
                    get_template_part('partials/header/menu');
                    get_template_part('partials/header/user-nav');
                    ?>
                </div>
			</div>
			<!-- /new-mobile-header-nav -->

			<div class="newHeader-top">
                <?php
                // get_template_part('partials/header/contact-number');
                get_template_part('partials/header/social-networks');
                 get_template_part('partials/header/user-nav');
                get_template_part('partials/header/currency-switcher');
                ?>
            </div>
			<!-- /newHeader-top -->
			<!--<div class="newHeader-bottom">
                
				<ul class="user-nav">
				<?php
 if ( is_user_logged_in() ) {

    $edit_profile_url = "";
    $my_properties_url = "";
    $favorites_url = "";

    if ( isset( $inspiry_options[ 'inspiry_edit_profile_page' ] ) && !empty( $inspiry_options[ 'inspiry_edit_profile_page' ] ) ) {
        $edit_profile_url = get_permalink( ire_wpml_translated_page_id ( $inspiry_options[ 'inspiry_edit_profile_page' ] ) );
    }

    if ( isset( $inspiry_options[ 'inspiry_my_properties_page' ] ) && !empty( $inspiry_options[ 'inspiry_my_properties_page' ] ) ) {
        $my_properties_url = get_permalink ( ire_wpml_translated_page_id ( $inspiry_options[ 'inspiry_my_properties_page' ] ) );
    }
    
    /*
     * Logout
     */
    ?>
    <li>
        <a href="<?php echo wp_logout_url( esc_url( home_url( '/' ) ) ); ?>">
            <i class="fa fa-sign-out"></i>
            <?php echo ! empty( $inspiry_options['inspiry_header_user_nav_logout'] ) ?
                esc_html( $inspiry_options['inspiry_header_user_nav_logout'] ) :
                esc_html__( 'Logout', 'inspiry' ); ?></a>
    </li>
    <?php

    /*
     * Profile
     */
    if (!empty($edit_profile_url)) {
        ?>
        <li>
        <a href="<?php echo esc_url( $edit_profile_url ); ?>">
            <i class="fa fa-user"></i>
            <?php echo ! empty( $inspiry_options['inspiry_header_user_nav_profile'] ) ?
                esc_html( $inspiry_options['inspiry_header_user_nav_profile'] ) :
                esc_html__( 'Profile', 'inspiry' ); ?></a>
        </li>
        <?php
    }

    /*
     * My Properties
     */
    if (!empty($my_properties_url)) {
        ?>
        <li>
        <a href="<?php echo esc_url( $my_properties_url ); ?>">
            <i class="fa fa-th-list"></i>
            <?php echo ! empty( $inspiry_options['inspiry_header_user_nav_properties'] ) ?
                esc_html( $inspiry_options['inspiry_header_user_nav_properties'] ) :
                esc_html__( 'My Properties', 'inspiry' ); ?></a>
        </li>
        <?php
    }

    /*
     * Submit
     */
    if ((!empty( $submit_property_url)) && (false)) {
        ?>
        <li>
        <a class="submit-property-link" href="<?php echo esc_url( $submit_property_url ); ?>">
            <i class="fa fa-plus-circle"></i>
            <?php echo ! empty( $inspiry_options['inspiry_header_user_nav_submit'] ) ?
                esc_html( $inspiry_options['inspiry_header_user_nav_submit'] ) :
                esc_html__( 'Submit', 'inspiry' ); ?></a>
        </li>
        <?php
    }

} else {

    ?>
    <li>
        <a class="login-register-link" href="#login-modal" data-toggle="modal">
            <?php
            if ( $is_header_variation_three ) {
                include( get_template_directory() . '/images/svg/icon-lock.svg' );
            } else {
                echo '<i class="fa fa-sign-in"></i>';
            }
            esc_html_e( 'Login / Sign up', 'inspiry' ); ?>
        </a>
    </li>
    <?php

    if ( ! empty( $submit_property_url ) && ! ( $inspiry_options['inspiry_submit_for_registered_only'] ) ) {
        ?><li><a class="submit-property-link" href="<?php echo esc_url( $submit_property_url ); ?>"><i class="fa fa-plus-circle"></i><?php esc_html_e( 'Submit Property', 'inspiry' ); ?></a></li><?php
    }

}
?>

				</ul>
            </div>-->
			<!-- /newHeader-bottom -->
		</div>
		<!-- /headerRight -->
	</div>
	<!-- /newContainer -->
</header>
<!-- .site-header -->
