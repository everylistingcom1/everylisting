<?php commentPHPContext('/wp-content/themes/Real-Spaces-Child/partials/header/map.php'); ?>

<?php

/*
 * Google Map with Properties Markers
 */
if (! class_exists('Inspiry_Real_Estate')) {
    return;
}

global $inspiry_options;
global $paged;

// Image Banner should be displayed with header variation one to keep things in order
if ($inspiry_options['inspiry_header_variation'] == '1') {
    get_template_part('partials/header/banner');
}

$map_args = array(
    'post_type' => 'property',
    'posts_per_page' => is_front_page() ? 30 : 1000,
    'meta_query' => array(
        array(
            'key' => 'REAL_HOMES_property_address',
            'compare' => 'EXISTS'
        )
    )
);

if (is_page_template('page-templates/properties-search.php')) {

    // Apply Search Filter

    $search = true;

    if (empty($_GET)) {

        $search = false;
    }

    $map_args = apply_filters('inspiry_property_search', $map_args);
} elseif (is_page_template('page-templates/home.php')) {

    // Apply Homepage Properties Filter
    $map_args = apply_filters('inspiry_home_properties', $map_args);
} elseif (is_page_template('page-templates/properties-list.php') || is_page_template('page-templates/properties-list-with-sidebar.php') || is_page_template('page-templates/properties-grid.php') || is_page_template('page-templates/properties-grid-with-sidebar.php') || is_page_template('page-templates/properties-list-half-map.php')) {

    global $paged;
    if (is_front_page()) {
        $paged = (get_query_var('page')) ? get_query_var('page') : 1;
    }

    $map_args = array(
        'post_type' => 'property',
        'paged' => $paged,
        'posts_per_page' => is_front_page() ? 30 : 1000
    );

    // Apply Properties Filter
    $map_args = apply_filters('inspiry_properties_filter', $map_args);
    $map_args = apply_filters('inspiry_sort_properties', $map_args);
} elseif (is_tax()) {

    global $wp_query;
    /* Taxonomy Query */
    $map_args['tax_query'] = array(
        array(
            'taxonomy' => $wp_query->query_vars['taxonomy'],
            'field' => 'slug',
            'terms' => $wp_query->query_vars['term']
        )
    );
}

$map_query = new WP_Query($map_args);

$properties_data = array();

if (($map_query->have_posts()) || (is_page_template('page-templates/home.php'))) :

    if ((!$map_query->have_posts()) && (is_page_template('page-templates/home.php'))) {
        cluster_cache_reset(true);
    }

    while ($map_query->have_posts()) :
        $map_query->the_post();
        if (isset($search)) {
            array_push($properties_data, get_the_ID());
        }
    endwhile;
    
    $file = $_GET['file'];
    if ((isset($properties_data)) && (count($properties_data, 1) > 0)) {
        $file = cluster_cache_create_ref_file($properties_data, "1d");
    }

    unset($bounds);
    if (isset($file) && !empty($file)) {
        $bounds = cluster_cache_ids_bounds($file);
        if (isset($bounds) && (startsWith($bounds, '{}'))) {
            unset($bounds);
        }
    }

    inspiry_enqueue_scripts();
    wp_enqueue_script('properties-google-map');

    if (((isset($properties_data)) && (count($properties_data, 1) > 0)) || ((!$map_query->have_posts()) && (is_page_template('page-templates/home.php')))) {
        $properties_map_data = array(
            'properties' => (isset($properties_data)) ? $properties_data : null,
            'properties_bounds' => (isset($bounds)) ? $bounds : null,
            'closeIcon' => get_template_directory_uri() . '/images/map/close.png',
            'clusterIcon' => get_template_directory_uri() . '/images/map/cluster-icon.png'
        );
    } else {
        // $zoomFile = '/usr/local/mapmarkerindexes/zoom.json';
        // $allFile = '/usr/local/mapmarkerindexes/all.json';
        $zoomFile = cluster_cache_json_location() . '/zoom.json';
        $allFile = cluster_cache_json_location() . '/all.json';
        // echo $zoomFile . file_get_contents($zoomFile, FALSE, null);
        // echo $allFile . file_get_contents($allFile, FALSE, null);
        $properties_map_data = array(
            'closeIcon' => get_template_directory_uri() . '/images/map/close.png',
            'clusterIcon' => get_template_directory_uri() . '/images/map/cluster-icon.png',
            'zoom' => file_exists($zoomFile) ? file_get_contents($zoomFile, FALSE, null) : null,
            'countries' => file_exists($allFile) ? file_get_contents($allFile, FALSE, null) : null,
            'zoomFileLocation' => file_exists($zoomFile) ? $zoomFile : null,
            'countriesFileLocation' => file_exists($allFile) ? $allFile : null
        );
    }

    wp_localize_script('properties-google-map', 'propertiesMapData', $properties_map_data);

    wp_reset_postdata();

    $map_class = 'header-two';
    if ($inspiry_options['inspiry_header_variation'] == '1') {
        $map_class = 'header-one';
    }
    ?>
<style type="text/css">
.map-select {
	position: absolute;
	z-index: 100;
	right: -6px;
	margin-top: 7px;
}

.pd0 {
	padding: 0 !important;
}

.ms-options-wrap input[type="checkbox"] {
	display: block !important;
	-webkit-appearance: checkbox !important;
}
</style>
<div id="map-head" class="<?php echo esc_attr( $map_class ); ?>">

	<div class='row'>
		<div class='col-md-8 map-select col-xs-7'>
			<div class='row'>
				<div class="col-md-2 pd0"></div>
				<div class="row showmob">
					<div class="col-md-12 text-left mt24">
						<button class="showmob2">Filters</button>
					</div>
				</div>
				<div id="hide12" class="map-options">
					<div class='col-md-8 pd0 float-right'>
                    
                   <?php
    // $search = "<select class = 'lifestyles'>";
    // $search .= "<option value = ''>Select</option>";
    // $search .= "<option value = 'Beach Living'>Beach Living</option>";
    // $search .= "<option value = 'Mountain Living'>Mountain Living</option>";
    // $search .= "<option value = 'City Life'>City Life</option>";
    // $search .= "<option value = 'Private Island'>Private Island</option>";
    // $search .= "<option value = 'Hotel Investment'>Hotel Investment</option>";
    // $search .= "<option value = 'Farm Land'>Farm Land</option>";
    // $search .= "<option value = 'Short-Term Rental Investment'>Short-Term Rental Investment</option>";
    // $search .= "</select>";
    // echo $search;
    ?>
                </div>
					<div
						class="container d-flex flex-column flex-md-row align-items-md-right" style="width: max-content; position: absolute; right: 10px;">
						<div class="row">
							<div class='col-md-10'>
                <?php
    $countries = countries_list(false, true);
    sort($countries);

    $drop = "<select id='countryname' class='selectpicker' data-width='120%'>";
    $drop .= "<option value = 'Worldwide'>Worldwide</option>";
    foreach ($countries as $k => $v) {
        $drop .= "<option value = '" . $v . "'>" . $v . "</option>";
    }
    $drop .= "</select>";
    if (count($countries) > 0)
        echo $drop;
    ?>
                </div>
							<div class='col-md-2 pd0'>
								<button class="form-submit-btn clear_btn countrySearch"
									style="background-color: #57AA5B !important; color: white; visibility: hidden;">APPLY</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="map-div">
		<img id="map-image">
		<div id="listing-map"></div>
	</div>
</div>
<!-- End Map Head -->


<?php
else :
    if ($inspiry_options['inspiry_header_variation'] != '1') {
        // Image Banner
        get_template_part('partials/header/banner');
    }
endif;


