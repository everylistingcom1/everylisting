<?php commentPHPContext('/wp-content/themes/Real-Spaces-Child/partials/header/menu.php'); ?>

<nav id="site-main-nav" class="site-main-nav">
    <?php
    if (!function_exists('compare_title')) :
        function compare_title($a, $b) {
            return strnatcmp(get_the_title($a), get_the_title($b));
        }
    endif; // compare_title
    if (!function_exists('jurisdiction_nav_menu_items')) :
        function jurisdiction_nav_menu_items($items) {
//            echo '<!-- HERE: ' . $items . ' -->';
//             $homelink = '<li class="home"><a href="' . home_url( '/' ) . '">' . __('Home') . '</a></li>';
//             // add the home link to the end of the menu
//             $items = $items . $homelink;
            $jurisdictions_country__args = array(
                    'post_type' => 'jurisdiction',
                    'posts_per_page' => -1,
                    'meta_query' => array(
                        array(
                        'key' => 'IDXGenerator_jurisdiction_type',
                        'value' => 'COUNTRY',
                        'compare' => '=',
                        'type'  => 'text'
                    )
                )
            );
            $locations = array();
            $countries_query = new WP_Query(apply_filters('inspiry_home_properties', $jurisdictions_country__args));
            while ($countries_query->have_posts()) {
                $countries_query->the_post();
                $continent = get_post_custom()['IDXGenerator_continent'][0];
                if (!isset($locations[$continent])) {
                    $locations[$continent] = array();
                }
                array_push($locations[$continent], get_post());
            }
            unset($replacement);
            unset($classes);
            $matches = array();
            if (preg_match_all('/<li([^>]*?)class=\"([^\\"]*)\"><a href=\"#locations\">Locations<\/a><\/li>/', $items, $matches, PREG_PATTERN_ORDER) > 0) {
                $other = $matches[1];
                $classes = $matches[2];
                $replacement = $matches[0];
            }
            $locationshtml = '';
            if ((isset($classes)) && (isset($replacement))) {
                echo '<!-- HERE, $classes: ';
                print_r($classes);
                echo ' -->';
                $i = 0;
                foreach ($replacement as $oneReplacement) {
                    foreach ($locations as $key => $value) {
                        usort($value, 'compare_title');
                        $locationshtml .= '<li class="'. $classes[0] . '"><a href="#">'. $key .'</a>';
                        $locationshtml .= '<ul class="sub-menu">';
                        foreach ($value as $onevalue) {
                            $locationshtml .= '<li' . $other[$i] . 'class="' . $classes[$i] . '"><a href="' . get_permalink($onevalue) . '">' . get_the_title($onevalue) . '</a></li>';
                        }
                        $locationshtml .= '</ul></li>';
                    }
                    $items = str_replace($oneReplacement, $locationshtml, $items);
                    $i++;
                }
            }
//             echo '<!-- HTML: ';
//             echo $locationshtml;
//             echo ' -->';
//             echo '<!-- REGEX: ';
//             echo 'class: ' . $classes;
//             echo ' - replacement: ' . $replacement;
//             echo ' -->';
            return $items;
        }
    endif; // jurisdiction_nav_menu_items
    
    add_filter('wp_nav_menu_items', 'jurisdiction_nav_menu_items');
    wp_nav_menu( array(
        'theme_location' => 'primary',
        'container' => false,
        'menu_class' => 'main-menu clearfix',
        'fallback_cb' => false,
    ) );
    ?>
</nav>

