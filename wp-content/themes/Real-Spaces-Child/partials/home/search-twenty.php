
 <style>
/*     Advanced search  button      */

#advancesearchLink_foropen {
  width: 50px;
  background: #d6d4d4 !important;
  float: left;
  line-height: 48px;
  text-align: center;
  cursor: pointer;
  margin-left: 60px;
  position: absolute;
  right: -60px;
  top: 6px;
  border: 2px solid #8e9191;
}

#advancesearchLink_foropen:hover {
  color: #fff ;
}

    .tooltiptext {
		  margin-left: 0px !important;
		  width: 50% !important;
	  
		position: absolute;
		display: inline-block;
		right: -282px;
		}

.hero {
	position:relative;
	
}
 </style>
 
 <div id="homeProperties" class="property-listing-two">
    <div class='row' id="pt121">
        <div class="col-md-12 col-xs-12">
            <h1 class="font90" style="width:100% !important">Find Your Next Property</h1>
        </div>
        <div class="col-md-3 col-sm-2  col-xs-1"></div>
        <div class="col-md-6 col-sm-8 col-xs-10">
            <form role="search" method="get" id="searchform" class="searchform" action="/properties-search/">
                <div style="text-align: center;" class="hero">
                    <input value="Search" name="keyword" class="keyword_txt" id="search_btn" type="text" placeholder="Search country, city, neighborhood" style="border: 1px solid gray; width: 100%; height: 50px; padding: 30px; color:black; box-shadow: 0 0 7.49px 0.51px rgba(0,0,0,.5);">
						<a id='advancesearchLink_foropen' class="form-submit-btn advancesearchLink" ><i class="fa fa-plus" aria-hidden="true"></i></a>	
                </div>
            </form>
        </div>
    </div>
</div>
