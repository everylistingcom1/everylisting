<?php commentPHPContext('/wp-content/themes/Real-Spaces-Child/partials/home/search.php'); ?>

	<?php

	/*
	 * Property search form for homepage
	 */
	global $inspiry_options;
	?>


	<div id="searchDiv" class="main-advance-search Junaid" style="background-position-x: center;background-position-y: center;background-size: cover;background-repeat-x: no-repeat;background-repeat-y: no-repeat;">
		<div class="homeSearchCon">
			<div class="newContainer homeSearchHeading">
				<h3>
					<img src="/wp-content/themes/inspiry-real-places/images/FooterLogo.webp"/><br/>
					Browse through  
					<span>
						<?php
						$count_posts = wp_count_posts('property');
						$published_posts = $count_posts->publish;
						echo number_format($published_posts);
						$terms = count(countries_list(false, true));
						?>		
					</span> Active Property Listings
					<?php if ($terms > 0) { ?>
    					 in  
    					<span>
    						<?php
    						echo number_format($terms); 
    						?>
    					</span> 
    					Countries
					<?php }?>
				</h3>
			</div>

			<div class="newContainer homeSearchBox">
				<form class="advance-search-form" action="/properties-search/" method="get">
					<div class="option-bar property-keyword">

						<input type="text" name="keyword" class="keyword-txt" id='hideosearchcookies1'   placeholder="Feature and location (city or country)">

					</div>   
					<div class="option-bar form-control-buttons">
						<input type="submit" value="Search" class="form-submit-btn" id="search_btn">
												<a id='advancesearchLink_foropen' class="form-submit-btn advancesearchLink"><i class="fa fa-plus" aria-hidden="true"></i></a>	
					</div>	
				</form>

				<!-- .container -->
				<div class="break">
					
				</div>
				<div id="advancesearchCon">
					<!-- <div id="advancesearchLink">
						Advanced Search
					</div> -->
				</div>
			</div>	
			<input type="hidden" id="selected_country">
			<script>
				

			</script>



		</div><!-- .advance-search -->
	</div>

	<div id="advanceSearchBox">
		<h1>Advanced Search</h1>
		<h3>Narrow down your search to more specific criteria</h3>
		<?php
		get_template_part( 'partials/search/form' );
		?>


		<div id="advancesearchClose">
			close
		</div>
	</div>
	
<?php     
    $images = glob(BASEFILESYSTEMPATH . '/mediacode/randomhomeimages/*');
    $useImage = $images[rand(0, count($images) - 1)];
?>

<script>
    if (document.getElementById("searchDiv") != undefined) {
        document.getElementById("searchDiv").style.backgroundImage = "url('<?php echo str_replace(BASEFILESYSTEMPATH, '', $useImage); ?>')"
    }
</script>
