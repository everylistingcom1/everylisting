<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
 
    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
   
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}



function bn_disable_google_map_js_on_profile() {
	// edit profile
	
            if (is_page_template('page-templates/edit-profile')) {
				
				 wp_dequeue_script('properties-google-map');
			     wp_deregister_script('properties-google-map');
				  wp_dequeue_script('property-google-map');
			     wp_deregister_script('property-google-map');
				 
				 
		
                   

                    // Google Map API
                    wp_dequeue_script('google-map-api');
					wp_deregister_script('google-map-api');
                
            } else {
                console('is_page_template is false', false);
            }

}
add_action('wp_print_scripts', 'bn_disable_google_map_js_on_profile',  100);




// ADDING TAXONOMY FOR COUNTRIES
add_action( 'init', 'create_country_taxonomies', 0 );

function create_country_taxonomies() {
	$labels = array(
		'name'              => _x( 'Countries', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Country', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Countries', 'textdomain' ),
		'all_items'         => __( 'All Countries', 'textdomain' ),
		'parent_item'       => __( 'Parent Country', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Country:', 'textdomain' ),
		'edit_item'         => __( 'Edit Country', 'textdomain' ),
		'update_item'       => __( 'Update Country', 'textdomain' ),
		'add_new_item'      => __( 'Add New Country', 'textdomain' ),
		'new_item_name'     => __( 'New Country Name', 'textdomain' ),
		'menu_name'         => __( 'Country', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'country' ),
	);

	register_taxonomy( 'country', array( 'property' ), $args );
}

// SIMPLE SEARCH WIDGET
function smallenvelop_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Simple Search', 'smallenvelop' ),
        'id' => 'simple-search',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h1>',
        'after_title' => '</h1>',
    ) );
}
add_action( 'widgets_init', 'smallenvelop_widgets_init' );

add_filter( 'fep_menu_buttons', 'fep_cus_fep_menu_buttons', 99 );

function fep_cus_fep_menu_buttons( $menu )
{
    unset( $menu['settings'] );
    return $menu;
}

// Message plugin count

